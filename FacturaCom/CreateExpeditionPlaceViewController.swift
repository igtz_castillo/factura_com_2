//
//  CreateExpeditionPlaceViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 17/05/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class CreateExpeditionPlaceViewController: UIViewController, UITextFieldDelegate {
  
  private var mainScrollView: UIScrollView! = nil
  
  private var clientDataLabel: LabelSeparator! = nil
  
  private var razonSocialTextField: CustomTextFieldWithTitleView! = nil
  private var calleTextField: CustomTextFieldWithTitleView! = nil
  private var numeroExteriorTextField: CustomTextFieldWithTitleView! = nil
  private var numeroInteriorTextField: CustomTextFieldWithTitleView! = nil
  private var coloniaTextField: CustomTextFieldWithTitleView! = nil
  private var codigoPostalTextField: CustomTextFieldWithTitleView! = nil
  private var ciudadTextField: CustomTextFieldWithTitleView! = nil
  private var estadoTextField: CustomTextFieldWithTitleView! = nil
  
  //View for buttons
  private var backViewButtons: UIView! = nil
  
  //Buttons
  private var createExpeditionPlaceButton: UIButton! = nil

  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
    tapGesture.numberOfTouchesRequired = 1
    self.view.addGestureRecognizer(tapGesture)
    
    self.initNavigationBar()
    self.initMainScrollView()

    self.initClientDataLabel()
    self.initRazonSocialTextField()
    self.initCalleTextField()
    self.initNumeroExteriorTextField()
    self.initNumeroInteriorTextField()
    self.initColoniaTextField()
    self.initCodigoPostalTextField()
    self.initCiudadTextField()
    self.initEstadoTextField()

    self.initBackViewButtons()

    
  }
  
  private func initNavigationBar() {
    
    self.initLeftNavigationView()
    
  }
  
  private func initLeftNavigationView() {
    
    self.title = "Crear sucursal"
    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
  }
  
  private func initMainScrollView() {
    
    mainScrollView = UIScrollView.init(frame: UIScreen.main.bounds)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.frame.size.height + (165.0 * UtilityManager.sharedInstance.conversionHeight))
    mainScrollView.contentSize = newContentSize
    
    mainScrollView.backgroundColor = UIColor.white
    
    self.view.addSubview(mainScrollView)
    
  }
  
  private func initClientDataLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: mainScrollView.frame.size.width,
                                    height: 56.0 * UtilityManager.sharedInstance.conversionHeight)
    
    clientDataLabel = LabelSeparator.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.CreateClientDetailViewController.clientDataLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    clientDataLabel.attributedText = stringWithFormat
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: clientDataLabel.frame.size.width,
                               height: clientDataLabel.frame.size.height)
    clientDataLabel.frame = newFrame
    clientDataLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(clientDataLabel)
    
  }
  
  private func initRazonSocialTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: clientDataLabel.frame.origin.y + clientDataLabel.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    razonSocialTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                             title: ViewControllersConstants.CreateExpeditionPlaceViewController.branchNameText ,
                                                             image: nil,
                                                             colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                             positionInYInsideView: nil,
                                                             positionInXInsideView: nil)

    razonSocialTextField.mainTextField.textColor = UIColor.black
    razonSocialTextField.mainTextField.placeholder = ViewControllersConstants.CreateExpeditionPlaceViewController.branchNameText
    razonSocialTextField.backgroundColor = UIColor.white
    razonSocialTextField.mainTextField.delegate = self
    razonSocialTextField.mainTextField.tag = 1
    razonSocialTextField.mainTextField.returnKeyType = .next
    self.mainScrollView.addSubview(razonSocialTextField)
    
  }
  
  private func initCalleTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: razonSocialTextField.frame.origin.y + razonSocialTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    calleTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                       title: ViewControllersConstants.CreateClientDetailViewController.calleTextFieldText,
                                                       image: nil,
                                                       colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                       positionInYInsideView: nil,
                                                       positionInXInsideView: nil)

    calleTextField.mainTextField.textColor = UIColor.black
    calleTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.calleTextFieldText
    calleTextField.backgroundColor = UIColor.white
    calleTextField.mainTextField.delegate = self
    calleTextField.mainTextField.tag = 2
    calleTextField.mainTextField.returnKeyType = .next
    self.mainScrollView.addSubview(calleTextField)
    
  }
  
  private func initNumeroExteriorTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: calleTextField.frame.origin.y + calleTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    numeroExteriorTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                title: ViewControllersConstants.CreateClientDetailViewController.numeroExteriorTextFieldText,
                                                                image: nil,
                                                                colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                positionInYInsideView: nil,
                                                                positionInXInsideView: nil)
  
    numeroExteriorTextField.mainTextField.textColor = UIColor.black
    numeroExteriorTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.numeroExteriorTextFieldText
    numeroExteriorTextField.backgroundColor = UIColor.white
    numeroExteriorTextField.mainTextField.delegate = self
    numeroExteriorTextField.mainTextField.tag = 3
    numeroExteriorTextField.mainTextField.returnKeyType = .next
    self.mainScrollView.addSubview(numeroExteriorTextField)
    
  }
  
  private func initNumeroInteriorTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: numeroExteriorTextField.frame.origin.y + numeroExteriorTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    numeroInteriorTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                title: ViewControllersConstants.CreateClientDetailViewController.numeroInteriorTextFieldText,
                                                                image: nil,
                                                                colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                positionInYInsideView: nil,
                                                                positionInXInsideView: nil)
 
    numeroInteriorTextField.mainTextField.textColor = UIColor.black
    numeroInteriorTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.numeroInteriorTextFieldText
    numeroInteriorTextField.backgroundColor = UIColor.white
    numeroInteriorTextField.mainTextField.delegate = self
    numeroInteriorTextField.mainTextField.tag = 4
    numeroInteriorTextField.mainTextField.returnKeyType = .next
    self.mainScrollView.addSubview(numeroInteriorTextField)
    
  }
  
  private func initColoniaTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: numeroInteriorTextField.frame.origin.y + numeroInteriorTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    coloniaTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                         title: ViewControllersConstants.CreateClientDetailViewController.coloniaTextFieldText,
                                                         image: nil,
                                                         colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                         positionInYInsideView: nil,
                                                         positionInXInsideView: nil)

    coloniaTextField.mainTextField.textColor = UIColor.black
    coloniaTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.coloniaTextFieldText
    coloniaTextField.backgroundColor = UIColor.white
    coloniaTextField.mainTextField.delegate = self
    coloniaTextField.mainTextField.tag = 5
    coloniaTextField.mainTextField.returnKeyType = .next
    self.mainScrollView.addSubview(coloniaTextField)
    
  }
  
  private func initCodigoPostalTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: coloniaTextField.frame.origin.y + coloniaTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    codigoPostalTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                              title: ViewControllersConstants.CreateClientDetailViewController.codigoPostalTextFieldText,
                                                              image: nil,
                                                              colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                              positionInYInsideView: nil,
                                                              positionInXInsideView: nil)

    codigoPostalTextField.mainTextField.textColor = UIColor.black
    codigoPostalTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.codigoPostalTextFieldText
    codigoPostalTextField.backgroundColor = UIColor.white
    codigoPostalTextField.mainTextField.delegate = self
    codigoPostalTextField.mainTextField.tag = 6
    codigoPostalTextField.mainTextField.returnKeyType = .next
    self.mainScrollView.addSubview(codigoPostalTextField)
    
  }
  
  private func initCiudadTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: codigoPostalTextField.frame.origin.y + codigoPostalTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    ciudadTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                        title: ViewControllersConstants.CreateClientDetailViewController.ciudadTextFieldText,
                                                        image: nil,
                                                        colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                        positionInYInsideView: nil,
                                                        positionInXInsideView: nil)

    ciudadTextField.mainTextField.textColor = UIColor.black
    ciudadTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.ciudadTextFieldText
    ciudadTextField.backgroundColor = UIColor.white
    ciudadTextField.mainTextField.delegate = self
    ciudadTextField.mainTextField.tag = 7
    ciudadTextField.mainTextField.returnKeyType = .next
    self.mainScrollView.addSubview(ciudadTextField)
    
  }
  
  private func initEstadoTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: ciudadTextField.frame.origin.y + ciudadTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    estadoTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                        title: ViewControllersConstants.CreateClientDetailViewController.estadoTextFieldText,
                                                        image: nil,
                                                        colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                        positionInYInsideView: nil,
                                                        positionInXInsideView: nil)

    estadoTextField.mainTextField.textColor = UIColor.black
    estadoTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.estadoTextFieldText
    estadoTextField.backgroundColor = UIColor.white
    estadoTextField.mainTextField.delegate = self
    estadoTextField.mainTextField.tag = 8
    estadoTextField.mainTextField.returnKeyType = .next
    self.mainScrollView.addSubview(estadoTextField)
    
  }
  
  private func initBackViewButtons() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: estadoTextField.frame.origin.y + estadoTextField.frame.size.height + (15.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: self.view.frame.size.width,
                                   height: 350.0 * UtilityManager.sharedInstance.conversionHeight)
    
    backViewButtons = UIView.init(frame: frameForView)
    backViewButtons.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    self.mainScrollView.addSubview(backViewButtons)
    //    self.initLastUpdateLabel()
    self.initCreateClientButton()
    
  }
  
  private func initCreateClientButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                     y: 21.0 * UtilityManager.sharedInstance.conversionHeight,
                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    createExpeditionPlaceButton = UIButton.init(frame: frameForButton)
    createExpeditionPlaceButton.addTarget(self,
                                 action: #selector(createClientButtonPressed),
                                 for: .touchUpInside)
    createExpeditionPlaceButton.backgroundColor = UtilityManager.sharedInstance.backGroundColorApp
    createExpeditionPlaceButton.setBackgroundColor(color: UIColor.init(red: 255.0/255.0, green: 164.0/255.0, blue: 78.0/255.0, alpha: 1.0), forState: .highlighted)
    createExpeditionPlaceButton.setAttributedTitle(stringWithFormat, for: .normal)
    createExpeditionPlaceButton.contentHorizontalAlignment = .left
    
    createExpeditionPlaceButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.CreateClientDetailViewController.registerClientButtonText))
    
    self.backViewButtons.addSubview(createExpeditionPlaceButton)
    
  }
  
  private func createGenericLabelForButton(text: String) -> UILabel {
    
    let frameForLabel = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 200.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    let genericLabel = UILabel.init(frame: frameForLabel)
    genericLabel.numberOfLines = 0
    genericLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: text,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    genericLabel.attributedText = stringWithFormat
    genericLabel.sizeToFit()
    let newFrame = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                               y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                               width: genericLabel.frame.size.width,
                               height: genericLabel.frame.size.height)
    genericLabel.frame = newFrame
    
    return genericLabel
    
  }
  
  @objc private func createClientButtonPressed() {
    
    if UtilityManager.sharedInstance.isValidText(testString: razonSocialTextField.mainTextField.text!) == false {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Se requiere la razón social del cliente",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    } else
        if UtilityManager.sharedInstance.isValidText(testString: calleTextField.mainTextField.text!) == false {
          
          let alertController = UIAlertController(title: "ERROR",
                                                  message: "Se requiere una calle",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
        } else
          if UtilityManager.sharedInstance.isValidText(testString: numeroExteriorTextField.mainTextField.text!) == false {
            
            let alertController = UIAlertController(title: "ERROR",
                                                    message: "Se requiere un número exterior",
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
            alertController.addAction(cancelAction)
            
            let actualController = UtilityManager.sharedInstance.currentViewController()
            actualController.present(alertController, animated: true, completion: nil)
            
          } else
            if UtilityManager.sharedInstance.isValidText(testString: coloniaTextField.mainTextField.text!) == false {
              
              let alertController = UIAlertController(title: "ERROR",
                                                      message: "Se requiere el nombre de una colonia",
                                                      preferredStyle: UIAlertControllerStyle.alert)
              
              let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
              alertController.addAction(cancelAction)
              
              let actualController = UtilityManager.sharedInstance.currentViewController()
              actualController.present(alertController, animated: true, completion: nil)
              
            } else
              if (UtilityManager.sharedInstance.isValidText(testString: codigoPostalTextField.mainTextField.text!) == false) || ((codigoPostalTextField.mainTextField.text?.characters.count) != 5) {
                
                let alertController = UIAlertController(title: "ERROR",
                                                        message: "Se requiere un código postal de 5 caracteres",
                                                        preferredStyle: UIAlertControllerStyle.alert)
                
                let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
                alertController.addAction(cancelAction)
                
                let actualController = UtilityManager.sharedInstance.currentViewController()
                actualController.present(alertController, animated: true, completion: nil)
                
              } else
                if UtilityManager.sharedInstance.isValidText(testString: ciudadTextField.mainTextField.text!) == false {
                  
                  let alertController = UIAlertController(title: "ERROR",
                                                          message: "Se requiere una ciudad o delegación",
                                                          preferredStyle: UIAlertControllerStyle.alert)
                  
                  let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
                  alertController.addAction(cancelAction)
                  
                  let actualController = UtilityManager.sharedInstance.currentViewController()
                  actualController.present(alertController, animated: true, completion: nil)
                  
                } else
                  if UtilityManager.sharedInstance.isValidText(testString: estadoTextField.mainTextField.text!) == false {
                    
                    let alertController = UIAlertController(title: "ERROR",
                                                            message: "Se requiere un estado",
                                                            preferredStyle: UIAlertControllerStyle.alert)
                    
                    let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
                    alertController.addAction(cancelAction)
                    
                    let actualController = UtilityManager.sharedInstance.currentViewController()
                    actualController.present(alertController, animated: true, completion: nil)
                    
                  } else {
                            
                      self.createExpeditionPlace()
                            
                    }
    
  }
  
  private func createExpeditionPlace() {
    
    let finalParams = self.getAllParameters()
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.requestToCreateNewExpeditionPlace(params: finalParams, actionsToMakeWhenSucceeded: { 
      
      _ = self.navigationController?.popViewController(animated: true)
      
      UtilityManager.sharedInstance.hideLoader()
      
    }, actionsToMakeWhenFailed: {
    
      _ = self.navigationController?.popViewController(animated: true)
      
      UtilityManager.sharedInstance.hideLoader()
    
    })
    
  }
  
  private func getAllParameters() -> [String: AnyObject] {
    
    var params: [String: AnyObject] = [String: AnyObject]()

    params["sucursal"] = razonSocialTextField.mainTextField.text! as AnyObject
    params["calle"] = calleTextField.mainTextField.text! as AnyObject
    params["numero_exterior"] = numeroExteriorTextField.mainTextField.text! as AnyObject
    params["numero_interior"] = numeroInteriorTextField.mainTextField.text! as AnyObject
    params["codpos"] = codigoPostalTextField.mainTextField.text! as AnyObject
    params["colonia"] = coloniaTextField.mainTextField.text! as AnyObject
    params["estado"] = estadoTextField.mainTextField.text! as AnyObject
    params["ciudad"] = ciudadTextField.mainTextField.text! as AnyObject
    
    return params
    
  }
  
  @objc private func hideKeyboard() {
    
    self.view.endEditing(true)
    
  }
  
  //MARK: - UITextFieldDelegate
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    let nextTage = textField.tag + 1
    
    let nextResponder = textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
    
    if (nextResponder != nil){
      
      nextResponder?.becomeFirstResponder()
      
    } else {
      
      textField.resignFirstResponder()
      
    }
    
    return false
    
  }
  
  
}
