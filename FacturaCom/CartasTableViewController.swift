//
//  CartasTableViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 13/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class CartasTableViewController: TableViewWithSearchBarViewController {
  
  private var cartasArray: Array<Carta>! = Array<Carta>()
  private var filteredCartasArray: Array<Carta>! = Array<Carta>()
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
    
  }
  
  init(style: UITableViewStyle, newArrayOfCartas: Array<Carta>) {
    
    cartasArray = newArrayOfCartas
    
    super.init(style: style, newArrayOfElements: cartasArray)
    
    self.editNavigationBar()
    self.initValues()
    
  }
  
  private func editNavigationBar() {
    
    self.title = ViewControllersConstants.MainTabController.cartasButtonText
    self.changeNavigationBarTitle()
    
  }
  
  private func changeNavigationBarTitle() {
    
    let titleLabel = UILabel.init(frame: CGRect.zero)
    
    let font = UIFont(name: "SFUIText-Regular",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.NameOfCFDIScreen.cartasTableTitleText,
      attributes:[NSFontAttributeName:font!,
                  NSParagraphStyleAttributeName:style,
                  NSForegroundColorAttributeName:color
      ]
    )
    titleLabel.attributedText = stringWithFormat
    titleLabel.sizeToFit()
    self.navigationItem.titleView = titleLabel
    
  }
  
  private func initValues() {
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.requestToGetAllCartas(params: nil, actionsToMakeWhenSucceeded: { arrayOfCartas in
      
      print(arrayOfCartas)
      self.cartasArray = arrayOfCartas
      self.tableView.reloadData()
      
      UtilityManager.sharedInstance.hideLoader()
      
    }, actionsToMakeWhenFailed: {
      
      UtilityManager.sharedInstance.hideLoader()
      
    })
    
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      return filteredCartasArray.count
      
    }
    
    return cartasArray.count
    
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CustomCellTableViewCell
    
    cell.selectionStyle = UITableViewCellSelectionStyle.none
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      cell.setData(newData: filteredCartasArray[indexPath.row])
      
    } else {
      
      cell.setData(newData: cartasArray[indexPath.row])
      
    }
    
    return cell
    
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    var cartaData: Carta
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      cartaData = filteredCartasArray[indexPath.row]
      
    } else {
      
      cartaData = cartasArray[indexPath.row]
      
    }
    
    let facturaDetailScreen = CartaDetailViewController.init(newCartaData: cartaData)
    self.navigationController?.pushViewController(facturaDetailScreen, animated: true)
    
  }
  
  func filterContentForSearchText(searchText: String, scope: String = "All") {
    
    filteredCartasArray = cartasArray.filter { element in
      
      return element.folio.lowercased().contains(searchText.lowercased()) || element.fechaTimbrado.lowercased().contains(searchText.lowercased()) || element.razonSocialReceptor.lowercased().contains(searchText.lowercased())
      
    }
    
    self.tableView.reloadData()
    
  }
  
  override func actionToDoWhenChangeCompany() {
    
    self.initValues()
    
  }
  
  override func updateSearchResults(for searchController: UISearchController) {
    
    self.filterContentForSearchText(searchText: searchController.searchBar.text!)
    
  }
  
  override func createSomethingButtonPressed() {
    
    let createCartaScreen = CartaCreateViewController()
    self.navigationController?.pushViewController(createCartaScreen, animated: true)
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    
    self.initValues()
    
    if User.session.actualUsingCompany.urlLogo != nil {
      
      self.companyView.imageFromUrl(urlString: User.session.actualUsingCompany.urlLogo!)
      
    }
    
  }
  
}
