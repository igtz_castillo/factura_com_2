//
//  Client.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 11/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class Client {
  
  var calle: String! = nil
  var ciudad: String! = nil
  var codigoPostal: String! = nil
  var colonia: String! = nil
  var delegacion: String! = nil
  var estado: String! = nil
  var pais: String! = nil
  var interior: String! = nil
  var localidad: String! = nil
  var numero: String! = nil
  var rfc: String! = nil
  var razonSocial: String! = nil
  var uid: String! = nil
  var contacto: Contact! = nil
  var numberOfCFDIs: String! = nil
  var arrayOfBankAccounts: [Option] = [Option]()
  
  init() {}
  
}
