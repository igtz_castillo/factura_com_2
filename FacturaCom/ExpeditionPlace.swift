//
//  ExpeditionPlace.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 11/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class ExpeditionPlace {
  
  var sucUID: String! = nil
  var sucursalName: String! = nil
  
  init(newSucUID: String!, newSucursalName: String!) {
  
    sucUID = newSucUID
    sucursalName = newSucursalName
    
  }
  
}
