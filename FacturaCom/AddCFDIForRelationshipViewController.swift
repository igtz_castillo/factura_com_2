//
//  AddCFDIForRelationshipViewController.swift
//  FacturaCom
//
//  Created by Israel on 04/02/18.
//  Copyright © 2018 Alejandro Aristi C. All rights reserved.
//

import UIKit

protocol AddCFDIForRelationshipDelegate {
  
  func addCFDIToRelationship(_ cdfiToAddToRelationship: CFDIVersionThree)
  func noCFDIsToRelationshipSelected()
  
}

class AddCFDIForRelationshipViewController: TableViewWithSearchBarViewController {
  
  private var cfdisArray: Array<CFDIVersionThree>! = Array<CFDIVersionThree>()
  private var filteredCFDIsArray: Array<CFDIVersionThree>! = Array<CFDIVersionThree>()
  private var isACFDISelected: Bool = false
  var delegateToAddRelationship: AddCFDIForRelationshipDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
    
  }
  
  init(style: UITableViewStyle, newArrayOfCFDIVersionThree: Array<CFDIVersionThree>) {
    
    cfdisArray = newArrayOfCFDIVersionThree
    
    super.init(style: style, newArrayOfElements: cfdisArray)
    
    self.editNavigationBar()
    self.initValues()
    
  }
  
  private func editNavigationBar() {
    
    self.title = ViewControllersConstants.NameOfCFDIScreen.facturasTableTitleText
    self.changeNavigationBarTitle()
    self.initRightNavigationView()
    
  }
  
  private func changeNavigationBarTitle() {
    
    if #available(iOS 11.0, *) {
      
      self.navigationItem.title = ViewControllersConstants.NameOfCFDIScreen.facturasTableTitleText
      self.navigationItem.largeTitleDisplayMode = .never
      
    } else {
      
      let titleLabel = UILabel.init(frame: CGRect.zero)
      
      let font = UIFont(name: "SFUIText-Regular",
                        size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
      let color = UIColor.white
      let style = NSMutableParagraphStyle()
      style.alignment = NSTextAlignment.center
      
      let stringWithFormat = NSMutableAttributedString(
        string: ViewControllersConstants.NameOfCFDIScreen.facturasTableTitleText,
        attributes:[NSFontAttributeName:font!,
                    NSParagraphStyleAttributeName:style,
                    NSForegroundColorAttributeName:color
        ]
      )
      titleLabel.attributedText = stringWithFormat
      titleLabel.sizeToFit()
      self.navigationItem.titleView = titleLabel
      
    }
    
  }
  
  private func initRightNavigationView() {
    
    self.navigationItem.leftBarButtonItems?.removeAll()
    
    
    self.navigationItem.hidesBackButton = true
    let backButtonItem = UIBarButtonItem(title: "Crea un CFDI", style: .plain, target: self, action: #selector(backPressed))
    self.navigationItem.leftBarButtonItem = backButtonItem
    
    self.navigationItem.rightBarButtonItems?.removeAll()
    
  }
  
  private func initValues() {
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.getAllVersion33CFDI(actionsToMakeWhenSucceeded: { (arrayOfCFDIs) in
      
      self.cfdisArray = arrayOfCFDIs
      self.filteredCFDIsArray = arrayOfCFDIs
      self.tableView.reloadData()
      UtilityManager.sharedInstance.hideLoader()
      
    }, actionsToMakeWhenFailed: {
      
      UtilityManager.sharedInstance.hideLoader()
      
    })
    
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      return filteredCFDIsArray.count
      
    }
    
    return cfdisArray.count
    
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CustomCellTableViewCell
    
    cell.selectionStyle = UITableViewCellSelectionStyle.none
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      cell.setData(newData: filteredCFDIsArray[indexPath.row])
      
    } else {
      
      cell.setData(newData: cfdisArray[indexPath.row])
      
    }
    
    return cell
    
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    let alert = UIAlertController.init(title: "Relación con éste CFDI", message: "¿Estás seguro que deseas agregarlo?", preferredStyle: .alert)
    
    let okAction = UIAlertAction.init(title: "Sí", style: .default) { (alertAction) in
      
      var genericCFDIData: CFDIVersionThree
      
      if self.searchController.isActive && self.searchController.searchBar.text != "" {
        
        genericCFDIData = self.filteredCFDIsArray[indexPath.row]
        
      } else {
        
        genericCFDIData = self.cfdisArray[indexPath.row]
        
      }
      
      self.delegateToAddRelationship?.addCFDIToRelationship(genericCFDIData)
      self.isACFDISelected = true
      self.navigationController?.popViewController(animated: true)
      
    }
    
    let cancelAction = UIAlertAction.init(title: "Cancelar", style: .cancel, handler: nil)
    
    alert.addAction(okAction)
    alert.addAction(cancelAction)
    
    self.present(alert, animated: true, completion: nil)

  }
  
  func filterContentForSearchText(searchText: String, scope: String = "All") {
    
    filteredCFDIsArray = cfdisArray.filter { element in
      
      return element.folio.lowercased().contains(searchText.lowercased()) || element.fechaTimbrado.lowercased().contains(searchText.lowercased()) || element.razonSocialReceptor.lowercased().contains(searchText.lowercased())
      
    }
    
    self.tableView.reloadData()
    
  }
  
  override func updateSearchResults(for searchController: UISearchController) {
    
    self.filterContentForSearchText(searchText: searchController.searchBar.text!)
    
  }
  
  override func createSomethingButtonPressed() {
    
//    let createGenericCFDIScreen = CreateGenericCFDIFirstPartViewController()
//    self.navigationController?.pushViewController(createGenericCFDIScreen, animated: true)
    
  }
  
  @objc private func backPressed() {
    
    if isACFDISelected {
      
      self.navigationController?.popViewController(animated: true)
      
    } else {
      
      let alert = UIAlertController.init(title: "Aviso", message: "No has seleccionado ningún CFDI, ¿deseas regresar?", preferredStyle: .alert)
      
      let okAction = UIAlertAction.init(title: "Sí", style: .default) { (alertAction) in
      
        self.delegateToAddRelationship?.noCFDIsToRelationshipSelected()
        self.navigationController?.popViewController(animated: true)
        
      }
      
      let cancelAction = UIAlertAction.init(title: "Cancelar", style: .cancel, handler: nil)
      
      alert.addAction(okAction)
      alert.addAction(cancelAction)
      
      self.present(alert, animated: true, completion: nil)
      
    }
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    
    self.initValues()
    
    if User.session.actualUsingCompany.urlLogo != nil {
      
      self.companyView.imageFromUrl(urlString: User.session.actualUsingCompany.urlLogo!)
      
    }
    
  }
  
  override func actionToDoWhenChangeCompany() {
    
    self.initValues()
    
  }
  
}
