//
//  ServerManager.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 24/03/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation
import Alamofire

class ServerManager: NSObject {
  
  static let sharedInstance = ServerManager()
  
  static let developmentServer = "http://devfactura.in/api"
  static let productionServer  = "https://factura.com/api"
  
  static let versionOneOfEndPoint = "/v1"
  static let versionTwoOfEndPoint = "/v2"
  static let versionThreePoinThreeEndPoint = "/v3"
  
  //////////////////////////////////////////////////////////////////////////////////////////
  //CHANGE THESE VALUES FOR THE CORRECT TYPE OF SERVER//////////////////////////////////////
                                                                                          //
  let typeOfServerVersionOne =   developmentServer + versionOneOfEndPoint                 //
  let typeOfServerVersionTwo =   developmentServer + versionTwoOfEndPoint                 //
  let typeOfServerVersionThree = developmentServer + versionThreePoinThreeEndPoint        //
                                                                                          //
  //////////////////////////////////////////////////////////////////////////////////////////
  
  func requestToLogin(mail: String, password: String, actionsToMakeWhenSucceeded: @escaping (_ json: [String: AnyObject]) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let developmentServer = "http://devfactura.in/api"
    //let productionServer  = "https://factura.com/api"
    
    let urlToRequest = "\(developmentServer)/movil/login"  //This is the only one which is different
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
    let params = ["email": mail,
                  "password": password,
                  "api_key": "XjWJpwEPy2ks23Kh"
                  ]

    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
 
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 200 {
            
            let json = try! JSONSerialization.jsonObject(with: response.data!, options: [])
            print(json)
            
            UserDefaults.standard.set(mail, forKey: UtilityManager.sharedInstance.kLastValidUserEmail)
            UserDefaults.standard.set(password, forKey: UtilityManager.sharedInstance.kLastValidUserPassword)
            
            actionsToMakeWhenSucceeded(json as! [String: AnyObject])
            
          } else {
            
            if response.response?.statusCode == 400 {
              
              let json = try! JSONSerialization.jsonObject(with: response.data!, options: []) as! NSDictionary
              
              let message = (json["message"] as? String != nil ? json["message"] as! String : "")
              
              if message != "" {
                
                let alertController = UIAlertController(title: "ERROR",
                                                        message: message,
                                                        preferredStyle: UIAlertControllerStyle.alert)
                
                let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                  
                  actionsToMakeWhenFailed()
                  
                }
                alertController.addAction(cancelAction)
                
                let actualController = UtilityManager.sharedInstance.currentViewController()
                actualController.present(alertController, animated: true, completion: nil)
                
              } else {
              
                actionsToMakeWhenFailed()
                
              }
              
            } else {
              
              let alertController = UIAlertController(title: "ERROR",
                                                      message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                      preferredStyle: UIAlertControllerStyle.alert)
              
              let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                
                actionsToMakeWhenFailed()
                
              }
              alertController.addAction(cancelAction)
              
              let actualController = UtilityManager.sharedInstance.currentViewController()
              actualController.present(alertController, animated: true, completion: nil)
              
            }
            
          }
          
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
      
        actionsToMakeWhenFailed()
      
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
    
  }
  
  func requestDataFromActualCompany(actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/current/account"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
      
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
            
          do {
              
            let json = try JSONSerialization.jsonObject(with: response.data!, options: [])
            print(json)
            
            if json as? [String: AnyObject] != nil {
              
              let jsonFromServer = (json as! [String: AnyObject])
              
              if jsonFromServer["data"] as? [String: AnyObject] != nil {
                
                let data = jsonFromServer["data"] as! [String: AnyObject]
                
                let newRazonSocial = (data["razon_social"] as? String != nil ? data["razon_social"] as! String : "")
                let newRegimenFiscal = (data["regimen_fiscal"] as? String != nil ? data["regimen_fiscal"] as! String : "")
                let newCalle = (data["calle"] as? String != nil ? data["calle"] as! String : "")
                let newExterior = (data["exterior"] as? String != nil ? data["exterior"] as! String : "")
                let newInterior = (data["interior"] as? String != nil ? data["interior"] as! String : "")
                let newColonia = (data["colonia"] as? String != nil ? data["colonia"] as! String : "")
                let newCodPos = (data["codpos"] as? String != nil ? data["codpos"] as! String : "")
                let newCiudad = (data["ciudad"] as? String != nil ? data["ciudad"] as! String : "")
                let newEstado = (data["estado"] as? String != nil ? data["estado"] as! String : "")
                let newEmail = (data["email"] as? String != nil ? data["email"] as! String : "")
                let newRFC = (data["rfc"] as? String != nil ? data["rfc"] as! String : "")
                
                User.session.actualUsingCompany.regimenFiscal = newRegimenFiscal
                User.session.actualUsingCompany.calle = newCalle
                User.session.actualUsingCompany.exterior = newExterior
                User.session.actualUsingCompany.interior = newInterior
                User.session.actualUsingCompany.colonia = newColonia
                User.session.actualUsingCompany.codPos = newCodPos
                User.session.actualUsingCompany.ciudad = newCiudad
                User.session.actualUsingCompany.estado = newEstado
                User.session.actualUsingCompany.email = newEmail
                User.session.actualUsingCompany.name = newRazonSocial
                User.session.actualUsingCompany.rfc = newRFC
               
                actionsToMakeWhenSucceeded()
                
              } else {
                
                actionsToMakeWhenFailed()
                
              }
              
            } else {
              
              actionsToMakeWhenFailed()
              
            }
              
            } catch(_) {
              
              let alertController = UIAlertController(title: "ERROR",
                                                      message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                      preferredStyle: UIAlertControllerStyle.alert)
              
              let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                
                actionsToMakeWhenFailed()
                
              }
              alertController.addAction(cancelAction)
              
              let actualController = UtilityManager.sharedInstance.currentViewController()
              actualController.present(alertController, animated: true, completion: nil)
              
            }
            
        }  else {
            
          actionsToMakeWhenFailed()
            
        }

    }
  
  }
  
  func requestDataFromCompany(companyUID: String, actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/account/\(companyUID)"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            let json = try JSONSerialization.jsonObject(with: response.data!, options: [])
            print(json)
            
            for i in 0...User.session.arrayOfCompanies.count - 1 {
              
              if User.session.arrayOfCompanies[i].uidAccount == companyUID {
                
                if json as? [String: AnyObject] != nil {
                  
                  let jsonFromServer = (json as! [String: AnyObject])
                  
                  if jsonFromServer["data"] as? [String: AnyObject] != nil {
                    
                    let data = jsonFromServer["data"] as! [String: AnyObject]
                    
                    let newRazonSocial = (data["razon_social"] as? String != nil ? data["razon_social"] as! String : "")
                    let newRegimenFiscal = (data["regimen_fiscal"] as? String != nil ? data["regimen_fiscal"] as! String : "")
                    let newCalle = (data["calle"] as? String != nil ? data["calle"] as! String : "")
                    let newExterior = (data["exterior"] as? String != nil ? data["exterior"] as! String : "")
                    let newInterior = (data["interior"] as? String != nil ? data["interior"] as! String : "")
                    let newColonia = (data["colonia"] as? String != nil ? data["colonia"] as! String : "")
                    let newCodPos = (data["codpos"] as? String != nil ? data["codpos"] as! String : "")
                    let newCiudad = (data["ciudad"] as? String != nil ? data["ciudad"] as! String : "")
                    let newEstado = (data["estado"] as? String != nil ? data["estado"] as! String : "")
                    let newEmail = (data["email"] as? String != nil ? data["email"] as! String : "")
                    let newRFC = (data["rfc"] as? String != nil ? data["rfc"] as! String : "")
                    
                    
                    User.session.arrayOfCompanies[i].regimenFiscal = newRegimenFiscal
                    User.session.arrayOfCompanies[i].calle = newCalle
                    User.session.arrayOfCompanies[i].exterior = newExterior
                    User.session.arrayOfCompanies[i].interior = newInterior
                    User.session.arrayOfCompanies[i].colonia = newColonia
                    User.session.arrayOfCompanies[i].codPos = newCodPos
                    User.session.arrayOfCompanies[i].ciudad = newCiudad
                    User.session.arrayOfCompanies[i].estado = newEstado
                    User.session.arrayOfCompanies[i].email = newEmail
                    User.session.arrayOfCompanies[i].name = newRazonSocial
                    User.session.arrayOfCompanies[i].rfc = newRFC
                    
                    actionsToMakeWhenSucceeded()
                    
                    return
                    
                  } else {
                    
                    actionsToMakeWhenFailed()
                    
                  }
                  
                } else {
                  
                  actionsToMakeWhenFailed()
                  
                }
                
              }
            
            }
            
            //I guess i have to do actionsToDoWhenFailed
            
            actionsToMakeWhenFailed()
            
          } catch(_) {
            
            let alertController = UIAlertController(title: "ERROR",
                                                    message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
              
              actionsToMakeWhenFailed()
              
            }
            alertController.addAction(cancelAction)
            
            let actualController = UtilityManager.sharedInstance.currentViewController()
            actualController.present(alertController, animated: true, completion: nil)
            
          }
          
        }  else {
          
          actionsToMakeWhenFailed()
          
        }
        
    }
    
  }
  
  
  func requestToGetRegimenFiscal(actionsToMakeWhenSucceeded: @escaping (_ arrayOfRegimenFiscal: [Option]) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/settings/regimen_fiscal"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            let json = try JSONSerialization.jsonObject(with: response.data!, options: [])
            print(json)
            
            if json as? [String: AnyObject] != nil {
              
              let finalJson = (json as! [String:AnyObject])
              var arrayOfRegimenOptions = [Option]()
              
              let arrayOfRegimen = finalJson["data"] as? Array<[String: AnyObject]> != nil ? finalJson["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
              
              for regimenOption in arrayOfRegimen {
                
                let newId = regimenOption["id"] as? String != nil ? regimenOption["id"] as! String : ""
                let newName = regimenOption["name"] as? String != nil ? regimenOption["name"] as! String : ""
                
                let newRegimenOption = Option(id: newId, name: newName, type: "")
                arrayOfRegimenOptions.append(newRegimenOption)
                
              }
              
              actionsToMakeWhenSucceeded(arrayOfRegimenOptions)
              
            } else {
              
              actionsToMakeWhenFailed()
              
            }
            
          } catch(_) {
            
            let alertController = UIAlertController(title: "ERROR",
                                                    message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
              
              actionsToMakeWhenFailed()
              
            }
            alertController.addAction(cancelAction)
            
            let actualController = UtilityManager.sharedInstance.currentViewController()
            actualController.present(alertController, animated: true, completion: nil)
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
      }
    
    }
    
  }
  
 //////NUEVO SERVICIO PARA LO DE PRODUCTOS
  func getAllUserProducts(_ actionsToMakeWhenSucceeded: @escaping (_ arrayOfUserProducts: Array<ProductService>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/products/list"
  
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
              
              print(json)
              
              let rawProductServiceArray = json["data"] as? Array<AnyObject> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
              
              print(rawProductServiceArray)
              
              var arrayOfProductService: Array<ProductService> = Array<ProductService>()
              
              for rawProductService in rawProductServiceArray {
          
                let newCode: String = (rawProductService["code"] as? String != nil ? rawProductService["code"] as! String : "no code")
                let newName: String! = (rawProductService["name"] as? String != nil ? rawProductService["name"] as! String : "no name")
                let newPrice: String! = (rawProductService["price"] as? Double != nil ? String(rawProductService["price"] as! Double) : "-1.00")
                let newSku: String! = (rawProductService["sku"] as? String != nil ? rawProductService["sku"] as! String : "no sku")
                let newUnidad: String! = (rawProductService["unidad"] as? String != nil ? rawProductService["unidad"] as! String : "no unidad")
                let newClaveProdServ: String! = (rawProductService["claveprodserv"] as? String != nil ? rawProductService["claveprodserv"] as! String : "no claveprodserv")
                let newClaveUnidad: String! = (rawProductService["claveunidad"] as? String != nil ? rawProductService["claveunidad"] as! String : "no claveunidad")
                
                let newProductService = ProductService()
                newProductService.code = newCode
                newProductService.name = newName
                newProductService.price = newPrice
                newProductService.sku = newSku
                newProductService.unidad = newUnidad
                newProductService.claveProdServ = newClaveProdServ
                newProductService.claveUnidad = newClaveUnidad

                arrayOfProductService.append(newProductService)
                
              }
              
              actionsToMakeWhenSucceeded(arrayOfProductService)
              
            }
            
          } catch _{
            
            actionsToMakeWhenFailed()
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  func requestToGetAllBanks(actionsToMakeWhenSucceeded: @escaping (_ arrayOfRegimenFiscal: [Option]) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/settings/banks"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            let json = try JSONSerialization.jsonObject(with: response.data!, options: [])
            print(json)
            
            if json as? [String: AnyObject] != nil {
              
              let finalJson = (json as! [String:AnyObject])
              var arrayOfBankOptions = [Option]()
              
              let arrayOfRegimen = finalJson["data"] as? Array<[String: AnyObject]> != nil ? finalJson["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
              
              for regimenOption in arrayOfRegimen {
                
                let newId = regimenOption["id"] as? String != nil ? regimenOption["id"] as! String : ""
                let newName = regimenOption["bank"] as? String != nil ? regimenOption["bank"] as! String : ""
                
                let newBankOption = Option(id: newId, name: newName, type: "")
                arrayOfBankOptions.append(newBankOption)
                
              }
              
              actionsToMakeWhenSucceeded(arrayOfBankOptions)
              
            } else {
              
              actionsToMakeWhenFailed()
              
            }
            
          } catch(_) {
            
            let alertController = UIAlertController(title: "ERROR",
                                                    message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
              
              actionsToMakeWhenFailed()
              
            }
            alertController.addAction(cancelAction)
            
            let actualController = UtilityManager.sharedInstance.currentViewController()
            actualController.present(alertController, animated: true, completion: nil)
            
          }
          
        } else {
          
          let alertController = UIAlertController(title: "ERROR",
                                                  message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            actionsToMakeWhenFailed()
            
          }
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
        }
        
    }
    
  }
  
  func requestToCreateNewFactura(params: [String: AnyObject], actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/cfdi33/create"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
      print(String(data: try JSONSerialization.data(withJSONObject: params, options: []), encoding: .utf8))
      
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 200 {
            
            do {
              
              let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as! [String: AnyObject]
              
              let response = json["response"] as? String != nil ? json["response"] as! String : "no error"
              
              if response == "error" {
                
                let alertController = UIAlertController(title: "ERROR",
                                                        message: "Error en la petición al servidor",
                                                        preferredStyle: UIAlertControllerStyle.alert)
                
                let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                  
                  actionsToMakeWhenFailed()
                  
                }
                
                alertController.addAction(cancelAction)
                
                let actualController = UtilityManager.sharedInstance.currentViewController()
                actualController.present(alertController, animated: true, completion: nil)
                
              } else
              
              if response == "success" {

                actionsToMakeWhenSucceeded()
   
              }
              
            } catch {
              
               print("json error: \(error.localizedDescription)")
              
              let alertController = UIAlertController(title: "ERROR",
                                                      message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                      preferredStyle: UIAlertControllerStyle.alert)
              
              let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                
                actionsToMakeWhenFailed()
                
              }
              alertController.addAction(cancelAction)
              
              let actualController = UtilityManager.sharedInstance.currentViewController()
              actualController.present(alertController, animated: true, completion: nil)
              
              
            }
            
          } else {
            
            let alertController = UIAlertController(title: "ERROR",
                                                    message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
              
              actionsToMakeWhenFailed()
              
            }
            alertController.addAction(cancelAction)
            
            let actualController = UtilityManager.sharedInstance.currentViewController()
            actualController.present(alertController, animated: true, completion: nil)
            
          }
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        actionsToMakeWhenFailed()
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
  }
  
  func getAllVersion33CFDI(actionsToMakeWhenSucceeded: @escaping (_ arrayCFDIUseOptions: Array<CFDIVersionThree>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/cfdi33/list"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:AnyObject] {
              
              print(json)
              
              do {
                
                if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:AnyObject] {
                  
                  print(json)
                  
                  let rawTypeOfTax = json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
                  
                  print(rawTypeOfTax)
                  
                  var arrayOfCFDIVersionThree: Array<CFDIVersionThree> = Array<CFDIVersionThree>()
                  
                  let arrayOfCFDIS = ((json as? [String: AnyObject])?["data"] as? Array<AnyObject>) != nil ? ((json as? [String: AnyObject])?["data"] as! Array<AnyObject>) : Array<AnyObject>()
                  
                  for factura in arrayOfCFDIS {
                    
                    let newUuid = (factura as? [String: AnyObject])?["UUID"] as? String != nil ? (factura as? [String: AnyObject])?["UUID"] as! String : ""
                    
                    let newStatus = (factura as? [String: AnyObject])?["Status"] as? String != nil ? (factura as? [String: AnyObject])?["Status"] as! String : ""
                    
                    let newFolio = (factura as? [String: AnyObject])?["Folio"] as? String != nil ? (factura as? [String: AnyObject])?["Folio"] as! String : ""
                    
                    let newVersion = (factura as? [String: AnyObject])?["Version"] as? String != nil ? (factura as? [String: AnyObject])?["Version"] as! String : ""
                    
                    let newNumOrder = (factura as? [String: AnyObject])?["NumOrder"] as? String != nil ? (factura as? [String: AnyObject])?["NumOrder"] as! String : ""
                    
                    let newFechaTimbrado = (factura as? [String: AnyObject])?["FechaTimbrado"] as? String != nil ? (factura as? [String: AnyObject])?["FechaTimbrado"] as! String : ""
                    
                    let newDescuento = (factura as? [String: AnyObject])?["Descuento"] as? String != nil ? (factura as? [String: AnyObject])?["Descuento"] as! String : ""
                    
                    let newSubtotal = (factura as? [String: AnyObject])?["Subtotal"] as? String != nil ? (factura as? [String: AnyObject])?["Subtotal"] as! String : ""
                    
                    let newReferenceClient = (factura as? [String: AnyObject])?["ReferenceClient"] as? String != nil ? (factura as? [String: AnyObject])?["ReferenceClient"] as! String : ""
                    
                    let newReceptor = (factura as? [String: AnyObject])?["Receptor"] as? String != nil ? (factura as? [String: AnyObject])?["Receptor"] as! String : ""
                    
                    let newRazonSocialReceptor = (factura as? [String: AnyObject])?["RazonSocialReceptor"] as? String != nil ? (factura as? [String: AnyObject])?["RazonSocialReceptor"] as! String : ""
                    
                    let newTotal = (factura as? [String: AnyObject])?["Total"] as? String != nil ? (factura as? [String: AnyObject])?["Total"] as! String : ""
                    
                    let newUid = (factura as? [String: AnyObject])?["UID"] as? String != nil ? (factura as? [String: AnyObject])?["UID"] as! String : ""
                    
                    let newCFDIVersionThree = CFDIVersionThree.init(newUuid: newUuid,
                                                                  newStatus: newStatus,
                                                                   newFolio: newFolio,
                                                                 newVersion: newVersion,
                                                                newNumOrder: newNumOrder,
                                                           newFechaTimbrado: newFechaTimbrado,
                                                               newDescuento: newDescuento,
                                                                newSubtotal: newSubtotal,
                                                         newReferenceClient: newReferenceClient,
                                                                newReceptor: newReceptor,
                                                                   newTotal: newTotal,
                                                                     newUid: newUid)
                    newCFDIVersionThree.razonSocialReceptor = newRazonSocialReceptor
                    
                    arrayOfCFDIVersionThree.append(newCFDIVersionThree)
                    
                  }
                  
                  actionsToMakeWhenSucceeded(arrayOfCFDIVersionThree)
                  
                }
                
              } catch _{
                
                actionsToMakeWhenFailed()
                
              }
              
            }
            
          } catch _{
            
            actionsToMakeWhenFailed()
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }
  
  func requestToGetAllFacturas(params: [String: AnyObject]?, actionsToMakeWhenSucceeded: @escaping (_ arrayOfFacturas: Array<Factura>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    var urlToRequest = "\(typeOfServerVersionOne)/invoices/"
    
    if params != nil {
    
    let month = (params?["month"] as? String != nil ? params?["month"] as! String : "")
    let year = (params?["year"] as? String != nil ? params?["year"] as! String : "")
    let rfc = (params?["rfc"] as? String != nil ? params?["rfc"] as! String : "")
 
    urlToRequest = "\(typeOfServerVersionOne)/invoices/?month=\(month)&?year=\(year)&rfc=\(rfc)"
    
    }
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    var finalParams: [String: AnyObject] = [String: AnyObject]()
    
    if params != nil {
      
      finalParams = params!
      
    }
    
    
    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: finalParams, options: [])
      
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 200 {
            
            let json = try! JSONSerialization.jsonObject(with: response.data!, options: [])
            print(json)
            
            let message = ((json as? [String: AnyObject])?["data"] as? String) != nil ? ((json as? [String: AnyObject])?["data"] as! String) : ""

            if message == "" {
              
              let arrayOfFacturas = ((json as? [String: AnyObject])?["data"] as? Array<AnyObject>) != nil ? ((json as? [String: AnyObject])?["data"] as! Array<AnyObject>) : Array<AnyObject>()
              
              var finalArrayOfFacturas: Array<Factura> = Array<Factura>()
              
              for factura in arrayOfFacturas {
                
                let newUuid = (factura as? [String: AnyObject])?["UUID"] as? String != nil ? (factura as? [String: AnyObject])?["UUID"] as! String : ""
                
                let newStatus = (factura as? [String: AnyObject])?["Status"] as? String != nil ? (factura as? [String: AnyObject])?["Status"] as! String : ""
                
                let newFolio = (factura as? [String: AnyObject])?["Folio"] as? String != nil ? (factura as? [String: AnyObject])?["Folio"] as! String : ""
                
                let newIva = (factura as? [String: AnyObject])?["IVA"] as? String != nil ? (factura as? [String: AnyObject])?["IVA"] as! String : ""
                
                let newNumOrder = (factura as? [String: AnyObject])?["NumOrder"] as? String != nil ? (factura as? [String: AnyObject])?["NumOrder"] as! String : ""
                
                let newFechaTimbrado = (factura as? [String: AnyObject])?["FechaTimbrado"] as? String != nil ? (factura as? [String: AnyObject])?["FechaTimbrado"] as! String : ""
                
                let newDescuento = (factura as? [String: AnyObject])?["Descuento"] as? String != nil ? (factura as? [String: AnyObject])?["Descuento"] as! String : ""
                
                let newSubtotal = (factura as? [String: AnyObject])?["Subtotal"] as? String != nil ? (factura as? [String: AnyObject])?["Subtotal"] as! String : ""
                
                let newReferenceClient = (factura as? [String: AnyObject])?["ReferenceClient"] as? String != nil ? (factura as? [String: AnyObject])?["ReferenceClient"] as! String : ""
                
                let newReceptor = (factura as? [String: AnyObject])?["Receptor"] as? String != nil ? (factura as? [String: AnyObject])?["Receptor"] as! String : ""
                
                let newRazonSocialReceptor = (factura as? [String: AnyObject])?["RazonSocialReceptor"] as? String != nil ? (factura as? [String: AnyObject])?["RazonSocialReceptor"] as! String : ""
                
                let newTotal = (factura as? [String: AnyObject])?["Total"] as? String != nil ? (factura as? [String: AnyObject])?["Total"] as! String : ""
                
                let newUid = (factura as? [String: AnyObject])?["UID"] as? String != nil ? (factura as? [String: AnyObject])?["UID"] as! String : ""
                
                let newFactura = Factura.init(newUuid: newUuid,
                                              newStatus: newStatus,
                                              newFolio: newFolio,
                                              newIva: newIva,
                                              newNumOrder: newNumOrder,
                                              newFechaTimbrado: newFechaTimbrado,
                                              newDescuento: newDescuento,
                                              newSubtotal: newSubtotal,
                                              newReferenceClient: newReferenceClient,
                                              newReceptor: newReceptor,
                                              newTotal: newTotal,
                                              newUid: newUid)
                newFactura.razonSocialReceptor = newRazonSocialReceptor
                
                finalArrayOfFacturas.append(newFactura)
                
              }
              
              actionsToMakeWhenSucceeded(finalArrayOfFacturas)
              
            } else
              if message == "No se encontraron datos correspondientes a tu búsqueda" {
                
                actionsToMakeWhenSucceeded(Array<Factura>())
                
              }
            
          } else {
            
            print("Response status code in Facturas: \(response.response?.statusCode)")
            
            if response.response?.statusCode == 400 {
              
              do {
                
                let json = try JSONSerialization.jsonObject(with: response.data!, options: [])
                
                if json as? NSDictionary != nil {
                  
                  let message = (json as! NSDictionary)["status"] as? String != nil ? (json as! NSDictionary)["status"] as! String : ""
                  
                  if message == "No se encontraron datos correspondientes a tu búsqueda" {
                    
                    actionsToMakeWhenSucceeded(Array<Factura>())
                    
                  }
                  
                  
                }
                
              } catch(_) {
                
                let alertController = UIAlertController(title: "ERROR",
                                                        message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                        preferredStyle: UIAlertControllerStyle.alert)
                
                let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                  
                  actionsToMakeWhenFailed()
                  
                }
                alertController.addAction(cancelAction)
                
                let actualController = UtilityManager.sharedInstance.currentViewController()
                actualController.present(alertController, animated: true, completion: nil)
                
              }
              
            }
            
            actionsToMakeWhenFailed()
            
          }
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        actionsToMakeWhenFailed()
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
  }
  
  func downloadCFDIVersionThreePDF(uidValue: String, actionsToMakeWhenSucceeded: @escaping (_ destinationPDF: URL?) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/cfdi33/\(uidValue)/pdf"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
      let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
      let fileURL = documentsURL.appendingPathComponent("factura.pdf")
      
      return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    }
    
    Alamofire.download(requestConnection, to: destination).response { response in
      
      if response.response?.statusCode == 200 {
        
        actionsToMakeWhenSucceeded(response.destinationURL)
        
      } else {
        
        
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
          
          actionsToMakeWhenFailed()
          
        }
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
        actionsToMakeWhenFailed()
        
      }
      
    }
    
  }
  
  func downloadCFDIVersionThreeXML(uidValue: String, actionsToMakeWhenSucceeded: @escaping (_ destinationXML: URL?) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/cfdi33/\(uidValue)/xml"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
      let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
      let fileURL = documentsURL.appendingPathComponent("cfdi.xml")
      
      return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    }
    
    Alamofire.download(requestConnection, to: destination).response { response in
      
      if response.response?.statusCode == 200 {
        
        actionsToMakeWhenSucceeded(response.destinationURL)
        
      } else {
        
        
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
          
          actionsToMakeWhenFailed()
          
        }
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
        actionsToMakeWhenFailed()
        
      }
      
    }
    
  }
  
  func sendCFDIVersionThreeViaEMail(uidValue: String, actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/cfdi33/\(uidValue)/email"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          actionsToMakeWhenSucceeded()
          
        } else {
          
          
          let alertController = UIAlertController(title: "ERROR",
                                                  message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            actionsToMakeWhenFailed()
            
          }
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
          actionsToMakeWhenFailed()
          
        }
        
    }
    
  }
  
  func requestToCancelCFDIVersionThree(facturaUid: String, actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/invoice/\(facturaUid)/cancel"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          print(response)
          
          actionsToMakeWhenSucceeded()
          
        } else {
          
          let alertController = UIAlertController(title: "ERROR",
                                                  message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            actionsToMakeWhenFailed()
            
          }
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
          
        }
        
    }
    
  }
  
  func downloadFacturaPDF(uidValue: String, actionsToMakeWhenSucceeded: @escaping (_ destinationPDF: URL?) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    //  let developmentServer = "http://devfactura.in/api"
    let productionServer  = "https://factura.com/api"
    
    let urlToRequest = "\(productionServer)/publica/invoice/\(uidValue)/pdf"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
      let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
      let fileURL = documentsURL.appendingPathComponent("factura.pdf")
    
      return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    }
    
    Alamofire.download(requestConnection, to: destination).response { response in
      
      if response.response?.statusCode == 200 {
        
        actionsToMakeWhenSucceeded(response.destinationURL)
        
      } else {
        
        
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
          
          actionsToMakeWhenFailed()
          
        }
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
        actionsToMakeWhenFailed()
        
      }
      
    }
    
  }
  
  func requestToCancelFactura(facturaUid: String, actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/invoice/\(facturaUid)/cancel"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
            
          print(response)
            
          actionsToMakeWhenSucceeded()
            
        } else {
            
          let alertController = UIAlertController(title: "ERROR",
                                                    message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
              
            actionsToMakeWhenFailed()
              
          }
          alertController.addAction(cancelAction)
            
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
            
        }
        
    }
    
  }
  
  func requestToCancelRecibo(reciboUid: String, actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/fees/\(reciboUid)/cancel"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          print(response)
          
          actionsToMakeWhenSucceeded()
          
        } else {
          
          let alertController = UIAlertController(title: "ERROR",
                                                  message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            actionsToMakeWhenFailed()
            
          }
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
          
        }
        
    }
    
  }
  
  func requestToCancelNote(noteUid: String, actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/notes/\(noteUid)/cancel"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          print(response)
          
          actionsToMakeWhenSucceeded()
          
        } else {
          
          let alertController = UIAlertController(title: "ERROR",
                                                  message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            actionsToMakeWhenFailed()
            
          }
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
          
        }
        
    }
    
  }
  
  func requestToCancelCarta(cartaUid: String, actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/porte/\(cartaUid)/cancel"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          print(response)
          
          actionsToMakeWhenSucceeded()
          
        } else {
          
          let alertController = UIAlertController(title: "ERROR",
                                                  message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            actionsToMakeWhenFailed()
            
          }
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
          
        }
        
    }
    
  }
  
  func requestToCreateNewRecibo(params: [String: AnyObject], actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionTwo)/fees/create"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
      
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 200 {
            
            do {
              
              let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as! [String: AnyObject]
              
              let response = json["response"] as? String != nil ? json["response"] as! String : "no error"
              
              if response == "error" {
                
                let alertController = UIAlertController(title: "ERROR",
                                                        message: "Error en la petición al servidor",
                                                        preferredStyle: UIAlertControllerStyle.alert)
                
                let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                  
                  actionsToMakeWhenFailed()
                  
                }
                
                alertController.addAction(cancelAction)
                
                let actualController = UtilityManager.sharedInstance.currentViewController()
                actualController.present(alertController, animated: true, completion: nil)
                
              } else
                
                if response == "no error" {
                  
                  let status = json["status"] as? String != nil ? json["status"] as! String : ""
                  
                  if status == "success" {
                    
                    actionsToMakeWhenSucceeded()
                    
                  }
                  
              }
              
            } catch(_) {
              
              let alertController = UIAlertController(title: "ERROR",
                                                      message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                      preferredStyle: UIAlertControllerStyle.alert)
              
              let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                
                actionsToMakeWhenFailed()
                
              }
              alertController.addAction(cancelAction)
              
              let actualController = UtilityManager.sharedInstance.currentViewController()
              actualController.present(alertController, animated: true, completion: nil)
              
              
            }
            
          } else {
            
            let alertController = UIAlertController(title: "ERROR",
                                                    message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
              
              actionsToMakeWhenFailed()
              
            }
            alertController.addAction(cancelAction)
            
            let actualController = UtilityManager.sharedInstance.currentViewController()
            actualController.present(alertController, animated: true, completion: nil)
            
          }
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        actionsToMakeWhenFailed()
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
  }
  
  
  func downloadReciboPDF(uidValue: String, actionsToMakeWhenSucceeded: @escaping (_ destinationPDF: URL?) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    //  let developmentServer = "http://devfactura.in/api"
    let productionServer  = "https://factura.com/api"
    
    let urlToRequest = "\(productionServer)/publica/fees/\(uidValue)/pdf"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
      let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
      let fileURL = documentsURL.appendingPathComponent("recibo.pdf")
      
      return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    }
    
    Alamofire.download(requestConnection, to: destination).response { response in
      
      if response.response?.statusCode == 200 {
        
        actionsToMakeWhenSucceeded(response.destinationURL)
        
      } else {
        
        
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
          
          actionsToMakeWhenFailed()
          
        }
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
        actionsToMakeWhenFailed()
        
      }
      
    }
    
  }
  
  func requestToCreateNewNota(params: [String: AnyObject], actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionTwo)/notes/create"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
      
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 200 {
            
            do {
              
              let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as! [String: AnyObject]
              
              let response = json["response"] as? String != nil ? json["response"] as! String : "no error"
              
              if response == "error" {
                
                let alertController = UIAlertController(title: "ERROR",
                                                        message: "Error en la petición al servidor",
                                                        preferredStyle: UIAlertControllerStyle.alert)
                
                let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                  
                  actionsToMakeWhenFailed()
                  
                }
                
                alertController.addAction(cancelAction)
                
                let actualController = UtilityManager.sharedInstance.currentViewController()
                actualController.present(alertController, animated: true, completion: nil)
                
              } else
                
                if response == "no error" {
                  
                  let status = json["status"] as? String != nil ? json["status"] as! String : ""
                  
                  if status == "success" {
                    
                    actionsToMakeWhenSucceeded()
                    
                  }
                  
              }
              
            } catch(_) {
              
              let alertController = UIAlertController(title: "ERROR",
                                                      message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                      preferredStyle: UIAlertControllerStyle.alert)
              
              let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                
                actionsToMakeWhenFailed()
                
              }
              alertController.addAction(cancelAction)
              
              let actualController = UtilityManager.sharedInstance.currentViewController()
              actualController.present(alertController, animated: true, completion: nil)
              
              
            }
            
          } else {
            
            let alertController = UIAlertController(title: "ERROR",
                                                    message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
              
              actionsToMakeWhenFailed()
              
            }
            alertController.addAction(cancelAction)
            
            let actualController = UtilityManager.sharedInstance.currentViewController()
            actualController.present(alertController, animated: true, completion: nil)
            
          }
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        actionsToMakeWhenFailed()
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
  }
  
  func downloadNotaPDF(uidValue: String, actionsToMakeWhenSucceeded: @escaping (_ destinationPDF: URL?) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    //  let developmentServer = "http://devfactura.in/api"
    let productionServer  = "https://factura.com/api"
    
    let urlToRequest = "\(productionServer)/publica/notes/\(uidValue)/pdf"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
      let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
      let fileURL = documentsURL.appendingPathComponent("nota.pdf")
      
      return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    }
    
    Alamofire.download(requestConnection, to: destination).response { response in
      
      if response.response?.statusCode == 200 {
        
        actionsToMakeWhenSucceeded(response.destinationURL)
        
      } else {
        
        
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
          
          actionsToMakeWhenFailed()
          
        }
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
        actionsToMakeWhenFailed()
        
      }
      
    }
    
  }
  
  func requestToCreateNewPorte(params: [String: AnyObject], actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionTwo)/porte/create"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
      
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 200 {
            
            do {
              
              let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as! [String: AnyObject]
              
              let response = json["response"] as? String != nil ? json["response"] as! String : "no error"
              
              if response == "error" {
                
                let alertController = UIAlertController(title: "ERROR",
                                                        message: "Error en la petición al servidor",
                                                        preferredStyle: UIAlertControllerStyle.alert)
                
                let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                  
                  actionsToMakeWhenFailed()
                  
                }
                
                alertController.addAction(cancelAction)
                
                let actualController = UtilityManager.sharedInstance.currentViewController()
                actualController.present(alertController, animated: true, completion: nil)
                
              } else
                
                if response == "no error" {
                  
                  let status = json["status"] as? String != nil ? json["status"] as! String : ""
                  
                  if status == "success" {
                    
                    actionsToMakeWhenSucceeded()
                    
                  }
                  
              }
              
            } catch(_) {
              
              let alertController = UIAlertController(title: "ERROR",
                                                      message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                      preferredStyle: UIAlertControllerStyle.alert)
              
              let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                
                actionsToMakeWhenFailed()
                
              }
              alertController.addAction(cancelAction)
              
              let actualController = UtilityManager.sharedInstance.currentViewController()
              actualController.present(alertController, animated: true, completion: nil)
              
              
            }
            
          } else {
            
            let alertController = UIAlertController(title: "ERROR",
                                                    message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
              
              actionsToMakeWhenFailed()
              
            }
            alertController.addAction(cancelAction)
            
            let actualController = UtilityManager.sharedInstance.currentViewController()
            actualController.present(alertController, animated: true, completion: nil)
            
          }
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        actionsToMakeWhenFailed()
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
  }
  
  func downloadCartaPortePDF(uidValue: String, actionsToMakeWhenSucceeded: @escaping (_ destinationPDF: URL?) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    //  let developmentServer = "http://devfactura.in/api"
    let productionServer  = "https://factura.com/api"
    
    let urlToRequest = "\(productionServer)/publica/porte/\(uidValue)/pdf"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
      let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
      let fileURL = documentsURL.appendingPathComponent("carta.pdf")
      
      return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    }
    
    Alamofire.download(requestConnection, to: destination).response { response in
      
      if response.response?.statusCode == 200 {
        
        actionsToMakeWhenSucceeded(response.destinationURL)
        
      } else {
        
        
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
          
          actionsToMakeWhenFailed()
          
        }
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
        actionsToMakeWhenFailed()
        
      }
      
    }
    
  }
  
  func requestToCancelCartaPorte(noteUid: String, actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/fees/\(noteUid)/cancel"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          print(response)
          
          actionsToMakeWhenSucceeded()
          
        } else {
          
          let alertController = UIAlertController(title: "ERROR",
                                                  message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            actionsToMakeWhenFailed()
            
          }
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
          
        }
        
    }
    
  }
  
  func downloadFacturaXML(uidValue: String, actionsToMakeWhenSucceeded: @escaping (_ destinationXML: URL?) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    //  let developmentServer = "http://devfactura.in/api"
    let productionServer  = "https://factura.com/api"
    
    let urlToRequest = "\(productionServer)/publica/invoice/\(uidValue)/xml"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
      let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
      let fileURL = documentsURL.appendingPathComponent("factura.xml")
      
      return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    }
    
    Alamofire.download(requestConnection, to: destination).response { response in
      
      if response.response?.statusCode == 200 {
        
        actionsToMakeWhenSucceeded(response.destinationURL)
        
      } else {
        
        
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
          
          actionsToMakeWhenFailed()
          
        }
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
        actionsToMakeWhenFailed()
        
      }
      
    }
    
  }
  
  func downloadReciboXML(uidValue: String, actionsToMakeWhenSucceeded: @escaping (_ destinationXML: URL?) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    //  let developmentServer = "http://devfactura.in/api"
    let productionServer  = "https://factura.com/api"
    
    let urlToRequest = "\(productionServer)/publica/fees/\(uidValue)/xml"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
      let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
      let fileURL = documentsURL.appendingPathComponent("recibo.xml")
      
      return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    }
    
    Alamofire.download(requestConnection, to: destination).response { response in
      
      if response.response?.statusCode == 200 {
        
        actionsToMakeWhenSucceeded(response.destinationURL)
        
      } else {
        
        
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
          
          actionsToMakeWhenFailed()
          
        }
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
        actionsToMakeWhenFailed()
        
      }
      
    }
    
  }
  
  func downloadNotaXML(uidValue: String, actionsToMakeWhenSucceeded: @escaping (_ destinationXML: URL?) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    //  let developmentServer = "http://devfactura.in/api"
    let productionServer  = "https://factura.com/api"
    
    let urlToRequest = "\(productionServer)/publica/notes/\(uidValue)/xml"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
      let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
      let fileURL = documentsURL.appendingPathComponent("nota.xml")
      
      return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    }
    
    Alamofire.download(requestConnection, to: destination).response { response in
      
      if response.response?.statusCode == 200 {
        
        actionsToMakeWhenSucceeded(response.destinationURL)
        
      } else {
        
        
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
          
          actionsToMakeWhenFailed()
          
        }
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
        actionsToMakeWhenFailed()
        
      }
      
    }
    
  }
  
  func downloadCartaPorteXML(uidValue: String, actionsToMakeWhenSucceeded: @escaping (_ destinationPDF: URL?) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    //  let developmentServer = "http://devfactura.in/api"
    let productionServer  = "https://factura.com/api"
    
    let urlToRequest = "\(productionServer)/publica/porte/\(uidValue)/xml"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
      let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
      let fileURL = documentsURL.appendingPathComponent("carta.xml")
      
      return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    }
    
    Alamofire.download(requestConnection, to: destination).response { response in
      
      if response.response?.statusCode == 200 {
        
        actionsToMakeWhenSucceeded(response.destinationURL)
        
      } else {
        
        
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
          
          actionsToMakeWhenFailed()
          
        }
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
        actionsToMakeWhenFailed()
        
      }
      
    }
    
  }
  
  func sendFacturaViaEMail(uidValue: String, actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/invoice/\(uidValue)/email"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
        
          actionsToMakeWhenSucceeded()
        
        } else {
        
        
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
          
          actionsToMakeWhenFailed()
          
        }
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
        actionsToMakeWhenFailed()
        
      }
      
    }
    
  }
  
  func sendReciboViaEMail(uidValue: String, actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/fees/\(uidValue)/email"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          actionsToMakeWhenSucceeded()
          
        } else {
          
          
          let alertController = UIAlertController(title: "ERROR",
                                                  message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            actionsToMakeWhenFailed()
            
          }
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
          actionsToMakeWhenFailed()
          
        }
        
    }
    
  }
  
  func sendNotaViaEMail(uidValue: String, actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/notes/\(uidValue)/email"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          actionsToMakeWhenSucceeded()
          
        } else {
          
          
          let alertController = UIAlertController(title: "ERROR",
                                                  message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            actionsToMakeWhenFailed()
            
          }
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
          actionsToMakeWhenFailed()
          
        }
        
    }
    
  }
  
  func sendCartaPorteViaEMail(uidValue: String, actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/porte/\(uidValue)/email"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          actionsToMakeWhenSucceeded()
          
        } else {
          
          
          let alertController = UIAlertController(title: "ERROR",
                                                  message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            actionsToMakeWhenFailed()
            
          }
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
          actionsToMakeWhenFailed()
          
        }
        
    }
    
  }
  
  func accountDetails(uid: String, actionsToMakeWhenSucceeded: @escaping (_ json: [String: AnyObject]) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)settings/currencies"  //This is the only one which is different
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 200 {
            
            let json = try! JSONSerialization.jsonObject(with: response.data!, options: [])
            print(json)
            
            actionsToMakeWhenSucceeded(json as! [String: AnyObject])
            
          } else {
            
            actionsToMakeWhenFailed()
            
          }
      }
    
  }
  
  func requestToCreateNewClient(params: [String: AnyObject], actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/clients/create"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
      
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 201 {
            
            print(response)
            
            actionsToMakeWhenSucceeded()
            
          } else
          
          if response.response?.statusCode == 200 {
          
              do {
            
                let json = try (JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as! [String:Any])
                
                if json["status"] as? String != nil {
                  
                  let statusMessage = json["status"] as! String
                  if statusMessage == "already" {
                    
                    let alertController = UIAlertController(title: "ERROR",
                                                            message: "El RFC ya se encuentra dado de alta",
                                                            preferredStyle: UIAlertControllerStyle.alert)
                    
                    let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                      
                      actionsToMakeWhenFailed()
                      
                    }
                    alertController.addAction(cancelAction)
                    
                    let actualController = UtilityManager.sharedInstance.currentViewController()
                    actualController.present(alertController, animated: true, completion: nil)
                    
                  }
                  
                } else
                if json["message"] as? [String: AnyObject] != nil {
                  
                  let alertController = UIAlertController(title: "ERROR",
                                                          message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                          preferredStyle: UIAlertControllerStyle.alert)
                  
                  let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    
                    actionsToMakeWhenFailed()
                    
                  }
                  alertController.addAction(cancelAction)
                  
                  let actualController = UtilityManager.sharedInstance.currentViewController()
                  actualController.present(alertController, animated: true, completion: nil)
                  
                }
              
              }catch(_) {
            
                let alertController = UIAlertController(title: "ERROR",
                                                        message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                        preferredStyle: UIAlertControllerStyle.alert)
                
                let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                  
                  actionsToMakeWhenFailed()
                  
                }
                alertController.addAction(cancelAction)
                
                let actualController = UtilityManager.sharedInstance.currentViewController()
                actualController.present(alertController, animated: true, completion: nil)
            
              }
          
          
          } else {
            
            let alertController = UIAlertController(title: "ERROR",
                                                    message: "Error de al crear un nuevo cliente, favor de intentar más tarde",
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
              
              actionsToMakeWhenFailed()
              
            }
            alertController.addAction(cancelAction)
            
            let actualController = UtilityManager.sharedInstance.currentViewController()
            actualController.present(alertController, animated: true, completion: nil)
            
          }
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        actionsToMakeWhenFailed()
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }

  }
  
  func requestToUpdateClient(clientUid: String, params: [String: AnyObject], actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/clients/\(clientUid)/update"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
      
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 201 {
            
            print(response)
            
            actionsToMakeWhenSucceeded()
            
          } else {
            
            actionsToMakeWhenFailed()
            
          }
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        actionsToMakeWhenFailed()
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
  }
  
  func clientsByRFC(rfc: String?, actionsToMakeWhenSucceeded: @escaping (_ arrayOfClients: Array<Client>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    var urlToRequest = "\(typeOfServerVersionOne)/clients"
    
    if rfc != nil {
      
      urlToRequest = "\(typeOfServerVersionOne)/clients/\(rfc!)"
      
    }
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
            
              print(json)
              
              let rawArrayClients = json["data"] as? Array<AnyObject> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
              
              print(rawArrayClients)
              
              var arrayOfClients: Array<Client> = Array<Client>()
   
              for rawClient in rawArrayClients {
               
                let newInterior = (rawClient["Interior"] as? String != nil && rawClient["Interior"] as? String != "No Aplica" ? rawClient["Interior"] as! String : "")
                
                let newColonia = (rawClient["Colonia"] as? String != nil && rawClient["Colonia"] as? String != "No Aplica" ? rawClient["Colonia"] as! String : "")
                
                let newDelegacion = (rawClient["Delegacion"] as? String != nil && rawClient["Delegacion"] as? String != "No Aplica" ? rawClient["Delegacion"] as! String : "")
                
                let newNumero = (rawClient["Numero"] as? String != nil && rawClient["Numero"] as? String != "No Aplica" ? rawClient["Numero"] as! String : "")
                
                let newLocalidad = (rawClient["Localidad"] as? String != nil && rawClient["Localidad"] as? String != "No Aplica" ? rawClient["Localidad"] as! String : "")
                
                let newRazonSocial = (rawClient["RazonSocial"] as? String != nil && rawClient["RazonSocial"] as? String != "No Aplica" ? rawClient["RazonSocial"] as! String : "")
                
                let newCiudad = (rawClient["Ciudad"] as? String != nil && rawClient["Ciudad"] as? String != "No Aplica" ? rawClient["Ciudad"] as! String : "")
                
                let newUID = (rawClient["UID"] as? String != nil && rawClient["UID"] as? String != "No Aplica" ? rawClient["UID"] as! String : "")
                
                let newEstado = (rawClient["Estado"] as? String != nil && rawClient["Estado"] as? String != "No Aplica" ? rawClient["Estado"] as! String : "")
                
                let newPais = (rawClient["Pais"] as? String != nil && rawClient["Pais"] as? String != "No Aplica" ? rawClient["Pais"] as! String : "México")
                
                let newCalle = (rawClient["Calle"] as? String != nil && rawClient["Calle"] as? String != "No Aplica" ? rawClient["Calle"] as! String : "")
                
                let newRFC = (rawClient["RFC"] as? String != nil && rawClient["RFC"] as? String != "No Aplica" ? rawClient["RFC"] as! String : "")
                
                let newCodigPostal = (rawClient["CodigoPostal"] as? String != nil && rawClient["CodigoPostal"] as? String != "No Aplica" ? rawClient["CodigoPostal"] as! String : "")
                
                let newRawContacto = (rawClient["Contacto"] as? [String: AnyObject] != nil ? rawClient["Contacto"] as! [String: AnyObject] : [String: AnyObject]())
                
                let newNumberOfCFDIs = (rawClient["cfdis"] as? String != nil ? rawClient["cfdis"] as! String : "")
                
                let arrayOfBankAccounts = (rawClient["cuentas_banco"] as? Array<[String: AnyObject]> != nil ? rawClient["cuentas_banco"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>())
                
                var arrayOfBankAccountsOption: [Option] = [Option]()
                
                for bankAccount in arrayOfBankAccounts {
                  
                  let newAccountNumber = bankAccount["cuenta"] as? String != nil ? bankAccount["cuenta"] as! String : ""
                  let newName = bankAccount["banco"] as? String != nil ? bankAccount["banco"] as! String : ""
                  
                  let newOption = Option(id: "", name: newName, type: newAccountNumber)
                  
                  arrayOfBankAccountsOption.append(newOption)
                  
                }
                
                let newContacto = self.createNewContact(newRawContacto)
                
                let newClient = Client()
                newClient.calle = newCalle
                newClient.ciudad = newCiudad
                newClient.codigoPostal = newCodigPostal
                newClient.colonia = newColonia
                newClient.contacto = newContacto
                newClient.delegacion = newDelegacion
                newClient.estado = newEstado
                newClient.interior = newInterior
                newClient.localidad = newLocalidad
                newClient.numero = newNumero
                newClient.razonSocial = newRazonSocial
                newClient.rfc = newRFC
                newClient.uid = newUID
                newClient.numberOfCFDIs = newNumberOfCFDIs
                newClient.arrayOfBankAccounts = arrayOfBankAccountsOption
                newClient.pais = newPais
                
                arrayOfClients.append(newClient)
                
              }
              
              actionsToMakeWhenSucceeded(arrayOfClients)
            
            }
          
          } catch _{
          
            actionsToMakeWhenFailed()
          
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }
  
  func requestToDeleteClient(clientID: String, actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/clients/destroy/\(clientID)"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
            
          print(response)
            
          actionsToMakeWhenSucceeded()
            
        } else {
            
          let alertController = UIAlertController(title: "ERROR",
                                                  message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                  preferredStyle: UIAlertControllerStyle.alert)
            
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
              
            actionsToMakeWhenFailed()
              
          }
            alertController.addAction(cancelAction)
            
            let actualController = UtilityManager.sharedInstance.currentViewController()
            actualController.present(alertController, animated: true, completion: nil)
            
          }
    }
  
  }
  
  private func createNewContact(_ withRawData: [String: AnyObject]) -> Contact {
    
    let newApellidos = (withRawData["Apellidos"] as? String != nil && withRawData["Apellidos"] as? String != "No Aplica" ? withRawData["Apellidos"] as! String : "")
    let newEmail = (withRawData["Email"] as? String != nil && withRawData["Email"] as? String != "No Aplica" ? withRawData["Email"] as! String : "")
    let newNombre = (withRawData["Nombre"] as? String != nil && withRawData["Nombre"] as? String != "No Aplica" ? withRawData["Nombre"] as! String : "")
    let newTelefono = (withRawData["Telefono"] as? String != nil && withRawData["Telefono"] as? String != "No Aplica" ? withRawData["Telefono"] as! String : "")
    
    let newContact = Contact()
    newContact.apellidos = newApellidos
    newContact.email = newEmail
    newContact.telefono = newTelefono
    newContact.nombre = newNombre
    
    return newContact
    
  }
  
  
  func getLocations(actionsToMakeWhenSucceeded: @escaping (_ arrayOfExpeditionPlace: Array<ExpeditionPlace>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/locations"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
              
              print(json)
              
              do {
                
                if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
                  
                  print(json)
                  
                  let rawArrayClients = json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
                  
                  print(rawArrayClients)
                  
                  var arrayExpeditionPlace: Array<ExpeditionPlace> = Array<ExpeditionPlace>()
                  
                  for expeditionPlace in rawArrayClients {
                    
                    let newSucUID = (expeditionPlace["SucUID"] as? String != nil ? expeditionPlace["UID"] as! String : (expeditionPlace["SucUID"] as? Int != nil ? String(expeditionPlace["SucUID"] as! Int) : "0"))
                    let newSucursal = (expeditionPlace["Sucursal"] as? String != nil && expeditionPlace["Sucursal"] as? String != "No Aplica" ? expeditionPlace["Sucursal"] as! String : "")
                    
                    let newExpeditionPlace = ExpeditionPlace(newSucUID: newSucUID, newSucursalName: newSucursal)
                    
                    arrayExpeditionPlace.append(newExpeditionPlace)
                    
                  }
                  
                  actionsToMakeWhenSucceeded(arrayExpeditionPlace)
                  
                }
                
              } catch _{
                
                actionsToMakeWhenFailed()
                
              }
              
            }
            
          } catch _{
            
            actionsToMakeWhenFailed()
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }
  
  
  
  func requestToCreateNewExpeditionPlace(params: [String: AnyObject], actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/locations/create"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
      
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 200 {
            
            print(response)
            
            actionsToMakeWhenSucceeded()
            
          } else {
            
            actionsToMakeWhenFailed()
            
          }
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        actionsToMakeWhenFailed()
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
  }
  
  ///Recibos
  
  func requestToGetAllRecibos(params: [String: AnyObject]?, actionsToMakeWhenSucceeded: @escaping (_ arrayOfFacturas: Array<Recibo>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    var urlToRequest = "\(typeOfServerVersionOne)/fees/"
    
    if params != nil {
      
      let month = (params?["month"] as? String != nil ? params?["month"] as! String : "")
      let year = (params?["year"] as? String != nil ? params?["year"] as! String : "")
      let rfc = (params?["rfc"] as? String != nil ? params?["rfc"] as! String : "")
      
      urlToRequest = "\(typeOfServerVersionOne)/fees/?month=\(month)&?year=\(year)&rfc=\(rfc)"
      
    }
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    var finalParams: [String: AnyObject] = [String: AnyObject]()
    
    if params != nil {
      
      finalParams = params!
      
    }
    
    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: finalParams, options: [])
      
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 200 {
            
            let json = try! JSONSerialization.jsonObject(with: response.data!, options: [])
            print(json)
            
            let arrayOfRecibos = ((json as? [String: AnyObject])?["data"] as? Array<AnyObject>) != nil ? ((json as? [String: AnyObject])?["data"] as! Array<AnyObject>) : Array<AnyObject>()
            
            var finalArrayOfRecibos: Array<Recibo> = Array<Recibo>()
            
            for recibo in arrayOfRecibos {
              
              let newUuid = (recibo as? [String: AnyObject])?["UUID"] as? String != nil ? (recibo as? [String: AnyObject])?["UUID"] as! String : ""
              
              let newStatus = (recibo as? [String: AnyObject])?["Status"] as? String != nil ? (recibo as? [String: AnyObject])?["Status"] as! String : ""
              
              let newFolio = (recibo as? [String: AnyObject])?["Folio"] as? String != nil ? (recibo as? [String: AnyObject])?["Folio"] as! String : ""
              
              let newIva = (recibo as? [String: AnyObject])?["IVA"] as? String != nil ? (recibo as? [String: AnyObject])?["IVA"] as! String : ""
              
              let newNumOrder = (recibo as? [String: AnyObject])?["NumOrder"] as? String != nil ? (recibo as? [String: AnyObject])?["NumOrder"] as! String : ""
              
              let newFechaTimbrado = (recibo as? [String: AnyObject])?["FechaTimbrado"] as? String != nil ? (recibo as? [String: AnyObject])?["FechaTimbrado"] as! String : ""
              
              let newDescuento = (recibo as? [String: AnyObject])?["Descuento"] as? String != nil ? (recibo as? [String: AnyObject])?["Descuento"] as! String : ""
              
              let newSubtotal = (recibo as? [String: AnyObject])?["Subtotal"] as? String != nil ? (recibo as? [String: AnyObject])?["Subtotal"] as! String : ""
              
              let newReferenceClient = (recibo as? [String: AnyObject])?["ReferenceClient"] as? String != nil ? (recibo as? [String: AnyObject])?["ReferenceClient"] as! String : ""
              
              let newReceptor = (recibo as? [String: AnyObject])?["Receptor"] as? String != nil ? (recibo as? [String: AnyObject])?["Receptor"] as! String : ""
              
              let newRazonSocialReceptor = (recibo as? [String: AnyObject])?["RazonSocialReceptor"] as? String != nil ? (recibo as? [String: AnyObject])?["RazonSocialReceptor"] as! String : ""
              
              let newTotal = (recibo as? [String: AnyObject])?["Total"] as? String != nil ? (recibo as? [String: AnyObject])?["Total"] as! String : ""
              
              let newUid = (recibo as? [String: AnyObject])?["UID"] as? String != nil ? (recibo as? [String: AnyObject])?["UID"] as! String : ""
              
              let newRecibo = Recibo.init(newUuid: newUuid,
                                            newStatus: newStatus,
                                            newFolio: newFolio,
                                            newIva: newIva,
                                            newNumOrder: newNumOrder,
                                            newFechaTimbrado: newFechaTimbrado,
                                            newDescuento: newDescuento,
                                            newSubtotal: newSubtotal,
                                            newReferenceClient: newReferenceClient,
                                            newReceptor: newReceptor,
                                            newTotal: newTotal,
                                            newUid: newUid)
              newRecibo.razonSocialReceptor = newRazonSocialReceptor
              
              finalArrayOfRecibos.append(newRecibo)
              
            }
            
            actionsToMakeWhenSucceeded(finalArrayOfRecibos)
            
          } else {
            
            if response.response?.statusCode == 400 {
              
              do {
                
                let json = try JSONSerialization.jsonObject(with: response.data!, options: [])
                
                if json as? NSDictionary != nil {
                  
                  let message = (json as! NSDictionary)["data"] as? String != nil ? (json as! NSDictionary)["data"] as! String : ""
                  
                  if message == "No se encontraron datos correspondientes a tu búsqueda" {
                    
                    actionsToMakeWhenSucceeded(Array<Recibo>())
                    
                  }
                  
                }
                
              } catch(_) {
                
                let alertController = UIAlertController(title: "ERROR",
                                                        message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                        preferredStyle: UIAlertControllerStyle.alert)
                
                let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                  
                  actionsToMakeWhenFailed()
                  
                }
                alertController.addAction(cancelAction)
                
                let actualController = UtilityManager.sharedInstance.currentViewController()
                actualController.present(alertController, animated: true, completion: nil)
                
              }
                
            }
              
          }
            
        actionsToMakeWhenFailed()
          
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        actionsToMakeWhenFailed()
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
  }
  
  func requestToGetAllNotas(params: [String: AnyObject]?, actionsToMakeWhenSucceeded: @escaping (_ arrayOfFacturas: Array<Nota>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    var urlToRequest = "\(typeOfServerVersionOne)/notes/"
    
    if params != nil {
      
      let month = (params?["month"] as? String != nil ? params?["month"] as! String : "")
      let year = (params?["year"] as? String != nil ? params?["year"] as! String : "")
      let rfc = (params?["rfc"] as? String != nil ? params?["rfc"] as! String : "")
      
      urlToRequest = "\(typeOfServerVersionOne)/notes/?month=\(month)&?year=\(year)&rfc=\(rfc)"
      
    }
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    var finalParams: [String: AnyObject] = [String: AnyObject]()
    
    if params != nil {
      
      finalParams = params!
      
    }
    
    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: finalParams, options: [])
      
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 200 {
            
            let json = try! JSONSerialization.jsonObject(with: response.data!, options: [])
            print(json)
            
            let arrayOfRecibos = ((json as? [String: AnyObject])?["data"] as? Array<AnyObject>) != nil ? ((json as? [String: AnyObject])?["data"] as! Array<AnyObject>) : Array<AnyObject>()
            
            var finalArrayOfRecibos: Array<Nota> = Array<Nota>()
            
            for recibo in arrayOfRecibos {
              
              let newUuid = (recibo as? [String: AnyObject])?["UUID"] as? String != nil ? (recibo as? [String: AnyObject])?["UUID"] as! String : ""
              
              let newStatus = (recibo as? [String: AnyObject])?["Status"] as? String != nil ? (recibo as? [String: AnyObject])?["Status"] as! String : ""
              
              let newFolio = (recibo as? [String: AnyObject])?["Folio"] as? String != nil ? (recibo as? [String: AnyObject])?["Folio"] as! String : ""
              
              let newIva = (recibo as? [String: AnyObject])?["IVA"] as? String != nil ? (recibo as? [String: AnyObject])?["IVA"] as! String : ""
              
              let newNumOrder = (recibo as? [String: AnyObject])?["NumOrder"] as? String != nil ? (recibo as? [String: AnyObject])?["NumOrder"] as! String : ""
              
              let newFechaTimbrado = (recibo as? [String: AnyObject])?["FechaTimbrado"] as? String != nil ? (recibo as? [String: AnyObject])?["FechaTimbrado"] as! String : ""
              
              let newDescuento = (recibo as? [String: AnyObject])?["Descuento"] as? String != nil ? (recibo as? [String: AnyObject])?["Descuento"] as! String : ""
              
              let newSubtotal = (recibo as? [String: AnyObject])?["Subtotal"] as? String != nil ? (recibo as? [String: AnyObject])?["Subtotal"] as! String : ""
              
              let newReferenceClient = (recibo as? [String: AnyObject])?["ReferenceClient"] as? String != nil ? (recibo as? [String: AnyObject])?["ReferenceClient"] as! String : ""
              
              let newReceptor = (recibo as? [String: AnyObject])?["Receptor"] as? String != nil ? (recibo as? [String: AnyObject])?["Receptor"] as! String : ""
              
              let newRazonSocialReceptor = (recibo as? [String: AnyObject])?["RazonSocialReceptor"] as? String != nil ? (recibo as? [String: AnyObject])?["RazonSocialReceptor"] as! String : ""
              
              let newTotal = (recibo as? [String: AnyObject])?["Total"] as? String != nil ? (recibo as? [String: AnyObject])?["Total"] as! String : ""
              
              let newUid = (recibo as? [String: AnyObject])?["UID"] as? String != nil ? (recibo as? [String: AnyObject])?["UID"] as! String : ""
              
              let newRecibo = Nota.init(newUuid: newUuid,
                                          newStatus: newStatus,
                                          newFolio: newFolio,
                                          newIva: newIva,
                                          newNumOrder: newNumOrder,
                                          newFechaTimbrado: newFechaTimbrado,
                                          newDescuento: newDescuento,
                                          newSubtotal: newSubtotal,
                                          newReferenceClient: newReferenceClient,
                                          newReceptor: newReceptor,
                                          newTotal: newTotal,
                                          newUid: newUid)
              newRecibo.razonSocialReceptor = newRazonSocialReceptor
              
              finalArrayOfRecibos.append(newRecibo)
              
            }
            
            actionsToMakeWhenSucceeded(finalArrayOfRecibos)
            
          } else {
            
            actionsToMakeWhenFailed()
            
          }
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        actionsToMakeWhenFailed()
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
  }
  
  func requestToGetAllCartas(params: [String: AnyObject]?, actionsToMakeWhenSucceeded: @escaping (_ arrayOfFacturas: Array<Carta>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    var urlToRequest = "\(typeOfServerVersionOne)/porte/"
    
    if params != nil {
      
      let month = (params?["month"] as? String != nil ? params?["month"] as! String : "")
      let year = (params?["year"] as? String != nil ? params?["year"] as! String : "")
      let rfc = (params?["rfc"] as? String != nil ? params?["rfc"] as! String : "")
      
      urlToRequest = "\(typeOfServerVersionOne)/porte/?month=\(month)&?year=\(year)&rfc=\(rfc)"
      
    }
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    var finalParams: [String: AnyObject] = [String: AnyObject]()
    
    if params != nil {
      
      finalParams = params!
      
    }
    
    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: finalParams, options: [])
      
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 200 {
            
            let json = try! JSONSerialization.jsonObject(with: response.data!, options: [])
            print(json)
            
            let arrayOfRecibos = ((json as? [String: AnyObject])?["data"] as? Array<AnyObject>) != nil ? ((json as? [String: AnyObject])?["data"] as! Array<AnyObject>) : Array<AnyObject>()
            
            var finalArrayOfRecibos: Array<Carta> = Array<Carta>()
            
            for recibo in arrayOfRecibos {
              
              let newUuid = (recibo as? [String: AnyObject])?["UUID"] as? String != nil ? (recibo as? [String: AnyObject])?["UUID"] as! String : ""
              
              let newStatus = (recibo as? [String: AnyObject])?["Status"] as? String != nil ? (recibo as? [String: AnyObject])?["Status"] as! String : ""
              
              let newFolio = (recibo as? [String: AnyObject])?["Folio"] as? String != nil ? (recibo as? [String: AnyObject])?["Folio"] as! String : ""
              
              let newIva = (recibo as? [String: AnyObject])?["IVA"] as? String != nil ? (recibo as? [String: AnyObject])?["IVA"] as! String : ""
              
              let newNumOrder = (recibo as? [String: AnyObject])?["NumOrder"] as? String != nil ? (recibo as? [String: AnyObject])?["NumOrder"] as! String : ""
              
              let newFechaTimbrado = (recibo as? [String: AnyObject])?["FechaTimbrado"] as? String != nil ? (recibo as? [String: AnyObject])?["FechaTimbrado"] as! String : ""
              
              let newDescuento = (recibo as? [String: AnyObject])?["Descuento"] as? String != nil ? (recibo as? [String: AnyObject])?["Descuento"] as! String : ""
              
              let newSubtotal = (recibo as? [String: AnyObject])?["Subtotal"] as? String != nil ? (recibo as? [String: AnyObject])?["Subtotal"] as! String : ""
              
              let newReferenceClient = (recibo as? [String: AnyObject])?["ReferenceClient"] as? String != nil ? (recibo as? [String: AnyObject])?["ReferenceClient"] as! String : ""
              
              let newReceptor = (recibo as? [String: AnyObject])?["Receptor"] as? String != nil ? (recibo as? [String: AnyObject])?["Receptor"] as! String : ""
              
              let newRazonSocialReceptor = (recibo as? [String: AnyObject])?["RazonSocialReceptor"] as? String != nil ? (recibo as? [String: AnyObject])?["RazonSocialReceptor"] as! String : ""
              
              let newTotal = (recibo as? [String: AnyObject])?["Total"] as? String != nil ? (recibo as? [String: AnyObject])?["Total"] as! String : ""
              
              let newUid = (recibo as? [String: AnyObject])?["UID"] as? String != nil ? (recibo as? [String: AnyObject])?["UID"] as! String : ""
              
              let newRecibo = Carta.init(newUuid: newUuid,
                                          newStatus: newStatus,
                                          newFolio: newFolio,
                                          newIva: newIva,
                                          newNumOrder: newNumOrder,
                                          newFechaTimbrado: newFechaTimbrado,
                                          newDescuento: newDescuento,
                                          newSubtotal: newSubtotal,
                                          newReferenceClient: newReferenceClient,
                                          newReceptor: newReceptor,
                                          newTotal: newTotal,
                                          newUid: newUid)
              newRecibo.razonSocialReceptor = newRazonSocialReceptor
              
              finalArrayOfRecibos.append(newRecibo)
              
            }
            
            actionsToMakeWhenSucceeded(finalArrayOfRecibos)
            
          } else {
            
            actionsToMakeWhenFailed()
            
          }
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        actionsToMakeWhenFailed()
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
  }
  
  func requestToUpdateCompany(clientUID: String, params: [String: AnyObject], actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/account/\(clientUID)/update"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
      
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 200  {
            
            do {
              
              let json = try JSONSerialization.jsonObject(with: response.data!, options: [])
              print(json)
              
              if json as? [String: AnyObject] != nil {
                
                let jsonFromServer = (json as! [String: AnyObject])
                
                if jsonFromServer["data"] as? [String: AnyObject] != nil {
                  
                  let data = jsonFromServer["data"] as! [String: AnyObject]
                  
                  let newRazonSocial = (data["razon_social"] as? String != nil ? data["razon_social"] as! String : "")
                  let newRegimenFiscal = (data["regimen_fiscal"] as? String != nil ? data["regimen_fiscal"] as! String : "")
                  let newCalle = (data["calle"] as? String != nil ? data["calle"] as! String : "")
                  let newExterior = (data["exterior"] as? String != nil ? data["exterior"] as! String : "")
                  let newInterior = (data["interior"] as? String != nil ? data["interior"] as! String : "")
                  let newColonia = (data["colonia"] as? String != nil ? data["colonia"] as! String : "")
                  let newCodPos = (data["codpos"] as? String != nil ? data["codpos"] as! String : "")
                  let newCiudad = (data["ciudad"] as? String != nil ? data["ciudad"] as! String : "")
                  let newEstado = (data["estado"] as? String != nil ? data["estado"] as! String : "")
                  let newEmail = (data["email"] as? String != nil ? data["email"] as! String : "")
                  let newRFC = (data["rfc"] as? String != nil ? data["rfc"] as! String : "")
                  
                  User.session.actualUsingCompany.regimenFiscal = newRegimenFiscal
                  User.session.actualUsingCompany.calle = newCalle
                  User.session.actualUsingCompany.exterior = newExterior
                  User.session.actualUsingCompany.interior = newInterior
                  User.session.actualUsingCompany.colonia = newColonia
                  User.session.actualUsingCompany.codPos = newCodPos
                  User.session.actualUsingCompany.ciudad = newCiudad
                  User.session.actualUsingCompany.estado = newEstado
                  User.session.actualUsingCompany.email = newEmail
                  User.session.actualUsingCompany.name = newRazonSocial
                  User.session.actualUsingCompany.rfc = newRFC
                  
                  
                  User.session.name = newRazonSocial
                  User.session.rfc = newRFC
                  
                  actionsToMakeWhenSucceeded()
                  
                } else {
                  
                  actionsToMakeWhenFailed()
                  
                }
                
              } else {
                
                actionsToMakeWhenFailed()
                
              }
              
            } catch(_) {
              
              let alertController = UIAlertController(title: "ERROR",
                                                      message: "Error de conexión con el servidor, favor de intentar más tarde",
                                                      preferredStyle: UIAlertControllerStyle.alert)
              
              let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                
                actionsToMakeWhenFailed()
                
              }
              alertController.addAction(cancelAction)
              
              let actualController = UtilityManager.sharedInstance.currentViewController()
              actualController.present(alertController, animated: true, completion: nil)
              
            }
            
          } else {
            
            actionsToMakeWhenFailed()
            
          }
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        actionsToMakeWhenFailed()
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
  }
  
  //PRODUCTS DOESN'T WORK!!
  func getProducts(actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/products"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
              
              print(json)
              
              do {
                
                if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
                  
                  print(json)
                  
                  let rawArrayClients = json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
                  
                  print(rawArrayClients)
                  
                  var arrayExpeditionPlace: Array<ExpeditionPlace> = Array<ExpeditionPlace>()
                  
                  for expeditionPlace in rawArrayClients {
                    
                    let newSucUID = String(expeditionPlace["SucUID"] as? Int != nil ? expeditionPlace["SucUID"] as! Int : -1)
                    let newSucursal = (expeditionPlace["Sucursal"] as? String != nil && expeditionPlace["Sucursal"] as? String != "No Aplica" ? expeditionPlace["Sucursal"] as! String : "")
                    
                    let newExpeditionPlace = ExpeditionPlace(newSucUID: newSucUID, newSucursalName: newSucursal)
                    
                    arrayExpeditionPlace.append(newExpeditionPlace)
                    
                  }
                  
                  actionsToMakeWhenSucceeded()
                  
                }
                
              } catch _{
                
                actionsToMakeWhenFailed()
                
              }
              
            }
            
          } catch _{
            
            actionsToMakeWhenFailed()
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }
  
  func getUnits(actionsToMakeWhenSucceeded: @escaping (_ arrayOfUnits: Array<Unity>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/catalogo/ClaveUnidad"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
              
              print(json)
              
              do {
                
                if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
                  
                  print(json)
                  
                  let rawUnitsArray = json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
                  
                  print(rawUnitsArray)
                  
                  var arrayOfUnits: Array<Unity> = Array<Unity>()
                  
                  for unit in rawUnitsArray {
                    
                    let newId = unit["key"] as? String != nil ? unit["key"] as! String : ""
                    let newName = unit["name"] as? String != nil ? unit["name"] as! String : ""
                    
                    let newUnity = Unity(newId: newId, newName: newName)
                    arrayOfUnits.append(newUnity)
                    
                  }
                  
                  actionsToMakeWhenSucceeded(arrayOfUnits)
                  
                }
                
              } catch _{
                
                actionsToMakeWhenFailed()
                
              }
              
            }
            
          } catch _{
            
            actionsToMakeWhenFailed()
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }
  
  func getPaymentMethods(actionsToMakeWhenSucceeded: @escaping (_ arrayPaymentMethod: Array<PaymentMethod>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/catalogo/MetodoPago"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
              
              print(json)
              
              do {
                
                if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
                  
                  print(json)
                  
                  let rawPaymentMethodArray = json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
                  
                  print(rawPaymentMethodArray)
                  
                  var arrayPaymentMethod: Array<PaymentMethod> = Array<PaymentMethod>()
                  
                  for paymentMethod in rawPaymentMethodArray {
                    
                    let newId = (paymentMethod["key"] as? String != nil ? paymentMethod["key"] as! String : (paymentMethod["key"] as? Int != nil ? String(paymentMethod["key"] as! Int) : "0"))
                    let newMethodName = (paymentMethod["name"] as? String != nil && paymentMethod["name"] as? String != "No Aplica" ? paymentMethod["name"] as! String : "")
                    
                    let newPaymentMethod = PaymentMethod(newId: newId, newName: newMethodName)
                    
                    arrayPaymentMethod.append(newPaymentMethod)
                    
                  }
                  
                  actionsToMakeWhenSucceeded(arrayPaymentMethod)
                  
                }
                
              } catch _{
                
                actionsToMakeWhenFailed()
                
              }
              
            }
            
          } catch _{
            
            actionsToMakeWhenFailed()
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }
  
  func getWayToPay(actionsToMakeWhenSucceeded: @escaping (_ arrayWayToPay: Array<WayToPay>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/catalogo/FormaPago"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
              
              print(json)
              
              do {
                
                if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
                  
                  print(json)
                  
                  let rawWayToPayArray = json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
                  
                  print(rawWayToPayArray)
                  
                  var arrayWayToPay: Array<WayToPay> = Array<WayToPay>()
                  
                  for paymentMethod in rawWayToPayArray {
                    
                    let newId = String(paymentMethod["key"] as? String != nil ? paymentMethod["key"] as! String : "-1")
                    let newWayName = (paymentMethod["name"] as? String != nil && paymentMethod["name"] as? String != "No Aplica" ? paymentMethod["name"] as! String : "")
                    
                    let newWayToPay = WayToPay(newId: newId, newName: newWayName)
                    
                    arrayWayToPay.append(newWayToPay)
                    
                  }
                  
                  actionsToMakeWhenSucceeded(arrayWayToPay)
                  
                }
                
              } catch _{
                
                actionsToMakeWhenFailed()
                
              }
              
            }
            
          } catch _{
            
            actionsToMakeWhenFailed()
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }
  
  func getCurrencies(actionsToMakeWhenSucceeded: @escaping (_ arrayCurrency: Array<Currency>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/catalogo/Moneda"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
              
              print(json)
              
              do {
                
                if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
                  
                  print(json)
                  
                  let rawCurrencyArray = json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
                  
                  print(rawCurrencyArray)
                  
                  var arrayCurrency: Array<Currency> = Array<Currency>()
                  
                  for currency in rawCurrencyArray {
                    
                    let newId = currency["key"] as? String != nil ? currency["key"] as! String : ""
                    let newName = currency["name"] as? String != nil ? currency["name"] as! String : ""
                    
                    let newCurrency = Currency(newId: newId, newName: newName)
                    
                    arrayCurrency.append(newCurrency)
                    
                  }
                  
                  actionsToMakeWhenSucceeded(arrayCurrency)
                  
                }
                
              } catch _{
                
                actionsToMakeWhenFailed()
                
              }
              
            }
            
          } catch _{
            
            actionsToMakeWhenFailed()
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }
  
  func getSeries(actionsToMakeWhenSucceeded: @escaping (_ arrayCurrency: Array<Serie>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/series"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
              
              print(json)
              
              do {
                
                if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
                  
                  print(json)
                  
                  let rawSerieArray = json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
                  
                  print(rawSerieArray)
                  
                  var arraySerie: Array<Serie> = Array<Serie>()
                  
                  for serie in rawSerieArray {
                    
                    let newId = (serie["SerieID"] as? Int != nil ? String(serie["SerieID"] as! Int) : "")
                    let newSerieName = (serie["SerieName"] as? String != nil && serie["SerieName"] as? String != "No Aplica" ? serie["SerieName"] as! String : "")
                    let newSerieType = (serie["SerieType"] as? String != nil && serie["SerieType"] as? String != "No Aplica" ? serie["SerieType"] as! String : "")
                    
                    let newSerie = Serie(newId: newId, newName: newSerieName, newType: newSerieType)
                      
                    arraySerie.append(newSerie)
                    
                  }
                  
                  actionsToMakeWhenSucceeded(arraySerie)
                  
                }
                
              } catch _{
                
                actionsToMakeWhenFailed()
                
              }
              
            }
            
          } catch _{
            
            actionsToMakeWhenFailed()
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }
  
  func getCountries(actionsToMakeWhenSucceeded: @escaping (_ arrayCountry: Array<Country>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/catalogo/Pais"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
              
              print(json)
              
              do {
                
                if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
                  
                  print(json)
                  
                  let rawCountryArray = json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
                  
                  print(rawCountryArray)
                  
                  var arrayCountries: Array<Country> = Array<Country>()
                  
                  for country in rawCountryArray {
                    
                    let newId = (country["key"] as? String != nil ? country["key"] as! String : (country["key"] as? Int != nil ? String(country["key"] as! Int) : "0"))
                    let newCountryName = (country["name"] as? String != nil && country["name"] as? String != "No Aplica" ? country["name"] as! String : "")
                    
                    let newCountry = Country(newId: newId, newName: newCountryName)
                    
                    arrayCountries.append(newCountry)
                    
                  }
                  
                  actionsToMakeWhenSucceeded(arrayCountries)
                  
                }
                
              } catch _{
                
                actionsToMakeWhenFailed()
                
              }
              
            }
            
          } catch _{
            
            actionsToMakeWhenFailed()
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }
  
  func getTypeOfTaxes(actionsToMakeWhenSucceeded: @escaping (_ arrayCFDIUseOptions: Array<TypeOfTax>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/catalogo/Impuesto"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
              
              print(json)
              
              do {
                
                if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
                  
                  print(json)
                  
                  let rawTypeOfTax = json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
                  
                  print(rawTypeOfTax)
                  
                  var arrayTypeOfTax: Array<TypeOfTax> = Array<TypeOfTax>()
                  
                  for typeOfTax in rawTypeOfTax {
                    
                    let newId = (typeOfTax["key"] as? String != nil ? typeOfTax["key"] as! String : (typeOfTax["key"] as? Int != nil ? String(typeOfTax["key"] as! Int) : "0"))
                    let newName = (typeOfTax["name"] as? String != nil && typeOfTax["name"] as? String != "No Aplica" ? typeOfTax["name"] as! String : "")
                    
                    let newTypeOfTax = TypeOfTax.init(newId: newId, newName: newName, newCode: newId)
                    
                    arrayTypeOfTax.append(newTypeOfTax)
                    
                  }
                  
                  actionsToMakeWhenSucceeded(arrayTypeOfTax)
                  
                }
                
              } catch _{
                
                actionsToMakeWhenFailed()
                
              }
              
            }
            
          } catch _{
            
            actionsToMakeWhenFailed()
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }
  
  func getAllCFDIUse(actionsToMakeWhenSucceeded: @escaping (_ arrayCFDIUseOptions: Array<CFDIUseOption>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/catalogo/UsoCfdi"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
              
              print(json)
              
              do {
                
                if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
                  
                  print(json)
                  
                  let rawCFDIUseOptions = json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
                  
                  print(rawCFDIUseOptions)
                  
                  var arrayCFDIUseOptions: Array<CFDIUseOption> = Array<CFDIUseOption>()
                  
                  for cfdiUseOption in rawCFDIUseOptions {
                    
                    let newId = (cfdiUseOption["key"] as? String != nil ? cfdiUseOption["key"] as! String : (cfdiUseOption["key"] as? Int != nil ? String(cfdiUseOption["key"] as! Int) : "0"))
                    let newName = (cfdiUseOption["name"] as? String != nil && cfdiUseOption["name"] as? String != "No Aplica" ? cfdiUseOption["name"] as! String : "")
                    let newUser = (cfdiUseOption["use"] as? String != nil && cfdiUseOption["use"] as? String != "No Aplica" ? cfdiUseOption["use"] as! String : "")
                    
                    let newCFDIUseOption = CFDIUseOption.init(newId: newId, newName: newName, newUse: newUser)
                    
                    arrayCFDIUseOptions.append(newCFDIUseOption)
                    
                  }
                  
                  actionsToMakeWhenSucceeded(arrayCFDIUseOptions)
                  
                }
                
              } catch _{
                
                actionsToMakeWhenFailed()
                
              }
              
            }
            
          } catch _{
            
            actionsToMakeWhenFailed()
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }
  
  func getTypesOfRelationship(actionsToMakeWhenSucceeded: @escaping (_ arrayCFDIUseOptions: Array<TypeOfInvoiceRelationship>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionThree)/catalogo/Relacion"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
              
              print(json)
              
              do {
                
                if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
                  
                  print(json)
                  
                  let rawTypeOfInvoiceRelationship = json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
                  
                  print(rawTypeOfInvoiceRelationship)
                  
                  var arrayTypeOfInvoiceRelationship: Array<TypeOfInvoiceRelationship> = Array<TypeOfInvoiceRelationship>()
                  
                  for typeOfInvoiceRelationship in rawTypeOfInvoiceRelationship {
                    
                    let newId = (typeOfInvoiceRelationship["key"] as? String != nil ? typeOfInvoiceRelationship["key"] as! String : (typeOfInvoiceRelationship["key"] as? Int != nil ? String(typeOfInvoiceRelationship["key"] as! Int) : "0"))
                    let newName = (typeOfInvoiceRelationship["name"] as? String != nil && typeOfInvoiceRelationship["name"] as? String != "No Aplica" ? typeOfInvoiceRelationship["name"] as! String : "")
                    
                    let newCFDIUseOption = TypeOfInvoiceRelationship.init(newId: newId, newName: newName, newCode: newId)
                    
                    arrayTypeOfInvoiceRelationship.append(newCFDIUseOption)
                    
                  }
                  
                  actionsToMakeWhenSucceeded(arrayTypeOfInvoiceRelationship)
                  
                }
                
              } catch _{
                
                actionsToMakeWhenFailed()
                
              }
              
            }
            
          } catch _{
            
            actionsToMakeWhenFailed()
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }
  
  func requestToChangePassword(params: [String: AnyObject], actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionOne)/profile/update/password"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
      
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 200 {
            
            print(response)
            
            actionsToMakeWhenSucceeded()
            
          } else {
            
            actionsToMakeWhenFailed()
            
          }
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        actionsToMakeWhenFailed()
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
  }
  
  func requestToGetAllKindOfTaxes(actionsToMakeWhenSucceeded: @escaping (_ iepsOptions: Array<Option>, _ ishOptions: Array<Option>, _ ivaOptions: Array<Option>, _ retIsrOptions: Array<Option>, _ retIvaOptions: Array<Option>, _ retIvaPorte: Array<Option>) -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    let urlToRequest = "\(typeOfServerVersionTwo)/settings/taxes"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "GET"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    Alamofire.request(requestConnection)
      .validate(statusCode: 200..<400)
      .responseJSON{ response in
        if response.response?.statusCode == 200 {
          
          do {
            
            if let json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? [String:Any] {
              
              print(json)

              let taxes = json["data"] as? [String: AnyObject] != nil ? json["data"] as! [String: AnyObject] : [String: AnyObject]()
                
              var arrayIepsOptions = Array<Option>()
              var arrayIshOptions = Array<Option>()
              var arrayIvaOptions = Array<Option>()
              var arrayRetIsrOptions = Array<Option>()
              var arrayRetIvaOptions = Array<Option>()
              var arrayRetIvaPorteOptions = Array<Option>()

              let ieps = taxes["ieps"] as? Array<[String: AnyObject]> != nil ? taxes["ieps"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
              let ish = taxes["ish"] as? Array<[String: AnyObject]> != nil ? taxes["ish"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
              let iva = taxes["iva"] as? Array<[String: AnyObject]> != nil ? taxes["iva"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
              let retisr = taxes["retisr"] as? Array<[String: AnyObject]> != nil ? taxes["retisr"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
              let retiva = taxes["retiva"] as? Array<[String: AnyObject]> != nil ? taxes["retiva"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
              let retivaporte = taxes["retivaporte"] as? Array<[String: AnyObject]> != nil ? taxes["retivaporte"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>()
              
              for element in ieps {
                  
                let name = element["name"] as? String != nil ? element["name"] as! String : ""
                let value = element["value"] as? String != nil ? element["value"] as! String : ""
                  
                let newOption = Option(id: name, name: name, type: value)
                arrayIepsOptions.append(newOption)
                  
              }
                
              for element in ish {
                  
                let name = element["name"] as? String != nil ? element["name"] as! String : ""
                let value = element["value"] as? String != nil ? element["value"] as! String : ""
                  
                let newOption = Option(id: name, name: name, type: value)
                arrayIshOptions.append(newOption)
                  
              }
                
              for element in iva {
                  
                let name = element["name"] as? String != nil ? element["name"] as! String : ""
                let value = element["value"] as? String != nil ? element["value"] as! String : ""
                  
                let newOption = Option(id: name, name: name, type: value)
                arrayIvaOptions.append(newOption)
                  
              }
                
              for element in retisr {
                  
                let name = element["name"] as? String != nil ? element["name"] as! String : ""
                let value = element["value"] as? String != nil ? element["value"] as! String : ""
                  
                let newOption = Option(id: name, name: name, type: value)
                arrayRetIsrOptions.append(newOption)
                  
              }
                
              for element in retiva {
                  
                let name = element["name"] as? String != nil ? element["name"] as! String : ""
                let value = element["value"] as? String != nil ? element["value"] as! String : ""
                  
                let newOption = Option(id: name, name: name, type: value)
                arrayRetIvaOptions.append(newOption)
                  
              }
                
              for element in retivaporte {
                  
                let name = element["name"] as? String != nil ? element["name"] as! String : ""
                let value = element["value"] as? String != nil ? element["value"] as! String : ""
                  
                let newOption = Option(id: name, name: name, type: value)
                arrayRetIvaPorteOptions.append(newOption)
                  
              }
                
              actionsToMakeWhenSucceeded(arrayIepsOptions, arrayIshOptions, arrayIvaOptions, arrayRetIsrOptions, arrayRetIvaOptions, arrayRetIvaPorteOptions)

            }
            
          } catch _{
            
            actionsToMakeWhenFailed()
            
          }
          
        } else {
          
          actionsToMakeWhenFailed()
          
        }
    }
    
  }

  func requestForForgottenPassword(params: [String: AnyObject], actionsToMakeWhenSucceeded: @escaping () -> Void, actionsToMakeWhenFailed: @escaping () -> Void) {
    
    //  let developmentServer = "http://devfactura.in/api/movil"
    let productionServer  = "https://factura.com/api/movil"
    
    let urlToRequest = "\(productionServer)/resetpwd"
    
    var requestConnection = URLRequest.init(url: NSURL.init(string: urlToRequest)! as URL)
    requestConnection.httpMethod = "POST"
    requestConnection.setValue("application/json", forHTTPHeaderField: "Content-Type")
    requestConnection.setValue(User.session.apiKey, forHTTPHeaderField: "F-API-KEY")
    requestConnection.setValue(User.session.secretKey, forHTTPHeaderField: "F-SECRET-KEY")
    
    do {
      
      let _ = requestConnection.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
      
      Alamofire.request(requestConnection)
        .validate(statusCode: 200..<400)
        .responseJSON{ response in
          if response.response?.statusCode == 200 {
            
            print(response)
            
            actionsToMakeWhenSucceeded()
            
          } else {
            
            actionsToMakeWhenFailed()
            
          }
      }
      
    } catch(_) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        actionsToMakeWhenFailed()
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
  }
  
}
