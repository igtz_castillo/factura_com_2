//
//  CreateGenericCFDIFirstPartViewController.swift
//  FacturaCom
//
//  Created by Israel on 25/09/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class CreateGenericCFDIFirstPartViewController: UIViewController, CustomTextFieldWithTitleViewDelegate, ButtonToActivePickerViewDelegate, ButtonToActivePickerViewCreateNewExpeditionPlaceDelegate, AddCFDIForRelationshipDelegate {
  
  private var genericCFDIDataToCreate: GenericCFDI! = GenericCFDI.init(empty: true)
  
  private var arrayOfButtonsToShowPickerView: [ButtonToActivePickerView]! = [ButtonToActivePickerView]()
  
  //Elements from server
  private let arrayOfCFDIType: Array<TypeOfCFDI>! = [TypeOfCFDI.init(newId: "factura", newName: "Factura", newCode: "factura"),
                                                     TypeOfCFDI.init(newId: "factura_hotel", newName: "Factura para hoteles", newCode: "factura_hotel"),
                                                     TypeOfCFDI.init(newId: "honorarios", newName: "Recibo de honorarios", newCode: "honorarios"),
                                                     TypeOfCFDI.init(newId: "nota_cargo", newName: "Nota de cargo", newCode: "nota_cargo"),
                                                     TypeOfCFDI.init(newId: "donativos", newName: "Donativo", newCode: "donativos"),
                                                     TypeOfCFDI.init(newId: "arrendamiento", newName: "Recibo de arrendamiento", newCode: "arrendamiento"),
                                                     TypeOfCFDI.init(newId: "nota_credito", newName: "Nota de crédito", newCode: "nota_credito"),
                                                     TypeOfCFDI.init(newId: "nota_devolucion", newName: "Nota de devolución", newCode: "nota_devolucion"),
                                                     TypeOfCFDI.init(newId: "carta_porte", newName: "Carta porte", newCode: "carta_porte")]
  
  private var arrayOfExpeditionPlaces: Array<ExpeditionPlace>! = Array<ExpeditionPlace>()
  private var arrayOfStampingDate: Array<Option>! = Array<Option>()
  private var arrayOfClients: Array<Client>! = Array<Client>()
  private var arrayOfCFDIUseOption: Array<CFDIUseOption>! = Array<CFDIUseOption>()
  
  //These are used just when RelatedInvoiceOption is equal to "Sí"
  private var arrayOfTypeOfInvoiceRelationship: Array<TypeOfInvoiceRelationship>! = Array<TypeOfInvoiceRelationship>()
  private var arrayCFDIRelationship: Array<CFDIRelationship>! = Array<CFDIRelationship>()

  
 
  private var mainScrollView: UIScrollView! = nil
  private var generalInfoLabel: LabelSeparator! = nil
  
  private var typeOfCFDIToCreateButton: ButtonToActivePickerView! = nil
  private var expeditionPlaceButton: ButtonToActivePickerView! = nil
  private var selectClientButton: ButtonToActivePickerView! = nil
  private var stampingDateButton: ButtonToActivePickerView! = nil
  private var useOfCFDIButton: ButtonToActivePickerView! = nil
  private var relatedInvoiceButton: ButtonToActivePickerView! = nil
  
  private var typeOfInvoiceRelationshipButton: ButtonToActivePickerView! = nil
  private var cfdiRelationshipButton: ButtonToActivePickerView! = nil
  
  private var lastButtonToShowPickerView: ButtonToActivePickerView! = nil
  
  private var willAppearAfterCreateExpeditinoPlace: Bool = false
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.editNavigationController()
    self.initMainScrollView()
    self.getAllInformationFromServer()
    
  }
  
  private func editNavigationController() {
    
    self.changeBackButtonItem()
    self.changeNavigationBarTitle()
    self.changeNavigationRigthButtonItem()
    
  }
  
  private func changeBackButtonItem() {
    
    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
  }
  
  private func changeNavigationBarTitle() {
    
    self.title = ViewControllersConstants.CreateGenericCFDI.createGenericCFDITitleText
    
    let titleLabel = UILabel.init(frame: CGRect.zero)
    
    let font = UIFont(name: "SFUIText-Regular",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.CreateGenericCFDI.createGenericCFDITitleText,
      attributes:[NSFontAttributeName:font!,
                  NSParagraphStyleAttributeName:style,
                  NSForegroundColorAttributeName:color
      ]
    )
    titleLabel.attributedText = stringWithFormat
    titleLabel.sizeToFit()
    self.navigationItem.titleView = titleLabel
    
  }
  
  private func changeNavigationRigthButtonItem() {
    
    let rightButton = UIBarButtonItem(title: ViewControllersConstants.FacturaCreateViewController.nextButonNavigationBarText,
                                      style: UIBarButtonItemStyle.plain,
                                      target: self,
                                      action: #selector(nextButtonPressed))
    
    let fontForButtonItem =  UIFont(name: "SFUIText-Regular",
                                    size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    
    let attributesDict: [String:AnyObject] = [NSFontAttributeName: fontForButtonItem!,
                                              NSForegroundColorAttributeName: UIColor.orange
    ]
    
    rightButton.setTitleTextAttributes(attributesDict, for: .normal)
    
    self.navigationItem.rightBarButtonItem = rightButton
    
  }
  
  private func getAllInformationFromServer() {
    
    UtilityManager.sharedInstance.showLoader()
    
    ////////// CALL FOR TYPE OF CFDI, after get the info nest all the next code
  
    ServerManager.sharedInstance.getLocations(actionsToMakeWhenSucceeded: { (expeditionPlacesArray) in
      
      self.arrayOfExpeditionPlaces = expeditionPlacesArray
      
      let createNewExpeditionPlaceOption = ExpeditionPlace.init(newSucUID: ButtonToActivePickerView.SpecialOption.createNewExpeditionPlace, newSucursalName: "Agregar sucursal")
      
      self.arrayOfExpeditionPlaces.append(createNewExpeditionPlaceOption)
      
      ServerManager.sharedInstance.clientsByRFC(rfc: nil, actionsToMakeWhenSucceeded: { (clients) in
        
        self.arrayOfClients = clients.sorted{ $0.razonSocial < $1.razonSocial }
        
        ServerManager.sharedInstance.getAllCFDIUse(actionsToMakeWhenSucceeded: { (arrayOfCFDIUse) in
          
          self.arrayOfCFDIUseOption = arrayOfCFDIUse
          
          ServerManager.sharedInstance.getTypesOfRelationship(actionsToMakeWhenSucceeded: { (arrayTypeOfInvoiceRelationship) in
            
            self.arrayOfTypeOfInvoiceRelationship = arrayTypeOfInvoiceRelationship
            
            self.initGeneralInfo()
            self.initTypeOfCFDIToCreateButton()
            self.initExpeditionPlaceButton()
            self.initSelectClientButton()
            self.initStampingDateButton()
            self.initUseOfCFDIButton()
            self.initRelatedInvoiceButton()
            self.initTypeOfInvoiceRelationshipButton()
            self.initCFDIRelationshipButton()
            
            self.addGestureToHideOptionsOfButtons()
            
            UtilityManager.sharedInstance.hideLoader()

            
          }, actionsToMakeWhenFailed: {
            
            UtilityManager.sharedInstance.hideLoader()
            
          })
          
        }, actionsToMakeWhenFailed: {
          
          UtilityManager.sharedInstance.hideLoader()
          
        })

      }, actionsToMakeWhenFailed: {
                
        UtilityManager.sharedInstance.hideLoader()
                
      })
              
    }, actionsToMakeWhenFailed: {
              
      UtilityManager.sharedInstance.hideLoader()
              
    })
    
  }
  
  private func addGestureToHideOptionsOfButtons() {
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(hideOptionsAndKeyboard))
    tapGesture.numberOfTapsRequired = 1
    
    self.view.addGestureRecognizer(tapGesture)
    
  }
  
  private func initMainScrollView() {
    
    mainScrollView = UIScrollView.init(frame: self.view.frame)
    
    if UtilityManager.sharedInstance.isIpad() == false {
      
      let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                       height: mainScrollView.frame.size.height + (95.0 * UtilityManager.sharedInstance.conversionHeight))
      
      mainScrollView.contentSize = newContentSize
      
    }
    
    self.view.addSubview(mainScrollView)
    
  }
  
  private func initGeneralInfo() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: mainScrollView.frame.size.width,
                                    height: 56.0 * UtilityManager.sharedInstance.conversionHeight)
    
    generalInfoLabel = LabelSeparator.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateViewController.generalInfoLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    generalInfoLabel.attributedText = stringWithFormat
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: generalInfoLabel.frame.size.width,
                               height: generalInfoLabel.frame.size.height)
    generalInfoLabel.frame = newFrame
    generalInfoLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(generalInfoLabel)
    
  }
  
  private func initTypeOfCFDIToCreateButton() {
    
    if typeOfCFDIToCreateButton != nil {
      
      typeOfCFDIToCreateButton.removeFromSuperview()
      typeOfCFDIToCreateButton = nil
      
    }
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: generalInfoLabel.frame.origin.y + generalInfoLabel.frame.size.height,
                                     width: UIScreen.main.bounds.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var arrayOfOptions: [Option] = [Option]()
    
    for cdfiType in arrayOfCFDIType {
      
      let newOption = Option(id: cdfiType.id, name: cdfiType.name, type: cdfiType.code)
      arrayOfOptions.append(newOption)
      
    }
    
    typeOfCFDIToCreateButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                             newTitle: ViewControllersConstants.FacturaCreateViewController.typeOfCFDIButtonText,
                                                             newArrayOfOptions: arrayOfOptions)
    typeOfCFDIToCreateButton.drawBottomBorder()
    typeOfCFDIToCreateButton.delegate = self
    typeOfCFDIToCreateButton.delegateForCreateNewExpeditionPlace = self
    _ = typeOfCFDIToCreateButton.selectElementWithName(nameToLookFor: "Factura")
    
    self.mainScrollView.addSubview(typeOfCFDIToCreateButton)
    self.arrayOfButtonsToShowPickerView.append(typeOfCFDIToCreateButton)
    
  }
  
  private func initExpeditionPlaceButton() {
    
    if expeditionPlaceButton != nil {
      
      expeditionPlaceButton.removeFromSuperview()
      expeditionPlaceButton = nil
      
    }
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: typeOfCFDIToCreateButton.frame.origin.y + typeOfCFDIToCreateButton.frame.size.height,
                                     width: UIScreen.main.bounds.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var arrayOfOptions: [Option] = [Option]()
    
    for place in arrayOfExpeditionPlaces {
      
      let newOption = Option(id: place.sucUID, name: place.sucursalName, type: "")
      arrayOfOptions.append(newOption)
      
    }
    
    expeditionPlaceButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                          newTitle: ViewControllersConstants.FacturaCreateViewController.expeditionPlaceButtontext,
                                                          newArrayOfOptions: arrayOfOptions)
    expeditionPlaceButton.drawBottomBorder()
    expeditionPlaceButton.delegate = self
    expeditionPlaceButton.delegateForCreateNewExpeditionPlace = self
    _ = expeditionPlaceButton.selectElementWithName(nameToLookFor: "Principal")
    
    self.mainScrollView.addSubview(expeditionPlaceButton)
    self.arrayOfButtonsToShowPickerView.append(expeditionPlaceButton)
    
  }
  
  private func initSelectClientButton() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: expeditionPlaceButton.frame.origin.y + expeditionPlaceButton.frame.size.height,
                                     width: UIScreen.main.bounds.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var arrayOfOptions: [Option] = [Option]()
    
    for client in arrayOfClients {
      
      let newOption = Option(id: client.uid , name: client.razonSocial, type: client.rfc)
      arrayOfOptions.append(newOption)
      
    }
    
    selectClientButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                       newTitle: ViewControllersConstants.FacturaCreateViewController.selectClientButtonText,
                                                       newArrayOfOptions: arrayOfOptions,
                                                       withSearchTextField: true)
    selectClientButton.delegate = self
    selectClientButton.drawBottomBorder()
    
    self.mainScrollView.addSubview(selectClientButton)
    self.arrayOfButtonsToShowPickerView.append(selectClientButton)
    
  }
  
  private func initStampingDateButton() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: selectClientButton.frame.origin.y + selectClientButton.frame.size.height,
                                     width: UIScreen.main.bounds.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let formatString = "dd MMMM yyyy"
    let formatToServerString = "yyyy-MM-dd'T'HH:mm:ss"
    let beforeYesterdayString = Date().beforeYesterdayStringFormat(format: formatString)
    let yesterdayString = Date().yesterdayStringFormat(format: formatString)
    let todayString = Date().todayStringFormat(format: formatString)
    let beforeYesterdayServerString = Date().beforeYesterdayStringFormat(format: formatToServerString)
    let yesterdayServerString = Date().yesterdayStringFormat(format: formatToServerString)
    let todayServerString = Date().todayStringFormat(format: formatToServerString)
    let beforeYesterdayOption: Option = Option(id: beforeYesterdayServerString, name: beforeYesterdayString, type: beforeYesterdayString)
    let yesterdayOption: Option = Option(id: yesterdayServerString, name: yesterdayString, type: yesterdayString)
    let todayOption: Option = Option(id: todayServerString, name: todayString, type: todayString)
    
    
    arrayOfStampingDate = [todayOption, yesterdayOption, beforeYesterdayOption]
    
    stampingDateButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                       newTitle: ViewControllersConstants.CreateGenericCFDI.stampingDateButtonTitleText,
                                                       newArrayOfOptions: arrayOfStampingDate)
    stampingDateButton.delegate = self
    stampingDateButton.drawBottomBorder()
    
    self.mainScrollView.addSubview(stampingDateButton)
    self.arrayOfButtonsToShowPickerView.append(stampingDateButton)
    
  }
  
  
  private func initUseOfCFDIButton() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: stampingDateButton.frame.origin.y + stampingDateButton.frame.size.height,
                                     width: UIScreen.main.bounds.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var arrayOfOptions: [Option] = [Option]()
    
    for element in arrayOfCFDIUseOption {
      
      let newOption = Option(id: element.id , name: element.name, type: element.use)
      arrayOfOptions.append(newOption)
      
    }
    
    useOfCFDIButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                       newTitle: ViewControllersConstants.CreateGenericCFDI.useOfCFDIButtonTitleText,
                                                       newArrayOfOptions: arrayOfOptions)
    useOfCFDIButton.delegate = self
    useOfCFDIButton.drawBottomBorder()
    
    self.mainScrollView.addSubview(useOfCFDIButton)
    self.arrayOfButtonsToShowPickerView.append(useOfCFDIButton)
    
  }
  
  private func initRelatedInvoiceButton() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: useOfCFDIButton.frame.origin.y + useOfCFDIButton.frame.size.height,
                                     width: UIScreen.main.bounds.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let arrayOfOptions: [Option] = [Option(id: "01" , name: "Sí", type: "01"),
                                    Option(id: "02", name: "No", type: "02")]
    
    relatedInvoiceButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                    newTitle: ViewControllersConstants.CreateGenericCFDI.relatedInvoiceButtonTitleText,
                                                    newArrayOfOptions: arrayOfOptions)
    relatedInvoiceButton.delegate = self
    relatedInvoiceButton.drawBottomBorder()
    
    self.mainScrollView.addSubview(relatedInvoiceButton)
    self.arrayOfButtonsToShowPickerView.append(relatedInvoiceButton)
    
  }
  
  private func initTypeOfInvoiceRelationshipButton() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: relatedInvoiceButton.frame.origin.y + relatedInvoiceButton.frame.size.height,
                                     width: UIScreen.main.bounds.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var arrayOfOptions: [Option] = [Option]()
    
    for element in arrayOfTypeOfInvoiceRelationship {
      
      let newOption = Option(id: element.id , name: element.name, type: element.code)
      arrayOfOptions.append(newOption)
      
    }
    
    typeOfInvoiceRelationshipButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                         newTitle: ViewControllersConstants.CreateGenericCFDI.typeOfInvoiceRelationshipButtonTitleText,
                                                         newArrayOfOptions: arrayOfOptions)
    typeOfInvoiceRelationshipButton.delegate = self
    typeOfInvoiceRelationshipButton.drawBottomBorder()
    
    typeOfInvoiceRelationshipButton.alpha = 0.0
    
    self.mainScrollView.addSubview(typeOfInvoiceRelationshipButton)
    self.arrayOfButtonsToShowPickerView.append(typeOfInvoiceRelationshipButton)
    
  }
  
  private func initCFDIRelationshipButton() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: typeOfInvoiceRelationshipButton.frame.origin.y + typeOfInvoiceRelationshipButton.frame.size.height,
                                     width: UIScreen.main.bounds.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var arrayOfOptions: [Option] = [Option]()
    
    for element in self.arrayCFDIRelationship {
      
      let newOption = Option(id: element.id , name: element.name, type: element.code)
      arrayOfOptions.append(newOption)
      
    }
    
    cfdiRelationshipButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                                    newTitle: ViewControllersConstants.CreateGenericCFDI.typeOfCFDIRelationshipButtonTitleText,
                                                                    newArrayOfOptions: arrayOfOptions)
    cfdiRelationshipButton.delegate = self
    cfdiRelationshipButton.drawBottomBorder()
    
    cfdiRelationshipButton.alpha = 0.0
    
    self.mainScrollView.addSubview(cfdiRelationshipButton)
    self.arrayOfButtonsToShowPickerView.append(cfdiRelationshipButton)
    
  }
  
  private func hideOptions() {
    
    self.hideTypeOfInvoiceRelationshipButton()
//    self.hideTypeOfCFDIRelationshipButton()
    
  }
  
  private func showOptions() {
    
    self.showTypeOfInvoiceRelationshipButton()
//    self.showTypeOfCFDIRelationshipButton()
    
  }
  
  private func hideTypeOfInvoiceRelationshipButton() {
    
    UIView.animate(withDuration: 0.35, animations: {
      
      self.typeOfInvoiceRelationshipButton.alpha = 0.0
      
    }, completion: { (finished) in
      
      
      
    })
    
  }
  
  private func showTypeOfInvoiceRelationshipButton() {
    
    UIView.animate(withDuration: 0.35, animations: {
      
      self.typeOfInvoiceRelationshipButton.alpha = 1.0
      
    }, completion: { (finished) in
      
      
      
    })
    
  }
  
  private func hideTypeOfCFDIRelationshipButton() {
    
    UIView.animate(withDuration: 0.35, animations: {
    
      self.cfdiRelationshipButton.alpha = 0.0
    
    }, completion: { (finished) in
      
    
    
    })
    
  }
  
  private func showTypeOfCFDIRelationshipButton() {
    
    UIView.animate(withDuration: 0.35, animations: {
      
      self.cfdiRelationshipButton.alpha = 1.0
      
    }, completion: { (finished) in
      
      
      
    })
    
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    
    super.viewWillDisappear(animated)
    
    
    if typeOfCFDIToCreateButton != nil {
      
      typeOfCFDIToCreateButton.hidePickerView()
      
    }
    
    if expeditionPlaceButton != nil {
      
      expeditionPlaceButton.hidePickerView()
      
    }
    
    if selectClientButton != nil {
      
      selectClientButton.hidePickerView()
      
    }
    
    if stampingDateButton != nil {
      
      stampingDateButton.hidePickerView()
      
    }
    
    if useOfCFDIButton != nil {
      
      useOfCFDIButton.hidePickerView()
      
    }
    
    if relatedInvoiceButton != nil {
      
      relatedInvoiceButton.hidePickerView()
      
    }
    
    if typeOfInvoiceRelationshipButton != nil {
      
      typeOfInvoiceRelationshipButton.hidePickerView()
      
    }
    
    if cfdiRelationshipButton != nil {
      
      cfdiRelationshipButton.hidePickerView()
      
    }
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(true)
    
    if willAppearAfterCreateExpeditinoPlace == true {
      
      UtilityManager.sharedInstance.showLoader()
      
      ServerManager.sharedInstance.getLocations(actionsToMakeWhenSucceeded: { (expeditionPlacesArray) in
        
        self.arrayOfExpeditionPlaces = expeditionPlacesArray
        
        let createNewExpeditionPlaceOption = ExpeditionPlace.init(newSucUID: ButtonToActivePickerView.SpecialOption.createNewExpeditionPlace, newSucursalName: "Agregar sucursal")
        
        self.arrayOfExpeditionPlaces.append(createNewExpeditionPlaceOption)
        
        self.initExpeditionPlaceButton()
        
        self.willAppearAfterCreateExpeditinoPlace = false
        
        UtilityManager.sharedInstance.hideLoader()
        
      }, actionsToMakeWhenFailed: {
        
        self.willAppearAfterCreateExpeditinoPlace = false
        UtilityManager.sharedInstance.hideLoader()
        
      })
      
    }
    
  }
  
  func lastButtonToShowPickerView(sender: ButtonToActivePickerView) {
    
    self.hideKeyboard()
    
    lastButtonToShowPickerView = sender
    
    for button in arrayOfButtonsToShowPickerView {
      
      if button != lastButtonToShowPickerView {
        
        button.hidePickerView()
        
      }
      
    }
    
  }
  
  func createNewExpeditionPlace(sender: ButtonToActivePickerView) {
    
    if sender == self.expeditionPlaceButton {
      
      self.expeditionPlaceButton.hidePickerView()
      
      let createExpeditionScreen = CreateExpeditionPlaceViewController()
      self.navigationController?.pushViewController(createExpeditionScreen, animated: true)
      self.willAppearAfterCreateExpeditinoPlace = true
      
    }
    
  }
  
  
  func selectActionFinalized(sender: ButtonToActivePickerView) {
    
    if sender == self.relatedInvoiceButton {
      
      if let valueSelected = self.relatedInvoiceButton.getValueSelected() {
        
        if valueSelected.name == "Sí" {
          
          UtilityManager.sharedInstance.showLoader()
          
          ServerManager.sharedInstance.getAllVersion33CFDI(actionsToMakeWhenSucceeded: { (all33CFDIVersion) in
            
            self.showOptions()
            
            let addRelationshipVC = AddCFDIForRelationshipViewController.init(style: .plain, newArrayOfCFDIVersionThree: all33CFDIVersion)
            addRelationshipVC.delegateToAddRelationship = self
            
            self.navigationController?.pushViewController(addRelationshipVC, animated: true)
            
            UtilityManager.sharedInstance.hideLoader()
            
          }, actionsToMakeWhenFailed: {
            
            UtilityManager.sharedInstance.hideLoader()
            
          })
          
        } else {
          
          if genericCFDIDataToCreate.arrayOfCFDIWithRelationship.count > 0 {
            
            let alert = UIAlertController.init(title: "Aviso", message: "Todos los CFDI's con los que se guarda relación serán eliminados, ¿deseas proseguir?", preferredStyle: .alert)
            
            let okAction = UIAlertAction.init(title: "Sí", style: .default, handler: { (action) in
              
              self.genericCFDIDataToCreate.arrayOfCFDIWithRelationship.removeAll()
              self.genericCFDIDataToCreate.cfdiRelationship = nil
              self.hideOptions()
              
            })
            
            let cancelAction = UIAlertAction.init(title: "Cancelar", style: .cancel, handler: { (action) in})
            
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
            
          }

        }
        
      }
      
    } else
    
    if sender == self.typeOfInvoiceRelationshipButton {
    
//      if let optionSelectedFromTypeInvoiceRelationship = self.typeOfInvoiceRelationshipButton.getValueSelected() {
//
//       //I assume that I have to do a request to server BUT I will ask to Fco
//
//
//
//      }
      
    }
    
  }

  
  //MARK: CustomTextFieldWithTitleViewDelegate
  
  func customTextFieldSelected(sender: CustomTextFieldWithTitleView) {
    
    self.hideAllOptionsOfButtons()
    
  }
  
  private func hideAllOptionsOfButtons() {
    
    for button in arrayOfButtonsToShowPickerView {
      
      button.hidePickerView()
      
    }
    
  }
  
  @objc private func hideOptionsAndKeyboard() {
    
    self.hideAllOptionsOfButtons()
    self.hideKeyboard()
    
  }
  
  private func hideKeyboard() {
    
    self.view.endEditing(true)
    
  }
  
  
  
  @objc private func backButtonPressed() {
    
    self.hideOptionsAndKeyboard()
    
    _ = self.navigationController?.popViewController(animated: true)
    
  }
  
  @objc private func nextButtonPressed() {
    
    self.hideOptionsAndKeyboard()
    
    self.getSelectedData()
    
    //    let secondPartScreen = FacturaCreateSecondPartViewController.init(facturaToCreate: facturaDataToCreate)
    
    //    _ = self.navigationController?.pushViewController(secondPartScreen, animated: true)
    
  }
  
  private func getSelectedData() {
    
    //THIS WILL BE UNCOMMENTED
    
    if self.getTypeOfCFDISelected() != true {
      
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor elige un tipo de CFDI a crear",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    } else
    
    if self.getExpeditionPlaceSelected() != true {
     
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor elige un lugar de expedición",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
        
    } else
    
    if self.getClientSelected() != true {
    
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor elige un cliente",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    } else
      
    if self.getStampingDateSelected() != true {
      
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor elige una fecha de timbrado",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
        
    } else
    
    if self.getUseOfCFDIButtonSelected() != true {
    
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor elige un uso de CFDI",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
        
    } else
    
    if self.getRelatedInvoiceButtonSelected() == true {
      
      if self.relatedInvoiceButton.getValueSelected()?.name == "Sí" {
       
        if self.getTypeOfInvoiceRelationshipButtonSelected() != true {
          
          let alertController = UIAlertController(title: "Error",
                                                  message: "Por favor elige un tipo de relación",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
        } else {
          
          let createGenericCDFISecondPart = CreateGenericCFDISecondPartViewController(newGenericCFDIDataToCreate: genericCFDIDataToCreate)
          self.navigationController?.pushViewController(createGenericCDFISecondPart, animated: true)
          
        }
        
      } else {
        
        let createGenericCDFISecondPart = CreateGenericCFDISecondPartViewController(newGenericCFDIDataToCreate: genericCFDIDataToCreate)
        self.navigationController?.pushViewController(createGenericCDFISecondPart, animated: true)
        
      }
        
    } else {
      
      let createGenericCDFISecondPart = CreateGenericCFDISecondPartViewController(newGenericCFDIDataToCreate: genericCFDIDataToCreate)
      self.navigationController?.pushViewController(createGenericCDFISecondPart, animated: true)
      
      
    }
//
//    if self.getClientSelected() != true {
//      
//      let alertController = UIAlertController(title: "Error",
//                                              message: "Por favor elige un cliente",
//                                              preferredStyle: UIAlertControllerStyle.alert)
//      
//      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
//      alertController.addAction(cancelAction)
//      
//      let actualController = UtilityManager.sharedInstance.currentViewController()
//      actualController.present(alertController, animated: true, completion: nil)
//      
//    }
//    
//    if self.getStampingDateSelected() != true {
//      
//      let alertController = UIAlertController(title: "Error",
//                                              message: "Por favor elige una fecha de timbrado",
//                                              preferredStyle: UIAlertControllerStyle.alert)
//      
//      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
//      alertController.addAction(cancelAction)
//      
//      let actualController = UtilityManager.sharedInstance.currentViewController()
//      actualController.present(alertController, animated: true, completion: nil)
//      
//    }
//    
//    if self.getUseOfCFDIButtonSelected() != true {
//      
//      let alertController = UIAlertController(title: "Error",
//                                              message: "Por favor elige un uso de CFDI",
//                                              preferredStyle: UIAlertControllerStyle.alert)
//      
//      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
//      alertController.addAction(cancelAction)
//      
//      let actualController = UtilityManager.sharedInstance.currentViewController()
//      actualController.present(alertController, animated: true, completion: nil)
//      
//    }
//    
//    if self.getTypeOfInvoiceRelationshipButtonSelected() != true {
//      
//      let alertController = UIAlertController(title: "Error",
//                                              message: "Por favor elige un tipo de relación",
//                                              preferredStyle: UIAlertControllerStyle.alert)
//      
//      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
//      alertController.addAction(cancelAction)
//      
//      let actualController = UtilityManager.sharedInstance.currentViewController()
//      actualController.present(alertController, animated: true, completion: nil)
//      
//    }
//    
//    if self.getTypeOfCFDIRelationshipButtonButtonSelected() != true {
//      
//      let alertController = UIAlertController(title: "Error",
//                                              message: "Por favor elige un tipo de CFDI relacionados",
//                                              preferredStyle: UIAlertControllerStyle.alert)
//      
//      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
//      alertController.addAction(cancelAction)
//      
//      let actualController = UtilityManager.sharedInstance.currentViewController()
//      actualController.present(alertController, animated: true, completion: nil)
//      
//    }
    
    
    

    

    
  }
  
  private func getTypeOfCFDISelected() -> Bool {
    
    let typeOfCFDISelected = typeOfCFDIToCreateButton.getValueSelected()
    
    if typeOfCFDISelected != nil {
      
      for typeOfCFDI in arrayOfCFDIType {
        
        if typeOfCFDISelected!.id == typeOfCFDI.id {
          
          genericCFDIDataToCreate.typeOfCFDI = typeOfCFDI
          return true
          
        }
        
      }
      
      //supposedly never happen
      return false
      
    } else {
      
      return false
      
    }
    
  }
  
  //Return true if found a value
  private func getExpeditionPlaceSelected() -> Bool {
    
    let expeditionOptionSelected = expeditionPlaceButton.getValueSelected()
    
    if expeditionOptionSelected != nil {
      
      for expedition in arrayOfExpeditionPlaces {
        
        if expeditionOptionSelected!.id == expedition.sucUID {
          
          genericCFDIDataToCreate.expeditionPlace = expedition
          return true
          
        }
        
      }
      
      //supposedly never happen
      return false
      
    } else {
      
      return false
      
    }
    
  }
  
  //Return true if found a value
  private func getClientSelected() -> Bool {
    
    let clientOptionSelected = selectClientButton.getValueSelected()
    
    if clientOptionSelected != nil {
      
      for client in arrayOfClients {
        
        if clientOptionSelected!.id == client.uid {
          
          genericCFDIDataToCreate.client = client
          return true
          
        }
        
      }
      
      //supposedly never happen
      return false
      
    } else {
      
      return false
      
    }
    
  }

  private func getStampingDateSelected() -> Bool {
    
    let stampingDateSelected = stampingDateButton.getValueSelected()
    
    if stampingDateSelected != nil {
      
      for stampingDate in arrayOfStampingDate {
        
        if stampingDateSelected!.id == stampingDate.id {
          
          genericCFDIDataToCreate.stampingDate = stampingDate
          return true
          
        }
        
      }
      
      //supposedly never happen
      return false
      
    } else {
      
      return false
      
    }
    
  }
  
  private func getUseOfCFDIButtonSelected() -> Bool {
    
    let useOfCFDISelected = useOfCFDIButton.getValueSelected()
    
    if useOfCFDISelected != nil {
      
      for cfdiUseOption in arrayOfCFDIUseOption {
        
        if useOfCFDISelected!.id == cfdiUseOption.id {
          
          genericCFDIDataToCreate.cfdiUseOption = cfdiUseOption
          return true
          
        }
        
      }
      
      //supposedly never happen
      return false
      
    } else {
      
      return false
      
    }
    
  }
  
  private func getRelatedInvoiceButtonSelected() -> Bool {
    
    let relatedInvoiceSelected = relatedInvoiceButton.getValueSelected()
    
    if relatedInvoiceSelected != nil {
      
      return true
        
    } else {
      
      return false
      
    }
    
  }
  
  private func getTypeOfInvoiceRelationshipButtonSelected() -> Bool {
    
    let typeOfInvoiceRelationshipSelected = typeOfInvoiceRelationshipButton.getValueSelected()
    
    if typeOfInvoiceRelationshipSelected != nil {
      
      for typeOfInvoiceRelationship in arrayOfTypeOfInvoiceRelationship {
        
        if typeOfInvoiceRelationshipSelected!.id == typeOfInvoiceRelationship.id {
          
          genericCFDIDataToCreate.typeOfInvoiceRelationship = typeOfInvoiceRelationship
          return true
          
        }
        
      }
      
      //supposedly never happen
      return false
      
    } else {
      
      return false
      
    }
    
  }
  
  private func getTypeOfCFDIRelationshipButtonButtonSelected() -> Bool {
    
    let typeOfCFDIRelationshipSelected = cfdiRelationshipButton.getValueSelected()
    
    if typeOfCFDIRelationshipSelected != nil {
      
      for typeOfCFDIRelationship in arrayCFDIRelationship {
        
        if typeOfCFDIRelationshipSelected!.id == typeOfCFDIRelationship.id {
          
          genericCFDIDataToCreate.cfdiRelationship = typeOfCFDIRelationship
          return true
          
        }
        
      }
      
      //supposedly never happen
      return false
      
    } else {
      
      return false
      
    }
    
  }

  func addCFDIToRelationship(_ cdfiToAddToRelationship: CFDIVersionThree) {
    
    genericCFDIDataToCreate.arrayOfCFDIWithRelationship.append(cdfiToAddToRelationship)
    
  }
  
  func noCFDIsToRelationshipSelected() {
    
    self.hideOptions()
    self.relatedInvoiceButton.selectElementWithName(nameToLookFor: "No")
    
  }
  
}
