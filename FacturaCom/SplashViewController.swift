//
//  SplashViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 24/03/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
  
  private var mainTabBarController: MainTabBarController! = nil
    
  override func loadView() {
        
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.white //UIColor.init(patternImage: UIImage.init(named: "fondo_login")!)
    
    var backgroundImageName = "fondo_nuevo"
    
    if UtilityManager.sharedInstance.isIpad() == true {
      
      backgroundImageName = "iPad_Login_img"
      
    }
    
    let imageViewBackground = UIImageView.init(image: UIImage.init(named: backgroundImageName)!)
    imageViewBackground.contentMode = .scaleToFill
    imageViewBackground.frame = self.view.frame
    self.view.addSubview(imageViewBackground)
    
    self.editNavigationBar()
    
  }
  
  private func editNavigationBar() {
    
    self.navigationController?.isNavigationBarHidden = true
    
  }
  
  override func viewDidLoad() {
    
    let lastValidEmail = UserDefaults.standard.string(forKey: UtilityManager.sharedInstance.kLastValidUserEmail)
    let lastValidPassword = UserDefaults.standard.string(forKey: UtilityManager.sharedInstance.kLastValidUserPassword)
    
    if lastValidEmail != nil && lastValidPassword != nil {
      
      UtilityManager.sharedInstance.showLoader()
      
      ServerManager.sharedInstance.requestToLogin(mail: lastValidEmail!,
        password: lastValidPassword!,
        actionsToMakeWhenSucceeded: { (json) in
          
          self.changeValuesOfCompany(json: json)
          
          UtilityManager.sharedInstance.hideLoader()
          
          self.initAndChangeRootToMainTabBarController()

                                                
        }, actionsToMakeWhenFailed: {
      
          UtilityManager.sharedInstance.hideLoader()
          
          let loginScreen = LoginViewController()
          self.navigationController?.pushViewController(loginScreen, animated: true)
        
        })
      
    } else {
      
      let loginScreen = LoginViewController()
      self.navigationController?.pushViewController(loginScreen, animated: true)
      
    }
    
  }
  
  private func changeValuesOfCompany(json: [String: AnyObject]) {
    
    let arrayOfCompanies = (json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>())
    
    if arrayOfCompanies.count > 0 {
      
      var companies = Array<Company>()
      
      for company in arrayOfCompanies {
        
        let uidAccount: String = (company["cuenta_uid"] as? String != nil ? company["cuenta_uid"] as! String : "")
        let urlLogo: String? = (company["logo_full"] as? String != nil ? company["logo_full"] as? String : nil)
        let name: String = (company["name"] as? String != nil ? company["name"] as! String : "")
        let rfc: String = (company["rfc"] as? String != nil ? company["rfc"] as! String : "")
        let apiKey: String = (company["apikey"] as? String != nil ? company["apikey"] as! String : "")
        let secretKey: String = (company["secretkey"] as? String != nil ? company["secretkey"] as! String : "")
        
        let newCompany = Company.init()
        
        newCompany.uidAccount = uidAccount
        newCompany.urlLogo = urlLogo
        newCompany.name = name
        newCompany.rfc = rfc
        newCompany.apiKey = apiKey
        newCompany.secretKey = secretKey
        
        companies.append(newCompany)
        
      }
      
      User.session.arrayOfCompanies = companies
      
      let companySelectedRawData = arrayOfCompanies[0]
      let uidAccount: String = (companySelectedRawData["cuenta_uid"] as? String != nil ? companySelectedRawData["cuenta_uid"] as! String : "")
      let urlLogo: String? = (companySelectedRawData["logo_full"] as? String != nil ? companySelectedRawData["logo_full"] as? String : nil)
      let name: String = (companySelectedRawData["name"] as? String != nil ? companySelectedRawData["name"] as! String : "")
      let rfc: String = (companySelectedRawData["rfc"] as? String != nil ? companySelectedRawData["rfc"] as! String : "")
      let apiKey: String = (companySelectedRawData["apikey"] as? String != nil ? companySelectedRawData["apikey"] as! String : "")
      let secretKey: String = (companySelectedRawData["secretkey"] as? String != nil ? companySelectedRawData["secretkey"] as! String : "")
      
      User.session.uidAccount = uidAccount
      User.session.urlLogo = urlLogo
      User.session.name = name
      User.session.rfc = rfc
      User.session.apiKey = apiKey
      User.session.secretKey = secretKey
      
      User.session.actualUsingCompany.uidAccount = uidAccount
      User.session.actualUsingCompany.urlLogo = urlLogo
      User.session.actualUsingCompany.name = name
      User.session.actualUsingCompany.rfc = rfc
      User.session.actualUsingCompany.apiKey = apiKey
      User.session.actualUsingCompany.secretKey = secretKey
      
    }
    
    let userInfo = (json["profile"] as? [String: AnyObject] != nil ? json["profile"] as! [String: AnyObject] : [String: AnyObject]())
    if userInfo.count > 0 {
      
      let newEmail: String = (userInfo["email"] as? String != nil ? userInfo["email"] as! String : "")
      let newImageURL: String = (userInfo["image"] as? String != nil ? userInfo["image"] as! String : "")
      let newName: String = (userInfo["name"] as? String != nil ? userInfo["name"] as! String : "")
      
      if newEmail != "" {
        
        User.session.email = newEmail
        
      }
      if newImageURL != "" {
        
        User.session.urlLogo = newImageURL
        
      }
      if newName != "" {
        
        User.session.name = newName
        
      }
      
    }
    
  }
  
  private func initAndChangeRootToMainTabBarController() {
    
    mainTabBarController = MainTabBarController()
    mainTabBarController.tabBar.barTintColor = UtilityManager.sharedInstance.backgroundColorForTabBar
    mainTabBarController.tabBar.isTranslucent = false
    mainTabBarController.tabBar.tintColor = UIColor.white
    
    var arrayOfViewControllers = [UINavigationController]()
    arrayOfViewControllers.append(self.createFirstBarItem())
    arrayOfViewControllers.append(self.createSecondBarItem())
//    arrayOfViewControllers.append(self.createThirdBarItem())
    arrayOfViewControllers.append(self.createFourthBarItem())
    arrayOfViewControllers.append(self.createFifthBarItem())
    
    mainTabBarController.viewControllers = arrayOfViewControllers
    mainTabBarController.selectedIndex = 0
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    UIView.transition(with: appDelegate.window!,
                      duration: 0.25,
                      options: UIViewAnimationOptions.transitionCrossDissolve,
                      animations: {
                        self.view.alpha = 0.0
                        appDelegate.window?.rootViewController = self.mainTabBarController
                        appDelegate.window?.makeKeyAndVisible()
    }, completion: nil)
    
  }
  
  //MARK: - TabController
  
  //MARK: - CFDI's ViewController
  private func createFirstBarItem() -> UINavigationController {
    
    //    let imageFacturasNonSelected = UIImage.init(named: "Tab_1")?.withRenderingMode(.alwaysOriginal)
    //    let imageFacturasSelected = UIImage.init(named: "Tab_1_on_Dark_ON")?.withRenderingMode(.alwaysOriginal)
    
    let cfdisTableViewController =  AllCFDIsTableViewController.init(style: .plain, newArrayOfCFDIVersionThree: Array<CFDIVersionThree>())
    
    let tabOneBarItem = UITabBarItem.init(title: ViewControllersConstants.MainTabController.cfdisButtonBarItemText,
                                          image: nil,
                                          selectedImage: nil)
    
    let newNavController = UINavigationController.init(rootViewController: cfdisTableViewController)
    newNavController.navigationBar.barTintColor = UtilityManager.sharedInstance.backGroundColorApp
    
    tabOneBarItem.tag = 1
    tabOneBarItem.imageInsets = UIEdgeInsets.init(top: tabOneBarItem.imageInsets.top + (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                  left: tabOneBarItem.imageInsets.left,
                                                  bottom: tabOneBarItem.imageInsets.bottom - (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                  right: tabOneBarItem.imageInsets.right)
    newNavController.tabBarItem = tabOneBarItem
    newNavController.navigationBar.barStyle = .black
    
    return newNavController
    
  }
  
  //MARK: - Clientes ViewController
  private func createSecondBarItem() -> UINavigationController {
    
    //    let imageRecibosNonSelected = UIImage.init(named: "Tab_2")?.withRenderingMode(.alwaysOriginal)
    //    let imageRecibosSelected = UIImage.init(named: "Tab_2_on_Dark_ON")?.withRenderingMode(.alwaysOriginal)
    
    let listOfClientsScreen = ClientsTableViewController.init(style: .plain, newArrayOfElements: Array<Client>())
    
    //This will be changed in future
    
    let tabTwoBarItem = UITabBarItem.init(title: ViewControllersConstants.MainTabController.clientsButtonBarItemText,
                                          image: nil,
                                          selectedImage: nil)
    
    let newNavController = UINavigationController.init(rootViewController: listOfClientsScreen)
    newNavController.navigationBar.barTintColor = UtilityManager.sharedInstance.backGroundColorApp
    
    tabTwoBarItem.tag = 2
    tabTwoBarItem.imageInsets = UIEdgeInsets.init(top: tabTwoBarItem.imageInsets.top + (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                  left: tabTwoBarItem.imageInsets.left,
                                                  bottom: tabTwoBarItem.imageInsets.bottom - (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                  right: tabTwoBarItem.imageInsets.right)
    newNavController.tabBarItem = tabTwoBarItem
    newNavController.navigationBar.barStyle = .black
    
    return newNavController
    
  }
  
  //MARK: -  Productos ViewController
  private func createThirdBarItem() -> UINavigationController  {
    
    //    let imageNotasNonSelected = UIImage.init(named: "Tab_3")?.withRenderingMode(.alwaysOriginal)
    //    let imageNotasSelected = UIImage.init(named: "Tab_3_on_Dark_ON")?.withRenderingMode(.alwaysOriginal)
    
    let productsViewController = UIViewController()
    
    let tabThirdBarItem = UITabBarItem.init(title: ViewControllersConstants.MainTabController.productsButtonBarItemText,
                                            image: nil,
                                            selectedImage: nil)
    
    let newNavController = UINavigationController.init(rootViewController: productsViewController)
    newNavController.navigationBar.barTintColor = UtilityManager.sharedInstance.backGroundColorApp
    
    tabThirdBarItem.tag = 3
    tabThirdBarItem.imageInsets = UIEdgeInsets.init(top: tabThirdBarItem.imageInsets.top + (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                    left: tabThirdBarItem.imageInsets.left,
                                                    bottom: tabThirdBarItem.imageInsets.bottom - (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                    right: tabThirdBarItem.imageInsets.right)
    newNavController.tabBarItem = tabThirdBarItem
    newNavController.navigationBar.barStyle = .black
    
    return newNavController
    
  }
  
  //MARK: - Empresas ViewController
  private func createFourthBarItem() -> UINavigationController  {
    
    //    let imageCartasNonSelected = UIImage.init(named: "Tab_4")?.withRenderingMode(.alwaysOriginal)
    //    let imageCartasSelected = UIImage.init(named: "Tab_4_on_Dark_ON")?.withRenderingMode(.alwaysOriginal)
    
    let listOfCompaniesScreen = CompaniesTableViewController.init(style: .plain, newArrayOfCompanies: User.session.arrayOfCompanies)
    
    let tabFourthBarItem = UITabBarItem.init(title: ViewControllersConstants.MainTabController.companiesButtonBarItemText,
                                             image: nil,
                                             selectedImage: nil)
    
    let newNavController = UINavigationController.init(rootViewController: listOfCompaniesScreen)
    newNavController.navigationBar.barTintColor = UtilityManager.sharedInstance.backGroundColorApp
    
    tabFourthBarItem.tag = 4
    tabFourthBarItem.imageInsets = UIEdgeInsets.init(top: tabFourthBarItem.imageInsets.top + (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                     left: tabFourthBarItem.imageInsets.left,
                                                     bottom: tabFourthBarItem.imageInsets.bottom - (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                     right: tabFourthBarItem.imageInsets.right)
    newNavController.tabBarItem = tabFourthBarItem
    newNavController.navigationBar.barStyle = .black
    
    return newNavController
    
  }
  
  //MARK: - MiCuenta ViewController
  private func createFifthBarItem() -> UINavigationController  {
    
    //    let imageMenuNonSelected = UIImage.init(named: "Tab_5")?.withRenderingMode(.alwaysOriginal)
    //    let imageMenuSelected = UIImage.init(named: "Tab_5_on_Dark_ON")?.withRenderingMode(.alwaysOriginal)
    
    let profileScreen = ProfileViewController()
    
    let tabFifthBarItem = UITabBarItem.init(title: ViewControllersConstants.MainTabController.myAccountButtonBarItemText,
                                            image: nil,
                                            selectedImage: nil)
    
    let newNavController = UINavigationController.init(rootViewController: profileScreen)
    newNavController.navigationBar.barTintColor = UtilityManager.sharedInstance.backGroundColorApp
    
    tabFifthBarItem.tag = 5
    tabFifthBarItem.imageInsets = UIEdgeInsets.init(top: tabFifthBarItem.imageInsets.top + (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                    left: tabFifthBarItem.imageInsets.left,
                                                    bottom: tabFifthBarItem.imageInsets.bottom - (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                    right: tabFifthBarItem.imageInsets.right)
    newNavController.tabBarItem = tabFifthBarItem
    newNavController.navigationBar.barStyle = .black
    
    return newNavController
    
  }

  
}
