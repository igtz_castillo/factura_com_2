//
//  CreateAccountViewController.swift
//  FacturaCom
//
//  Created by Israel on 12/03/18.
//  Copyright © 2018 Alejandro Aristi C. All rights reserved.
//

import UIKit

class CreateAccountViewController: UIViewController {
  
  private var titleLabel: UILabel! = nil
  private var emailTextField: CustomTextFieldForLoginView! = nil
  private var passwordTextField: CustomTextFieldForLoginView! = nil
  private var rfcTextField: CustomTextFieldForLoginView! = nil
  private var backButton: UIButton! = nil
  private var tryButton: UIButton! = nil
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.white //UIColor.init(patternImage: UIImage.init(named: "fondo_login")!)
    
    var backgroundImageName = "fondo_nuevo"
    
    if UtilityManager.sharedInstance.isIpad() == true {
      
      backgroundImageName = "iPad_Login_img"
      
    }
    
    let imageViewBackground = UIImageView.init(image: UIImage.init(named: backgroundImageName)!)
    imageViewBackground.contentMode = .scaleToFill
    imageViewBackground.frame = self.view.frame
    self.view.addSubview(imageViewBackground)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.editNavigationBar()
    self.createTitleLabel()
    self.createEmailTextField()
    self.createPasswordTextField()
    self.createRFCTextField()
    self.createBackButton()
    self.createTryButton()
    //    self.createDontHaveAccountLabel()
    
  }
  
  private func editNavigationBar() {
    
    self.title = ""
    self.navigationController?.isNavigationBarHidden = true
    
  }
  
  private func createTitleLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 80.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: UIScreen.main.bounds.size.width - (40.0 * UtilityManager.sharedInstance.conversionWidth),
                                    height: CGFloat.greatestFiniteMagnitude)
    
    titleLabel = UILabel.init(frame: frameForLabel)
    titleLabel.numberOfLines = 3
    titleLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Semibold",
                      size: 50.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    let finalString = "Some \nstring from \nserver"
    
    let stringWithFormat = NSMutableAttributedString(
      string: finalString as String,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
 
    titleLabel!.attributedText = stringWithFormat
    titleLabel!.sizeToFit()
    titleLabel!.adjustsFontSizeToFitWidth = true
    
    let newFrame = CGRect.init(x: (self.view.frame.size.width / 2.0) - (titleLabel.frame.size.width / 2.0),
                               y: (80.0 * UtilityManager.sharedInstance.conversionHeight),
                               width: titleLabel.frame.size.width,
                               height: titleLabel.frame.size.height)
    
    titleLabel.frame = newFrame
    
    self.view.addSubview(titleLabel)
    
  }
  
  private func createEmailTextField() {
    
    var positionInY = titleLabel.frame.origin.y + titleLabel.frame.size.height + (45.0 * UtilityManager.sharedInstance.conversionHeight)
    
    if UtilityManager.sharedInstance.isIpad() {
      
      positionInY = titleLabel.frame.origin.y + titleLabel.frame.size.height + (300.00 - UIApplication.shared.statusBarFrame.size.height) * UtilityManager.sharedInstance.conversionHeight
      
    }
    let frameOfView = CGRect.init(x: 20.0 * UtilityManager.sharedInstance.conversionWidth,
                                  y: positionInY,
                                  width: UIScreen.main.bounds.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                  height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    emailTextField = CustomTextFieldForLoginView.init(frame: frameOfView, imageName: "mail_Icon", newPlaceholderString: "Email")
    emailTextField.mainTextField.autocapitalizationType = .none
    
    self.view.addSubview(emailTextField)
    
  }
  
  private func createPasswordTextField() {
    
    let frameOfView = CGRect.init(x: 20.0 * UtilityManager.sharedInstance.conversionWidth,
                                  y: emailTextField.frame.origin.y + emailTextField.frame.size.height + (25.0 * UtilityManager.sharedInstance.conversionWidth),
                                  width: UIScreen.main.bounds.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                  height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    passwordTextField = CustomTextFieldForLoginView.init(frame: frameOfView, imageName: "pass_Icon", newPlaceholderString: "Contraseña")
    passwordTextField.mainTextField.isSecureTextEntry = true
    
    self.view.addSubview(passwordTextField)
    
  }
  
  private func createRFCTextField() {
    
    let frameOfView = CGRect.init(x: 20.0 * UtilityManager.sharedInstance.conversionWidth,
                                  y: passwordTextField.frame.origin.y + passwordTextField.frame.size.height + (25.0 * UtilityManager.sharedInstance.conversionWidth),
                                  width: UIScreen.main.bounds.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                  height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    rfcTextField = CustomTextFieldForLoginView.init(frame: frameOfView, imageName: "rfc_Icon", newPlaceholderString: "RFC")
    rfcTextField.mainTextField.autocapitalizationType = .allCharacters
    
    self.view.addSubview(rfcTextField)
    
  }
  
  private func createBackButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.orange
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.CreateAccountViewController.backButtonText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color,
                  NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue
                 ]
    )
    
    let frameForButton = CGRect.init(x: (20.0 * UtilityManager.sharedInstance.conversionWidth),
                                     y: rfcTextField.frame.origin.y + rfcTextField.frame.size.height + (35.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: 120.0 * UtilityManager.sharedInstance.conversionWidth,
                                     height: 30.0 * UtilityManager.sharedInstance.conversionHeight)
    backButton = UIButton.init(frame: frameForButton)
    backButton.addTarget(self,
                                   action: #selector(backButtonPressed),
                                   for: .touchUpInside)
    backButton.backgroundColor = UIColor.clear
    backButton.setAttributedTitle(stringWithFormat, for: .normal)
    
    self.view.addSubview(backButton)
    
  }
  
  private func createTryButton() {
    
    let font = UIFont.init(name: "SFUIText-Bold",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.CreateAccountViewController.tryButtonText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color,
                  NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue
      ]
    )
    
    let frameForButton = CGRect.init(x: self.view.frame.size.width - (150.0 * UtilityManager.sharedInstance.conversionWidth),
                                     y: rfcTextField.frame.origin.y + rfcTextField.frame.size.height + (35.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: 130.0 * UtilityManager.sharedInstance.conversionWidth,
                                     height: 30.0 * UtilityManager.sharedInstance.conversionHeight)
    tryButton = UIButton.init(frame: frameForButton)
    tryButton.addTarget(self,
                         action: #selector(tryButtonPressed),
                         for: .touchUpInside)
    tryButton.backgroundColor = UIColor.init(red: 34.0/255.0, green: 49.0/255.0, blue: 63.0/255.0, alpha: 1.0)
    tryButton.layer.cornerRadius = 5.0 * UtilityManager.sharedInstance.conversionHeight
    tryButton.layer.shadowOpacity = 1.0
    tryButton.layer.shadowRadius = 0.0
    tryButton.layer.masksToBounds = false
    tryButton.layer.shadowColor = UIColor.gray.cgColor
    tryButton.layer.shadowOffset = CGSize.init(width: 0.0, height: 2.0)
    tryButton.setAttributedTitle(stringWithFormat, for: .normal)
    
    self.view.addSubview(tryButton)
    
  }
  
  @objc private func backButtonPressed() {
    
    self.navigationController?.popViewController(animated: true)
    
  }
  
  @objc private func tryButtonPressed() {
    
    
  }
  
  private func changeValuesOfCompany(json: [String: AnyObject]) {
    
    let arrayOfCompanies = (json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>())
    
    if arrayOfCompanies.count > 0 {
      
      var companies = Array<Company>()
      
      for company in arrayOfCompanies {
        
        let uidAccount: String = (company["cuenta_uid"] as? String != nil ? company["cuenta_uid"] as! String : "")
        let urlLogo: String? = (company["logo_full"] as? String != nil ? company["logo_full"] as? String : nil)
        let name: String = (company["name"] as? String != nil ? company["name"] as! String : "")
        let rfc: String = (company["rfc"] as? String != nil ? company["rfc"] as! String : "")
        let apiKey: String = (company["apikey"] as? String != nil ? company["apikey"] as! String : "")
        let secretKey: String = (company["secretkey"] as? String != nil ? company["secretkey"] as! String : "")
        
        let newCompany = Company.init()
        
        newCompany.uidAccount = uidAccount
        newCompany.urlLogo = urlLogo
        newCompany.name = name
        newCompany.rfc = rfc
        newCompany.apiKey = apiKey
        newCompany.secretKey = secretKey
        
        companies.append(newCompany)
        
      }
      
      User.session.arrayOfCompanies = companies
      
      let companySelectedRawData = arrayOfCompanies[0]
      let uidAccount: String = (companySelectedRawData["cuenta_uid"] as? String != nil ? companySelectedRawData["cuenta_uid"] as! String : "")
      let urlLogo: String? = (companySelectedRawData["logo_full"] as? String != nil ? companySelectedRawData["logo_full"] as? String : nil)
      let name: String = (companySelectedRawData["name"] as? String != nil ? companySelectedRawData["name"] as! String : "")
      let rfc: String = (companySelectedRawData["rfc"] as? String != nil ? companySelectedRawData["rfc"] as! String : "")
      let apiKey: String = (companySelectedRawData["apikey"] as? String != nil ? companySelectedRawData["apikey"] as! String : "")
      let secretKey: String = (companySelectedRawData["secretkey"] as? String != nil ? companySelectedRawData["secretkey"] as! String : "")
      
      User.session.uidAccount = uidAccount
      User.session.urlLogo = urlLogo
      User.session.name = name
      User.session.rfc = rfc
      User.session.apiKey = apiKey
      User.session.secretKey = secretKey
      
      User.session.actualUsingCompany.uidAccount = uidAccount
      User.session.actualUsingCompany.urlLogo = urlLogo
      User.session.actualUsingCompany.name = name
      User.session.actualUsingCompany.rfc = rfc
      User.session.actualUsingCompany.apiKey = apiKey
      User.session.actualUsingCompany.secretKey = secretKey
      
      let userInfo = (json["profile"] as? [String: AnyObject] != nil ? json["profile"] as! [String: AnyObject] : [String: AnyObject]())
      if userInfo.count > 0 {
        
        let newEmail: String = (userInfo["email"] as? String != nil ? userInfo["email"] as! String : "")
        let newImageURL: String = (userInfo["image"] as? String != nil ? userInfo["image"] as! String : "")
        let newName: String = (userInfo["name"] as? String != nil ? userInfo["name"] as! String : "")
        
        if newEmail != "" {
          
          User.session.email = newEmail
          
        }
        if newImageURL != "" {
          
          User.session.urlLogo = newImageURL
          
        }
        if newName != "" {
          
          User.session.name = newName
          
        }
        
      }
      
    }
    
    //    let data = (json["data"] as? Array<AnyObject>)?[0] as? [String: AnyObject] != nil ? (json["data"] as? Array<AnyObject>)?[0] as! [String: AnyObject] : [String: AnyObject]()
    //
    //    let uidAccount: String = (data["cuenta_uid"] as? String != nil ? data["cuenta_uid"] as! String : "")
    //    let urlLogo: String? = (data["logo"] as? String != nil ? data["logo"] as? String : nil)
    //    let name: String = (data["name"] as? String != nil ? data["name"] as! String : "")
    //    let rfc: String = (data["rfc"] as? String != nil ? data["rfc"] as! String : "")
    //    let apiKey: String = (data["apikey"] as? String != nil ? data["apikey"] as! String : "")
    //    let secretKey: String = (data["secretkey"] as? String != nil ? data["secretkey"] as! String : "")
    //
    //    Company.session.name = name
    //    Company.session.uidAccount = uidAccount
    //    Company.session.urlLogo = urlLogo
    //    Company.session.rfc = rfc
    //    Company.session.apiKey = apiKey
    //    Company.session.secretKey = secretKey
    
  }

  override func viewDidLoad() {
    
    
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    self.editNavigationBar()
    
  }
  
  
}

