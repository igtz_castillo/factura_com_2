//
//  CompanyDetailViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 24/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class CompanyDetailViewController: UIViewController, CustomTextFieldWithTitleViewAndPickerViewDelegate {
  
  private var companyData: Company! = nil
  
  private var mainScrollView: UIScrollView! = nil
  
  private var clientDataLabel: LabelSeparator! = nil
  
  private var razonSocialTextField: CustomTextFieldWithTitleView! = nil
  private var rfcTextField: CustomTextFieldWithTitleView! = nil
  private var regimenFiscalTextField: CustomTextFieldWithTitleViewAndPickerView! = nil
  private var calleTextField: CustomTextFieldWithTitleView! = nil
  private var numeroExteriorTextField: CustomTextFieldWithTitleView! = nil
  private var numeroInteriorTextField: CustomTextFieldWithTitleView! = nil
  private var coloniaTextField: CustomTextFieldWithTitleView! = nil
  private var codigoPostalTextField: CustomTextFieldWithTitleView! = nil
  private var ciudadTextField: CustomTextFieldWithTitleView! = nil
  private var estadoTextField: CustomTextFieldWithTitleView! = nil
  private var localidadTextField: CustomTextFieldWithTitleView! = nil
  private var paisTextField: CustomTextFieldWithTitleView! = nil
  private var firstEmailToSendCFDITextField: CustomTextFieldWithTitleView! = nil
  
  //Buttons
  private var updateProfileButton: UIButton! = nil
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
    
  }
  
  init(newCompanyData: Company) {
    
    companyData = newCompanyData
    
    super.init(nibName: nil, bundle: nil)
    
  }
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.initNavigationBar()
    
    self.initMainScrollView()
    
    self.getInfoFromServer()
    
    
  }
  
  private func initNavigationBar() {
    
    self.initLeftNavigationView()
    
  }
  
  private func initLeftNavigationView() {
    
    self.title = ViewControllersConstants.NameOfScreen.empresasTableText
    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
  }
  
  private func changeNavigationBarTitle() {
    
    self.title = ViewControllersConstants.MainTabController.menuButtonText
    
    let titleLabel = UILabel.init(frame: CGRect.zero)
    
    let font = UIFont(name: "SFUIText-Regular",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.NameOfCFDIScreen.menuTableTitleText,
      attributes:[NSFontAttributeName:font!,
                  NSParagraphStyleAttributeName:style,
                  NSForegroundColorAttributeName:color
      ]
    )
    titleLabel.attributedText = stringWithFormat
    titleLabel.sizeToFit()
    self.navigationItem.titleView = titleLabel
    
  }
  
  private func initMainScrollView() {
    
    mainScrollView = UIScrollView.init(frame: UIScreen.main.bounds)
    
    if UtilityManager.sharedInstance.isIpad() == false {
      
      let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                       height: mainScrollView.frame.size.height + (380.0 * UtilityManager.sharedInstance.conversionHeight))
      mainScrollView.contentSize = newContentSize
      
    }
    
    mainScrollView.backgroundColor = UIColor.white
    
    self.view.addSubview(mainScrollView)
    
  }
  
  private func initClientDataLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: mainScrollView.frame.size.width,
                                    height: 56.0 * UtilityManager.sharedInstance.conversionHeight)
    
    clientDataLabel = LabelSeparator.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.notionsLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    clientDataLabel.attributedText = stringWithFormat
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: clientDataLabel.frame.size.width,
                               height: clientDataLabel.frame.size.height)
    clientDataLabel.frame = newFrame
    clientDataLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(clientDataLabel)
    
  }
  
  private func getInfoFromServer() {
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.requestToGetRegimenFiscal(actionsToMakeWhenSucceeded: { arrayOfRegimenOption in

      ServerManager.sharedInstance.requestDataFromCompany(companyUID: self.companyData.uidAccount, actionsToMakeWhenSucceeded: {
        
        self.initClientDataLabel()
        self.initRazonSocialTextField()
        self.initRFCTextField()
        self.initRegimenFiscalTextField(regimenOptions: arrayOfRegimenOption)
        self.initCalleTextField()
        self.initNumeroExteriorTextField()
        self.initNumeroInteriorTextField()
        self.initColoniaTextField()
        self.initCodigoPostalTextField()
        self.initCiudadTextField()
        self.initEstadoTextField()
        self.initFirstEmailToSendCFDITextField()
        self.initUpdateProfileButton()
        
        UtilityManager.sharedInstance.hideLoader()
        
      }, actionsToMakeWhenFailed: {
        
        UtilityManager.sharedInstance.hideLoader()
        
      })
      
    }, actionsToMakeWhenFailed: {
    
      UtilityManager.sharedInstance.hideLoader()

    })
    
    
    
    
 
    
  }
  
  private func initRazonSocialTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: clientDataLabel.frame.origin.y + clientDataLabel.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    razonSocialTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                             title: ViewControllersConstants.ClientDetailViewController.razonSocialTextFieldText ,
                                                             image: nil,
                                                             colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                             positionInYInsideView: nil,
                                                             positionInXInsideView: nil)
    razonSocialTextField.mainTextField.text = companyData.name
    razonSocialTextField.mainTextField.textColor = UIColor.black
    razonSocialTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText
    razonSocialTextField.backgroundColor = UIColor.white
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(razonSocialTextField)
    
  }
  
  private func initRFCTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: razonSocialTextField.frame.origin.y + razonSocialTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    rfcTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                     title: ViewControllersConstants.ClientDetailViewController.rfcTextFieldText,
                                                     image: nil,
                                                     colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                     positionInYInsideView: nil,
                                                     positionInXInsideView: nil)
    rfcTextField.mainTextField.text = companyData.rfc
    rfcTextField.mainTextField.textColor = UIColor.black
    rfcTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText
    rfcTextField.backgroundColor = UIColor.white
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(rfcTextField)
    
  }
  
  private func initRegimenFiscalTextField(regimenOptions: [Option]) {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: rfcTextField.frame.origin.y + rfcTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    
    
    
    regimenFiscalTextField = CustomTextFieldWithTitleViewAndPickerView.init(frame: frameForView,
                                                                            title: ViewControllersConstants.ClientDetailViewController.regimenFiscalText,
                                                                            image: nil,
                                                              colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                            positionInYInsideView: nil,
                                                            positionInXInsideView: nil,
                                                                       newOptions: regimenOptions,
                                                                titleOfPickerView: ViewControllersConstants.ClientDetailViewController.regimenFiscalText)
    
    regimenFiscalTextField.mainTextField.text = companyData.regimenFiscal
    regimenFiscalTextField.mainTextField.textColor = UIColor.black
    regimenFiscalTextField.mainTextField.placeholder = "Régimen fiscal"
    regimenFiscalTextField.backgroundColor = UIColor.white
    regimenFiscalTextField.delegate = self
    self.mainScrollView.addSubview(regimenFiscalTextField)
    
    
  }
  
  private func initCalleTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: regimenFiscalTextField.frame.origin.y + regimenFiscalTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    calleTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                       title: ViewControllersConstants.ClientDetailViewController.calleTextFieldText,
                                                       image: nil,
                                                       colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                       positionInYInsideView: nil,
                                                       positionInXInsideView: nil)
    calleTextField.mainTextField.text = companyData.calle
    calleTextField.mainTextField.textColor = UIColor.black
    calleTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText
    calleTextField.backgroundColor = UIColor.white
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(calleTextField)
    
  }
  
  private func initNumeroExteriorTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: calleTextField.frame.origin.y + calleTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    numeroExteriorTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                title: ViewControllersConstants.ClientDetailViewController.numeroInteriorTextFieldText,
                                                                image: nil,
                                                                colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                positionInYInsideView: nil,
                                                                positionInXInsideView: nil)
    numeroExteriorTextField.mainTextField.text = companyData.exterior
    numeroExteriorTextField.mainTextField.textColor = UIColor.black
    numeroExteriorTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText
    numeroExteriorTextField.backgroundColor = UIColor.white
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(numeroExteriorTextField)
    
  }
  
  private func initNumeroInteriorTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: numeroExteriorTextField.frame.origin.y + numeroExteriorTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    numeroInteriorTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                title: ViewControllersConstants.ClientDetailViewController.numeroInteriorTextFieldText,
                                                                image: nil,
                                                                colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                positionInYInsideView: nil,
                                                                positionInXInsideView: nil)
    numeroInteriorTextField.mainTextField.text = companyData.interior
    numeroInteriorTextField.mainTextField.textColor = UIColor.black
    numeroInteriorTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText
    numeroInteriorTextField.backgroundColor = UIColor.white
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(numeroInteriorTextField)
    
  }
  
  private func initColoniaTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: numeroInteriorTextField.frame.origin.y + numeroInteriorTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    coloniaTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                         title: ViewControllersConstants.ClientDetailViewController.coloniaTextFieldText,
                                                         image: nil,
                                                         colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                         positionInYInsideView: nil,
                                                         positionInXInsideView: nil)
    coloniaTextField.mainTextField.text = companyData.colonia
    coloniaTextField.mainTextField.textColor = UIColor.black
    coloniaTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText
    coloniaTextField.backgroundColor = UIColor.white
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(coloniaTextField)
    
  }
  
  private func initCodigoPostalTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: coloniaTextField.frame.origin.y + coloniaTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    codigoPostalTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                              title: ViewControllersConstants.ClientDetailViewController.codigoPostalTextFieldText,
                                                              image: nil,
                                                              colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                              positionInYInsideView: nil,
                                                              positionInXInsideView: nil)
    codigoPostalTextField.mainTextField.text = companyData.codPos
    codigoPostalTextField.mainTextField.textColor = UIColor.black
    codigoPostalTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText
    codigoPostalTextField.backgroundColor = UIColor.white
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(codigoPostalTextField)
    
  }
  
  private func initCiudadTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: codigoPostalTextField.frame.origin.y + codigoPostalTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    ciudadTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                        title: ViewControllersConstants.ClientDetailViewController.ciudadTextFieldText,
                                                        image: nil,
                                                        colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                        positionInYInsideView: nil,
                                                        positionInXInsideView: nil)
    ciudadTextField.mainTextField.text = companyData.ciudad
    ciudadTextField.mainTextField.textColor = UIColor.black
    ciudadTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText
    ciudadTextField.backgroundColor = UIColor.white
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(ciudadTextField)
    
  }
  
  private func initEstadoTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: ciudadTextField.frame.origin.y + ciudadTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    estadoTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                        title: ViewControllersConstants.ClientDetailViewController.estadoTextFieldText,
                                                        image: nil,
                                                        colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                        positionInYInsideView: nil,
                                                        positionInXInsideView: nil)
    estadoTextField.mainTextField.text = companyData.estado
    estadoTextField.mainTextField.textColor = UIColor.black
    estadoTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText
    estadoTextField.backgroundColor = UIColor.white
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(estadoTextField)
    
  }
  
  private func initFirstEmailToSendCFDITextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: estadoTextField.frame.origin.y + estadoTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    firstEmailToSendCFDITextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                      title: "Email",//ViewControllersConstants.ClientDetailViewController.firstEmailToSendCFDITextFieldText,
                                                                      image: nil,
                                                                      colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                      positionInYInsideView: nil,
                                                                      positionInXInsideView: nil)
    firstEmailToSendCFDITextField.mainTextField.text = companyData.email
    firstEmailToSendCFDITextField.mainTextField.textColor = UIColor.black
    firstEmailToSendCFDITextField.mainTextField.placeholder = "Email"//ViewControllersConstants.ClientDetailViewController.firstEmailToSendCFDITextFieldText,
    firstEmailToSendCFDITextField.mainTextField.isEnabled = false
    firstEmailToSendCFDITextField.mainTextField.isUserInteractionEnabled = false
    firstEmailToSendCFDITextField.backgroundColor = UIColor.white

    self.mainScrollView.addSubview(firstEmailToSendCFDITextField)
    
  }
  
  private func initUpdateProfileButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: 16.0 * UtilityManager.sharedInstance.conversionWidth,
                                     y: firstEmailToSendCFDITextField.frame.origin.y + firstEmailToSendCFDITextField.frame.size.height + 10.0 * UtilityManager.sharedInstance.conversionHeight,
                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    updateProfileButton = UIButton.init(frame: frameForButton)
    updateProfileButton.addTarget(self,
                                  action: #selector(updateProfileButtonPressed),
                                  for: .touchUpInside)
    updateProfileButton.backgroundColor = UIColor.orange
    updateProfileButton.setBackgroundColor(color: UIColor.init(red: 255.0/255.0, green: 164.0/255.0, blue: 78.0/255.0, alpha: 1.0), forState: .highlighted)
    updateProfileButton.setAttributedTitle(stringWithFormat, for: .normal)
    updateProfileButton.contentHorizontalAlignment = .left
    
    updateProfileButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.ClientDetailViewController.updateProfileButtonText))
    
    self.mainScrollView.addSubview(updateProfileButton)
    
  }
  
  private func createGenericLabelForButton(text: String) -> UILabel {
    
    let frameForLabel = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 200.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    let genericLabel = UILabel.init(frame: frameForLabel)
    genericLabel.numberOfLines = 0
    genericLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: text,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    genericLabel.attributedText = stringWithFormat
    genericLabel.sizeToFit()
    let newFrame = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                               y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                               width: genericLabel.frame.size.width,
                               height: genericLabel.frame.size.height)
    genericLabel.frame = newFrame
    
    return genericLabel
    
  }
  
  @objc private func updateProfileButtonPressed() {
    
    
    if UtilityManager.sharedInstance.isValidText(testString: razonSocialTextField.mainTextField.text!) == false {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Se requiere un nombre de razón social",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    } else
      if UtilityManager.sharedInstance.isValidEmail(testStr: firstEmailToSendCFDITextField .mainTextField.text!) == false {
        
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Se requiere un email para la empresa",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
    } else
      if (rfcTextField.mainTextField.text?.characters.count)! < 12 && (rfcTextField.mainTextField.text?.characters.count)! > 13 {
          
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Se requiere un RFC de mínimo 12 caracteres y máximo 13 caracteres",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
    } else
      if UtilityManager.sharedInstance.isValidText(testString: numeroExteriorTextField.mainTextField.text!) == false {
    
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Se requiere un número exterior",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
      
    } else
        
      if UtilityManager.sharedInstance.isValidText(testString: codigoPostalTextField.mainTextField.text!) == false {
    
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Se requiere un código postal",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
    } else
        
      if UtilityManager.sharedInstance.isValidText(testString: coloniaTextField.mainTextField.text!) == false {
          
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Se requiere el nombre de una colonia",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
    } else
        
      if UtilityManager.sharedInstance.isValidText(testString: estadoTextField.mainTextField.text!) == false {
          
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Se requiere el nombre de un estado",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
    } else
        
      if UtilityManager.sharedInstance.isValidText(testString: ciudadTextField.mainTextField.text!) == false {
          
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Se requiere el nombre de una ciudad",
                                                preferredStyle: UIAlertControllerStyle.alert)
          
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
        alertController.addAction(cancelAction)
          
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
          
    } else
        
      if UtilityManager.sharedInstance.isValidText(testString: calleTextField.mainTextField.text!) == false {
          
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Se requiere el nombre de una calle",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
    } else {
        
        var params: [String: AnyObject] = [String: AnyObject]()
        
        params["razons"] = razonSocialTextField.mainTextField.text! as AnyObject
        params["email"] = firstEmailToSendCFDITextField.mainTextField.text! as AnyObject
        params["rfc"] = rfcTextField.mainTextField.text! as AnyObject
        params["calle"] = calleTextField.mainTextField.text! as AnyObject
        params["numero_exterior"] = numeroExteriorTextField.mainTextField.text! as AnyObject
        params["numero_interior"] = numeroInteriorTextField.mainTextField.text! as AnyObject
        params["codpos"] = codigoPostalTextField.mainTextField.text! as AnyObject
        params["colonia"] = coloniaTextField.mainTextField.text! as AnyObject
        params["estado"] = estadoTextField.mainTextField.text! as AnyObject
        params["ciudad"] = ciudadTextField.mainTextField.text! as AnyObject
        
        UtilityManager.sharedInstance.showLoader()
        
        ServerManager.sharedInstance.requestToUpdateCompany(clientUID: User.session.actualUsingCompany.uidAccount,
                                                            params: params, actionsToMakeWhenSucceeded: {
                                                              
                                                              self.razonSocialTextField.mainTextField.text = User.session.actualUsingCompany.name
                                                              self.firstEmailToSendCFDITextField.mainTextField.text = User.session.actualUsingCompany.email
                                                              
                                                              UtilityManager.sharedInstance.hideLoader()
                                                              
                                                              let alertController = UIAlertController(title: "ÉXITO",
                                                                                                      message: "Actualización exitosa",
                                                                                                      preferredStyle: UIAlertControllerStyle.alert)
                                                              
                                                              let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
                                                              
                                                              alertController.addAction(cancelAction)
                                                              
                                                              let actualController = UtilityManager.sharedInstance.currentViewController()
                                                              actualController.present(alertController, animated: true, completion: nil)
                                                              
        }, actionsToMakeWhenFailed: {
          
          UtilityManager.sharedInstance.hideLoader()
          
          let alertController = UIAlertController(title: "ERROR",
                                                  message: "Hubo problemas al actualizar",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
        })
        
    }
    
  }
  
  //MARK: - CustomTextFieldWithTitleViewAndPickerViewDelegate
  
  func customTextFieldSelected(sender: CustomTextFieldWithTitleViewAndPickerView) {
    
    
    
  }
  
  func hidePickerView(sender: CustomTextFieldWithTitleViewAndPickerView) {
    
    self.view.endEditing(true)
    
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    
    super.viewWillDisappear(animated)
    
  }
  
}
