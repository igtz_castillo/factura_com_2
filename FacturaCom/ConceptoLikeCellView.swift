//
//  ConceptoLikeCellView.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 21/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

protocol ConceptoLikeViewCellDelegate {
  
  func conceptoCellPressed(cell: ConceptoLikeViewCell)
  func deleteConcepto(cell: ConceptoLikeViewCell)
  
}

class ConceptoLikeViewCell: UIView {
  
  enum Position {
    case left
    case right
  }
  
  private var ctoButton: ConceptoButton! = nil
  private var labelAction: UILabel! = nil
  private var actualPosition: Position! = .right
  var delegate: ConceptoLikeViewCellDelegate?
  
  private let positionForButtonWhenLeftDrag = CGPoint.init(x: -90.0 * UtilityManager.sharedInstance.conversionWidth,
                                                           y: 0.0)
  
  private let positionForButtonWhenRightDrag = CGPoint.init(x: 0.0,
                                                            y: 0.0)
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override init(frame: CGRect) {
    
    super.init(frame: frame)
   
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.backgroundColor = UIColor.white
    
    self.initLabelAction()
    self.initButtonForCreateConcepto()
    
  }
  
  private func initButtonForCreateConcepto() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: 0.0,
                                     width: self.frame.size.width,
                                     height: self.frame.size.height)
    
    ctoButton = ConceptoButton.init(frame: frameForButton)
    ctoButton.changeMyTitle("")
    ctoButton.drawTopBorder()
    ctoButton.drawBottomBorder()
    ctoButton.addTarget(self, action: #selector(buttonPressed), for: UIControlEvents.touchUpInside)
    ctoButton.isUserInteractionEnabled = true
    ctoButton.backgroundColor = UIColor.white
    self.addGestureToButton()
    
    self.addSubview(ctoButton)
    
  }
  
  private func addGestureToButton() {
    
    let dragGesture = UIPanGestureRecognizer.init(target: self, action: #selector(dragGestureActivated))
    dragGesture.maximumNumberOfTouches = 1
    ctoButton.addGestureRecognizer(dragGesture)
    
  }
  
  func setStringLabelOfButton(newName: String) {
    
    ctoButton.changeMyTitle(newName)
    
  }
  
  private func initLabelAction() {
    
    let frameForLabel = CGRect.init(x: self.frame.size.width - 90.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 0.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 90.0 * UtilityManager.sharedInstance.conversionHeight,
                                    height: self.frame.size.height)
    
    labelAction = UILabel.init(frame: frameForLabel)
    labelAction.numberOfLines = 0
    labelAction.lineBreakMode = .byWordWrapping
    labelAction.backgroundColor = UIColor.red
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: "Borrar",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    labelAction.attributedText = stringWithFormat
    
    labelAction.isUserInteractionEnabled = true
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(doDeleteAction))
    tapGesture.numberOfTapsRequired = 1
    labelAction.addGestureRecognizer(tapGesture)
    
    self.addSubview(labelAction)
    
  }
  
  @objc private func buttonPressed() {
    
    self.delegate?.conceptoCellPressed(cell: self)
    
  }
  
  @objc private func doDeleteAction() {
    
    self.delegate?.deleteConcepto(cell: self)
    
  }
  
  @objc private func dragGestureActivated(gestureRecognized: UIPanGestureRecognizer) {
    
    let vel = gestureRecognized.velocity(in: self.ctoButton)
    
    if vel.x > 0 {
      
      if actualPosition != nil {
        
        if actualPosition == Position.left {
          
          self.actualPosition = .right
          
          UIView.animate(withDuration: 0.3, animations: { 
            
            self.ctoButton.frame = CGRect.init(x: self.positionForButtonWhenRightDrag.x,
                                               y: self.positionForButtonWhenRightDrag.y,
                                           width: self.ctoButton.frame.size.width,
                                          height: self.ctoButton.frame.size.height)
            
          })
          
        }
        
      }

    } else {
      
      if actualPosition != nil {
        
        if actualPosition == Position.right {
          
          self.actualPosition = .left
          
          UIView.animate(withDuration: 0.3, animations: {
            
            self.ctoButton.frame = CGRect.init(x: self.positionForButtonWhenLeftDrag.x,
                                               y: self.positionForButtonWhenLeftDrag.y,
                                               width: self.ctoButton.frame.size.width,
                                               height: self.ctoButton.frame.size.height)
            
          })
          
        }
        
      }
      
    }
    
  }
  
}
