//
//  CustomTextFieldForLoginView.swift
//  FacturaCom
//
//  Created by Israel on 12/03/18.
//  Copyright © 2018 Alejandro Aristi C. All rights reserved.
//

import Foundation
import UIKit

class CustomTextFieldForLoginView: UIView {
  
  var mainTextField: UITextField! = nil
  private var imageNameString: String! = ""
  private var imageForTextField: UIImageView! = nil
  private var placeholderString: String! = ""
  
  init(frame: CGRect, imageName: String, newPlaceholderString: String) {
    super.init(frame: frame)
    self.imageNameString = imageName
    self.placeholderString = newPlaceholderString
    self.changeAppearance()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func changeAppearance() {
    self.backgroundColor = UIColor.clear
    self.initImageForTextField()
    self.setAppearanceOfTextField()
  }
  
  private func initImageForTextField() {
    let image = UIImage.init(named: self.imageNameString)
    self.imageForTextField = UIImageView.init(image: image)
    self.imageForTextField.frame = CGRect.init(x: 3.0 * UtilityManager.sharedInstance.conversionWidth,
                                               y: (self.frame.size.height/2.0) - (self.imageForTextField.frame.size.height/2.0),
                                           width: self.imageForTextField.frame.size.width,
                                          height: self.imageForTextField.frame.size.height)
    self.addSubview(self.imageForTextField)
  }
  
  private func setAppearanceOfTextField() {
    self.initMainTextField()

    //Left spacing
    let paddingView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 7.0 * UtilityManager.sharedInstance.conversionWidth, height: 20))
    self.mainTextField.leftView = paddingView
    self.mainTextField.leftViewMode = .always
    
    //Placeholder style
    self.mainTextField.attributedPlaceholder = NSAttributedString(string: placeholderString,
                                                              attributes: [NSForegroundColorAttributeName: UIColor.init(red: 214.0/255.0, green: 159.0/255.0, blue: 228.0/255.0, alpha: 1.0),
                                                                           NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14.0 * UtilityManager.sharedInstance.conversionWidth)])
    
    //Text style
    self.mainTextField.typingAttributes = [NSForegroundColorAttributeName: UIColor.init(red: 214.0/255.0, green: 159.0/255.0, blue: 228.0/255.0, alpha: 1.0),
                                           NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14.0 * UtilityManager.sharedInstance.conversionWidth)]
    self.mainTextField.defaultTextAttributes = [NSForegroundColorAttributeName: UIColor.init(red: 214.0/255.0, green: 159.0/255.0, blue: 228.0/255.0, alpha: 1.0),
                                                NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14.0 * UtilityManager.sharedInstance.conversionWidth)]
    
    //Border style
    self.mainTextField.layer.borderColor = UIColor.white.cgColor
    self.mainTextField.layer.cornerRadius = 12.0 * UtilityManager.sharedInstance.conversionHeight
    self.mainTextField.layer.borderWidth = 1.0 * UtilityManager.sharedInstance.conversionHeight
    self.mainTextField.backgroundColor = UIColor.clear
  }
  
  private func initMainTextField() {
    let frameForTextField = CGRect.init(x: self.imageForTextField.frame.origin.x + self.imageForTextField.frame.size.width + (5.0 * UtilityManager.sharedInstance.conversionWidth),
                                        y: 3.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: self.frame.size.width - (self.imageForTextField.frame.origin.x + self.imageForTextField.frame.size.width + (5.0 * UtilityManager.sharedInstance.conversionWidth)),
                                   height: self.frame.size.height - (6.0 * UtilityManager.sharedInstance.conversionHeight))
    
    self.mainTextField = UITextField.init(frame: frameForTextField)
    self.addSubview(self.mainTextField)
  }
  
}
