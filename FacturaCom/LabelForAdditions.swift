//
//  LabelForAdditions.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 13/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class LabelForAdditions: UILabel {
  
  
  override func drawText(in rect: CGRect) {
    
    let insets = UIEdgeInsets.init()
    
//    let insets = UIEdgeInsets.init(top: 0 * UtilityManager.sharedInstance.conversionHeight,
//                                   left: 0,
//                                   bottom: 0 * UtilityManager.sharedInstance.conversionHeight,
//                                   right: 0)
//    
    let newRect = CGRect.init(x: self.frame.size.width - (self.intrinsicContentSize.width + (18.0 * UtilityManager.sharedInstance.conversionWidth)),
                              y: (20.0 * UtilityManager.sharedInstance.conversionHeight),
                              width: self.frame.size.width,
                              height: 30.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let lastRect = UIEdgeInsetsInsetRect(newRect, insets)
    
    super.drawText(in: lastRect)
    
  }
//
  
  
}
