//
//  Concepto.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 20/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class Concepto {
  
  var claveProdServ: String! = nil
  var numberIdentification: String! = nil
  var quantity: Double! = 0.0
  var unit: Unity! = nil
  var conceptDescription: String! = nil
  var price: Double! = 0.0
  var subtotal: Double! = 0.0
  var siAduana: Bool! = nil
  var pedimentoAduana: String! = nil
  var aduanaLoc: String! = nil
  var fechaAduana: String! = nil //YYYY-MM-dd
  var discountPercentage: Double! = 0.0
  var arrayOfParts: Array<Parte> = Array<Parte>()
  var isTaxIncludedInPrice: Bool! = false
  var finalBase: Double! = 0.0 //base final
  
  var applyIva: Bool! = false
  var applyIsh: Bool! = false
  var applyIeps: Bool! = false
  var applyRetIva: Bool! = false
  var applyRetIsr: Bool! = false
  var applyRetIvaPorte: Bool! = false
  
  var finalDiscount: Double! = 0.0
  var finalIva: Double! = 0.0
  var finalIsh: Double! = 0.0
  var finalIeps: Double! = 0.0
  var finalRetIva: Double! = 0.0
  var finalRetIsr: Double! = 0.0
  var finalRetIvaPorte: Double! = 0.0
  
  var ivaValueToSendToServer: Double! = 0.0
  var ishValueToSendToServer: Double! = 0.0
  var iepsValueToSendToServer: Double! = 0.0
  var retIvaValueToSendToServer: Double! = 0.0
  var retIsrValueToSendToServer: Double! = 0.0
  var retIvaPorteValueToSendToServer: Double! = 0.0
  
  init() {}
  
}

