//
//  MenuTableViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 04/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {
  
  private var leftButtonItemView: UIBarButtonItem! = nil
  private var rightButtonItemView: UIBarButtonItem! = nil
  private var companyView: UIImageView! = nil
  private var userView: UIImageView! = nil
  
  private let kNumberOfOptions = 2
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override init(style: UITableViewStyle) {
    
    super.init(style: style)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.initNavigationBar()
    
  }
  
  private func initNavigationBar() {
    
    self.title = ViewControllersConstants.NameOfCFDIScreen.menuTableTitleText
    
    let leftButton = UIBarButtonItem(title: "",
                                     style: UIBarButtonItemStyle.plain,
                                     target: self,
                                     action: nil)
    
    self.navigationItem.rightBarButtonItem = leftButton

    self.initLeftNavigationView()
    self.changeNavigationBarTitle()
    self.initRightNavigationView()
    self.initTableView()
    
  }
  
  private func initLeftNavigationView() {
    
    let frameForView = CGRect.init(x: 0.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: 0.0 * UtilityManager.sharedInstance.conversionHeight,
                                   width: 22.0 * UtilityManager.sharedInstance.conversionWidth,
                                   height: 22.0 * UtilityManager.sharedInstance.conversionHeight)
    
    companyView = UIImageView.init(frame: frameForView)
    companyView.backgroundColor = UIColor.lightGray
    
    if User.session.actualUsingCompany.urlLogo != nil {
      
      companyView.imageFromUrl(urlString: User.session.actualUsingCompany.urlLogo!)
      
    }
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(leftButtonItemPressed))
    tapGesture.numberOfTapsRequired = 1
    companyView.addGestureRecognizer(tapGesture)
    
    leftButtonItemView = UIBarButtonItem.init(customView: companyView)
    self.navigationItem.leftBarButtonItem = leftButtonItemView
    
    self.initGrayTriangle()
    
  }
  
  private func initGrayTriangle() {
    
    let frameForArrowView = CGRect.init(x: companyView.frame.origin.x + companyView.frame.size.width + (0.0 * UtilityManager.sharedInstance.conversionWidth),
                                        y: companyView.frame.origin.y + companyView.frame.size.height + (0.0 * UtilityManager.sharedInstance.conversionHeight),
                                        width: 8.0 * UtilityManager.sharedInstance.conversionWidth,
                                        height: 4.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let arrowImageView = UIImageView.init(frame: frameForArrowView)
    arrowImageView.image = UIImage.init(named: "triangle.png")
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(leftButtonItemPressed))
    tapGesture.numberOfTapsRequired = 1
    arrowImageView.addGestureRecognizer(tapGesture)
    
    let leftTriangleButtonItem = UIBarButtonItem.init(customView: arrowImageView)
    
    self.navigationItem.leftBarButtonItems?.append(leftTriangleButtonItem)
    
  }
  
  private func changeNavigationBarTitle() {
    
    self.title = ViewControllersConstants.MainTabController.menuButtonText
    
    let titleLabel = UILabel.init(frame: CGRect.zero)
    
    let font = UIFont(name: "SFUIText-Regular",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.NameOfCFDIScreen.menuTableTitleText,
      attributes:[NSFontAttributeName:font!,
                  NSParagraphStyleAttributeName:style,
                  NSForegroundColorAttributeName:color
      ]
    )
    titleLabel.attributedText = stringWithFormat
    titleLabel.sizeToFit()
    self.navigationItem.titleView = titleLabel
    
  }
  
  private func initRightNavigationView() {
    
    let frameForView = CGRect.init(x: 0.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: 0.0 * UtilityManager.sharedInstance.conversionHeight,
                                   width: 22.0 * UtilityManager.sharedInstance.conversionWidth,
                                   height: 22.0 * UtilityManager.sharedInstance.conversionHeight)
    
    userView = UIImageView.init(frame: frameForView)
    userView.image = UIImage.init(named: "profile.png")
    
    if User.session.urlLogo != nil {
      
      userView.imageFromUrl(urlString: User.session.urlLogo!)
      
    }
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(profileButtonPressed))
    tapGesture.numberOfTapsRequired = 1
    userView.addGestureRecognizer(tapGesture)
    
    rightButtonItemView = UIBarButtonItem.init(customView: userView)
    self.navigationItem.rightBarButtonItem = rightButtonItemView
    
//    let imageBackButton = UIImage.init(named: "defaultUserSmallImage")?.withRenderingMode(.alwaysOriginal)
//    
//    let profileButton = UIBarButtonItem.init(image: imageBackButton,
//                                             style: .plain,
//                                             target: self,
//                                             action: #selector(profileButtonPressed))
//    
//    
//    self.navigationItem.setRightBarButton(profileButton, animated: false)
    
  }
  
  private func initTableView() {
    
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    
  }
  

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return kNumberOfOptions
    
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    return 44.0 * UtilityManager.sharedInstance.conversionHeight
    
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
    
    cell?.selectionStyle = UITableViewCellSelectionStyle.none
    
    switch indexPath.row {
    case 0:
      cell?.textLabel?.text = "Empresas"
      cell?.tag = 0
      break
    
    case 1:
      cell?.textLabel?.text = "Clientes"
      cell?.tag = 1
      break
    
//    case 2:
//      cell?.textLabel?.text = "Ayuda"
//      cell?.tag = 2
//      break
      
    default:
      cell?.textLabel?.text = "Empresas"
      cell?.tag = 0
    }
    
    cell?.accessoryType = .disclosureIndicator
    
    return cell != nil ? cell! : UITableViewCell.init()
    
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    switch indexPath.row {
    case 0:
      let listOfCompaniesScreen = CompaniesTableViewController.init(style: .plain, newArrayOfCompanies: User.session.arrayOfCompanies)
      listOfCompaniesScreen.navigationController?.navigationBar.tintColor = UIColor.orange
      self.navigationController?.pushViewController(listOfCompaniesScreen, animated: true)
      
      break
      
    case 1:
      let listOfClientsScreen = ClientsTableViewController.init(style: .plain, newArrayOfElements: Array<Client>())
      listOfClientsScreen.navigationController?.navigationBar.tintColor = UIColor.orange
      self.navigationController?.pushViewController(listOfClientsScreen, animated: true)
      break
      
//    case 2:
//      
//      let helpTable = HelpTableViewController.init(style: .plain)
//      self.navigationController?.pushViewController(helpTable, animated: true)
//      
//      break
      
    default:
      
      break
    }
    
  }
  
  @objc private func leftButtonItemPressed() {
    
    print("show action sheet")
    
    if UtilityManager.sharedInstance.isIpad() == false {
      
      if User.session.arrayOfCompanies.count > 1 {
        
        let actionSheet = UIAlertController.init(title: ViewControllersConstants.TableViewWithSearchViewController.actionSheetTitleText,
                                                 message: ViewControllersConstants.TableViewWithSearchViewController.actionSheetMessageText,
                                                 preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction.init(title: "Cancelar", style: .cancel) { action -> Void in}
        actionSheet.addAction(cancelActionButton)
        
        for company in User.session.arrayOfCompanies {
          
          let newActionButton = UIAlertAction.init(title: company.name, style: .default) { action -> Void in
            
            self.changeSelectedCompany(nextCompanySelected: company)
//            self.actionToDoWhenChangeCompany()
            
          }
          
          actionSheet.addAction(newActionButton)
          
        }
        
        self.present(actionSheet, animated: true, completion: nil)
        
      }
      
    } else {
      
      if User.session.arrayOfCompanies.count > 1 {
        
        let actionSheet = UIAlertController.init(title: ViewControllersConstants.TableViewWithSearchViewController.actionSheetTitleText,
                                                 message: ViewControllersConstants.TableViewWithSearchViewController.actionSheetMessageText,
                                                 preferredStyle: .actionSheet)
        
        actionSheet.modalPresentationStyle = .popover
        
        let cancelActionButton = UIAlertAction.init(title: "Cancelar", style: .cancel) { action -> Void in}
        actionSheet.addAction(cancelActionButton)
        
        for company in User.session.arrayOfCompanies {
          
          let newActionButton = UIAlertAction.init(title: company.name, style: .default) { action -> Void in
            
            self.changeSelectedCompany(nextCompanySelected: company)
//            self.actionToDoWhenChangeCompany()
            
          }
          
          actionSheet.addAction(newActionButton)
          
        }
        
        if let popoverController = actionSheet.popoverPresentationController {
          popoverController.barButtonItem = leftButtonItemView
        }
        
        
        self.present(actionSheet, animated: true, completion: nil)
        
      }
      
    }

    
  }
  
  private func changeSelectedCompany(nextCompanySelected: Company) {
    
    User.session.actualUsingCompany = nextCompanySelected
    
    User.session.rfc = User.session.actualUsingCompany.rfc
    User.session.apiKey = User.session.actualUsingCompany.apiKey
    User.session.secretKey = User.session.actualUsingCompany.secretKey
    User.session.uidAccount = User.session.actualUsingCompany.uidAccount
    
    if User.session.actualUsingCompany.urlLogo != nil {
      
      self.companyView.imageFromUrl(urlString: User.session.actualUsingCompany.urlLogo!)
      
    }
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    
    if User.session.actualUsingCompany.urlLogo != nil && self.companyView != nil {
      
      self.companyView.imageFromUrl(urlString: User.session.actualUsingCompany.urlLogo!)
      
    }
    
  }
  
  @objc private func profileButtonPressed() {
    
    let profileScreen = ProfileViewController()
    self.navigationController?.pushViewController(profileScreen, animated: true)
    
    print("show profile view controller")
    
  }
  
  
}
