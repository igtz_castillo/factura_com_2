//
//  ConceptoButton.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 20/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class ConceptoButton: UIButton {
  
  private var newTitleLabel: UILabel! = nil
  
  override init(frame: CGRect) {
    
    super.init(frame: frame)
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func changeMyTitle(_ to: String) {
    
    if newTitleLabel == nil {
      
      let frameForLabel = CGRect.init(x: 16.0 * UtilityManager.sharedInstance.conversionWidth,
                                      y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                      width: self.frame.size.width,
                                      height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
      
      newTitleLabel = UILabel.init(frame: frameForLabel)
      
    }
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: to,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    newTitleLabel.attributedText = stringWithFormat
    newTitleLabel.sizeToFit()
    let newFrame = CGRect.init(x: newTitleLabel.frame.origin.x,
                               y: newTitleLabel.frame.origin.y,
                               width: newTitleLabel.frame.size.width,
                               height: newTitleLabel.frame.size.height)
    
    newTitleLabel.frame = newFrame
    
    self.addSubview(newTitleLabel)
    
  }
  
  func changeMyTitle(_ to: String, color: UIColor) {
    
    if newTitleLabel == nil {
      
      let frameForLabel = CGRect.init(x: 16.0 * UtilityManager.sharedInstance.conversionWidth,
                                      y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                      width: self.frame.size.width,
                                      height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
      
      newTitleLabel = UILabel.init(frame: frameForLabel)
      
    }
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: to,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    newTitleLabel.attributedText = stringWithFormat
    newTitleLabel.sizeToFit()
    let newFrame = CGRect.init(x: newTitleLabel.frame.origin.x,
                               y: newTitleLabel.frame.origin.y,
                               width: newTitleLabel.frame.size.width,
                               height: newTitleLabel.frame.size.height)
    
    newTitleLabel.frame = newFrame
    
    self.addSubview(newTitleLabel)
    
  }
  
  func drawTopBorder() {
    
    let topBorder = CALayer()
    topBorder.backgroundColor = UIColor.lightGray.cgColor
    topBorder.frame = CGRect(x:0,y: 0, width:self.frame.size.width, height:0.5 * UtilityManager.sharedInstance.conversionWidth)
    self.layer.addSublayer(topBorder)
    
  }
  
  func drawBottomBorder() {
    
    let bottomBorder = CALayer()
    bottomBorder.backgroundColor = UIColor.lightGray.cgColor
    bottomBorder.frame = CGRect(x:0, y:self.frame.size.height - (0.5 * UtilityManager.sharedInstance.conversionWidth), width:self.frame.size.width, height:0.5 * UtilityManager.sharedInstance.conversionWidth)
    self.layer.addSublayer(bottomBorder)
    
  }

}

class ParteButton: ConceptoButton {}
