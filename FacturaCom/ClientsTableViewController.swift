//
//  ClientsTableViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 13/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class ClientsTableViewController: UITableViewController, UISearchResultsUpdating {
  
  var searchController: UISearchController!
  var leftButtonItemView: UIBarButtonItem! = nil
  
  //
  private var arrayOfElements: Array<Client>! = Array<Client>()
  private var filteredElements: Array<Client>! = Array<Client>()
  private var isSecondOrMoreTimesAppearing: Bool = false
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
    
  }
  
  init(style: UITableViewStyle, newArrayOfElements: Array<Client>) {
    
    super.init(style: style)
    
    arrayOfElements = newArrayOfElements
    filteredElements = newArrayOfElements
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.initNavigationBar()
    self.initSearchController()
    self.initTableView()
    self.loadInformationFromServer()
    
  }
  
  private func initNavigationBar() {
    
    self.changeNavigationBarTitle()
    self.initLeftNavigationView()
    self.initRightNavigationView()
    
  }
  
  private func changeNavigationBarTitle() {
    
    if #available(iOS 11.0, *) {
      
      self.navigationItem.title = ViewControllersConstants.NameOfScreen.clientsTableText
      self.navigationItem.largeTitleDisplayMode = .never
      
    } else {
      
      let titleLabel = UILabel.init(frame: CGRect.zero)
      
      let font = UIFont(name: "SFUIText-Regular",
                        size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
      let color = UIColor.white
      let style = NSMutableParagraphStyle()
      style.alignment = NSTextAlignment.center
      
      let stringWithFormat = NSMutableAttributedString(
        string: ViewControllersConstants.NameOfScreen.clientsTableText,
        attributes:[NSFontAttributeName:font!,
                    NSParagraphStyleAttributeName:style,
                    NSForegroundColorAttributeName:color
        ]
      )
      titleLabel.attributedText = stringWithFormat
      titleLabel.sizeToFit()
      self.navigationItem.titleView = titleLabel
      
    }
    
  }
  
  private func initLeftNavigationView() {
    
    self.title = ViewControllersConstants.NameOfScreen.clientsTableText
    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
//    let leftButton = UIBarButtonItem(title: ViewControllersConstants.ClientDetailViewController.backButtonNavigationBarText,
//                                     style: UIBarButtonItemStyle.plain,
//                                     target: self,
//                                     action: #selector(backButtonPressed))
//    
//    let fontForButtonItem =  UIFont(name: "SFUIText-Regular",
//                                    size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
//    
//    let attributesDict: [String:AnyObject] = [NSFontAttributeName: fontForButtonItem!,
//                                              NSForegroundColorAttributeName: UIColor.orange
//    ]
//    
//    leftButton.setTitleTextAttributes(attributesDict, for: .normal)
//    
//    self.navigationItem.leftBarButtonItem = leftButton
    
  }
  
  private func initRightNavigationView() {
    
    let imageBackButton = UIImage.init(named: "plus")?.withRenderingMode(.alwaysOriginal)
    
    let profileButton = UIBarButtonItem.init(image: imageBackButton,
                                             style: .plain,
                                             target: self,
                                             action: #selector(createSomethingButtonPressed))
    
    self.navigationItem.setRightBarButton(profileButton, animated: false)
    
  }
  
  private func initSearchController() {
    
    searchController = UISearchController.init(searchResultsController: nil)
    searchController.searchResultsUpdater = self
    searchController.dimsBackgroundDuringPresentation = false
    self.definesPresentationContext = true
    searchController.searchBar.barTintColor = UtilityManager.sharedInstance.backgroundColorForSearchBar
    self.tableView.tableHeaderView = searchController.searchBar
    
  }
  
  private func initTableView() {
    
    self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    
  }
  
  private func loadInformationFromServer() {
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.clientsByRFC(rfc: nil, actionsToMakeWhenSucceeded: { (allClients) in
      
      self.arrayOfElements = allClients
      self.filteredElements = allClients
      
      self.tableView.reloadData()
      
      UtilityManager.sharedInstance.hideLoader()
      
      
    }, actionsToMakeWhenFailed: {
    
      UtilityManager.sharedInstance.hideLoader()
    
    })
    
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      return filteredElements.count
      
    }
    
    return arrayOfElements.count
    
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    return 45.0 * UtilityManager.sharedInstance.conversionHeight
    
  }
  
  private func filterContentForSearchText(searchText: String, scope: String = "All") {
    
    filteredElements = arrayOfElements.filter { element in
      
      return element.contacto.nombre.lowercased().contains(searchText.lowercased()) || element.contacto.email.lowercased().contains(searchText.lowercased()) || element.rfc.lowercased().contains(searchText.lowercased()) || element.razonSocial.lowercased().contains(searchText.lowercased())
      
    }
    
    self.tableView.reloadData()
    
  }
  
  func updateSearchResults(for searchController: UISearchController) {
    
    self.filterContentForSearchText(searchText: searchController.searchBar.text!)
    
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
    cell = UITableViewCell.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      cell!.textLabel?.text = filteredElements[indexPath.row].razonSocial
      cell!.detailTextLabel?.text = filteredElements[indexPath.row].rfc
      
    } else {
      
      cell!.textLabel?.text = arrayOfElements[indexPath.row].razonSocial
      cell!.detailTextLabel?.text = arrayOfElements[indexPath.row].rfc
      
    }
    
    cell?.textLabel?.textColor = UtilityManager.sharedInstance.labelsAndLinesColor
    cell?.detailTextLabel?.textColor = UtilityManager.sharedInstance.labelsAndLinesColor
    
    return cell!
    
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    var selectedClient: Client
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      selectedClient = filteredElements[indexPath.row]
      
    } else {
      
      selectedClient = arrayOfElements[indexPath.row]
      
    }
    
    let clientScreen = ClientDetailViewController.init(newClientData: selectedClient)
    self.navigationController?.pushViewController(clientScreen, animated: true)
    
  }
  
  override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    
    var selectedClient: Client
    
    if self.searchController.isActive && self.searchController.searchBar.text != "" {
      
      selectedClient = self.filteredElements[indexPath.row]
      
    } else {
      
      selectedClient = self.arrayOfElements[indexPath.row]
      
    }
    
    if selectedClient.numberOfCFDIs != nil && Int(selectedClient.numberOfCFDIs)! == 0 {
      
      let deleteClient = UITableViewRowAction.init(style: .normal,
                                                   title: ViewControllersConstants.ClientsTableViewController.deleteClientActionText) { (tableView, indexPath) in
         
        UtilityManager.sharedInstance.showLoader()
                                                    
        ServerManager.sharedInstance.requestToDeleteClient(clientID: selectedClient.uid, actionsToMakeWhenSucceeded: {
          
          UtilityManager.sharedInstance.hideLoader()
          
          self.loadInformationFromServer()
 
         }, actionsToMakeWhenFailed: {
         
         UtilityManager.sharedInstance.hideLoader()
         
         })
                                           
      }
      
      deleteClient.backgroundColor = UIColor.red
      
      return [deleteClient]
      
    }
    
    return nil
    
  }
  
  override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    
    var selectedClient: Client
    
    if self.searchController.isActive && self.searchController.searchBar.text != "" {
      
      selectedClient = self.filteredElements[indexPath.row]
      
    } else {
      
      selectedClient = self.arrayOfElements[indexPath.row]
      
    }
    
    if selectedClient.numberOfCFDIs != nil && Int(selectedClient.numberOfCFDIs) != nil && Int(selectedClient.numberOfCFDIs)! == 0 {
     
      return true
      
    }

    return false
    
  }
  
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {}
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    
    if isSecondOrMoreTimesAppearing == false {
      
      isSecondOrMoreTimesAppearing = true
      
    } else {
      
      self.loadInformationFromServer()
      
    }
    
  }
  
  
  @objc private func backButtonPressed() {
    
    _ = self.navigationController?.popViewController(animated: true)
    
  }
  
  @objc open func createSomethingButtonPressed() {
    
    let newClient = Client.init()
    let createClientScreen = CreateClientViewController.init(newClientData: newClient)
    self.navigationController?.pushViewController(createClientScreen, animated: true)
    
    print("show profile view controller")
    
  }
  
}
