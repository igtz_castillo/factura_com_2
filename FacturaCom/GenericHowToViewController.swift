//
//  GenericHowToViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 19/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class GenericHowToViewController: UIViewController {
  
//  private var
  
  private var mainScrollView: UIScrollView! = nil
  private var videoView: UIView! = nil
  private var arrayOfStrings: Array<String> = Array<String>()
  private var finalHeight: CGFloat! = 260.0
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  init(newArrayOfStrings: Array<String>) {
    
    arrayOfStrings = newArrayOfStrings
    
    super.init(nibName: nil, bundle: nil)
    
  }
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.initNavigationBar()
    self.initMainScrollView()
    self.initVideoView()
    self.initAllLabelsFromStrings()
    
  }
  
  private func initNavigationBar() {
    
    self.initLeftNavigationView()
    self.initRightNavigationView()
    
  }
  
  private func initLeftNavigationView() {

    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
//    let leftButton = UIBarButtonItem(title: ViewControllersConstants.ClientDetailViewController.backButtonNavigationBarText,
//                                     style: UIBarButtonItemStyle.plain,
//                                     target: self,
//                                     action: #selector(backButtonPressed))
//    
//    let fontForButtonItem =  UIFont(name: "SFUIText-Regular",
//                                    size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
//    
//    let attributesDict: [String:AnyObject] = [NSFontAttributeName: fontForButtonItem!,
//                                              NSForegroundColorAttributeName: UIColor.orange
//    ]
//    
//    leftButton.setTitleTextAttributes(attributesDict, for: .normal)
//    
//    self.navigationItem.leftBarButtonItem = leftButton
    
  }
  
  private func initRightNavigationView() {
    
    let imageBackButton = UIImage.init(named: "profile")?.withRenderingMode(.alwaysOriginal)
    
    let profileButton = UIBarButtonItem.init(image: imageBackButton,
                                             style: .plain,
                                             target: self,
                                             action: #selector(profileButtonPressed))
    
    self.navigationItem.setRightBarButton(profileButton, animated: false)
    
  }
  
  private func initMainScrollView() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: 0.0,
                               width: self.view.frame.size.width,
                              height: self.view.frame.size.height)
    
    mainScrollView = UIScrollView.init(frame: frameForView)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.frame.size.height + (0.0 * UtilityManager.sharedInstance.conversionHeight))
    
    mainScrollView.contentSize = newContentSize
    self.view.addSubview(mainScrollView)
    
    self.finalHeight = finalHeight + (self.navigationController?.navigationBar.frame.height)!
    
  }
  
  private func initVideoView() {
  
    let frameForView = CGRect.init(x: 0.0,
                                   y: 0.0,
                               width: self.mainScrollView.frame.size.width,
                              height: 230.0 * UtilityManager.sharedInstance.conversionHeight)
    
    videoView = UIView.init(frame: frameForView)
    videoView.backgroundColor = UIColor.lightGray
    
    self.mainScrollView.addSubview(videoView)
  
  }
  
  private func initAllLabelsFromStrings() {
    
    let frameForFirstString = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                          y: videoView.frame.origin.y + videoView.frame.size.height + (30.0 * UtilityManager.sharedInstance.conversionHeight),
                                      width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: CGFloat.greatestFiniteMagnitude)
    
    if arrayOfStrings.count != 0 {
      
      if arrayOfStrings.count > 2 {
        
        let firstLabel = self.normalLabel(stringOfLabel: arrayOfStrings.first!, frameOfLabel: frameForFirstString)
        
        self.mainScrollView.addSubview(firstLabel)
        
        self.finalHeight = finalHeight + firstLabel.frame.size.height
        
        var frameForLabels = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                         y: firstLabel.frame.origin.y + firstLabel.frame.size.height + (30.0 * UtilityManager.sharedInstance.conversionHeight),
                                         width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                         height: CGFloat.greatestFiniteMagnitude)
        
        for i in 1...arrayOfStrings.count - 2 {
          
          let label = self.boldLabel(stringOfLabel: arrayOfStrings[i], frameOfLabel: frameForLabels)
          
          self.mainScrollView.addSubview(label)
          
          frameForLabels = CGRect.init(x: frameForLabels.origin.x,
                                       y: label.frame.origin.y + label.frame.size.height + (30.0 * UtilityManager.sharedInstance.conversionHeight),
                                       width: label.frame.size.width,
                                       height: CGFloat.greatestFiniteMagnitude)
          
          finalHeight = finalHeight + label.frame.size.height + (30.0 * UtilityManager.sharedInstance.conversionHeight)
          
        }
        
        let lastLabel = self.normalLabel(stringOfLabel: arrayOfStrings.last!, frameOfLabel: frameForLabels)
        self.mainScrollView.addSubview(lastLabel)
        
        finalHeight = finalHeight + lastLabel.frame.size.height + (30.0 * UtilityManager.sharedInstance.conversionHeight)
        
      } else
        
        if arrayOfStrings.count == 1 {
          
          let firstLabel = self.normalLabel(stringOfLabel: arrayOfStrings.first!, frameOfLabel: frameForFirstString)
          
          self.mainScrollView.addSubview(firstLabel)
          
          finalHeight = finalHeight + firstLabel.frame.size.height + (30.0 * UtilityManager.sharedInstance.conversionHeight)
          
        } else
      
        if arrayOfStrings.count == 2 {
            
          let firstLabel = self.normalLabel(stringOfLabel: arrayOfStrings.first!, frameOfLabel: frameForFirstString)
          
          self.mainScrollView.addSubview(firstLabel)
          
          let frameForSecondLabel = CGRect.init(x: frameForFirstString.origin.x,
                                                y: firstLabel.frame.origin.y + firstLabel.frame.size.height,
                                            width: frameForFirstString.size.width,
                                           height: CGFloat.greatestFiniteMagnitude)
          
          let secondLabel = self.normalLabel(stringOfLabel: arrayOfStrings.last!, frameOfLabel: frameForSecondLabel)
          
          self.mainScrollView.addSubview(secondLabel)
          
          finalHeight = finalHeight + firstLabel.frame.size.height + secondLabel.frame.size.height + (60.0 * UtilityManager.sharedInstance.conversionHeight)
          
        }
      
    }
    
    if finalHeight > mainScrollView.contentSize.height {
      
      let finalContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                        height: finalHeight)
      
      mainScrollView.contentSize = finalContentSize
      
    }
    
  }
  
  private func normalLabel(stringOfLabel: String, frameOfLabel: CGRect) -> UILabel {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: frameOfLabel.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    let label = UILabel.init(frame: frameForLabel)
    label.numberOfLines = 0
    label.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.black
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: stringOfLabel,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    label.attributedText = stringWithFormat
    label.sizeToFit()
    let newFrame = CGRect.init(x: frameOfLabel.origin.x,
                               y: frameOfLabel.origin.y,
                               width: frameOfLabel.size.width,
                               height: label.frame.size.height)
    label.frame = newFrame
    
    return label
    
  }
  
  private func boldLabel(stringOfLabel: String, frameOfLabel: CGRect) -> UILabel {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: frameOfLabel.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    let label = UILabel.init(frame: frameForLabel)
    label.numberOfLines = 0
    label.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Bold",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.black
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: stringOfLabel,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    label.attributedText = stringWithFormat
    label.sizeToFit()
    let newFrame = CGRect.init(x: frameOfLabel.origin.x,
                               y: frameOfLabel.origin.y,
                               width: frameOfLabel.size.width,
                               height: label.frame.size.height)
    label.frame = newFrame
    
    return label
    
  }
  
  @objc private func backButtonPressed() {
    
    _ = self.navigationController?.popViewController(animated: true)
    
  }

  @objc private func profileButtonPressed() {
    
    let profileScreen = ProfileViewController()
    self.navigationController?.pushViewController(profileScreen, animated: true)
    
  }
  
}
