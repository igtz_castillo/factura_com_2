//
//  Currency.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 11/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class Currency {

  var name: String! = nil
  var id: String! = nil
  
  init(newId: String, newName: String) {
    
    id = newId
    name = newName
    
  }
  
}
