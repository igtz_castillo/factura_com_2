//
//  CreatePartForGenericCFDIViewController.swift
//  FacturaCom
//
//  Created by Israel Gutiérrez Castillo on 25/10/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation
import UIKit

protocol CreatePartForGenericCFDIViewControllerDelegate {
  
  func parteJustCreated()
  
}

class CreatePartForGenericCFDIViewController: UIViewController, ButtonToActivePickerViewDelegate, CustomTextFieldWithTitleViewDelegate, UITextFieldDelegate {
  
  private let kInventedIdOfConcept: Int = 666
  private let parteToCreate: Parte = Parte.init()
  private var conceptToAddParts: Concepto! = nil
  private var arrayOfButtonsToShowPickerView: [ButtonToActivePickerView]! = [ButtonToActivePickerView]()
  private var arrayOfUnits: Array<Unity>! = Array<Unity>()
  private var productServiceSelected: ProductService! = nil
  
  private var mainScrollView: UIScrollView! = nil
  private var partInfoLabel: LabelSeparator! = nil
  
  private var conceptOptionsButton: ButtonToActivePickerView! = nil
  private var satCodeTextField: CustomTextFieldWithTitleView! = nil
  private var skuCodeTextField: CustomTextFieldWithTitleView! = nil
  private var quantityTextField: CustomTextFieldWithTitleView! = nil
  private var unityButton: ButtonToActivePickerView! = nil
  private var unityCodeTextField: CustomTextFieldWithTitleView! = nil
  private var priceTextField: CustomTextFieldWithTitleView! = nil
  
  private var createParteButton: UIButton! = nil
  private var importeLabel: LabelForAdditions! = nil
  
  var delegate: CreatePartForGenericCFDIViewControllerDelegate?

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  init(newConceptoToAddParts: Concepto, newProductServiceSelected: ProductService, newArrayOfUnits: Array<Unity>) {
    
    conceptToAddParts = newConceptoToAddParts
    productServiceSelected = newProductServiceSelected
    arrayOfUnits = newArrayOfUnits
    
    super.init(nibName: nil, bundle: nil)
    self.initValuesOfPartToCreate()
    
  }
  
  private func initValuesOfPartToCreate() {
    
    self.parteToCreate.productService = self.productServiceSelected
    self.parteToCreate.claveSAT = self.productServiceSelected.claveProdServ
    self.parteToCreate.parteSKU = self.productServiceSelected.sku
    self.parteToCreate.unityCode = self.productServiceSelected.claveUnidad
    
  }
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.white
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(viewTapped))
    tapGesture.numberOfTouchesRequired = 1
    self.view.addGestureRecognizer(tapGesture)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.editNavigationController()
    self.initMainScrollView()
    self.initPartInfoLabel()
//    self.initConceptOptionsButton()
    self.initSatCodeTextField()
    self.initSkuCodeTextField()
    self.initQuantityTextField()
    self.initUnityButton()
    self.initUnityCodeTextField()
    self.initPriceTextField()
    self.initImporteLabel()
    self.initCreateParteButton()
    
  }
  
  private func editNavigationController() {
    
    self.changeBackButtonItem()
    self.changeNavigationBarTitle()
    self.changeNavigationRigthButtonItem()
    
  }
  
  private func changeBackButtonItem() {
    
    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
  }
  
  private func changeNavigationBarTitle() {
    
    self.title = ViewControllersConstants.CreateGenericCFDI.createGenericCFDITitleText
    
    let titleLabel = UILabel.init(frame: CGRect.zero)
    
    let font = UIFont(name: "SFUIText-Regular",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.CreateGenericCFDI.createGenericCFDITitleText,
      attributes:[NSFontAttributeName:font!,
                  NSParagraphStyleAttributeName:style,
                  NSForegroundColorAttributeName:color
      ]
    )
    titleLabel.attributedText = stringWithFormat
    titleLabel.sizeToFit()
    self.navigationItem.titleView = titleLabel
    
  }
  
  private func changeNavigationRigthButtonItem() {
    
    let rightButton = UIBarButtonItem(title: "",
                                      style: UIBarButtonItemStyle.plain,
                                      target: nil,
                                      action: nil)
    
    let fontForButtonItem =  UIFont(name: "SFUIText-Regular",
                                    size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    
    let attributesDict: [String:AnyObject] = [NSFontAttributeName: fontForButtonItem!,
                                              NSForegroundColorAttributeName: UIColor.orange
    ]
    
    rightButton.setTitleTextAttributes(attributesDict, for: .normal)
    
    self.navigationItem.rightBarButtonItem = rightButton
    
  }
  
  private func initMainScrollView() {
    
    mainScrollView = UIScrollView.init(frame: self.view.frame)
    
    if UtilityManager.sharedInstance.isIpad() == false {
      
      let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                       height: mainScrollView.frame.size.height + (95.0 * UtilityManager.sharedInstance.conversionHeight))
      
      mainScrollView.contentSize = newContentSize
      
    }
    
    self.view.addSubview(mainScrollView)
    
  }
  
  private func initPartInfoLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: mainScrollView.frame.size.width,
                                    height: 56.0 * UtilityManager.sharedInstance.conversionHeight)
    
    partInfoLabel = LabelSeparator.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.CreatePartForGenericCFDI.partInfoLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    partInfoLabel.attributedText = stringWithFormat
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: partInfoLabel.frame.size.width,
                               height: partInfoLabel.frame.size.height)
    partInfoLabel.frame = newFrame
    partInfoLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(partInfoLabel)
    
  }
  
  private func initConceptOptionsButton() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: partInfoLabel.frame.origin.y + partInfoLabel.frame.size.height,
                                     width: mainScrollView.frame.size.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let newOption = Option(id: String(kInventedIdOfConcept), name: conceptToAddParts.conceptDescription, type: "")
    
    conceptOptionsButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                     newTitle: ViewControllersConstants.CreatePartForGenericCFDI.conceptOptionsButtonsText,
                                                     newArrayOfOptions: [newOption])
    conceptOptionsButton.delegate = self
    conceptOptionsButton.drawBottomBorder()
    
    if conceptToAddParts.conceptDescription != nil {
      
      _ = conceptOptionsButton.selectElementWithName(nameToLookFor: conceptToAddParts.conceptDescription)
      
    }
    
    self.mainScrollView.addSubview(conceptOptionsButton)
    self.arrayOfButtonsToShowPickerView.append(conceptOptionsButton)
    
  }
  
  private func initSatCodeTextField() {
    
//    let frameForView = CGRect.init(x: 0.0,
//                                   y: conceptOptionsButton.frame.origin.y + conceptOptionsButton.frame.size.height,
//                                   width: mainScrollView.frame.size.width,
//                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let frameForView = CGRect.init(x: 0.0,
                                     y: partInfoLabel.frame.origin.y + partInfoLabel.frame.size.height,
                                     width: mainScrollView.frame.size.width,
                                     height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    satCodeTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                              title: ViewControllersConstants.CreatePartForGenericCFDI.satCodeTextFieldText,
                                                              image: nil,
                                                              colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                              positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                              positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    satCodeTextField.mainTextField.textColor = UIColor.black
    satCodeTextField.mainTextField.placeholder = ViewControllersConstants.CreatePartForGenericCFDI.satCodeTextFieldText
    satCodeTextField.backgroundColor = UIColor.white
    satCodeTextField.delegate = self
    satCodeTextField.mainTextField.delegate = self
    satCodeTextField.mainTextField.tag = 1
    satCodeTextField.mainTextField.returnKeyType = .next
    satCodeTextField.mainTextField.text = self.productServiceSelected.claveProdServ
    self.mainScrollView.addSubview(satCodeTextField)
    
  }
  
  private func initSkuCodeTextField() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: satCodeTextField.frame.origin.y + satCodeTextField.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    skuCodeTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                         title: ViewControllersConstants.CreatePartForGenericCFDI.skuTextFieldText,
                                                         image: nil,
                                                         colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                         positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                         positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    skuCodeTextField.mainTextField.textColor = UIColor.black
    skuCodeTextField.mainTextField.placeholder = ViewControllersConstants.CreatePartForGenericCFDI.skuTextFieldText
    skuCodeTextField.backgroundColor = UIColor.white
    skuCodeTextField.delegate = self
    skuCodeTextField.mainTextField.delegate = self
    skuCodeTextField.mainTextField.tag = 2
    skuCodeTextField.mainTextField.returnKeyType = .next
    skuCodeTextField.mainTextField.text = self.productServiceSelected.sku
    self.mainScrollView.addSubview(skuCodeTextField)
    
  }
  
  private func initQuantityTextField() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: skuCodeTextField.frame.origin.y + skuCodeTextField.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    quantityTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                         title: ViewControllersConstants.CreatePartForGenericCFDI.quantityTextFieldText,
                                                         image: nil,
                                                         colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                         positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                         positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    quantityTextField.mainTextField.textColor = UIColor.black
    quantityTextField.mainTextField.placeholder = ViewControllersConstants.CreatePartForGenericCFDI.quantityTextFieldText
    quantityTextField.backgroundColor = UIColor.white
    quantityTextField.delegate = self
    quantityTextField.mainTextField.delegate = self
    quantityTextField.mainTextField.tag = 3
    quantityTextField.mainTextField.returnKeyType = .next
    self.mainScrollView.addSubview(quantityTextField)
    
  }
  
  private func initUnityButton() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: quantityTextField.frame.origin.y + quantityTextField.frame.size.height,
                                     width: mainScrollView.frame.size.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var arrayOfOptions = Array<Option>()
    
    for unit in arrayOfUnits {
      
      let newOption = Option.init(id: unit.id, name: unit.name, type: unit.name)
      arrayOfOptions.append(newOption)
      
    }
    
    unityButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                         newTitle: ViewControllersConstants.CreatePartForGenericCFDI.unityButtonText,
                                                         newArrayOfOptions: arrayOfOptions)
    unityButton.delegate = self
    unityButton.drawBottomBorder()
    
    _ = unityButton.selectElementWithName(nameToLookFor: self.productServiceSelected.unidad)
    
    self.mainScrollView.addSubview(unityButton)
    self.arrayOfButtonsToShowPickerView.append(unityButton)
    
  }
  
  private func initUnityCodeTextField() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: unityButton.frame.origin.y + unityButton.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    unityCodeTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                          title: ViewControllersConstants.CreatePartForGenericCFDI.unityCodeTextField,
                                                          image: nil,
                                                          colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                          positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                          positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    unityCodeTextField.mainTextField.textColor = UIColor.black
    unityCodeTextField.mainTextField.placeholder = ViewControllersConstants.CreatePartForGenericCFDI.unityCodeTextField
    unityCodeTextField.backgroundColor = UIColor.white
    unityCodeTextField.delegate = self
    unityCodeTextField.mainTextField.delegate = self
    unityCodeTextField.mainTextField.tag = 4
    unityCodeTextField.mainTextField.returnKeyType = .next
    unityCodeTextField.mainTextField.text = self.productServiceSelected.claveUnidad
    unityCodeTextField.mainTextField.isUserInteractionEnabled = false
    
    self.mainScrollView.addSubview(unityCodeTextField)
    
  }
  
  private func initPriceTextField() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: unityCodeTextField.frame.origin.y + unityCodeTextField.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    priceTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                           title: ViewControllersConstants.CreatePartForGenericCFDI.priceTextFieldText,
                                                           image: nil,
                                                           colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                           positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                           positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    priceTextField.mainTextField.textColor = UIColor.black
    priceTextField.mainTextField.placeholder = ViewControllersConstants.CreatePartForGenericCFDI.priceTextFieldText
    priceTextField.backgroundColor = UIColor.white
    priceTextField.delegate = self
    priceTextField.mainTextField.delegate = self
    priceTextField.mainTextField.tag = 5
    priceTextField.mainTextField.returnKeyType = .next
    self.mainScrollView.addSubview(priceTextField)
    
  }
  
  private func initImporteLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: priceTextField.frame.origin.y + priceTextField.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    importeLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
//    let finalImporte: Double = Double(quantityTextField.mainTextField.text!)! * Double(priceTextField.mainTextField.text!)!
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.GenericCFDIDetail.importeLabelText,// + String(finalImporte),
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    importeLabel.attributedText = stringWithFormat
    importeLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    self.mainScrollView.addSubview(importeLabel)
    
  }
  
  private func initCreateParteButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: (19.0 * UtilityManager.sharedInstance.conversionWidth),
                                     y: importeLabel.frame.origin.y + importeLabel.frame.size.height + (11.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    createParteButton = UIButton.init(frame: frameForButton)
    createParteButton.addTarget(self,
                           action: #selector(createParteButtonPressed),
                           for: .touchUpInside)
    createParteButton.backgroundColor = UIColor.orange
    createParteButton.setBackgroundColor(color: UIColor.init(red: 255.0/255.0, green: 164.0/255.0, blue: 78.0/255.0, alpha: 1.0), forState: .highlighted)
    createParteButton.setAttributedTitle(stringWithFormat, for: .normal)
    createParteButton.contentHorizontalAlignment = .left
    
    createParteButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.CreatePartForGenericCFDI.createParteButtonText))
    
    self.mainScrollView.addSubview(createParteButton)
    
  }
  
  @objc private func createParteButtonPressed(sender: AnyObject) {
    
    if sender as? UIButton != nil  {
      
      if (sender as! UIButton) == createParteButton {
        
        self.getAllDataSelected()
        
      }
      
    }
    
  }
  
  private func createGenericLabelForButton(text: String) -> UILabel {
    
    let frameForLabel = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 200.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    let genericLabel = UILabel.init(frame: frameForLabel)
    genericLabel.numberOfLines = 0
    genericLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: text,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    genericLabel.attributedText = stringWithFormat
    genericLabel.sizeToFit()
    let newFrame = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                               y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                               width: genericLabel.frame.size.width,
                               height: genericLabel.frame.size.height)
    genericLabel.frame = newFrame
    
    return genericLabel
    
  }
  
  private func changeTextOfImporteLabel(newString: String) {
    
    if importeLabel != nil {
      
      self.importeLabel.removeFromSuperview()
      self.importeLabel = nil
      
    }
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: priceTextField.frame.origin.y + priceTextField.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    importeLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: newString,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    importeLabel.attributedText = stringWithFormat
    importeLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(importeLabel)
    
  }
  
  @objc private func viewTapped() {
    
    self.view.endEditing(true)
    
  }
  
  private func getAllDataSelected() {
    
//    if self.getConceptOptionSelected() == false {
//
//      let alertController = UIAlertController(title: "Error",
//                                            message: "Por favor elige un concepto",
//                                     preferredStyle: UIAlertControllerStyle.alert)
//
//      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
//      alertController.addAction(cancelAction)
//
//      let actualController = UtilityManager.sharedInstance.currentViewController()
//      actualController.present(alertController, animated: true, completion: nil)
//
//    } else
    
    if self.getUnityButtonOptionSelected() == false {
        
        let alertController = UIAlertController(title: "Error",
                                                message: "Por favor elige una unidad",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
    } else
    
    if self.getSATCodeWroteDown() == false {
        
        let alertController = UIAlertController(title: "Error",
                                                message: "Por favor escribe una clave del SAT",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
    } else
    
    if self.getSKUWroteDown() == false {
        
      let alertController = UIAlertController(title: "Error",
                                            message: "Por favor escribe una clave SKU",
                                     preferredStyle: UIAlertControllerStyle.alert)
        
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
        
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
        
    } else
    
    if self.getQuantityWroteDown() == false {
        
      let alertController = UIAlertController(title: "Error",
                                            message: "Por favor escribe una cantidad",
                                     preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
        
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
        
    } else
      
    if self.getUnityCodeWroteDown() == false {
        
      let alertController = UIAlertController(title: "Error",
                                            message: "Por favor escribe un código de unidad",
                                     preferredStyle: UIAlertControllerStyle.alert)
        
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
        
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
        
    } else
    
    if self.getPriceWroteDown() == false {
        
      let alertController = UIAlertController(title: "Error",
                                            message: "Por favor escribe un precio",
                                     preferredStyle: UIAlertControllerStyle.alert)
        
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
        
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
        
    } else {
      
      //All the information is correct
      
      self.conceptToAddParts.arrayOfParts.append(parteToCreate)
      self.delegate?.parteJustCreated()
      self.navigationController?.popViewController(animated: true)
      
    }
    
  }
  
  //Return true if found a value
  private func getConceptOptionSelected() -> Bool {
    
    let conceptOptionSelected = conceptOptionsButton.getValueSelected()
    
    if conceptOptionSelected != nil {
      
      for concept in [conceptToAddParts] {
        
        if concept!.conceptDescription == conceptOptionSelected!.name {
          
          //in this specific case we only have one concept, that is the concept we have to add the Part, so conceptToAddParts is the concept that is already selected
          
          return true
          
        }
        
      }
  
      //supposedly never happen
      return false
      
    } else {
      
      return false
      
    }
    
  }
  
  private func getUnityButtonOptionSelected() -> Bool {
    
    let unityOptionSelected = unityButton.getValueSelected()
    
    if unityOptionSelected != nil {
      
      for unit in arrayOfUnits {
        
        if unit.name == unityOptionSelected!.name {
          
          self.conceptToAddParts.unit = unit
          self.parteToCreate.unity = unit
          
          return true
          
        }
        
      }
      
      //supposedly never happen
      return false
      
    } else {
      
      return false
      
    }
    
  }
  
  private func getSATCodeWroteDown() -> Bool {
    
    parteToCreate.claveSAT = satCodeTextField.mainTextField.text!
    
    return UtilityManager.sharedInstance.isValidText(testString: satCodeTextField.mainTextField.text!)
    
  }
  
  private func getSKUWroteDown() -> Bool {
    
    parteToCreate.parteSKU = skuCodeTextField.mainTextField.text!
    
    return UtilityManager.sharedInstance.isValidText(testString: skuCodeTextField.mainTextField.text!)
    
  }
  
  private func getQuantityWroteDown() -> Bool {
    
    if quantityTextField.mainTextField.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil && (quantityTextField.mainTextField.text!.characters.count > 0) {
      
      parteToCreate.quantity = Double(quantityTextField.mainTextField.text!)!
      
      return true
    
    }

    return false
    
  }
  
  private func getUnityCodeWroteDown() -> Bool {
    
    if UtilityManager.sharedInstance.isValidText(testString: unityCodeTextField.mainTextField.text!) && !unityCodeTextField.mainTextField.text!.isEmpty {
      
      parteToCreate.unityCode = unityCodeTextField.mainTextField.text!
      
      return true
      
    }
    
    return false
    
  }
  
  private func getPriceWroteDown() -> Bool {
    
    if priceTextField.mainTextField.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil && (priceTextField.mainTextField.text!.characters.count > 0) {
      
      parteToCreate.partePrice = Double(priceTextField.mainTextField.text!)!
      
      return true
      
    }
    
    return false
    
  }
  
  private func calculateAndChangeImportLabel() {
    
    if (quantityTextField.mainTextField.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil) && (priceTextField.mainTextField.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil) && (priceTextField.mainTextField.text!.characters.count > 0) && (quantityTextField.mainTextField.text!.characters.count > 0) {
      
      parteToCreate.finalImport = Double(quantityTextField.mainTextField.text!)! * Double(priceTextField.mainTextField.text!)!
      self.changeTextOfImporteLabel(newString: ViewControllersConstants.GenericCFDIDetail.importeLabelText + " \(parteToCreate.finalImport)")
      
    }
    
  }
  
  
  //MARK: - ButtonToActivePickerViewDelegate
  
  func lastButtonToShowPickerView(sender: ButtonToActivePickerView) {
    
    
    
  }
  
  
  func selectActionFinalized(sender: ButtonToActivePickerView) {
    
    
    
  }
  
  //MARK: CustomTextFieldWithTitleViewDelegate
  
  func customTextFieldSelected(sender: CustomTextFieldWithTitleView) {
    
    
    
  }
  
  //MARK: UITextFieldDelegate
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    
    if (textField == quantityTextField.mainTextField) || (textField == priceTextField.mainTextField) {
      
      self.calculateAndChangeImportLabel()
      
    }
    
  }
  
}



