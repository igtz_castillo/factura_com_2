//
//  User.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 24/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class User {
  
  static var session = User()
  
  var uidAccount: String! = nil
  var urlLogo: String?
  var name: String! = nil
  var rfc: String! = nil
  var apiKey: String! = nil
  var secretKey: String! = nil
  var email: String! = ""
  
  var actualUsingCompany: Company! = Company.init()
  var arrayOfCompanies: Array<Company>! = Array<Company>()
  
  func resetUserSession() {
    
    uidAccount = nil
    urlLogo = nil
    name = nil
    rfc = nil
    apiKey = nil
    secretKey = nil
    
    actualUsingCompany = Company.init()
    arrayOfCompanies = Array<Company>()
    
  }
  
}
