//
//  Company.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 03/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class Company {
  
  var uidAccount: String! = nil
  var urlLogo: String?
  var name: String! = nil
  var rfc: String! = nil
  var apiKey: String! = nil
  var secretKey: String! = nil
  
  // the next info is from detail
  
  var regimenFiscal: String! = nil
  var calle: String! = nil
  var exterior: String! = nil
  var interior: String! = nil
  var colonia: String! = nil
  var codPos: String! = nil
  var ciudad: String! = nil
  var estado: String! = nil
  var email: String! = nil
  
  init(){}
  
}

