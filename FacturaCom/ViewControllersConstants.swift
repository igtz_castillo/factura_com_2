//
//  ViewControllersConstants.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 29/03/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

class ViewControllersConstants {
  
  enum LoginViewController {
    
    static let emailTextFieldDescriptionText = "Email"
    static let passwordTextFieldDescriptionText = "Contraseña"
    static let missedPasswordText = "¿Olvidaste tu contraseña"
    static let loginButtonText = "Entrar"
    static let createAccountButtonText = "Crea cuenta"
    static let dontHaveAccountLabelText = "¿No tienes una cuenta?\nIngresa a Factura.com desde tu navegador\ny contrata tu plan. ¡Es fácil y rápido! "
    
  }
  
  enum MissedPasswordViewController {
    
    static let leftButtonText = "Atrás"
    static let descriptionLabelText = "No te preocupes, ingresa el correo electrónico con el que te registraste y te ayudamos a recuperar tu contraseña"
    static let sendMailButtonText = "Enviar"
    
  }
  
  enum CreateAccountViewController {
    
    static let backButtonText = "Ya tengo cuenta"
    static let tryButtonText = "Probar ahora"
    
  }
  
  enum PetitionSentCorrectlyViewController {
    
    static let leftButtonText = "Atrás"
    static let descriptionLabelText = "Hemos enviado el mail a tu correo"
    static let sendMailButtonText = "ok"
    
  }
  
  enum MainTabController {
    
    static let facturasButtonText = "Facturas"
    static let recibosButtonText = "Recibos"
    static let notasButtonText = "Notas"
    static let cartasButtonText = "Cartas"
    static let menuButtonText = "Menú"
    
    static let cfdisButtonBarItemText = "CFDI's"
    static let clientsButtonBarItemText = "Clientes"
    static let productsButtonBarItemText = "Productos"
    static let companiesButtonBarItemText = "Empresas"
    static let myAccountButtonBarItemText = "Mi cuenta"
    
  }
  
  enum CFDIsMenuViewController {
    
    static let cdfisMenuViewControllerText = "CFDI's Menú"
    
  }
  
  enum CreateGenericCFDI {
    
    static let createGenericCFDITitleText = "Crea un CFDI"
    static let stampingDateButtonTitleText = "Fecha de trimbrado"
    static let useOfCFDIButtonTitleText = "Uso CFDI"
    static let relatedInvoiceButtonTitleText = "Factura relacionada"
    static let typeOfInvoiceRelationshipButtonTitleText = "Tipo de relación"
    static let typeOfCFDIRelationshipButtonTitleText = "CFDI's relacionados"
    
  }
  
  enum CreatePartForGenericCFDI {
    
    static let partInfoLabelText = "Información general de la parte"
    static let conceptOptionsButtonsText = "Concepto"
    static let satCodeTextFieldText = "Clave SAT"
    static let skuTextFieldText = "SKU"
    static let quantityTextFieldText = "Cantidad"
    static let unityButtonText = "Unidad"
    static let unityCodeTextField = "Clave Unidad"
    static let priceTextFieldText = "Precio por unidad"
    static let createParteButtonText = "Agregar parte"
    
  }
  
  enum NameOfCFDIScreen {
    
    static let facturasTableTitleText = "Facturas"
    static let recibosTableTitleText = "Recibos de honorarios"
    static let notasTableTitleText = "Notas de Crédito"
    static let cartasTableTitleText = "Cartas Porte"
    static let menuTableTitleText = "Menú"
    
  }
  
  enum NameOfScreen {
    
    static let empresasTableText = "Empresas"
    static let clientsTableText = "Clientes"
    static let helpTableText = "Ayuda"
    static let profileControllerText = "Mi cuenta" 
    
  }
  
  enum HeaderTableViewCell {
    
    static let folioLabelText = "Folio"
    static let estadoLabelText = "Estado"
    static let fechaLabelText = "Fecha"
    static let razonSocialLabelText = "Razón social"
    
  }
  
  enum FacturaDetailViewcontroller {
    
    static let leftNavigationButtonText = "< Facturas"
    static let rightNavigationButtonText = "Siguiente"
    
  }
  
  enum ReciboDetailViewController {
    
    static let leftNavigationButtonText = "Recibos"
    static let rightNavigationButtonText = "Siguiente"
    
  }
  
  enum GenericCFDIDetail {
    
    static let rfcLabelText = "RFC"
    static let folioLabelText = "Folio"
    static let ordenPedidoLabelText = "Orden/Pedido"
    static let ivaLabelText = "IVA"
    static let descuentoLabelText = "Descuento"
    static let subtotalLabelText = "Subtotal"
    static let totalLabelText = "Total"
    static let pagadoLabelText = "Pagado"
    static let statusLabelText = "Status"
    static let fechaCreacionLabelText = "Fecha de creación"
    
    static let downloadPDFButtonText = "Descargar PDF"
    static let downloadXMLButtonText = "Descargar XML"
    static let cancelButtonText = "Cancelar"
    static let sendToClientButtonText = "Mandar al cliente"
    static let generateCFDI = "Generar factura"
    
    static let conceptOptionsButtonTitleText = "Concepto"
    
    static let unityCodeTextFieldText = "Clave unidad"
    static let priceTextFieldText = "Precio"
    
    static let importeLabelText = "Importe: $"
    
  }
  
  enum NotaCreateViewController {
    
    static let backButtonNavigationBarText = "< Recibos"
    static let titleNavigationBarText = "Crear nota nueva"
    static let nextButonNavigationBarText = "Siguiente"
    
  }
  
  enum ReciboCreateViewController {
    
    static let backButtonNavigationBarText = "< Recibos"
    static let titleNavigationBarText = "Crear recibo nuevo"
    static let nextButonNavigationBarText = "Siguiente"
    
  }
  
  enum CartaCreateViewController {
    
    static let titleNavigationBarText = "Crear carta nueva"
    static let nextButonNavigationBarText = "Siguiente"
    
  }
  
  enum FacturaCreateViewController {
    
    static let backButtonNavigationBarText = "< Facturas"
    static let titleNavigationBarText = "Crear factura nueva"
    
    //those are for the other create CFDI's viewcontrollers
    static let titleNavigationBarTextForFees = "Crear recibo nuevo"
    static let titleNavigationBarTextForNotes = "Crear nota nueva"
    static let titleNavigationBarTextForPorte = "Crear carta nueva"
    //
    
    static let nextButonNavigationBarText = "Siguiente"
    
    static let generalInfoLabelText = "Datos generales"
    
    static let typeOfCFDIButtonText = "Tipo de CFDI"
    static let typeOfCFDIActionSheetMessage = "Elige una opción"
    
    static let expeditionPlaceButtontext = "Lugar de expedición"
    static let expeditionPlaceActionSheetMessageText = "Elige una opción"
    
    static let pickerViewCancelButtonText = "Cancelar"
    static let pickerViewSelectButtonText = "Elegir"
    static let clientOrRFCLabelText = "Cliente o RFC genérico"
    
    static let remissionLabelText = "Datos de remisión"
    
    static let selectClientButtonText = "Selecciona cliente"
    static let paymentWayButtonText = "Forma de pago"
    static let paymentMethodButtonText = "Método de pago"
    static let numbersOfCreditCardTextFieldText = "Ingresa los últimos 4 dígitos de tu tarjeta"
    
    static let typeOfMoneyButtonText = "Moneda"
    static let exchangeRateTextFieldText = "Tipo de cambio"
    
    static let serieButtonText = "Serie"
    
    static let decimalNumberButtonText = "Número de decimales"
    
    
    static let orderTextFieldText = "# Orden/Pedido"
    
  }
  
  enum FacturaCreateSecondPartViewController {
    
    static let backButtonNavigationBarText = "< Crear"
    static let titleNavigationBarText = "Crear factura nueva"
    static let nextButonNavigationBarText = ""
    
    static let notionsLabelText = "Conceptos"
    
    static let unityButtonText = "Unidad"
    static let notionTextFieldText = "Concepto"
    
    static let quantityTextFieldText = "Cantidad"
    static let unityCostTextFieldText = "$ Precio Unitario"
    static let ivatTextFieldText = "IVA"
    static let discountTextFieldText = "Descuento %"
    
    static let taxesLabelSwitchText = "Impuestos"
    static let customHouseLabelSwitchText = "Aduana"
    
    static let discountAdditionLabelText = "Descuento"
    static let discountAdditionValueLabelText = "- $"
    static let subTotalAdditionLabelText = "Subtotal"
    static let subtotalValueAdditionLabelText = "$"
    
    static let secondPartSubTotalAdditionLabelText = "Subtotal: $"
    static let ivaAdditionLabelText = "IVA"
    static let ishAdditionLabelText = "ISH"
    static let iepsAdditionLabelText = "IEPS"
    static let retIvaAdditionLabelText = "RET IVA"
    static let retIsrAdditionLabelText = "RET ISR"
    static let retIvaPorteAdditionLabelText = "RET IVA PORTE"
    static let valueAdditionLabelText = "+ $"
    static let totalAdditionLabelText = "Total: $"
    
    static let titleForTaxesArea = "Detalle de impuestos"
    static let typeOfApplyingTaxes = "¿Impuestos incluidos en precio?"
    static let ivaLabelSwitchText = "IVA 16%"
    static let ishLabelSwitchText = "ISH"
    static let iepsLabelSwitchText = "IEPS"
    static let retIVALabelSwitchText = "RET. IVA"
    static let retISRLabelSwitchText = "RET. ISR"
    
    static let commentsTextFieldText = "Comentarios"
    
    static let sendViaMailLabelSwitchText = "Mandar por mail"
    static let payedLabelSwitchText = "Documento pagado/abonado"
    
  }
  
  enum FacturaCreateConceptoForGenericCFDI {
    
    static let typeOfDisccountSeparatorText = "Aplicar descuento"
    static let separatorForAduanaText = "Información de aduana"
    static let aduanaSwitchText = "Pedimento"
    
  }
  
  enum ProfileViewController {
    
    static let navigationtitleText = "Mi Cuenta"
    static let backButtonNavigationText = "< Menú"
    static let nameTextFieldText = "Nombre"
    static let emailTextFieldText = "Email"
    static let passwordTextFieldText = "Contraseña"
    
    static let descriptionPasswordLabelText = "Puedes o no cambiar tu contraseña"
    static let updateProfileButtonText = "Cambiar contraseña"
    
  }
  
  enum ClientDetailViewController {
    
    static let backButtonNavigationBarText = "< Menú"
    
    static let clientDataLabelText = "Datos del cliente /Creado"
    static let razonSocialTextFieldText = "Razón Social"
    static let rfcTextFieldText = "RFC"
    static let regimenFiscalText = "Régimen fiscal"
    static let calleTextFieldText = "Calle"
    static let numeroExteriorTextFieldText = "Número exterior"
    static let numeroInteriorTextFieldText = "Número interior"
    static let coloniaTextFieldText = "Colonia"
    static let codigoPostalTextFieldText = "Código Postal"
    static let ciudadTextFieldText = "Ciudad"
    static let estadoTextFieldText = "Estado"
    static let localidadTextFieldText = "Localidad"
    static let paisTextFieldText = "País"
    
    static let secondSeparatorLabelText = "Datos de contacto"
    
    static let clientNameTextFieldText = "Nombre(s) de tu cliente"
    static let clientLastNameTextFieldText = "Apellidos"
    static let firstEmailToSendCFDITextFieldText = "E-mail para envío de documentos CFDI*"
    static let secondEmailToSendCFDITextFieldText = "E-mail para envío de documentos CFDI* (Opcional)"
    static let thirdEmailToSendCFDITextFieldText = "E-mail para envío de documentos CFDI* (Opcional)"
    static let contactPhoneTextFieldText = "Teléfono de contacto"
    
    static let bankDataSeparatorLabelText = "Datos bancarios"
        
    static let accountNumberTextFieldText = "Número de cuenta"
    static let lastUpdateLabelText = "Última actualización: "
    
    static let updateProfileButtonText = "Actualizar perfil"

  }
  
  enum CreateExpeditionPlaceViewController {
    
    static let branchNameText = "Nombre de la sucursal"
    
  }
  
  enum CreateClientDetailViewController {
    
    static let backButtonNavigationBarText = "< Menú"
    
    static let clientDataLabelText = "Datos fiscales (los * son obligatorios)"
    static let razonSocialTextFieldText = "Razón Social *"
    static let rfcTextFieldText = "RFC *"
    static let calleTextFieldText = "Calle *"
    static let numeroExteriorTextFieldText = "Número exterior *"
    static let numeroInteriorTextFieldText = "Número interior"
    static let coloniaTextFieldText = "Colonia *"
    static let codigoPostalTextFieldText = "Código Postal *"
    static let ciudadTextFieldText = "Ciudad/Delegación *"
    static let estadoTextFieldText = "Estado *"
    static let localidadTextFieldText = "Localidad *"
    static let paisTextFieldText = "País *"
    static let regimenNumberTextFieldText = "Número de régimen"
    static let fiscalResidenceTextFieldText = "Residencia fiscal"
    
    static let secondSeparatorLabelText = "Datos de contacto"
    
    static let clientNameTextFieldText = "Nombre(s) de tu cliente *"
    static let clientLastNameTextFieldText = "Apellidos *"
    static let firstEmailToSendCFDITextFieldText = "E-mail para envío de documentos CFDI*"
    static let secondEmailToSendCFDITextFieldText = "E-mail para envío de documentos CFDI (Opcional)"
    static let thirdEmailToSendCFDITextFieldText = "E-mail para envío de documentos CFDI (Opcional)"
    static let contactPhoneTextFieldText = "Teléfono de contacto"
    
    static let bankDataSeparatorLabelText = "Datos bancarios"
    
    static let accountNumberTextFieldText = "Número de cuenta"
    static let quantityOfNumbersTextFieldText = "4 dígitos"
    static let lastUpdateLabelText = "Última actualización: "
    
    static let registerClientButtonText = "Registrar"
    
  }
  
  enum HelpTableViewController {
    
    static let howToCreateClientsText = "Crear clientes"
    static let howToCreateCompaniesText = "Crear empresas"
    static let howToCreateCFDIsText = "Crear CFDI'S"
    static let howToCreateProductsText = "Crear productos"
    static let howToCreateFasterCFDIsText = "Crear CFDI'S rápidos"
    static let howToCreateSeriesText = "Crear series"
    static let howToEditClientsText = "Editar clientes"
    static let howToEditCompaniesText = "Editar empresas"
    static let howToEditProductsText = "Editar productos"
    static let howToEditProfileText = "Editar perfil"
    
  }
  
  enum ContactViewController {
    
    static let firstLabelText = "  Nos importa tu opinión, si sucedió algún error o tienes alguna sugerencia o comentario  "
    static let secondLabelText = "¡Escríbenos tu mensaje!"
    static let textEditorText = "Mensaje            "
    static let sendButtonText = "Enviar"
    static let thirdLabelText = "Adolfo López Mateos, 400 piso 8\nGuadalajara Jalisco"
    static let fourthLabelText = "contacto@factura.com"
    
    static let directionToSendEmail = "soporte@factura.com"
    
  }
  
  enum TableViewWithSearchViewController {
    
    static let actionSheetTitleText = "Empresas"
    static let actionSheetMessageText = "Elige la empresa que desees usar"
    
  }
  
  enum CompaniesTableViewController {
    
    static let createCompanyMessage = "Crea tu empresa desde www.factura.com"
    
  }
  
  enum ClientsTableViewController {
    
    static let deleteClientActionText = "Eliminar"
    
  }
  
  enum CreateConceptoViewController {
    
    static let createConceptoButtonText = "Crear concepto"
    
  }
  
}
