//
//  ProfileViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 13/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, CustomTextFieldWithTitleViewDelegate {
  
  private var secondNavBar: UIView! = nil
  private var contactButton: UIButton! = nil
  private var closeSessionButton: UIButton! = nil
  
  private var mainScrollView: UIScrollView! = nil
  
  private var profileImage: UIImageView! = nil
  private var nameLabel: UILabel! = nil
  private var memberSinceLabel: UILabel! = nil
  
  private var companyNameTextField: CustomTextFieldWithTitleView! = nil
  private var companyEmailTextField: CustomTextFieldWithTitleView! = nil
  private var companyPasswordTextField: CustomTextFieldWithTitleView! = nil
  
  private var descriptionPasswordLabel: UILabel! = nil
  private var updateButton: UIButton! = nil
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
    tapGesture.numberOfTapsRequired = 1
    
    self.view.addGestureRecognizer(tapGesture)
      
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.editNavigationController()
    self.initSecondNavBar()
    
    self.getInformationFromServer()
    
  }
  
  private func editNavigationController() {
    
    self.changeBackButtonItem()
    self.changeNavigationBarTitle()
    self.changeNavigationRigthButtonItem()
    
  }
  
  private func changeBackButtonItem() {
    
    self.title = ViewControllersConstants.NameOfScreen.profileControllerText
    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
//    let leftButton = UIBarButtonItem(title: ViewControllersConstants.ProfileViewController.backButtonNavigationText,
//                                     style: UIBarButtonItemStyle.plain,
//                                     target: self,
//                                     action: #selector(backButtonPressed))
//    
//    let fontForButtonItem =  UIFont(name: "SFUIText-Regular",
//                                    size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
//    
//    let attributesDict: [String:AnyObject] = [NSFontAttributeName: fontForButtonItem!,
//                                              NSForegroundColorAttributeName: UIColor.orange
//    ]
//    
//    leftButton.setTitleTextAttributes(attributesDict, for: .normal)
//    
//    self.navigationItem.leftBarButtonItem = leftButton
    
  }

  private func changeNavigationBarTitle() {
    
    if #available(iOS 11.0, *) {
      
      self.navigationItem.title = ViewControllersConstants.NameOfScreen.profileControllerText
      self.navigationItem.largeTitleDisplayMode = .never
      
    } else {
      
      let titleLabel = UILabel.init(frame: CGRect.zero)
      
      let font = UIFont(name: "SFUIText-Regular",
                        size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
      let color = UIColor.white
      let style = NSMutableParagraphStyle()
      style.alignment = NSTextAlignment.center
      
      let stringWithFormat = NSMutableAttributedString(
        string: ViewControllersConstants.NameOfScreen.profileControllerText,
        attributes:[NSFontAttributeName:font!,
                    NSParagraphStyleAttributeName:style,
                    NSForegroundColorAttributeName:color
        ]
      )
      titleLabel.attributedText = stringWithFormat
      titleLabel.sizeToFit()
      self.navigationItem.titleView = titleLabel
      
    }
    
  }
  
  private func changeNavigationRigthButtonItem() {
    
    let rightButton = UIBarButtonItem(title: "",
                                      style: UIBarButtonItemStyle.plain,
                                      target: self,
                                      action: nil)
    
    let fontForButtonItem =  UIFont(name: "SFUIText-Regular",
                                    size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    
    let attributesDict: [String:AnyObject] = [NSFontAttributeName: fontForButtonItem!,
                                              NSForegroundColorAttributeName: UIColor.orange
    ]
    
    rightButton.setTitleTextAttributes(attributesDict, for: .normal)
    
    self.navigationItem.rightBarButtonItem = rightButton
    
  }
  
  private func initSecondNavBar() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y:(self.navigationController?.navigationBar.frame.origin.y)! + (self.navigationController?.navigationBar.frame.size.height)! + UIApplication.shared.statusBarFrame.height + (0.0 * UtilityManager.sharedInstance.conversionHeight),
                               width: UIScreen.main.bounds.width,
                              height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    secondNavBar = UIView.init(frame: frameForView)
    secondNavBar.backgroundColor = UtilityManager.sharedInstance.backgroundColorForSearchBar
    
    self.initContactButton()
    self.initCloseSessionButton()
    
    self.view.addSubview(secondNavBar)
    
  }
  
  private func initContactButton() {
    
    let font = UIFont(name: "SFUIText-Regular",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.orange
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: "Contacto",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: 16.0 * UtilityManager.sharedInstance.conversionWidth,
                                     y: 10.0 * UtilityManager.sharedInstance.conversionWidth,
                                 width: 75.0 * UtilityManager.sharedInstance.conversionWidth,
                                height: 25.0 * UtilityManager.sharedInstance.conversionHeight)
    contactButton = UIButton.init(frame: frameForButton)
    contactButton.addTarget(self,
                            action: #selector(contactButtonPressed),
                            for: .touchUpInside)
    contactButton.backgroundColor = UIColor.clear
    contactButton.setAttributedTitle(stringWithFormat, for: .normal)
    
    self.secondNavBar.addSubview(contactButton)
    
  }
  
  private func initCloseSessionButton() {
    
    let font = UIFont(name: "SFUIText-Regular",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.orange
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: "Cerrar sesión",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: self.view.frame.size.width - (116.0 * UtilityManager.sharedInstance.conversionWidth),
                                     y: 10.0 * UtilityManager.sharedInstance.conversionWidth,
                                     width: 100.0 * UtilityManager.sharedInstance.conversionWidth,
                                     height: 25.0 * UtilityManager.sharedInstance.conversionHeight)
    closeSessionButton = UIButton.init(frame: frameForButton)
    closeSessionButton.addTarget(self,
                            action: #selector(closeSessionButtonPressed),
                            for: .touchUpInside)
    closeSessionButton.backgroundColor = UIColor.clear
    closeSessionButton.setAttributedTitle(stringWithFormat, for: .normal)
    
    closeSessionButton.sizeToFit()
    
    let newFrameForLabel = CGRect.init(x: self.view.frame.size.width - (closeSessionButton.frame.size.width + (16.0 * UtilityManager.sharedInstance.conversionWidth)),
                                       y: closeSessionButton.frame.origin.y,
                                   width: closeSessionButton.frame.size.width,
                                  height: 25.0 * UtilityManager.sharedInstance.conversionHeight)
    closeSessionButton.frame = newFrameForLabel
    
    self.secondNavBar.addSubview(closeSessionButton)
    
  }
  
  private func getInformationFromServer() {
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.requestDataFromActualCompany(actionsToMakeWhenSucceeded: { 
    
      self.initMainScrollView()
      self.initProfileImage()
      self.initNameLabel()
      self.initMemberSinceLabel()
//      self.initCompanyNameTextField()
//      self.initCompanyEmailTextField()
      self.initCompanyPasswordTextField()
      self.initDescriptionPasswordLabel()
      self.initUpdateButton()
      
      UtilityManager.sharedInstance.hideLoader()
      
    }, actionsToMakeWhenFailed: {
    
      UtilityManager.sharedInstance.hideLoader()
    
    })
    
  }
  
  private func initMainScrollView() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: secondNavBar.frame.origin.y + secondNavBar.frame.size.height,
                               width: self.view.frame.size.width,
                              height: self.view.frame.size.height - secondNavBar.frame.size.height)
    
    mainScrollView = UIScrollView.init(frame: frameForView)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.frame.size.height + (115.0 * UtilityManager.sharedInstance.conversionHeight))
    
    mainScrollView.contentSize = newContentSize
    self.view.addSubview(mainScrollView)
    
  }
  
  private func initProfileImage() {
    
    let frameForView = CGRect.init(x: self.view.frame.size.width / 2.0 - (45.0 * UtilityManager.sharedInstance.conversionWidth),
                                   y: 20.0 * UtilityManager.sharedInstance.conversionHeight,
                                   width: 90.0 * UtilityManager.sharedInstance.conversionWidth,
                                   height: 90.0 * UtilityManager.sharedInstance.conversionHeight)
    
    profileImage = UIImageView.init(frame: frameForView)
    profileImage.image = UIImage.init(named: "defaultUserBigImage.png")
    profileImage.backgroundColor = UIColor.lightGray
    
    if User.session.urlLogo != nil {
      
      profileImage.imageFromUrl(urlString: User.session.urlLogo!)
      
    }
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(profileImagePressed))
    tapGesture.numberOfTapsRequired = 1
    profileImage.addGestureRecognizer(tapGesture)

    self.mainScrollView.addSubview(profileImage)
    
  }
  
  private func initNameLabel() {
    
    let frameForLabel = CGRect.init(x: 20.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: profileImage.frame.origin.y + profileImage.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                    width: self.view.frame.size.width - (40.0 * UtilityManager.sharedInstance.conversionWidth),
                                    height: CGFloat.greatestFiniteMagnitude)
    
    nameLabel = UILabel.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: User.session.name,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    nameLabel.attributedText = stringWithFormat
    nameLabel.sizeToFit()
    let newFrame = CGRect.init(x: ( self.view.frame.size.width / 2.0 ) - ( nameLabel.frame.size.width / 2.0 ),
                               y: profileImage.frame.origin.y + profileImage.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                               width: nameLabel.frame.size.width,
                               height: nameLabel.frame.size.height)
    
    nameLabel.frame = newFrame
    
    self.mainScrollView.addSubview(nameLabel)

    
  }
  
  private func initMemberSinceLabel() {
    
    let frameForLabel = CGRect.init(x: 20.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: nameLabel.frame.origin.y + nameLabel.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                    width: self.view.frame.size.width - (40.0 * UtilityManager.sharedInstance.conversionWidth),
                                    height: CGFloat.greatestFiniteMagnitude)
    
    memberSinceLabel = UILabel.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 12.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    memberSinceLabel.attributedText = stringWithFormat
    memberSinceLabel.sizeToFit()
    let newFrame = CGRect.init(x: ( self.view.frame.size.width / 2.0 ) - ( memberSinceLabel.frame.size.width / 2.0 ),
                               y: nameLabel.frame.origin.y + nameLabel.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                               width: memberSinceLabel.frame.size.width,
                               height: memberSinceLabel.frame.size.height)
    
    memberSinceLabel.frame = newFrame
    
    self.mainScrollView.addSubview(memberSinceLabel)
    
  }
  
  private func initCompanyNameTextField() {
    
    let frameForView = CGRect.init(x: 17.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: memberSinceLabel.frame.origin.y + memberSinceLabel.frame.size.height + (25.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: self.view.frame.size.width - (34.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 57.0 * UtilityManager.sharedInstance.conversionHeight)
    
    companyNameTextField  = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                              title: ViewControllersConstants.ProfileViewController.nameTextFieldText,
                                                              image: nil,
                                                              colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                              positionInYInsideView: nil,
                                                              positionInXInsideView: nil)
    
    companyNameTextField.mainTextField.text = User.session.name
    companyNameTextField.mainTextField.textColor = UIColor.black
    companyNameTextField.mainTextField.placeholder = ViewControllersConstants.ProfileViewController.nameTextFieldText
    companyNameTextField.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    companyNameTextField.delegate = self
    
    companyNameTextField.mainTextField.isUserInteractionEnabled = false
    
    self.mainScrollView.addSubview(companyNameTextField)
    
  }
  
  private func initCompanyEmailTextField() {
    
    let frameForView = CGRect.init(x: 17.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: companyNameTextField.frame.origin.y + companyNameTextField.frame.size.height + (15.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: self.view.frame.size.width - (34.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 57.0 * UtilityManager.sharedInstance.conversionHeight)
    
    companyEmailTextField  = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                              title: ViewControllersConstants.ProfileViewController.emailTextFieldText,
                                                              image: nil,
                                                              colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                              positionInYInsideView: nil,
                                                              positionInXInsideView: nil)
    
    companyEmailTextField.mainTextField.text = User.session.email
    companyEmailTextField.mainTextField.textColor = UIColor.black
    companyEmailTextField.mainTextField.placeholder = ViewControllersConstants.ProfileViewController.emailTextFieldText
    companyEmailTextField.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    companyEmailTextField.delegate = self
    companyEmailTextField.isUserInteractionEnabled = false
    self.mainScrollView.addSubview(companyEmailTextField)
    
  }
  
  private func initCompanyPasswordTextField() {

    let frameForView = CGRect.init(x: 17.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: memberSinceLabel.frame.origin.y + memberSinceLabel.frame.size.height + (100.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: self.view.frame.size.width - (34.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 57.0 * UtilityManager.sharedInstance.conversionHeight)
    
//    let frameForView = CGRect.init(x: 17.0 * UtilityManager.sharedInstance.conversionWidth,
//                                   y: companyEmailTextField.frame.origin.y + companyEmailTextField.frame.size.height + (15.0 * UtilityManager.sharedInstance.conversionHeight),
//                                   width: self.view.frame.size.width - (34.0 * UtilityManager.sharedInstance.conversionWidth),
//                                   height: 57.0 * UtilityManager.sharedInstance.conversionHeight)
    
    companyPasswordTextField  = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                               title: ViewControllersConstants.ProfileViewController.passwordTextFieldText,
                                                               image: nil,
                                                               colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                               positionInYInsideView: nil,
                                                               positionInXInsideView: nil)
  
    companyPasswordTextField.mainTextField.isSecureTextEntry = true
    companyPasswordTextField.mainTextField.textColor = UIColor.black
    companyPasswordTextField.mainTextField.placeholder = ViewControllersConstants.ProfileViewController.passwordTextFieldText
    companyPasswordTextField.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    companyPasswordTextField.delegate = self
    self.mainScrollView.addSubview(companyPasswordTextField)
    
  }
  
  private func initDescriptionPasswordLabel() {
    
    let frameForLabel = CGRect.init(x: 20.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: companyPasswordTextField.frame.origin.y + companyPasswordTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                    width: self.view.frame.size.width - (40.0 * UtilityManager.sharedInstance.conversionWidth),
                                    height: CGFloat.greatestFiniteMagnitude)
    
    descriptionPasswordLabel = UILabel.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.ProfileViewController.descriptionPasswordLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    descriptionPasswordLabel.attributedText = stringWithFormat
    descriptionPasswordLabel.sizeToFit()
    let newFrame = CGRect.init(x: ( self.view.frame.size.width / 2.0 ) - ( descriptionPasswordLabel.frame.size.width / 2.0 ),
                               y: companyPasswordTextField.frame.origin.y + companyPasswordTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                               width: descriptionPasswordLabel.frame.size.width,
                               height: descriptionPasswordLabel.frame.size.height)
    
    descriptionPasswordLabel.frame = newFrame
    
    self.mainScrollView.addSubview(descriptionPasswordLabel)
    
  }
  
  private func initUpdateButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: (19.0 * UtilityManager.sharedInstance.conversionWidth),
                                     y: descriptionPasswordLabel.frame.origin.y + descriptionPasswordLabel.frame.size.height + (13.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    updateButton = UIButton.init(frame: frameForButton)
    updateButton.addTarget(self,
                           action: #selector(updateButtonPressed),
                             for: .touchUpInside)
    updateButton.backgroundColor = UIColor.orange
    updateButton.setBackgroundColor(color: UIColor.init(red: 255.0/255.0, green: 164.0/255.0, blue: 78.0/255.0, alpha: 1.0), forState: .highlighted)
    updateButton.setAttributedTitle(stringWithFormat, for: .normal)
    updateButton.contentHorizontalAlignment = .left
    
    updateButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.ProfileViewController.updateProfileButtonText))
    
    
//    let frameForImage = CGRect.init(x: 150.0 * UtilityManager.sharedInstance.conversionWidth,
//                                    y: 10.0 * UtilityManager.sharedInstance.conversionHeight,
//                                width: 30.0 * UtilityManager.sharedInstance.conversionWidth,
//                               height: 30.0 * UtilityManager.sharedInstance.conversionHeight)
//    
//    let reloadImageView =
    
    
    self.mainScrollView.addSubview(updateButton)
    
  }
  
  private func createGenericLabelForButton(text: String) -> UILabel {
    
    let frameForLabel = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 200.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    let genericLabel = UILabel.init(frame: frameForLabel)
    genericLabel.numberOfLines = 0
    genericLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: text,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    genericLabel.attributedText = stringWithFormat
    genericLabel.sizeToFit()
    let newFrame = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                               y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                               width: genericLabel.frame.size.width,
                               height: genericLabel.frame.size.height)
    genericLabel.frame = newFrame
    
    return genericLabel
    
  }

  
  
  
  @objc private func backButtonPressed() {
    
    _ = self.navigationController?.popViewController(animated: true)
    
  }
  
  @objc private func contactButtonPressed() {
    
    let contactScreen = ContactViewController()
    self.navigationController?.pushViewController(contactScreen, animated: true)
    
  }
  
  @objc private func closeSessionButtonPressed() {
    
    let loginViewController = LoginViewController()
    let newNavController = UINavigationController.init(rootViewController: loginViewController)
    
    User.session.resetUserSession()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    UIView.transition(with: appDelegate.window!,
                              duration: 0.25,
                              options: UIViewAnimationOptions.transitionCrossDissolve,
                              animations: {
                                self.view.alpha = 0.0
                                appDelegate.window?.rootViewController = newNavController
                                appDelegate.window?.makeKeyAndVisible()
                                
    },
                              completion: nil)
    
  }
  
  @objc private func profileImagePressed() {
    
    
    
  }
  
  func customTextFieldSelected(sender: CustomTextFieldWithTitleView) {
    
    
    
  }
  
  @objc private func updateButtonPressed() {
    
    if companyPasswordTextField.mainTextField.text?.isEmpty == false {
      
      let alertController = UIAlertController(title: "¡PRECAUCIÓN!",
                                              message: "Se modificara el password de tu cuenta",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
      
        UtilityManager.sharedInstance.showLoader()
        
        let params: [String: AnyObject] = ["password": self.companyPasswordTextField.mainTextField.text! as AnyObject]
        
        ServerManager.sharedInstance.requestToChangePassword(params: params, actionsToMakeWhenSucceeded: {
          
          UtilityManager.sharedInstance.hideLoader()
          
          let alertController = UIAlertController(title: "Éxito",
                                                  message: "El password fue modificado exitosamente",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
          
            _ = self.navigationController?.popViewController(animated: true)
          
          }
          alertController.addAction(okAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
        
        }, actionsToMakeWhenFailed: {
        
          UtilityManager.sharedInstance.hideLoader()
          
          let alertController = UIAlertController(title: "ERROR",
                                                  message: "Hubo problemas al actualizar el password, inténtalo más tarde",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
          alertController.addAction(okAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
        
        })
      
      }
      
      let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: { (result: UIAlertAction) in})
      
      alertController.addAction(okAction)
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
      
    }    

    
  }
  
  @objc private func hideKeyboard() {
    
    self.view.endEditing(true)
    
  }
  
  
}
