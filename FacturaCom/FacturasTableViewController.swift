//
//  FacturasTableViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 31/03/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class FacturasTableViewController: TableViewWithSearchBarViewController {
  
  private var facturasArray: Array<Factura>! = Array<Factura>()
  private var filteredFacturasArray: Array<Factura>! = Array<Factura>()
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
    
  }
  
  init(style: UITableViewStyle, newArrayOfFacturasArray: Array<Factura>) {
    
    facturasArray = newArrayOfFacturasArray
    
    super.init(style: style, newArrayOfElements: facturasArray)
    
    self.editNavigationBar()
    self.initValues()
    
  }
  
  private func editNavigationBar() {
    
    self.title = ViewControllersConstants.NameOfCFDIScreen.facturasTableTitleText
    self.changeNavigationBarTitle()
    
  }
  
  private func changeNavigationBarTitle() {
    
    let titleLabel = UILabel.init(frame: CGRect.zero)
    
    let font = UIFont(name: "SFUIText-Regular",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.NameOfCFDIScreen.facturasTableTitleText,
      attributes:[NSFontAttributeName:font!,
                  NSParagraphStyleAttributeName:style,
                  NSForegroundColorAttributeName:color
      ]
    )
    titleLabel.attributedText = stringWithFormat
    titleLabel.sizeToFit()
    self.navigationItem.titleView = titleLabel
    
  }
  
  private func initValues() {
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.requestToGetAllFacturas(params: nil, actionsToMakeWhenSucceeded: { arrayOfFacturas in
      
      print(arrayOfFacturas)
      self.facturasArray = arrayOfFacturas
      self.tableView.reloadData()
      
      UtilityManager.sharedInstance.hideLoader()
      
    }, actionsToMakeWhenFailed: {
    
      UtilityManager.sharedInstance.hideLoader()
      
    })
    
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      return filteredFacturasArray.count
      
    }
    
    return facturasArray.count
    
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CustomCellTableViewCell
    
    cell.selectionStyle = UITableViewCellSelectionStyle.none
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      cell.setData(newData: filteredFacturasArray[indexPath.row])
      
    } else {
      
      cell.setData(newData: facturasArray[indexPath.row])
      
    }
    
    return cell
    
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    var facturaData: Factura
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      facturaData = filteredFacturasArray[indexPath.row]
      
    } else {
      
      facturaData = facturasArray[indexPath.row]
      
    }
    
    let facturaDetailScreen = FacturaDetailViewController.init(newFacturaData: facturaData)
    self.navigationController?.pushViewController(facturaDetailScreen, animated: true)
    
  }
  
  func filterContentForSearchText(searchText: String, scope: String = "All") {
    
    filteredFacturasArray = facturasArray.filter { element in
      
      return element.folio.lowercased().contains(searchText.lowercased()) || element.fechaTimbrado.lowercased().contains(searchText.lowercased()) || element.razonSocialReceptor.lowercased().contains(searchText.lowercased())
      
    }
    
    self.tableView.reloadData()
    
  }
  
  override func updateSearchResults(for searchController: UISearchController) {
    
    self.filterContentForSearchText(searchText: searchController.searchBar.text!)
    
  }
  
  override func createSomethingButtonPressed() {
    
    let createFacturaScreen = FacturaCreateViewController()
    self.navigationController?.pushViewController(createFacturaScreen, animated: true)
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    
    self.initValues()
    
    if User.session.actualUsingCompany.urlLogo != nil {
      
      self.companyView.imageFromUrl(urlString: User.session.actualUsingCompany.urlLogo!)
      
    }
    
  }
  
  override func actionToDoWhenChangeCompany() {
    
    self.initValues()
    
  }

}
