//
//  Contact.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 11/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class Contact {
  
  var apellidos: String! = nil
  var email: String! = nil
  var nombre: String! = nil
  var telefono: String! = nil
  
  init() {}
  
}
