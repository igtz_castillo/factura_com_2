//
//  WayToPay.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 11/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class WayToPay {
  
  var id: String! = nil
  var name: String! = nil
  
  init(newId: String!, newName: String) {
    
    id = newId
    name = newName
    
  }
  
}
