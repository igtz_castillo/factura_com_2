//
//  CustomHeaderTableCellView.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 29/03/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class CustomHeaderTableCellView: UITableViewCell {
  
  private var folioLabel: UILabel! = nil
  private var estadoLabel: UILabel! = nil
  private var fechaLabel: UILabel! = nil
  private var razonSocialLabel: UILabel! = nil
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.contentView.backgroundColor = UIColor.init(red: 229.0/255.0, green: 229.0/255.0, blue: 234.0/255.0, alpha: 1.0)
    self.initFolioLabel()
    self.initEstadoLabel()
    self.initFechaLabel()
    self.initRazonSocialLabel()
    
  }
  
  private func initFolioLabel() {
    
    let frameForLabel = CGRect.init(x: UtilityManager.sharedInstance.conversionPositionInXFromIPhoneToIPad(positionToConvert: 18.0 * UtilityManager.sharedInstance.conversionWidth),
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                width: 42.0 * UtilityManager.sharedInstance.conversionWidth,
                               height: 20.0 * UtilityManager.sharedInstance.conversionHeight)
    
    folioLabel = UILabel.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.HeaderTableViewCell.folioLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    folioLabel.attributedText = stringWithFormat
    folioLabel.sizeToFit()
    let newFrame = CGRect.init(x: folioLabel.frame.origin.x,
                               y: folioLabel.frame.origin.y,
                           width: folioLabel.frame.size.width,
                          height: folioLabel.frame.size.height)
    
    folioLabel.frame = newFrame
    
    self.contentView.addSubview(folioLabel)
    
  }
  
  private func initEstadoLabel() {
    
    let frameForLabel = CGRect.init(x: UtilityManager.sharedInstance.conversionPositionInXFromIPhoneToIPad(positionToConvert: 75.0 * UtilityManager.sharedInstance.conversionWidth),
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 42.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: 20.0 * UtilityManager.sharedInstance.conversionHeight)
    
    estadoLabel = UILabel.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.HeaderTableViewCell.estadoLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    estadoLabel.attributedText = stringWithFormat
    estadoLabel.sizeToFit()
    let newFrame = CGRect.init(x: estadoLabel.frame.origin.x,
                               y: estadoLabel.frame.origin.y,
                               width: estadoLabel.frame.size.width,
                               height: estadoLabel.frame.size.height)
    
    estadoLabel.frame = newFrame
    
    self.contentView.addSubview(estadoLabel)
    
  }
  
  private func initFechaLabel() {
    
    let frameForLabel = CGRect.init(x: UtilityManager.sharedInstance.conversionPositionInXFromIPhoneToIPad(positionToConvert: 134.0 * UtilityManager.sharedInstance.conversionWidth),
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 42.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: 20.0 * UtilityManager.sharedInstance.conversionHeight)
    
    fechaLabel = UILabel.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.HeaderTableViewCell.fechaLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    fechaLabel.attributedText = stringWithFormat
    fechaLabel.sizeToFit()
    let newFrame = CGRect.init(x: fechaLabel.frame.origin.x,
                               y: fechaLabel.frame.origin.y,
                               width: fechaLabel.frame.size.width,
                               height: fechaLabel.frame.size.height)
    
    fechaLabel.frame = newFrame
    
    self.contentView.addSubview(fechaLabel)
    
  }
  
  private func initRazonSocialLabel() {
    
    let frameForLabel = CGRect.init(x: UtilityManager.sharedInstance.conversionPositionInXFromIPhoneToIPad(positionToConvert: 240.0 * UtilityManager.sharedInstance.conversionWidth),
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 42.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: 20.0 * UtilityManager.sharedInstance.conversionHeight)
    
    razonSocialLabel = UILabel.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.HeaderTableViewCell.razonSocialLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    razonSocialLabel.attributedText = stringWithFormat
    razonSocialLabel.sizeToFit()
    let newFrame = CGRect.init(x: razonSocialLabel.frame.origin.x,
                               y: razonSocialLabel.frame.origin.y,
                               width: razonSocialLabel.frame.size.width,
                               height: razonSocialLabel.frame.size.height)
    
    razonSocialLabel.frame = newFrame
    
    self.contentView.addSubview(razonSocialLabel)
    
  }
  
}
