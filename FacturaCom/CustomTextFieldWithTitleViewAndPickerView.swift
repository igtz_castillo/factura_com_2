//
//  CustomTextFieldWithTitleViewAndPickerView.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 15/05/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

protocol CustomTextFieldWithTitleViewAndPickerViewDelegate {
  
  func customTextFieldSelected(sender: CustomTextFieldWithTitleViewAndPickerView)
  func hidePickerView(sender: CustomTextFieldWithTitleViewAndPickerView)
}

class CustomTextFieldWithTitleViewAndPickerView: UIView, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
  
  private var textOfTitleString: String?
  private var nameOfImageString: String?
  private var colorOfLabelsAndLines: UIColor?
  private var positionFromTop: CGFloat?
  private var positionFromLeft: CGFloat?
  private var iconImageView: UIImageView?
  private var titleLabel: UILabel?
  var mainTextField: UITextField! = nil
  
  private var titleForContainerViewText: String! = nil
  private var titleForContainerView: UILabel! = nil
  private var arrayOfOptions: [Option]! = [Option]()
  private var mainPickerView: UIPickerView! = nil
  private var containerViewForPicker: UIView! = nil
  private var isShowingPickerView: Bool = false
  private var indexSelected: Int = -1
  
  var delegate: CustomTextFieldWithTitleViewAndPickerViewDelegate?
  
  private let frameForShowing: CGRect = CGRect.init(x: 0.0,
                                                    y: UIScreen.main.bounds.height - (250.0 * UtilityManager.sharedInstance.conversionHeight),
                                                    width: UIScreen.main.bounds.width,
                                                    height: 250.0 * UtilityManager.sharedInstance.conversionHeight)
  
  private let frameForHiding: CGRect = CGRect.init(x: 0.0,
                                                   y: UIScreen.main.bounds.height,
                                                   width: UIScreen.main.bounds.width,
                                                   height: 250.0 * UtilityManager.sharedInstance.conversionHeight)
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  init(frame: CGRect, title: String?, image: String?, colorOfLabelAndLine: UIColor?) {
    
    textOfTitleString = title
    nameOfImageString = image
    colorOfLabelsAndLines = colorOfLabelAndLine
    
    super.init(frame: frame)
    self.initInterface()
    
  }
  
  init(frame: CGRect, title: String?, image: String?, colorOfLabelAndLine: UIColor?, positionInYInsideView: CGFloat?, positionInXInsideView: CGFloat?, newOptions: [Option], titleOfPickerView: String?) {
    
    textOfTitleString = title
    nameOfImageString = image
    colorOfLabelsAndLines = colorOfLabelAndLine
    positionFromTop = positionInYInsideView
    positionFromLeft = positionInXInsideView
    arrayOfOptions = newOptions
    titleForContainerViewText = titleOfPickerView
    
    super.init(frame: frame)
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.createTitleLabel()
    self.createIconImageView()
    
    self.createMainTextField()
    
    self.initContainerViewForPicker()
    self.initMainPickerView()
    
  }
  
  private func createTitleLabel() {
    
    if textOfTitleString != nil {
      
      titleLabel = UILabel.init(frame: CGRect.zero)
      
      let font = UIFont.init(name: "SFUIText-Light",
                             size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
      
      var color = UtilityManager.sharedInstance.labelsAndLinesColor
      
      if colorOfLabelsAndLines != nil {
        
        color = colorOfLabelsAndLines!
        
      }
      
      let style = NSMutableParagraphStyle()
      style.alignment = NSTextAlignment.center
      
      let stringWithFormat = NSMutableAttributedString(
        string: textOfTitleString!,
        attributes:[NSFontAttributeName:font!,
                    NSParagraphStyleAttributeName:style,
                    NSForegroundColorAttributeName:color
        ]
      )
      titleLabel!.attributedText = stringWithFormat
      titleLabel!.sizeToFit()
      
      var newFrame = CGRect.init(x: 0.0 * UtilityManager.sharedInstance.conversionWidth,
                                 y: 0.0 * UtilityManager.sharedInstance.conversionHeight,
                                 width: titleLabel!.frame.size.width,
                                 height: titleLabel!.frame.size.height)
      
      if positionFromTop != nil {
        
        newFrame = CGRect.init(x: 0.0 * UtilityManager.sharedInstance.conversionWidth,
                               y: positionFromTop!,
                               width: titleLabel!.frame.size.width,
                               height: titleLabel!.frame.size.height)
        
      }
      
      if positionFromLeft != nil {
        
        newFrame = CGRect.init(x: positionFromLeft!,
                               y: newFrame.origin.y,
                               width: titleLabel!.frame.size.width,
                               height: titleLabel!.frame.size.height)
        
      }
      
      titleLabel!.frame = newFrame
      self.addSubview(titleLabel!)
    }
    
  }
  
  private func createIconImageView() {
    
    let positionY: CGFloat
    
    if titleLabel != nil {
      
      positionY = titleLabel!.frame.origin.y + titleLabel!.frame.size.height + (29.0 * UtilityManager.sharedInstance.conversionHeight) //When has title but not icon
      
    } else {
      
      positionY = 29.0 * UtilityManager.sharedInstance.conversionHeight //When doesn't have an title but icon
      
    }
    
    
    if nameOfImageString != nil {
      
      iconImageView = UIImageView.init(image: UIImage.init(named: nameOfImageString!))
      let iconImageViewFrame = CGRect.init(x: 2.0 * UtilityManager.sharedInstance.conversionWidth,
                                           y: positionY,
                                           width: iconImageView!.frame.size.width,
                                           height: iconImageView!.frame.size.height)
      
      iconImageView!.frame = iconImageViewFrame
      
      self.addSubview(iconImageView!)
      
    }
    
    
  }
  
  private func createMainTextField() {
    
    var newPositionX: CGFloat
    var newPositionXForLine: CGFloat
    var newPositionY: CGFloat
    var newWidth: CGFloat
    var newWidthForLine: CGFloat
    
    if iconImageView != nil {
      
      newPositionX = iconImageView!.frame.origin.x + iconImageView!.frame.size.width + (20.0 * UtilityManager.sharedInstance.conversionWidth)
      
      newWidth = (self.frame.size.width) - (iconImageView!.frame.origin.x + iconImageView!.frame.size.width + (20.0 * UtilityManager.sharedInstance.conversionWidth))
      
      newWidthForLine = newPositionX + newWidth
      
      newPositionXForLine = -newPositionX
      
      
    } else {
      
      newPositionX = 4.0 * UtilityManager.sharedInstance.conversionWidth
      
      if positionFromLeft != nil {
        
        newPositionX = positionFromLeft!
        
      }
      
      newWidth = self.frame.size.width
      
      newPositionXForLine = -4.0 * UtilityManager.sharedInstance.conversionWidth
      
      newWidthForLine = self.frame.size.width
      
      if positionFromLeft != nil {
        
        newWidth = self.frame.size.width - (positionFromLeft! * 2.0)
        newWidthForLine = self.frame.size.width - (positionFromLeft! * 2.0)
        
      }
      
    }
    
    if titleLabel != nil {
      
      newPositionY = titleLabel!.frame.origin.y + titleLabel!.frame.size.height + (0.0 * UtilityManager.sharedInstance.conversionHeight)
      
    } else {
      
      newPositionY = 7.0 * UtilityManager.sharedInstance.conversionHeight
      
    }
    
    let frameForTextField = CGRect.init(x: newPositionX,
                                        y: newPositionY,
                                        width: newWidth,
                                        height: 30.0 * UtilityManager.sharedInstance.conversionHeight)
    
    mainTextField = UITextField.init(frame: frameForTextField)
    
    mainTextField.backgroundColor = UIColor.clear
    
    var colorOfLine = UtilityManager.sharedInstance.labelsAndLinesColor
    
    if colorOfLabelsAndLines != nil {
      
      colorOfLine = colorOfLabelsAndLines!
      
    }
    
    let border = CALayer()
    let width = CGFloat(0.5)
    border.borderColor = colorOfLine.cgColor
    border.borderWidth = width
    border.frame = CGRect.init(x: newPositionXForLine,
                               y: mainTextField.frame.size.height + (1.0 * UtilityManager.sharedInstance.conversionHeight),
                               width: newWidthForLine,
                               height: 0.5)
    mainTextField.layer.masksToBounds = false
    mainTextField.layer.addSublayer(border)
    mainTextField.clearButtonMode = UITextFieldViewMode.whileEditing
    mainTextField.font = UIFont.systemFont(ofSize: 13.5 * UtilityManager.sharedInstance.conversionHeight)
    mainTextField.delegate = self
    self.addSubview(mainTextField)
    
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    
    self.delegate?.customTextFieldSelected(sender: self)
    
  }
  
  private func initContainerViewForPicker() {
    
    containerViewForPicker = UIView.init(frame: frameForHiding)
    containerViewForPicker.backgroundColor = UIColor.clear
    
    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
    let blurEffectView = UIVisualEffectView(effect: blurEffect)
    //always fill the view
    blurEffectView.frame = CGRect.init(x: 0.0,
                                       y: 0.0,
                                       width: containerViewForPicker.frame.size.width,
                                       height: containerViewForPicker.frame.size.height)
    containerViewForPicker.addSubview(blurEffectView)
    
    
    self.initButtonsForContainer()
    
//    let frontViewController = UtilityManager.sharedInstance.currentViewController()
//    frontViewController.view.addSubview(containerViewForPicker)
    
    self.mainTextField.inputView = containerViewForPicker
    
  }
  
  private func initButtonsForContainer() {
    
    let positionForFirstButton = CGPoint.init(x: 10.0 * UtilityManager.sharedInstance.conversionWidth,
                                              y: 12.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let positionForSecondButton = CGPoint.init(x: 305.0 * UtilityManager.sharedInstance.conversionWidth,
                                               y: 12.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let cancelButton = createGenericButton(stringToShowInButton: ViewControllersConstants.FacturaCreateViewController.pickerViewCancelButtonText, position: positionForFirstButton)
    let selectButton = createGenericButton(stringToShowInButton: ViewControllersConstants.FacturaCreateViewController.pickerViewSelectButtonText, position: positionForSecondButton)
    
    cancelButton.tag = 1
    selectButton.tag = 2
    
    self.containerViewForPicker.addSubview(cancelButton)
    self.initTitleForContainerView()
    self.containerViewForPicker.addSubview(selectButton)
    
  }
  
  private func initTitleForContainerView() {
    
    let frameForLabel = CGRect.init(x: 90.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 18.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 180.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: 23.0 * UtilityManager.sharedInstance.conversionHeight)
    
    titleForContainerView = UILabel.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: titleForContainerViewText != nil ? titleForContainerViewText : "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    titleForContainerView.attributedText = stringWithFormat
    titleForContainerView.sizeToFit()
    let newFrame = CGRect.init(x: (self.containerViewForPicker.frame.size.width / 2.0) - (titleForContainerView.frame.size.width / 2.0),
                               y: titleForContainerView.frame.origin.y,
                               width: titleForContainerView.frame.size.width,
                               height: titleForContainerView.frame.size.height)
    
    titleForContainerView.frame = newFrame
    
    self.containerViewForPicker.addSubview(titleForContainerView)
    
  }
  
  private func createGenericButton(stringToShowInButton: String, position: CGPoint) -> UIButton {
    
    let genericButton = UIButton.init(frame: CGRect.zero)
    
    let font = UIFont(name: "SFUIDisplay-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.blue
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: stringToShowInButton,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    genericButton.setAttributedTitle(stringWithFormat, for: .normal)
    genericButton.backgroundColor = UIColor.clear
    genericButton.addTarget(self,
                            action: #selector(genericButtonPressed),
                            for: .touchUpInside)
    genericButton.sizeToFit()
    
    let frameForButton = CGRect.init(x: position.x,
                                     y: position.y,
                                     width: genericButton.frame.size.width,
                                     height: genericButton.frame.size.height)
    
    genericButton.frame = frameForButton
    genericButton.layer.cornerRadius = 3.0
    
    return genericButton
    
  }
  
  private func initMainPickerView() {
    
    let frameForPicker = CGRect.init(x: 0.0,
                                     y: (50.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: UIScreen.main.bounds.width,
                                     height: 250.0 * UtilityManager.sharedInstance.conversionHeight)
    
    mainPickerView = UIPickerView.init(frame: frameForPicker)
    mainPickerView.delegate = self
    mainPickerView.dataSource = self
    containerViewForPicker.addSubview(mainPickerView)
    
  }
  
  @objc private func genericButtonPressed(sender: AnyObject) {
    
    if ((sender as? UIButton) != nil) {
      
      if sender.tag == 1 { //Cancel
        
        self.hidePickerView()
        
      } else
        
        if sender.tag == 2 {
          
          if arrayOfOptions.count > 0 {
            
            self.hidePickerView()
            
            mainTextField.text = arrayOfOptions[mainPickerView.selectedRow(inComponent: 0)].name
//            self.delegate?.selectActionFinalized(sender: self)
            
          }
          
      }
      
    }
    
  }
  
  @objc private func myselfPressed() {
    
    if isShowingPickerView == false {
      
//      self.delegate?.lastButtonToShowPickerView(sender: self)
      self.showPickerView()
      isShowingPickerView = true
      
    } else {
      
      self.hidePickerView()
      isShowingPickerView = false
      
    }
    
  }
  
  private func showPickerView() {
    
//    UIView.animate(withDuration: 0.35, animations: {
//      
//      self.containerViewForPicker.frame = self.frameForShowing
//      
//    }) { (isFinished) in
//      
//      if isFinished == true {
//        
//        
//        
//      }
//      
//    }
    
  }
  
  func hidePickerView() {
    
    self.delegate?.hidePickerView(sender: self)
    
//    if isShowingPickerView == true {
//      
//      UIView.animate(withDuration: 0.35, animations: {
//        
//        self.containerViewForPicker.frame = self.frameForHiding
//        
//      }) { (isFinished) in
//        
//        if isFinished == true {
//          
//          
//          
//        }
//        
//      }
//      
//    }
    
  }
  
  func getValueSelected() -> Option? {
    
    if indexSelected == -1 {
      
      return nil
      
    } else {
      
      return arrayOfOptions[mainPickerView.selectedRow(inComponent: 0)]
      
    }    
    
  }

  //MARK: - PickerViewDelegate - DataSource
  
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    
    return 1
    
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    
    return arrayOfOptions.count
    
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
    return arrayOfOptions[row].name
    
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
    mainTextField.text = arrayOfOptions[row].name
    indexSelected = row
    
  }
  
}
