//
//  CustomCellTableViewCell.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 29/03/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class CustomCellTableViewCell: UITableViewCell {
  
  private var data: CFDI! = nil
  
  private var firstFieldLabel: UILabel! = nil
  private var secondFieldImage: UIImageView! = nil
  private var thirdFieldLabel: UILabel! = nil
  private var fourthFieldLabel: UILabel! = nil
  
  func setData(newData: CFDI) {
    
    data = newData
    
    self.updateValueForFirstField()
    self.updateValueForSecondField()
    self.updateValueForThirdField()
    self.updateValueForFourthField()
    
  }
  
  private func updateValueForFirstField() {
    
    if data.folio != nil && data.folio != " "{
      
      if firstFieldLabel == nil {
        
        self.initFirstFieldLabel()
        
      } else {
        
        self.changeDataToFirstField()
        
      }
      
    }
    
  }
  
  private func updateValueForSecondField() {
    
    if data.status != nil && data.status != " " {
      
      if secondFieldImage == nil {
        
        self.initSecondFieldLabel()
        
      } else {
        
        self.changeImageToSecondField()
        
      }
      
    }
    
  }
  
  private func updateValueForThirdField() {
    
    if data.fechaTimbrado != nil && data.fechaTimbrado != " " {
      
      if thirdFieldLabel == nil {
        
        self.initThirdFieldLabel()
        
      } else {
        
        self.changeDataToThirdField()
        
      }
      
    }
    
  }
  
  private func updateValueForFourthField() {
    
    if data.receptor != nil && data.receptor != " " {
      
      if fourthFieldLabel == nil {
        
        self.initFourthFieldLabel()
        
      } else {
        
        self.changeDataToFourthField()
        
      }
      
    }
    
  }
  
  private func initFirstFieldLabel() {
    
    let frameForLabel = CGRect.init(x: UtilityManager.sharedInstance.conversionPositionInXFromIPhoneToIPad(positionToConvert: 16.0 * UtilityManager.sharedInstance.conversionWidth),
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 80.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: 23.0 * UtilityManager.sharedInstance.conversionHeight)
    
    firstFieldLabel = UILabel.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    var finalString = ""
    
    if data.folio != nil {
      
      finalString = data.folio!
      
    }
    
    let stringWithFormat = NSMutableAttributedString(
      string: finalString,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    firstFieldLabel.attributedText = stringWithFormat
    firstFieldLabel.sizeToFit()
    let newFrame = CGRect.init(x: firstFieldLabel.frame.origin.x,
                               y: firstFieldLabel.frame.origin.y,
                               width: firstFieldLabel.frame.size.width,
                               height: firstFieldLabel.frame.size.height)
    
    firstFieldLabel.frame = newFrame
    
    self.addSubview(firstFieldLabel)
    
  }
  
  private func changeDataToFirstField() {
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    var finalString = ""
    
    if data.folio != nil {
      
      finalString = data.folio!
      
    }
    
    let stringWithFormat = NSMutableAttributedString(
      string: finalString,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    firstFieldLabel.attributedText = stringWithFormat
    firstFieldLabel.sizeToFit()
    let newFrame = CGRect.init(x: firstFieldLabel.frame.origin.x,
                               y: firstFieldLabel.frame.origin.y,
                               width: firstFieldLabel.frame.size.width,
                               height: firstFieldLabel.frame.size.height)
    
    firstFieldLabel.frame = newFrame
    
  }
  
  private func initSecondFieldLabel() {
    
    let frameForImageView = CGRect.init(x: UtilityManager.sharedInstance.conversionPositionInXFromIPhoneToIPad(positionToConvert: 87.0 * UtilityManager.sharedInstance.conversionWidth),
                                    y: 14.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: 18.0 * UtilityManager.sharedInstance.conversionHeight)
    
    secondFieldImage = UIImageView.init(frame: frameForImageView)
    
    var finalImage: UIImage = UIImage.init()
    
    if data.status != nil {
      
      let typeOfImage = data.status!
      
      switch typeOfImage {
      case "cancelada":
        finalImage = UIImage.init(named: "redCross")!
        break
      
      case "enviada":
        finalImage = UIImage.init(named: "greenArrow")!
        break
        
      case "archivada":
        finalImage = UIImage.init(named: "sheetOfPaper")!
        break
        
      default:
        finalImage = UIImage.init()
        
      }
   
    }
    
    secondFieldImage.image = finalImage
    
    self.addSubview(secondFieldImage)
    
  }
  
  private func changeImageToSecondField() {
    
    var finalImage: UIImage = UIImage.init()
    
    if data.status != nil {
      
      let typeOfImage = data.status!
      
      switch typeOfImage {
      case "cancelada":
        finalImage = UIImage.init(named: "redCross")!
        break
        
      case "enviada":
        finalImage = UIImage.init(named: "greenArrow")!
        break
        
      case "archivada":
        finalImage = UIImage.init(named: "sheetOfPaper")!
        break
        
      default:
        finalImage = UIImage.init()
        
      }
      
    }
    
    secondFieldImage.image = finalImage
    
  }
  
  private func initThirdFieldLabel() {
    
    let frameForLabel = CGRect.init(x: UtilityManager.sharedInstance.conversionPositionInXFromIPhoneToIPad(positionToConvert: 134.0 * UtilityManager.sharedInstance.conversionWidth),
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 97.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: 23.0 * UtilityManager.sharedInstance.conversionHeight)
    
    thirdFieldLabel = UILabel.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    var finalString = ""
    
    if data.fechaTimbrado != nil {
      
      finalString = data.fechaTimbrado!
      
    }
    
    let stringWithFormat = NSMutableAttributedString(
      string: finalString,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    thirdFieldLabel.attributedText = stringWithFormat
    thirdFieldLabel.sizeToFit()
    let newFrame = CGRect.init(x: thirdFieldLabel.frame.origin.x,
                               y: thirdFieldLabel.frame.origin.y,
                               width: thirdFieldLabel.frame.size.width,
                               height: thirdFieldLabel.frame.size.height)
    
    thirdFieldLabel.frame = newFrame
    
    self.addSubview(thirdFieldLabel)
    
  }
  
  private func changeDataToThirdField() {
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    var finalString = ""
    
    if data.fechaTimbrado != nil {
      
      finalString = data.fechaTimbrado!
      
    }
    
    let stringWithFormat = NSMutableAttributedString(
      string: finalString,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    thirdFieldLabel.attributedText = stringWithFormat
    thirdFieldLabel.sizeToFit()
    let newFrame = CGRect.init(x: thirdFieldLabel.frame.origin.x,
                               y: thirdFieldLabel.frame.origin.y,
                               width: thirdFieldLabel.frame.size.width,
                               height: thirdFieldLabel.frame.size.height)
    
    thirdFieldLabel.frame = newFrame
    
  }
  
  private func initFourthFieldLabel() {
    
    let frameForLabel = CGRect.init(x: UtilityManager.sharedInstance.conversionPositionInXFromIPhoneToIPad(positionToConvert: 240.0 * UtilityManager.sharedInstance.conversionWidth),
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 130.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: 23.0 * UtilityManager.sharedInstance.conversionHeight)
    
    fourthFieldLabel = UILabel.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.5 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    style.lineBreakMode = .byTruncatingTail
    
    var finalString = ""
    
    if data.razonSocialReceptor != nil {
      
      finalString = data.razonSocialReceptor!
      
    }
    
    let stringWithFormat = NSMutableAttributedString(
      string: finalString,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    fourthFieldLabel.attributedText = stringWithFormat
    let newFrame = CGRect.init(x: fourthFieldLabel.frame.origin.x,
                               y: fourthFieldLabel.frame.origin.y,
                               width: fourthFieldLabel.frame.size.width,
                               height: fourthFieldLabel.frame.size.height)
    
    fourthFieldLabel.frame = newFrame
    
    self.addSubview(fourthFieldLabel)
    
  }
  
  private func changeDataToFourthField() {
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.5 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    style.lineBreakMode = .byTruncatingTail
    
    var finalString = ""
    
    if data.razonSocialReceptor != nil {
      
      finalString = data.razonSocialReceptor!
      
    }
    
    let stringWithFormat = NSMutableAttributedString(
      string: finalString,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    fourthFieldLabel.attributedText = stringWithFormat
    let newFrame = CGRect.init(x: fourthFieldLabel.frame.origin.x,
                               y: fourthFieldLabel.frame.origin.y,
                               width: fourthFieldLabel.frame.size.width,
                               height: fourthFieldLabel.frame.size.height)
    
    fourthFieldLabel.frame = newFrame
    
  }

  
  
}
