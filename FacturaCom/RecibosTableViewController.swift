//
//  RecibosTableViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 13/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class RecibosTableViewController: TableViewWithSearchBarViewController {
  
  private var recibosArray: Array<Recibo>! = Array<Recibo>()
  private var filteredRecibosArray: Array<Recibo>! = Array<Recibo>()
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
    
  }
  
  init(style: UITableViewStyle, newArrayOfrecibosArray: Array<Recibo>) {
    
    recibosArray = newArrayOfrecibosArray
    
    super.init(style: style, newArrayOfElements: recibosArray)
    
    self.editNavigationBar()
    self.initValues()
    
  }
  
  private func editNavigationBar() {
    
    self.title = ViewControllersConstants.MainTabController.recibosButtonText
    self.changeNavigationBarTitle()
    
  }
  
  private func changeNavigationBarTitle() {
    
    let titleLabel = UILabel.init(frame: CGRect.zero)
    
    let font = UIFont(name: "SFUIText-Regular",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.NameOfCFDIScreen.recibosTableTitleText,
      attributes:[NSFontAttributeName:font!,
                  NSParagraphStyleAttributeName:style,
                  NSForegroundColorAttributeName:color
      ]
    )
    titleLabel.attributedText = stringWithFormat
    titleLabel.sizeToFit()
    self.navigationItem.titleView = titleLabel
    
  }
  
  private func initValues() {
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.requestToGetAllRecibos(params: nil, actionsToMakeWhenSucceeded: { arrayOfRecibos in
      
      print(arrayOfRecibos)
      self.recibosArray = arrayOfRecibos
      self.tableView.reloadData()
      
      UtilityManager.sharedInstance.hideLoader()
      
    }, actionsToMakeWhenFailed: {
      
      UtilityManager.sharedInstance.hideLoader()
      
    })
    
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      return filteredRecibosArray.count
      
    }
    
    return recibosArray.count
    
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CustomCellTableViewCell
    
    cell.selectionStyle = UITableViewCellSelectionStyle.none
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      cell.setData(newData: filteredRecibosArray[indexPath.row])
      
    } else {
      
      cell.setData(newData: recibosArray[indexPath.row])
      
    }
    
    return cell
    
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    var reciboData: Recibo
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      reciboData = filteredRecibosArray[indexPath.row]
      
    } else {
      
      reciboData = recibosArray[indexPath.row]
      
    }
    
    let facturaDetailScreen = ReciboDetailViewController.init(newReciboData: reciboData)
    self.navigationController?.pushViewController(facturaDetailScreen, animated: true)
    
  }
  
  func filterContentForSearchText(searchText: String, scope: String = "All") {
    
    filteredRecibosArray = recibosArray.filter { element in
      
      return element.folio.lowercased().contains(searchText.lowercased()) || element.fechaTimbrado.lowercased().contains(searchText.lowercased()) || element.razonSocialReceptor.lowercased().contains(searchText.lowercased())
      
    }
    
    self.tableView.reloadData()
    
  }
  
  override func actionToDoWhenChangeCompany() {
    
    self.initValues()
    
  }
  
  override func updateSearchResults(for searchController: UISearchController) {
    
    self.filterContentForSearchText(searchText: searchController.searchBar.text!)
    
  }
  
  override func createSomethingButtonPressed() {
    
    let createFacturaScreen = ReciboCreateViewController()
    self.navigationController?.pushViewController(createFacturaScreen, animated: true)
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    
    self.initValues()
    
    if User.session.actualUsingCompany.urlLogo != nil {
      
      self.companyView.imageFromUrl(urlString: User.session.actualUsingCompany.urlLogo!)
      
    }
    
  }
  
}
