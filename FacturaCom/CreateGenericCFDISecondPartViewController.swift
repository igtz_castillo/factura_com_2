//
//  CreateGenericCFDISecondPartViewController.swift
//  FacturaCom
//
//  Created by Israel on 29/09/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class CreateGenericCFDISecondPartViewController: UIViewController, CustomTextFieldWithTitleViewDelegate, ButtonToActivePickerViewDelegate {
  
  private var genericCFDIDataToCreate: GenericCFDI! = GenericCFDI.init(empty: true)
  
  private var arrayOfButtonsToShowPickerView: [ButtonToActivePickerView]! = [ButtonToActivePickerView]()
  
  //Elements from server
  
  private var generalInfoLabel: LabelSeparator! = nil
  
  private var arrayWayToPay: Array<WayToPay>! = Array<WayToPay>()
  private var arrayPaymentMethod: Array<PaymentMethod>! = Array<PaymentMethod>()
  private var arrayCurrencies: Array<Currency>! = Array<Currency>()
  private var arraySeries: Array<Serie>! = Array<Serie>()
  private var arrayDecimalNumbers: Array<Option>! = Array<Option>()
  
  private var mainScrollView: UIScrollView! = nil
  private var paymentWayButton: ButtonToActivePickerView! = nil
  private var paymentMethodButton: ButtonToActivePickerView! = nil
  private var numbersOfCreditCardTextField: CustomTextFieldWithTitleView! = nil
  
  private var typeOfMoneyButton: ButtonToActivePickerView! = nil
  private var exchangeRateTextField: CustomTextFieldWithTitleView! = nil
  
  private var serieButton: ButtonToActivePickerView! = nil
  private var decimalNumbersButton: ButtonToActivePickerView! = nil
  private var orderTextFieldView: CustomTextFieldWithTitleView! = nil
  
  private var lastButtonToShowPickerView: ButtonToActivePickerView! = nil
  
  private var willAppearAfterCreateExpeditinoPlace: Bool = false
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  init(newGenericCFDIDataToCreate: GenericCFDI) {
    
    genericCFDIDataToCreate = newGenericCFDIDataToCreate
    
    super.init(nibName: nil, bundle: nil)
    
  }
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.editNavigationController()
    self.initMainScrollView()
    self.getAllInformationFromServer()
    
  }
  
  private func editNavigationController() {
    
    self.changeBackButtonItem()
    self.changeNavigationBarTitle()
    self.changeNavigationRigthButtonItem()
    
  }
  
  private func changeBackButtonItem() {
    
    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
  }
  
  private func changeNavigationBarTitle() {
    
    self.title = ViewControllersConstants.MainTabController.facturasButtonText
    
    let titleLabel = UILabel.init(frame: CGRect.zero)
    
    let font = UIFont(name: "SFUIText-Regular",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateViewController.titleNavigationBarText,
      attributes:[NSFontAttributeName:font!,
                  NSParagraphStyleAttributeName:style,
                  NSForegroundColorAttributeName:color
      ]
    )
    titleLabel.attributedText = stringWithFormat
    titleLabel.sizeToFit()
    self.navigationItem.titleView = titleLabel
    
  }
  
  private func changeNavigationRigthButtonItem() {
    
    let rightButton = UIBarButtonItem(title: ViewControllersConstants.FacturaCreateViewController.nextButonNavigationBarText,
                                      style: UIBarButtonItemStyle.plain,
                                      target: self,
                                      action: #selector(nextButtonPressed))
    
    let fontForButtonItem =  UIFont(name: "SFUIText-Regular",
                                    size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    
    let attributesDict: [String:AnyObject] = [NSFontAttributeName: fontForButtonItem!,
                                              NSForegroundColorAttributeName: UIColor.orange
    ]
    
    rightButton.setTitleTextAttributes(attributesDict, for: .normal)
    
    self.navigationItem.rightBarButtonItem = rightButton
    
  }
  
  private func getAllInformationFromServer() {
    
    UtilityManager.sharedInstance.showLoader()
  
        ServerManager.sharedInstance.getWayToPay(actionsToMakeWhenSucceeded: { (waysToPay) in
          
          self.arrayWayToPay = waysToPay
          
          ServerManager.sharedInstance.getPaymentMethods(actionsToMakeWhenSucceeded: { (paymentMethods) in
            
            self.arrayPaymentMethod = paymentMethods
            
            ServerManager.sharedInstance.getCurrencies(actionsToMakeWhenSucceeded: { (currencies) in
              
              self.arrayCurrencies = currencies
              
              ServerManager.sharedInstance.getSeries(actionsToMakeWhenSucceeded: { (series) in
                
                self.arraySeries = series
                
                self.initGeneralInfo()
                self.initPaymentWayButton()
                self.initPaymentMethodButton()
                self.initNumbersOfCreditCardTextField()
                
                self.initTypeOfMoneyButton()
                self.initExchangeRateTextField()
                
                self.initSerieButton()
                self.initDecimalNumberButton()
                self.initOrderTextField()
                
                self.addGestureToHideOptionsOfButtons()
                
                UtilityManager.sharedInstance.hideLoader()
                
              }, actionsToMakeWhenFailed: {
                
                UtilityManager.sharedInstance.hideLoader()
                
              })
              
            }, actionsToMakeWhenFailed: {
              
              UtilityManager.sharedInstance.hideLoader()
              
            })
            
          }, actionsToMakeWhenFailed: {
            
            UtilityManager.sharedInstance.hideLoader()
            
          })
          
        }, actionsToMakeWhenFailed: {
          
          UtilityManager.sharedInstance.hideLoader()
          
        })
  
  }

  private func addGestureToHideOptionsOfButtons() {
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(hideOptionsAndKeyboard))
    tapGesture.numberOfTapsRequired = 1
    
    self.view.addGestureRecognizer(tapGesture)
    
  }

  private func initMainScrollView() {
    
    mainScrollView = UIScrollView.init(frame: self.view.frame)
    
    if UtilityManager.sharedInstance.isIpad() == false {
      
      let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                       height: mainScrollView.frame.size.height + (95.0 * UtilityManager.sharedInstance.conversionHeight))
      
      mainScrollView.contentSize = newContentSize
      
    }
    
    self.view.addSubview(mainScrollView)
    
  }
  
  private func initGeneralInfo() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: mainScrollView.frame.size.width,
                                    height: 56.0 * UtilityManager.sharedInstance.conversionHeight)
    
    generalInfoLabel = LabelSeparator.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateViewController.generalInfoLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    generalInfoLabel.attributedText = stringWithFormat
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: generalInfoLabel.frame.size.width,
                               height: generalInfoLabel.frame.size.height)
    generalInfoLabel.frame = newFrame
    generalInfoLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(generalInfoLabel)
    
  }
  
  private func initPaymentWayButton() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: generalInfoLabel.frame.origin.y + generalInfoLabel.frame.size.height,
                                     width: mainScrollView.frame.size.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var arrayOfOptions: [Option] = [Option]()
    
    for wayToPay in arrayWayToPay {
      
      let newOption = Option(id: wayToPay.id, name: wayToPay.name, type: "")
      arrayOfOptions.append(newOption)
      
    }
    
    paymentWayButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                     newTitle: ViewControllersConstants.FacturaCreateViewController.paymentWayButtonText,
                                                     newArrayOfOptions: arrayOfOptions)
    paymentWayButton.delegate = self
    paymentWayButton.drawBottomBorder()
    _ = paymentWayButton.selectElementWithName(nameToLookFor: "Pago en una sola exhibición")
    
    self.mainScrollView.addSubview(paymentWayButton)
    self.arrayOfButtonsToShowPickerView.append(paymentWayButton)
    
  }
  
  private func initPaymentMethodButton() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: paymentWayButton.frame.origin.y + paymentWayButton.frame.size.height,
                                     width: mainScrollView.frame.size.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var arrayOfOptions: [Option] = [Option]()
    
    for paymentMethod in arrayPaymentMethod {
      
      let newOption = Option(id: paymentMethod.id, name: paymentMethod.name, type: "")
      arrayOfOptions.append(newOption)
      
    }
    
    paymentMethodButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                        newTitle: ViewControllersConstants.FacturaCreateViewController.paymentMethodButtonText,
                                                        newArrayOfOptions: arrayOfOptions)
    paymentMethodButton.delegate = self
    paymentMethodButton.drawBottomBorder()
    
    self.mainScrollView.addSubview(paymentMethodButton)
    self.arrayOfButtonsToShowPickerView.append(paymentMethodButton)
    
  }
  
  private func initNumbersOfCreditCardTextField() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: paymentMethodButton.frame.origin.y + paymentMethodButton.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    numbersOfCreditCardTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                     title: ViewControllersConstants.FacturaCreateViewController.numbersOfCreditCardTextFieldText,
                                                                     image: nil,
                                                                     colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                     positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                                     positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    numbersOfCreditCardTextField.mainTextField.textColor = UIColor.black
    numbersOfCreditCardTextField.mainTextField.placeholder = "0000"
    numbersOfCreditCardTextField.backgroundColor = UIColor.white
    numbersOfCreditCardTextField.delegate = self
    
    numbersOfCreditCardTextField.alpha = 0.0
    
    self.mainScrollView.addSubview(numbersOfCreditCardTextField)
    
  }
  
  private func initTypeOfMoneyButton() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: paymentMethodButton.frame.origin.y + paymentMethodButton.frame.size.height,
                                     width: mainScrollView.frame.size.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var arrayOfOptions: [Option] = [Option]()
    
    for i in 0...arrayCurrencies.count - 1 {
      
      let newOption = Option(id: arrayCurrencies[i].id, name: arrayCurrencies[i].id, type: arrayCurrencies[i].name)
      arrayOfOptions.append(newOption)
      
    }
    
    typeOfMoneyButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                      newTitle: ViewControllersConstants.FacturaCreateViewController.typeOfMoneyButtonText,
                                                      newArrayOfOptions: arrayOfOptions)
    typeOfMoneyButton.delegate = self
    typeOfMoneyButton.drawBottomBorder()
    _ = typeOfMoneyButton.selectElementWithName(nameToLookFor: "MXN")
    
    self.mainScrollView.addSubview(typeOfMoneyButton)
    self.arrayOfButtonsToShowPickerView.append(typeOfMoneyButton)
    
  }
  
  private func initExchangeRateTextField() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: typeOfMoneyButton.frame.origin.y + typeOfMoneyButton.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    exchangeRateTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                              title: ViewControllersConstants.FacturaCreateViewController.exchangeRateTextFieldText,
                                                              image: nil,
                                                              colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                              positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                              positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    exchangeRateTextField.mainTextField.textColor = UIColor.black
    exchangeRateTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateViewController.exchangeRateTextFieldText
    exchangeRateTextField.mainTextField.keyboardType = .decimalPad
    exchangeRateTextField.backgroundColor = UIColor.white
    exchangeRateTextField.delegate = self
    
    exchangeRateTextField.alpha = 0.0
    
    self.mainScrollView.addSubview(exchangeRateTextField)
    
  }
  
  private func initSerieButton() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: typeOfMoneyButton.frame.origin.y + typeOfMoneyButton.frame.size.height,
                                     width: mainScrollView.frame.size.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var arrayOfOptions: [Option] = [Option]()
    
    for serie in arraySeries {
      
      if serie.type == "F" {
        
        let newOption = Option(id: serie.id, name: serie.name, type: serie.type)
        arrayOfOptions.append(newOption)
        
      }
      
    }
    
    serieButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                newTitle: ViewControllersConstants.FacturaCreateViewController.serieButtonText,
                                                newArrayOfOptions: arrayOfOptions)
    serieButton.delegate = self
    serieButton.drawBottomBorder()
    serieButton.selectFirstOption()
    
    self.mainScrollView.addSubview(serieButton)
    self.arrayOfButtonsToShowPickerView.append(serieButton)
    
  }
  
  private func initDecimalNumberButton() {
    
    if decimalNumbersButton != nil {
      
      decimalNumbersButton.removeFromSuperview()
      decimalNumbersButton = nil
      
    }
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: serieButton.frame.origin.y + serieButton.frame.size.height,
                                     width: UIScreen.main.bounds.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let twoDecimals: Option = Option(id: "2", name: "2", type: "2")
    let fourDecimals: Option = Option(id: "4", name: "4", type: "4")
    let sixDecimals: Option = Option(id: "6", name: "6", type: "6")
    
    arrayDecimalNumbers = [twoDecimals, fourDecimals, sixDecimals]
    
    decimalNumbersButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                             newTitle: ViewControllersConstants.FacturaCreateViewController.decimalNumberButtonText,
                                                             newArrayOfOptions: arrayDecimalNumbers)
    decimalNumbersButton.drawBottomBorder()
    decimalNumbersButton.delegate = self
    _ = decimalNumbersButton.selectElementWithName(nameToLookFor: "Factura")
    
    self.mainScrollView.addSubview(decimalNumbersButton)
    self.arrayOfButtonsToShowPickerView.append(decimalNumbersButton)
    
  }
  
  private func initOrderTextField() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: decimalNumbersButton.frame.origin.y + decimalNumbersButton.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    orderTextFieldView = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                           title: ViewControllersConstants.FacturaCreateViewController.orderTextFieldText,
                                                           image: nil,
                                                           colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                           positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                           positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    orderTextFieldView.mainTextField.textColor = UIColor.black
    orderTextFieldView.mainTextField.placeholder = ViewControllersConstants.FacturaCreateViewController.orderTextFieldText
    orderTextFieldView.backgroundColor = UIColor.white
    orderTextFieldView.delegate = self
    self.mainScrollView.addSubview(orderTextFieldView)
    
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    
    super.viewWillDisappear(animated)
    
    if paymentWayButton != nil {
      
      paymentWayButton.hidePickerView()
      
    }
    
    if paymentMethodButton != nil {
      
      paymentMethodButton.hidePickerView()
      
    }
    
    if typeOfMoneyButton != nil {
      
      typeOfMoneyButton.hidePickerView()
      
    }
    
    if serieButton != nil {
      
      serieButton.hidePickerView()
      
    }
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(true)
    
    if willAppearAfterCreateExpeditinoPlace == true {
    
    
    
    }
    
  }
  
  func lastButtonToShowPickerView(sender: ButtonToActivePickerView) {
    
    self.hideKeyboard()
    
    lastButtonToShowPickerView = sender
    
    for button in arrayOfButtonsToShowPickerView {
      
      if button != lastButtonToShowPickerView {
        
        button.hidePickerView()
        
      }
      
    }
    
  }
  
  func selectActionFinalized(sender: ButtonToActivePickerView) {
    
    if sender == typeOfMoneyButton {
      
      let currencyName = self.getNameOfCurrencySelected()
      
      if currencyName != "MXN" && currencyName != ""{
        
        self.showExchangeRateTextField()
        
      } else
        if currencyName == "MXN" {
          
          self.hideExchangeRateTextField()
          
      }
      
    } else
      if sender == paymentMethodButton {
        
        let paymentMethod = self.getNameOfPaymentMethodSelected()
        
        let paymentMethodWithoutNumber = paymentMethod.components(separatedBy: "-").last
        
        if paymentMethodWithoutNumber != nil && (paymentMethodWithoutNumber == "Tarjeta de crédito" || paymentMethodWithoutNumber == "Cheque nominativo" || paymentMethodWithoutNumber == "Transferencia electrónica de fondos" || paymentMethodWithoutNumber == "Tarjeta de débito" || paymentMethodWithoutNumber == "Tarjeta de servicio") {
          
          self.showNumbersOfCreditCardTextField()
          
        } else
          if paymentMethodWithoutNumber != "Tarjeta de crédito" && paymentMethodWithoutNumber != "Cheque nominativo" && paymentMethodWithoutNumber != "Transferencia electrónica de fondos" && paymentMethodWithoutNumber != "Tarjeta de débito" && paymentMethodWithoutNumber != "Tarjeta de servicio" {
            
            self.hideNumbersOfCreditCardTextField()
            
        }
        
    }
    
  }
  
  private func showNumbersOfCreditCardTextField() {
    
    let newFrameForTypeOfMoneyButton = CGRect.init(x: typeOfMoneyButton.frame.origin.x,
                                                   y: numbersOfCreditCardTextField.frame.origin.y + numbersOfCreditCardTextField.frame.size.height,
                                                   width: typeOfMoneyButton.frame.size.width,
                                                   height: typeOfMoneyButton.frame.size.height)
    
    let newFrameForExchangeRateTextField = CGRect.init(x: exchangeRateTextField.frame.origin.x,
                                                       y: newFrameForTypeOfMoneyButton.origin.y + newFrameForTypeOfMoneyButton.size.height,
                                                       width: exchangeRateTextField.frame.size.width,
                                                       height: exchangeRateTextField.frame.size.height)
    
    var newFrameForSerieButton = CGRect.init(x: serieButton.frame.origin.x,
                                             y: newFrameForTypeOfMoneyButton.origin.y + newFrameForTypeOfMoneyButton.size.height,
                                             width: serieButton.frame.size.width,
                                             height: serieButton.frame.size.height)
    
    if exchangeRateTextField.alpha == 1.0 {
      
      newFrameForSerieButton = CGRect.init(x: serieButton.frame.origin.x,
                                           y: newFrameForExchangeRateTextField.origin.y + newFrameForExchangeRateTextField.size.height,
                                           width: serieButton.frame.size.width,
                                           height: serieButton.frame.size.height)
      
    }
    
    let newFrameForDecimalButton = CGRect.init(x: decimalNumbersButton.frame.origin.x,
                                               y: newFrameForSerieButton.origin.y + newFrameForSerieButton.size.height,
                                               width: decimalNumbersButton.frame.size.width,
                                               height: decimalNumbersButton.frame.size.height)
    
    let newFrameForOrderTextField = CGRect.init(x: orderTextFieldView.frame.origin.x,
                                                y: newFrameForDecimalButton.origin.y + newFrameForDecimalButton.size.height,
                                                width: orderTextFieldView.frame.size.width,
                                                height: orderTextFieldView.frame.size.height)
    
    UIView.animate(withDuration: 0.3, animations: {
      
      self.typeOfMoneyButton.drawTopBorder()
      
      self.typeOfMoneyButton.frame = newFrameForTypeOfMoneyButton
      self.exchangeRateTextField.frame = newFrameForExchangeRateTextField
      self.serieButton.frame = newFrameForSerieButton
      self.decimalNumbersButton.frame = newFrameForDecimalButton
      self.orderTextFieldView.frame = newFrameForOrderTextField
      
      self.numbersOfCreditCardTextField.alpha = 1.0
      
    }, completion: { (isFinished) in
      
      if isFinished == true {
        
        self.mainScrollView.contentSize = CGSize.init(width: self.mainScrollView.contentSize.width,
                                                      height: self.mainScrollView.contentSize.height + self.numbersOfCreditCardTextField.frame.size.height)
        
      }
      
    })
    
  }
  
  private func hideNumbersOfCreditCardTextField() {
    
    let newFrameForTypeOfMoneyButton = CGRect.init(x: typeOfMoneyButton.frame.origin.x,
                                                   y: paymentMethodButton.frame.origin.y + paymentMethodButton.frame.size.height,
                                                   width: typeOfMoneyButton.frame.size.width,
                                                   height: typeOfMoneyButton.frame.size.height)
    
    let newFrameForExchangeRateTextField = CGRect.init(x: exchangeRateTextField.frame.origin.x,
                                                       y: newFrameForTypeOfMoneyButton.origin.y + newFrameForTypeOfMoneyButton.size.height,
                                                       width: exchangeRateTextField.frame.size.width,
                                                       height: exchangeRateTextField.frame.size.height)
    
    var newFrameForSerieButton = CGRect.init(x: serieButton.frame.origin.x,
                                             y: newFrameForTypeOfMoneyButton.origin.y + newFrameForTypeOfMoneyButton.size.height,
                                             width: serieButton.frame.size.width,
                                             height: serieButton.frame.size.height)
    
    if exchangeRateTextField.alpha == 1.0 {
      
      newFrameForSerieButton = CGRect.init(x: serieButton.frame.origin.x,
                                           y: newFrameForExchangeRateTextField.origin.y + newFrameForExchangeRateTextField.size.height,
                                           width: serieButton.frame.size.width,
                                           height: serieButton.frame.size.height)
      
    }
    
    let newFrameForDecimalButton = CGRect.init(x: decimalNumbersButton.frame.origin.x,
                                               y: newFrameForSerieButton.origin.y + newFrameForSerieButton.size.height,
                                               width: decimalNumbersButton.frame.size.width,
                                               height: decimalNumbersButton.frame.size.height)
    
    let newFrameForOrderTextField = CGRect.init(x: orderTextFieldView.frame.origin.x,
                                                y: newFrameForDecimalButton.origin.y + newFrameForDecimalButton.size.height,
                                                width: orderTextFieldView.frame.size.width,
                                                height: orderTextFieldView.frame.size.height)
    
    UIView.animate(withDuration: 0.3, animations: {
      
      self.typeOfMoneyButton.drawTopBorder()
      
      self.typeOfMoneyButton.frame = newFrameForTypeOfMoneyButton
      self.exchangeRateTextField.frame = newFrameForExchangeRateTextField
      self.serieButton.frame = newFrameForSerieButton
      self.decimalNumbersButton.frame = newFrameForDecimalButton
      self.orderTextFieldView.frame = newFrameForOrderTextField
      
      self.numbersOfCreditCardTextField.alpha = 0.0
      
    }, completion: { (isFinished) in
      
      if isFinished == true {
        
        self.mainScrollView.contentSize = CGSize.init(width: self.mainScrollView.contentSize.width,
                                                      height: self.mainScrollView.contentSize.height + self.numbersOfCreditCardTextField.frame.size.height)
        
      }
      
    })
    
  }
  
  private func showExchangeRateTextField() {
    
    let newFrameForSerieButton = CGRect.init(x: serieButton.frame.origin.x,
                                             y: exchangeRateTextField.frame.origin.y + exchangeRateTextField.frame.size.height,
                                             width: serieButton.frame.size.width,
                                             height: serieButton.frame.size.height)
    
    let newFrameForDecimalButton = CGRect.init(x: decimalNumbersButton.frame.origin.x,
                                               y: newFrameForSerieButton.origin.y + newFrameForSerieButton.size.height,
                                               width: decimalNumbersButton.frame.size.width,
                                               height: decimalNumbersButton.frame.size.height)
    
    let newFrameForOrderTextField = CGRect.init(x: orderTextFieldView.frame.origin.x,
                                                y: newFrameForDecimalButton.origin.y + newFrameForDecimalButton.size.height,
                                                width: orderTextFieldView.frame.size.width,
                                                height: orderTextFieldView.frame.size.height)
    
    UIView.animate(withDuration: 0.3, animations: {
      
      self.serieButton.drawTopBorder()
      self.serieButton.frame = newFrameForSerieButton
      self.decimalNumbersButton.frame = newFrameForDecimalButton
      self.orderTextFieldView.frame = newFrameForOrderTextField
      self.exchangeRateTextField.alpha = 1.0
      
    }, completion: { (isFinished) in
      
      if isFinished == true {
        
        self.mainScrollView.contentSize = CGSize.init(width: self.mainScrollView.contentSize.width,
                                                      height: self.mainScrollView.contentSize.height + self.exchangeRateTextField.frame.size.height)
        
      }
      
    })
    
  }
  
  private func hideExchangeRateTextField() {
    
    let newFrameForSerieButton = CGRect.init(x: serieButton.frame.origin.x,
                                             y: typeOfMoneyButton.frame.origin.y + typeOfMoneyButton.frame.size.height,
                                             width: serieButton.frame.size.width,
                                             height: serieButton.frame.size.height)
    
    let newFrameForDecimalButton = CGRect.init(x: decimalNumbersButton.frame.origin.x,
                                               y: newFrameForSerieButton.origin.y + newFrameForSerieButton.size.height,
                                               width: decimalNumbersButton.frame.size.width,
                                               height: decimalNumbersButton.frame.size.height)
    
    let newFrameForOrderTextField = CGRect.init(x: orderTextFieldView.frame.origin.x,
                                                y: newFrameForDecimalButton.origin.y + newFrameForDecimalButton.size.height,
                                                width: orderTextFieldView.frame.size.width,
                                                height: orderTextFieldView.frame.size.height)
    
    UIView.animate(withDuration: 0.3, animations: {
      
      self.exchangeRateTextField.alpha = 0.0
      self.serieButton.frame = newFrameForSerieButton
      self.decimalNumbersButton.frame = newFrameForDecimalButton
      self.orderTextFieldView.frame = newFrameForOrderTextField
      
    }, completion: { (isFinished) in
      
      if isFinished == true {
        
        self.mainScrollView.contentSize = CGSize.init(width: self.mainScrollView.contentSize.width,
                                                      height: self.mainScrollView.contentSize.height - self.exchangeRateTextField.frame.size.height)
        
      }
      
    })
    
  }
  
  //MARK: CustomTextFieldWithTitleViewDelegate
  
  func customTextFieldSelected(sender: CustomTextFieldWithTitleView) {
    
    self.hideAllOptionsOfButtons()
    
  }
  
  private func hideAllOptionsOfButtons() {
    
    for button in arrayOfButtonsToShowPickerView {
      
      button.hidePickerView()
      
    }
    
  }
  
  @objc private func hideOptionsAndKeyboard() {
    
    self.hideAllOptionsOfButtons()
    self.hideKeyboard()
    
  }
  
  private func hideKeyboard() {
    
    self.view.endEditing(true)
    
  }
  
  
  
  @objc private func backButtonPressed() {
    
    self.hideOptionsAndKeyboard()
    
    _ = self.navigationController?.popViewController(animated: true)
    
  }
  
  @objc private func nextButtonPressed() {
    
    self.hideOptionsAndKeyboard()
    
    self.getSelectedData()
    
    let thirdPartScreen = CreateGenericCFDIThirdPartViewController.init(newGenericCFDIDataToCreate: genericCFDIDataToCreate)
    
    _ = self.navigationController?.pushViewController(thirdPartScreen, animated: true)
    
  }
  
  private func getSelectedData() {
    
    //UNCOMMENT THIS IN FUTURE
    
    if self.getPaymentWaySelected() != true {
      
      
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor elige una forma de pago",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
      
    }
    
    if self.getPaymentMethodSelected() != true {
      
      
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor elige un método de pago",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
    if self.getCurrencySelected() != true {
      
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor elige un tipo de moneda",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
    if self.getDecimalNumberSelected() != true {
      
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor elige un número de decimales para el cálculo",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
    if self.getSerieSelected() != true {
      
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor elige una serie",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
    _ = self.getOrderWroteDown()
    
    if exchangeRateTextField.alpha == 1.0 && (self.getExchangeRateWroteDown() == false) {
      
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor escribe un tipo de cambio correcto",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }
    
    if numbersOfCreditCardTextField.alpha == 1.0 {
      
      _ = self.getCreditCardNumberWroteDown()
      
    }
    
    
  }
  
  //Return true if found a value

  
  //Return true if found a value
  private func getPaymentWaySelected() -> Bool {
    
    let paymentWaySelected = paymentWayButton.getValueSelected()
    
    if paymentWaySelected != nil {
      
      for paymentWay in arrayWayToPay {
        
        if paymentWaySelected!.id == paymentWay.id {
          
          genericCFDIDataToCreate.paymentWay = paymentWay
          return true
          
        }
        
      }
      
      //supposedly never happen
      return false
      
    } else {
      
      return false
      
    }
    
  }
  
  //Return true if found a value
  private func getPaymentMethodSelected() -> Bool {
    
    let paymentMethodSelected = paymentMethodButton.getValueSelected()
    
    if paymentMethodSelected != nil {
      
      for paymentMethod in arrayPaymentMethod {
        
        if paymentMethodSelected!.id == paymentMethod.id {
          
          genericCFDIDataToCreate.paymentMethod = paymentMethod
          return true
          
        }
        
      }
      
      //supposedly never happen
      return false
      
    } else {
      
      return false
      
    }
    
  }
  
  private func getNameOfPaymentMethodSelected() -> String {
    
    let paymentMethodSelected = paymentMethodButton.getValueSelected()
    
    if paymentMethodSelected != nil {
      
      return paymentMethodSelected!.name
      
    } else {
      
      return ""
      
    }
    
  }
  
  //Return true if found a value
  private func getCurrencySelected() -> Bool {
    
    let currencySelected = typeOfMoneyButton.getValueSelected()
    
    if currencySelected != nil {
      
      for currency in arrayCurrencies {
        
        if currencySelected!.name == currency.id {
          
          genericCFDIDataToCreate.typeOfMoney = currency
          return true
          
        }
        
      }
      
      //supposedly never happen
      return false
      
    } else {
      
      return false
      
    }
    
  }
  
  private func getNameOfCurrencySelected() -> String {
    
    let currencySelected = typeOfMoneyButton.getValueSelected()
    
    if currencySelected != nil {
      
      return currencySelected!.name
      
    } else {
      
      return ""
      
    }
    
  }
  
  //Return true if found a value
  private func getSerieSelected() -> Bool {
    
    let serieOptionSelected = serieButton.getValueSelected()
    
    if serieOptionSelected != nil {
      
      for serie in arraySeries {
        
        if serieOptionSelected!.name == serie.name {
          
          genericCFDIDataToCreate.serie = serie
          return true
          
        }
        
      }
      
      //supposedly never happen
      return false
      
    } else {
      
      return false
      
    }
    
  }
  
  //Return true if found a value
  private func getDecimalNumberSelected() -> Bool {
    
    let decimalNumberSelected = decimalNumbersButton.getValueSelected()
    
    if decimalNumberSelected != nil {
      
      for decimalNumber in arrayDecimalNumbers {
        
        if decimalNumberSelected!.name == decimalNumber.name {
          
          genericCFDIDataToCreate.decimalNumber = decimalNumber
          return true
          
        }
        
      }
      
      //supposedly never happen
      return false
      
    } else {
      
      return false
      
    }
    
  }
  
  //Return true if found a value
  private func getOrderWroteDown() -> Bool {
    
    genericCFDIDataToCreate.numOrder = orderTextFieldView.mainTextField.text!
    
    return UtilityManager.sharedInstance.isValidText(testString: genericCFDIDataToCreate.numOrder)
    
  }
  
  private func getExchangeRateWroteDown() -> Bool {
    
    genericCFDIDataToCreate.exchangeRate = exchangeRateTextField.mainTextField.text!
    
    let doubleValue = Double(self.exchangeRateTextField.mainTextField.text!)
    
    if doubleValue != nil && doubleValue! > 0.0 {
      
      return true
      
    } else {
      
      return false
      
    }
    //
    //    return !self.exchangeRateTextField.mainTextField.text!.isEmpty && exchangeRateTextField.mainTextField.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    
  }
  
  private func getCreditCardNumberWroteDown() -> Bool {
    
    genericCFDIDataToCreate.creditCardNumber = numbersOfCreditCardTextField.mainTextField.text!
    
    return !self.numbersOfCreditCardTextField.mainTextField.text!.isEmpty && numbersOfCreditCardTextField.mainTextField.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    
  }
  
}

