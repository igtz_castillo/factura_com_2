//
//  CreateConceptoForReciboViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 02/06/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

protocol CreateConceptoForReciboViewControllerDelegate {
  
  func conceptoDidCreate(newConcepto: Concepto)
  
}

class CreateConceptoForReciboViewController: UIViewController, ButtonToActivePickerViewDelegate, CustomTextFieldWithTitleViewDelegate, LabelWithSwitchUsingPickerDelegate, UITextFieldDelegate {
  
  private var conceptoToCreate: Concepto! = nil
  private var arrayOfUnits: Array<Unity>! = Array<Unity>()
  
  private var mainScrollView: UIScrollView! = nil
  
  private var unityButton: ButtonToActivePickerView! = nil
  private var notionCustomTextField: CustomTextFieldWithTitleView! = nil
  private var unityCostCustomTextField: CustomTextFieldWithTitleView! = nil
  private var quantityCustomTextField: CustomTextFieldWithTitleView! = nil
  private var discountCustomTextField: CustomTextFieldWithTitleView! = nil
  
  private var ivaLabelSwitch: LabelWithSwitch! = nil
//  private var ishLabelSwitch: LabelWithSwitch! = nil
//  private var iepsLabelSwitch: LabelWithSwitch! = nil
  private var retIVALabelSwitch: LabelWithSwitch! = nil
  private var retISRLabelSwitch: LabelWithSwitch! = nil
  private var arrayOfLabelWithSwitch: Array<LabelWithSwitch>! = Array<LabelWithSwitch>()
  
  private var optionsIva: Array<Option>! = nil
  private var optionsIsh: Array<Option>! = nil
  private var optionsIeps: Array<Option>! = nil
  private var optionsRetIva: Array<Option>! = nil
  private var optionsRetIsr: Array<Option>! = nil
  private var optionsRetIvaPorte: Array<Option>! = nil
  
  private var subTotalAdditionLabel: LabelForAdditions! = nil
  
  
  private var createButton: UIButton! = nil
  
  var delegate: CreateConceptoForReciboViewControllerDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  init(newArrayOfUnity: Array<Unity>) {
    
    self.arrayOfUnits = newArrayOfUnity
    
    super.init(nibName: nil, bundle: nil)
    
  }
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.white
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
    tapGesture.numberOfTouchesRequired = 1
    
    self.view.addGestureRecognizer(tapGesture)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.initMainScrollView()
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.requestToGetAllKindOfTaxes(actionsToMakeWhenSucceeded: { (iepsOptions, ishOptions, ivaOptions, retIsrOptions, retIvaOptions, retIvaPorteOptions) in
      
      print(iepsOptions, ishOptions, ivaOptions, retIsrOptions, retIvaOptions, retIvaPorteOptions)
      
      self.optionsIeps = iepsOptions
      self.optionsIsh = ishOptions
      self.optionsIva = ivaOptions
      self.optionsRetIsr = retIsrOptions
      self.optionsRetIva = retIvaOptions
      self.optionsRetIvaPorte = retIvaPorteOptions
      
      self.initUnityButton()
      self.initNotionCustomTextField()
      self.initUnityCostCustomTextField()
      self.initQuantityCustomTextField()
      self.initDiscountCustomTextField()
      
      self.initIvaLabelSwitch()
//      self.initIshLabelSwitch()
//      self.initIepsLabelSwitch()
      self.initRetIVALabelSwitch()
      self.initRetISRLabelSwitch()
      
      self.initSecondPartSubTotalAdditionLabel()
      
      self.initCreateButton()
      
      UtilityManager.sharedInstance.hideLoader()
      
    }, actionsToMakeWhenFailed: {
      
      _ = self.navigationController?.popViewController(animated: true)
      
      UtilityManager.sharedInstance.hideLoader()
      
    })
    
  }
  
  private func initMainScrollView() {
    
    mainScrollView = UIScrollView.init(frame: self.view.frame)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.frame.size.height + (180.0 * UtilityManager.sharedInstance.conversionHeight))
    
    mainScrollView.contentSize = newContentSize
    self.view.addSubview(mainScrollView)
    
  }
  
  private func initUnityButton() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: 0.0,
                                     width: UIScreen.main.bounds.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var arrayOfOptions: [Option] = [Option]()
    
    for unit in arrayOfUnits {
      
      let newOption = Option(id: "", name: unit.name, type: "")
      arrayOfOptions.append(newOption)
      
    }
    
    unityButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                newTitle: ViewControllersConstants.FacturaCreateSecondPartViewController.unityButtonText,
                                                newArrayOfOptions: arrayOfOptions)
    unityButton.drawBottomBorder()
    unityButton.delegate = self
    
    self.mainScrollView.addSubview(unityButton)
    
  }
  
  private func initNotionCustomTextField() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: unityButton.frame.origin.y + unityButton.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    notionCustomTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                              title: ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText,
                                                              image: nil,
                                                              colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                              positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                              positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    notionCustomTextField.mainTextField.textColor = UIColor.black
    notionCustomTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText
    notionCustomTextField.backgroundColor = UIColor.white
    notionCustomTextField.delegate = self
    notionCustomTextField.mainTextField.delegate = self
    notionCustomTextField.mainTextField.tag = 1
    notionCustomTextField.mainTextField.returnKeyType = .next
    
    self.mainScrollView.addSubview(notionCustomTextField)
    
  }
  
  private func initUnityCostCustomTextField() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: notionCustomTextField.frame.origin.y + notionCustomTextField.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    unityCostCustomTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                 title: ViewControllersConstants.FacturaCreateSecondPartViewController.unityCostTextFieldText,
                                                                 image: nil,
                                                                 colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                 positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                                 positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    unityCostCustomTextField.mainTextField.textColor = UIColor.black
    unityCostCustomTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.unityCostTextFieldText
    unityCostCustomTextField.mainTextField.keyboardType = .numberPad
    unityCostCustomTextField.backgroundColor = UIColor.white
    unityCostCustomTextField.delegate = self
    unityCostCustomTextField.mainTextField.delegate = self
    unityCostCustomTextField.mainTextField.tag = 2
    unityCostCustomTextField.mainTextField.returnKeyType = .next
    self.mainScrollView.addSubview(unityCostCustomTextField)
    
  }
  
  private func initQuantityCustomTextField() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: unityCostCustomTextField.frame.origin.y + unityCostCustomTextField.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    quantityCustomTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                title: ViewControllersConstants.FacturaCreateSecondPartViewController.quantityTextFieldText,
                                                                image: nil,
                                                                colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                                positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    quantityCustomTextField.mainTextField.textColor = UIColor.black
    quantityCustomTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.quantityTextFieldText
    quantityCustomTextField.backgroundColor = UIColor.white
    quantityCustomTextField.mainTextField.keyboardType = .numberPad
    quantityCustomTextField.delegate = self
    quantityCustomTextField.mainTextField.delegate = self
    quantityCustomTextField.mainTextField.tag = 3
    quantityCustomTextField.mainTextField.returnKeyType = .next
    self.mainScrollView.addSubview(quantityCustomTextField)
    
  }
  
  private func initDiscountCustomTextField() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: quantityCustomTextField.frame.origin.y + quantityCustomTextField.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    discountCustomTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                title: ViewControllersConstants.FacturaCreateSecondPartViewController.discountTextFieldText,
                                                                image: nil,
                                                                colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                                positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    discountCustomTextField.mainTextField.textColor = UIColor.black
    discountCustomTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.discountTextFieldText
    discountCustomTextField.backgroundColor = UIColor.white
    discountCustomTextField.delegate = self
    discountCustomTextField.mainTextField.delegate = self
    discountCustomTextField.mainTextField.tag = 4
    discountCustomTextField.mainTextField.returnKeyType = .next
    self.mainScrollView.addSubview(discountCustomTextField)
    
  }
  
  private func initIvaLabelSwitch() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: discountCustomTextField.frame.origin.y + discountCustomTextField.frame.size.height + 1.0,
                                   width: mainScrollView.frame.size.width,
                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
    
    ivaLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: true, optionsToShow: self.optionsIva, titleForPicker: "Opciones de IVA", viewToAddPickerView: UtilityManager.sharedInstance.currentViewController().view, constantStringWhenChangeValue: "IVA ")
    
    ivaLabelSwitch.isUserInteractionEnabled = true
    ivaLabelSwitch.delegateForPicker = self
    _ = ivaLabelSwitch.selectElementWithName(nameToLookFor: "16%")
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = .left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.ivaLabelSwitchText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    ivaLabelSwitch.attributedText = stringWithFormat
    arrayOfLabelWithSwitch.append(ivaLabelSwitch)
    self.mainScrollView.addSubview(ivaLabelSwitch)
    
  }
  
//  private func initIshLabelSwitch() {
//    
//    let frameForView = CGRect.init(x: 0.0,
//                                   y: ivaLabelSwitch.frame.origin.y + ivaLabelSwitch.frame.size.height,
//                                   width: mainScrollView.frame.size.width,
//                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
//    
//    ishLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false, optionsToShow: self.optionsIsh, titleForPicker: "Opciones de ISH", viewToAddPickerView: UtilityManager.sharedInstance.currentViewController().view, constantStringWhenChangeValue: "ISH ")
//    
//    ishLabelSwitch.isUserInteractionEnabled = true
//    ishLabelSwitch.delegateForPicker = self
//    
//    let font = UIFont(name: "SFUIText-Light",
//                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
//    let color = UtilityManager.sharedInstance.labelsAndLinesColor
//    let style = NSMutableParagraphStyle()
//    style.alignment = .left
//    
//    let stringWithFormat = NSMutableAttributedString(
//      string: ViewControllersConstants.FacturaCreateSecondPartViewController.ishLabelSwitchText,
//      attributes:[NSFontAttributeName: font!,
//                  NSParagraphStyleAttributeName: style,
//                  NSForegroundColorAttributeName: color
//      ]
//    )
//    
//    ishLabelSwitch.attributedText = stringWithFormat
//    arrayOfLabelWithSwitch.append(ishLabelSwitch)
//    self.mainScrollView.addSubview(ishLabelSwitch)
//    
//  }
//  
//  private func initIepsLabelSwitch() {
//    
//    let frameForView = CGRect.init(x: 0.0,
//                                   y: ishLabelSwitch.frame.origin.y + ishLabelSwitch.frame.size.height,
//                                   width: mainScrollView.frame.size.width,
//                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
//    
//    iepsLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false, optionsToShow: self.optionsIeps, titleForPicker: "Opciones de IEPS", viewToAddPickerView: UtilityManager.sharedInstance.currentViewController().view, constantStringWhenChangeValue: "IEPS ")
//    
//    iepsLabelSwitch.isUserInteractionEnabled = true
//    iepsLabelSwitch.delegateForPicker = self
//    
//    let font = UIFont(name: "SFUIText-Light",
//                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
//    let color = UtilityManager.sharedInstance.labelsAndLinesColor
//    let style = NSMutableParagraphStyle()
//    style.alignment = .left
//    
//    let stringWithFormat = NSMutableAttributedString(
//      string: ViewControllersConstants.FacturaCreateSecondPartViewController.iepsLabelSwitchText,
//      attributes:[NSFontAttributeName: font!,
//                  NSParagraphStyleAttributeName: style,
//                  NSForegroundColorAttributeName: color
//      ]
//    )
//    
//    iepsLabelSwitch.attributedText = stringWithFormat
//    arrayOfLabelWithSwitch.append(iepsLabelSwitch)
//    self.mainScrollView.addSubview(iepsLabelSwitch)
//    
//  }
  
  private func initRetIVALabelSwitch() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: ivaLabelSwitch.frame.origin.y + ivaLabelSwitch.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
    
    retIVALabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false, optionsToShow: self.optionsRetIva, titleForPicker: "Opciones de Ret IVA", viewToAddPickerView: UtilityManager.sharedInstance.currentViewController().view, constantStringWhenChangeValue: "RET. IVA ")
    
    retIVALabelSwitch.isUserInteractionEnabled = true
    retIVALabelSwitch.delegateForPicker = self
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = .left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.retIVALabelSwitchText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    retIVALabelSwitch.attributedText = stringWithFormat
    arrayOfLabelWithSwitch.append(retIVALabelSwitch)
    self.mainScrollView.addSubview(retIVALabelSwitch)
    
  }
  
  private func initRetISRLabelSwitch() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: retIVALabelSwitch.frame.origin.y + retIVALabelSwitch.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
    
    retISRLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false, optionsToShow: self.optionsRetIsr, titleForPicker: "Opciones de Ret ISR", viewToAddPickerView: UtilityManager.sharedInstance.currentViewController().view, constantStringWhenChangeValue: "RET. ISR ")
    
    retISRLabelSwitch.isUserInteractionEnabled = true
    retISRLabelSwitch.delegateForPicker = self
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = .left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.retISRLabelSwitchText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    retISRLabelSwitch.attributedText = stringWithFormat
    arrayOfLabelWithSwitch.append(retISRLabelSwitch)
    self.mainScrollView.addSubview(retISRLabelSwitch)
    
  }
  
  private func initSecondPartSubTotalAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: retISRLabelSwitch.frame.origin.y + retISRLabelSwitch.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    subTotalAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    subTotalAdditionLabel.attributedText = stringWithFormat
    subTotalAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(subTotalAdditionLabel)
    
  }
  
  private func initCreateButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: (19.0 * UtilityManager.sharedInstance.conversionWidth),
                                     y: subTotalAdditionLabel.frame.origin.y + subTotalAdditionLabel.frame.size.height + (11.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    createButton = UIButton.init(frame: frameForButton)
    createButton.addTarget(self,
                           action: #selector(createButtonPressed),
                           for: .touchUpInside)
    createButton.backgroundColor = UIColor.orange
    createButton.setBackgroundColor(color: UIColor.init(red: 255.0/255.0, green: 164.0/255.0, blue: 78.0/255.0, alpha: 1.0), forState: .highlighted)
    createButton.setAttributedTitle(stringWithFormat, for: .normal)
    createButton.contentHorizontalAlignment = .left
    
    createButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.CreateConceptoViewController.createConceptoButtonText))
    
    self.mainScrollView.addSubview(createButton)
    
  }
  
  private func createGenericLabelForButton(text: String) -> UILabel {
    
    let frameForLabel = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 200.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    let genericLabel = UILabel.init(frame: frameForLabel)
    genericLabel.numberOfLines = 0
    genericLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: text,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    genericLabel.attributedText = stringWithFormat
    genericLabel.sizeToFit()
    let newFrame = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                               y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                               width: genericLabel.frame.size.width,
                               height: genericLabel.frame.size.height)
    genericLabel.frame = newFrame
    
    return genericLabel
    
  }
  
  //MARK: - ButtonToActivePickerViewDelegate
  
  func lastButtonToShowPickerView(sender: ButtonToActivePickerView) {
    
    for labelWithSwitch in arrayOfLabelWithSwitch {
      
      if sender != labelWithSwitch {
        
        labelWithSwitch.hidePickerView()
        
      }
      
    }
    
  }
  
  func selectActionFinalized(sender: ButtonToActivePickerView) {
    
    //    if sender == typeOfMoneyButton {
    //
    //      let currencyName = self.getNameOfCurrencySelected()
    //
    //      if currencyName != "MXN" {
    //
    //        self.showExchangeRateTextField()
    //
    //      }
    //
    //    }
    
  }
  
  @objc private func createButtonPressed() {
    
    let unityValueSelected = unityButton.getValueSelected()
    
    if unityValueSelected == nil {
      
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor selecciona un tipo de unidad",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    } else
      
      if notionCustomTextField.mainTextField.text?.isEmpty == true {
        
        let alertController = UIAlertController(title: "Error",
                                                message: "Por favor escribe el nombre del concepto",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
      } else
        
        if unityCostCustomTextField.mainTextField.text?.isEmpty == true && unityCostCustomTextField.mainTextField.text!.rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted) != nil {
          
          let alertController = UIAlertController(title: "Error",
                                                  message: "Por favor escribe un costo por unidad válido del concepto",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
        } else
          
          if quantityCustomTextField.mainTextField.text?.isEmpty == true && quantityCustomTextField.mainTextField.text!.rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted) != nil {
            
            let alertController = UIAlertController(title: "Error",
                                                    message: "Por favor escribe la cantidad de elementos para el concepto",
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
            alertController.addAction(cancelAction)
            
            let actualController = UtilityManager.sharedInstance.currentViewController()
            actualController.present(alertController, animated: true, completion: nil)
            
          } else {
            
            let newConcepto = Concepto.init()
            
            newConcepto.conceptDescription = notionCustomTextField.mainTextField.text!
            newConcepto.unit = Unity(newId: unityValueSelected!.id, newName: unityValueSelected!.name) 
            newConcepto.price = Double(unityCostCustomTextField.mainTextField.text!)
            newConcepto.quantity = Double(quantityCustomTextField.mainTextField.text!)
            newConcepto.subtotal = (newConcepto.price * newConcepto.quantity)
            
            
            if discountCustomTextField.mainTextField.text?.isEmpty != true && discountCustomTextField.mainTextField.text!.rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted) == nil {
              
              newConcepto.discountPercentage = Double(discountCustomTextField.mainTextField.text!)
              
              newConcepto.finalDiscount = newConcepto.subtotal * (newConcepto.discountPercentage / 100.0)
              
            }
            
            if ivaLabelSwitch.getValueOfSwitch() == true {
              
              let ivaOptionSelected = ivaLabelSwitch.getValueSelected()
              
              if ivaOptionSelected != nil {
                
                newConcepto.applyIva = true
                newConcepto.ivaValueToSendToServer = Double(ivaOptionSelected!.type)!
                newConcepto.finalIva = (newConcepto.subtotal - newConcepto.finalDiscount) * ( Double(ivaOptionSelected!.type)! / 100.0)
                
              }
              
            }
            
//            if ishLabelSwitch.getValueOfSwitch() == true {
//              
//              let ishOptionSelected = ishLabelSwitch.getValueSelected()
//              
//              if ishOptionSelected != nil {
//                
//                newConcepto.applyIsh = true
//                newConcepto.ishValueToSendToServer = Double(ishOptionSelected!.type)!
//                newConcepto.finalIsh = newConcepto.subtotal * ( Double(ishOptionSelected!.type)! / 100.0)
//                
//              }
//              
//            }
//            
//            if iepsLabelSwitch.getValueOfSwitch() == true {
//              
//              let iepsOptionSelected = iepsLabelSwitch.getValueSelected()
//              
//              if iepsOptionSelected != nil {
//                
//                newConcepto.applyIeps = true
//                newConcepto.iepsValueToSendToServer = Double(iepsOptionSelected!.type)!
//                newConcepto.finalIeps = newConcepto.subtotal * ( Double(iepsOptionSelected!.type)! / 100.0)
//                
//              }
//              
//            }
            
            if retIVALabelSwitch.getValueOfSwitch() == true {
              
              let retIVAOptionSelected = retIVALabelSwitch.getValueSelected()
              
              if retIVAOptionSelected != nil {
                
                newConcepto.applyRetIva = true
                newConcepto.retIvaValueToSendToServer = Double(retIVAOptionSelected!.type)!
                newConcepto.finalRetIva = (newConcepto.subtotal - newConcepto.finalDiscount) * ( Double(retIVAOptionSelected!.type)! / 100.0)
                
              }
              
            }
            
            if retISRLabelSwitch.getValueOfSwitch() == true {
              
              let retISROptionSelected = retISRLabelSwitch.getValueSelected()
              
              if retISROptionSelected != nil {
                
                newConcepto.applyRetIsr = true
                newConcepto.retIsrValueToSendToServer = Double(retISROptionSelected!.type)!
                newConcepto.finalRetIsr = (newConcepto.subtotal - newConcepto.finalDiscount) * ( Double(retISROptionSelected!.type)! / 100.0)
                
              }
              
            }
            
            let alertController = UIAlertController(title: "Éxito",
                                                    message: "El concepto ha sido creado correctamente",
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
              
              self.delegate?.conceptoDidCreate(newConcepto: newConcepto)
              
              _ = self.navigationController?.popViewController(animated: true)
              
            }
            alertController.addAction(cancelAction)
            
            let actualController = UtilityManager.sharedInstance.currentViewController()
            actualController.present(alertController, animated: true, completion: nil)
            
            
    }
    
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    
    super.viewWillDisappear(animated)
    
    unityButton.hidePickerView()
    
    for labelSwitch in arrayOfLabelWithSwitch {
      
      labelSwitch.hidePickerView()
      
    }
    
  }
  
  //MARK: - LabelWithSwitchPickerDelegate
  
  func switchChangeToOn(sender: LabelWithSwitch) {
    
//    if sender == ishLabelSwitch {
//      
//      retIVALabelSwitch.switchView.setOn(false, animated: true)
//      retIVALabelSwitch.switchView.isEnabled = false
//      
//      retISRLabelSwitch.switchView.setOn(false, animated: true)
//      retISRLabelSwitch.switchView.isEnabled = false
//      
//      iepsLabelSwitch.switchView.setOn(false, animated: true)
//      iepsLabelSwitch.switchView.isEnabled = false
//      
//    } else
//      
//      if sender == iepsLabelSwitch {
//        
//        retIVALabelSwitch.switchView.setOn(false, animated: true)
//        retIVALabelSwitch.switchView.isEnabled = false
//        
//        retISRLabelSwitch.switchView.setOn(false, animated: true)
//        retISRLabelSwitch.switchView.isEnabled = false
//        
//        ishLabelSwitch.switchView.setOn(false, animated: true)
//        ishLabelSwitch.switchView.isEnabled = false
//        
//      } else
//        
//        if sender == retISRLabelSwitch || sender == retIVALabelSwitch {
//          
//          iepsLabelSwitch.switchView.setOn(false, animated: true)
//          iepsLabelSwitch.switchView.isEnabled = false
//          
//          ishLabelSwitch.switchView.setOn(false, animated: true)
//          ishLabelSwitch.switchView.isEnabled = false
//          
//    }
    
    
    
    
    
    for labelWithSwitch in arrayOfLabelWithSwitch {
      
      if sender != labelWithSwitch {
        
        labelWithSwitch.hidePickerView()
        
      }
      
    }
    
  }
  
  func switchChangeToOff(sender: LabelWithSwitch) {
    
//    if sender == ishLabelSwitch {
//      
//      retIVALabelSwitch.switchView.isEnabled = true
//      retISRLabelSwitch.switchView.isEnabled = true
//      iepsLabelSwitch.switchView.isEnabled = true
//      
//    } else
//      
//      if sender == iepsLabelSwitch {
//        
//        retIVALabelSwitch.switchView.isEnabled = true
//        retISRLabelSwitch.switchView.isEnabled = true
//        ishLabelSwitch.switchView.isEnabled = true
//        
//      } else
//        
//        if sender == retISRLabelSwitch || sender == retIVALabelSwitch && (retIVALabelSwitch.switchView.isOn == false && retISRLabelSwitch.switchView.isOn == false ) {
//          
//          iepsLabelSwitch.switchView.isEnabled = true
//          ishLabelSwitch.switchView.isEnabled = true
//          
//    }
    
  }
  
  //MARK: - CustomTextFieldWithTitleViewDelegate
  
  func customTextFieldSelected(sender: CustomTextFieldWithTitleView) {
    
    
    
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    
    for labelWithSwitch in arrayOfLabelWithSwitch {
      
      labelWithSwitch.hidePickerView()
      
    }
    
    unityButton.hidePickerView()
    
    if textField == self.quantityCustomTextField.mainTextField {
      
      let quantityDoubleValue = Double(self.quantityCustomTextField.mainTextField.text!)
      
      let quantityIntValue = Int(self.quantityCustomTextField.mainTextField.text!)
      
      let unitCostDoubleValue = Double(self.unityCostCustomTextField.mainTextField.text!)
      
      let unitCostIntValue = Int(self.unityCostCustomTextField.mainTextField.text!)
      
      if quantityDoubleValue != nil && unitCostDoubleValue != nil {
        
        self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String(quantityDoubleValue! * unitCostDoubleValue!))
        
      } else
        if quantityIntValue != nil && unitCostIntValue != nil {
          
          self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String(Double(quantityIntValue!) * Double(unitCostIntValue!)))
          
        } else
          if quantityDoubleValue != nil && unitCostIntValue != nil {
            
            self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String(quantityDoubleValue! * Double(unitCostIntValue!)))
            
          } else
            if quantityIntValue != nil && unitCostDoubleValue != nil {
              
              self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String(Double(quantityIntValue!) * unitCostDoubleValue!))
              
      }
      
    } else
      
      if textField == self.unityCostCustomTextField.mainTextField {
        
        let quantityDoubleValue = Double(self.quantityCustomTextField.mainTextField.text!)
        
        let unitCostDoubleValue = Double(self.unityCostCustomTextField.mainTextField.text!)
        
        if quantityDoubleValue != nil && unitCostDoubleValue != nil {
          
          self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String(quantityDoubleValue! * unitCostDoubleValue!))
          
        }
        
    }
    
  }
  
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    
    if textField == self.quantityCustomTextField.mainTextField {
      
      let quantityDoubleValue = Double(self.quantityCustomTextField.mainTextField.text!)
      
      let quantityIntValue = Int(self.quantityCustomTextField.mainTextField.text!)
      
      let unitCostDoubleValue = Double(self.unityCostCustomTextField.mainTextField.text!)
      
      let unitCostIntValue = Int(self.unityCostCustomTextField.mainTextField.text!)
      
      if quantityDoubleValue != nil && unitCostDoubleValue != nil {
        
        self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String(quantityDoubleValue! * unitCostDoubleValue!))
        
      } else
        if quantityIntValue != nil && unitCostIntValue != nil {
          
          self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String(Double(quantityIntValue!) * Double(unitCostIntValue!)))
          
        } else
          if quantityDoubleValue != nil && unitCostIntValue != nil {
            
            self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String(quantityDoubleValue! * Double(unitCostIntValue!)))
            
          } else
            if quantityIntValue != nil && unitCostDoubleValue != nil {
              
              self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String(Double(quantityIntValue!) * unitCostDoubleValue!))
              
      }
      
    } else
      
      if textField == self.unityCostCustomTextField.mainTextField {
        
        let quantityDoubleValue = Double(self.quantityCustomTextField.mainTextField.text!)
        
        let unitCostDoubleValue = Double(self.unityCostCustomTextField.mainTextField.text!)
        
        if quantityDoubleValue != nil && unitCostDoubleValue != nil {
          
          self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String(quantityDoubleValue! * unitCostDoubleValue!))
          
        }
        
    }
    
    return true
    
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    let nextTage = textField.tag + 1
    
    let nextResponder = textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
    
    if (nextResponder != nil){
      
      nextResponder?.becomeFirstResponder()
      
    } else {
      
      textField.resignFirstResponder()
      
    }
    
    return false
    
  }
  
  private func changeTextOfSubtotalLabel(newString: String) {
    
    if subTotalAdditionLabel != nil {
      
      self.subTotalAdditionLabel.removeFromSuperview()
      self.subTotalAdditionLabel = nil
      
    }
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: retISRLabelSwitch.frame.origin.y + retISRLabelSwitch.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    subTotalAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: newString,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    subTotalAdditionLabel.attributedText = stringWithFormat
    subTotalAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(subTotalAdditionLabel)
    
  }
  
  @objc private func hideKeyboard() {
    
    self.view.endEditing(true)
    
  }
  
}

