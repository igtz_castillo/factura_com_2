//
//  TableViewWithSearchBarViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 29/03/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class TableViewWithSearchBarViewController: UITableViewController {
  
  var searchController: UISearchController! = nil
  var leftButtonItemView: UIBarButtonItem! = nil
  
  
  //
  private var arrayOfElements: Array<CFDI>! = Array<CFDI>()
  private var filteredElements: Array<CFDI>! = Array<CFDI>()
  var companyView: UIImageView! = nil
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
    
  }
  
  init(style: UITableViewStyle, newArrayOfElements: Array<CFDI>) {
   
    super.init(style: style)
    
    arrayOfElements = newArrayOfElements
    filteredElements = newArrayOfElements
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.initNavigationBar()
    self.initSearchController()
    self.initTableView()
    
  }
  
  private func initNavigationBar() {
    
    let leftButton = UIBarButtonItem(title: "",
                                     style: UIBarButtonItemStyle.plain,
                                    target: self,
                                    action: nil)
    
    self.navigationItem.rightBarButtonItem = leftButton
    
    self.initLeftNavigationView()
    self.initRightNavigationView()
    
  }
  
  private func initLeftNavigationView() {
    
    self.navigationItem.leftBarButtonItems?.removeAll()
    
    let frameForView = CGRect.init(x: 0.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 0.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 22.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: 22.0 * UtilityManager.sharedInstance.conversionHeight)
      
    companyView = UIImageView.init(frame: frameForView)
    companyView.image = UIImage.init(named: "defaultUserSmallImage.png")
    companyView.backgroundColor = UIColor.lightGray
    
    if #available(iOS 11.0, *) {
    
      companyView.widthAnchor.constraint(equalToConstant: 22.0 * UtilityManager.sharedInstance.conversionWidth).isActive = true
      companyView.heightAnchor.constraint(equalToConstant: 22.0 * UtilityManager.sharedInstance.conversionHeight).isActive = true
      
    }
      
    if User.session.actualUsingCompany.urlLogo != nil {
        
      companyView.imageFromUrl(urlString: User.session.actualUsingCompany.urlLogo!)
        
    }
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(leftButtonItemPressed))
    tapGesture.numberOfTapsRequired = 1
    companyView.addGestureRecognizer(tapGesture)
    
    leftButtonItemView = UIBarButtonItem.init(customView: companyView)
    self.navigationItem.leftBarButtonItems = [leftButtonItemView]
    
    self.initGrayTriangle()
    
  }
  
  private func initGrayTriangle() {
    
    let frameForArrowView = CGRect.init(x: companyView.frame.origin.x + companyView.frame.size.width + (0.0 * UtilityManager.sharedInstance.conversionWidth),
                                        y: companyView.frame.origin.y + companyView.frame.size.height + (0.0 * UtilityManager.sharedInstance.conversionHeight),
                                        width: 8.0 * UtilityManager.sharedInstance.conversionWidth,
                                        height: 4.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let arrowImageView = UIImageView.init(frame: frameForArrowView)
    arrowImageView.image = UIImage.init(named: "triangle.png")
    
    if #available(iOS 11.0, *) {
      
      arrowImageView.widthAnchor.constraint(equalToConstant: 8.0 * UtilityManager.sharedInstance.conversionWidth).isActive = true
      arrowImageView.heightAnchor.constraint(equalToConstant: 4.0 * UtilityManager.sharedInstance.conversionHeight).isActive = true
      
    }
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(leftButtonItemPressed))
    tapGesture.numberOfTapsRequired = 1
    arrowImageView.addGestureRecognizer(tapGesture)
    
    let leftTriangleButtonItem = UIBarButtonItem.init(customView: arrowImageView)
    
    self.navigationItem.leftBarButtonItems?.append(leftTriangleButtonItem)
    
  }
  
  
  private func initRightNavigationView() {
    
    let imageBackButton = UIImage.init(named: "plus")?.withRenderingMode(.alwaysOriginal)
    
    let profileButton = UIBarButtonItem.init(image: imageBackButton,
                                          style: .plain,
                                         target: self,
                                         action: #selector(createSomethingButtonPressed))
    
    self.navigationItem.setRightBarButton(profileButton, animated: false)
    
  }
  
  private func initSearchController() {
    
    searchController = UISearchController.init(searchResultsController: nil)
    searchController.searchResultsUpdater = self
    searchController.dimsBackgroundDuringPresentation = false
    searchController.searchBar.placeholder = "Buscar"
    self.definesPresentationContext = true
    searchController.searchBar.barTintColor = UtilityManager.sharedInstance.backgroundColorForSearchBar
    
    
    self.tableView.tableHeaderView = searchController.searchBar
    
  }
  
  private func initTableView() {
    
    self.tableView.register(CustomCellTableViewCell.self, forCellReuseIdentifier: "cell")
    self.tableView.register(CustomHeaderTableCellView.self, forCellReuseIdentifier: "header")
    
  }
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    
    return 1
    
  }
  
  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    let header = tableView.dequeueReusableCell(withIdentifier: "header") as! CustomHeaderTableCellView

    return header.contentView
    
  }

  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
    return 44.0 * UtilityManager.sharedInstance.conversionHeight
    
  }
  
  @objc private func leftButtonItemPressed() {
    
    print("show action sheet")
    
    if UtilityManager.sharedInstance.isIpad() == false {
      
      if User.session.arrayOfCompanies.count > 1 {
        
        let actionSheet = UIAlertController.init(title: ViewControllersConstants.TableViewWithSearchViewController.actionSheetTitleText,
                                                 message: ViewControllersConstants.TableViewWithSearchViewController.actionSheetMessageText,
                                                 preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction.init(title: "Cancelar", style: .cancel) { action -> Void in}
        actionSheet.addAction(cancelActionButton)
        
        for company in User.session.arrayOfCompanies {
          
          let newActionButton = UIAlertAction.init(title: company.name, style: .default) { action -> Void in
            
            self.changeSelectedCompany(nextCompanySelected: company)
            self.actionToDoWhenChangeCompany()
            
          }
          
          actionSheet.addAction(newActionButton)
          
        }
        
        self.present(actionSheet, animated: true, completion: nil)
        
      }
      
    } else {
      
      if User.session.arrayOfCompanies.count > 1 {
        
        let actionSheet = UIAlertController.init(title: ViewControllersConstants.TableViewWithSearchViewController.actionSheetTitleText,
                                                 message: ViewControllersConstants.TableViewWithSearchViewController.actionSheetMessageText,
                                                 preferredStyle: .actionSheet)
        
        actionSheet.modalPresentationStyle = .popover
        
        let cancelActionButton = UIAlertAction.init(title: "Cancelar", style: .cancel) { action -> Void in}
        actionSheet.addAction(cancelActionButton)
        
        for company in User.session.arrayOfCompanies {
          
          let newActionButton = UIAlertAction.init(title: company.name, style: .default) { action -> Void in
            
            self.changeSelectedCompany(nextCompanySelected: company)
            self.actionToDoWhenChangeCompany()
            
          }
          
          actionSheet.addAction(newActionButton)
          
        }
        
        if let popoverController = actionSheet.popoverPresentationController {
          popoverController.barButtonItem = leftButtonItemView
        }
        
        
        self.present(actionSheet, animated: true, completion: nil)
        
      }
      
    }
    

    
  }
  
  func actionToDoWhenChangeCompany(){}
  
  @objc open func createSomethingButtonPressed() {
    
    print("show profile view controller")
    
  }
  
  private func changeSelectedCompany(nextCompanySelected: Company) {
    
    self.companyView.image = UIImage.init()
    self.companyView.backgroundColor = UIColor.lightGray
    
    User.session.actualUsingCompany = nextCompanySelected
    
    User.session.rfc = User.session.actualUsingCompany.rfc
    User.session.apiKey = User.session.actualUsingCompany.apiKey
    User.session.secretKey = User.session.actualUsingCompany.secretKey
    User.session.uidAccount = User.session.actualUsingCompany.uidAccount
    
    if User.session.actualUsingCompany.urlLogo != nil {
      
      self.companyView.imageFromUrl(urlString: User.session.actualUsingCompany.urlLogo!)
      
    }
    
  }
  
}

extension TableViewWithSearchBarViewController: UISearchResultsUpdating {
  
  func updateSearchResults(for searchController: UISearchController) {
    
    //self.filterContentForSearchText(searchText: searchController.searchBar.text!)
    
  }
  
}
