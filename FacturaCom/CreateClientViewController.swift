//
//  CreateClientViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 25/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class CreateClientViewController: UIViewController, UITextFieldDelegate {
  
  private var clientData: Client! = nil
  
  private var mainScrollView: UIScrollView! = nil
  
  private var clientDataLabel: LabelSeparator! = nil
  
  private var razonSocialTextField: CustomTextFieldWithTitleView! = nil
  private var rfcTextField: CustomTextFieldWithTitleView! = nil
  private var calleTextField: CustomTextFieldWithTitleView! = nil
  private var numeroExteriorTextField: CustomTextFieldWithTitleView! = nil
  private var numeroInteriorTextField: CustomTextFieldWithTitleView! = nil
  private var coloniaTextField: CustomTextFieldWithTitleView! = nil
  private var codigoPostalTextField: CustomTextFieldWithTitleView! = nil
  private var ciudadTextField: CustomTextFieldWithTitleView! = nil
  private var estadoTextField: CustomTextFieldWithTitleView! = nil
  private var localidadTextField: CustomTextFieldWithTitleView! = nil
  private var paisTextField: CustomTextFieldWithTitleView! = nil
  
  private var paisOptionButton: ButtonToActivePickerView! = nil
  private var arrayOfCountryOptions: Array<Country> = Array<Country>()
  private var regimenNumberTextField: CustomTextFieldWithTitleView! = nil
  private var fiscalResidenceTextField: CustomTextFieldWithTitleView! = nil
  
  private var viewForDisplaceElements: UIView! = nil
  
  private var secondSeparatorLabel: LabelSeparator! = nil
  
  private var clientNameTextField: CustomTextFieldWithTitleView! = nil
  private var clientLastNameTextField: CustomTextFieldWithTitleView! = nil
  private var firstEmailToSendCFDITextField: CustomTextFieldWithTitleView! = nil
  private var secondEmailToSendCFDITextField: CustomTextFieldWithTitleView! = nil
  private var thirdEmailToSendCFDITextField: CustomTextFieldWithTitleView! = nil
  private var contactPhoneTextField: CustomTextFieldWithTitleView! = nil
  
  private var bankDataSeparatorLabel: LabelSeparator! = nil
  
  private var bankOptionButton: ButtonToActivePickerView! = nil
  private var bankOptions: [Option]! = [Option]()
  private var accountNumberTextField: CustomTextFieldWithTitleView! = nil
  private let maxLenghtOfCodPos: Int = 5
  private let maxLenghtOfAccountNumbers: Int = 4
  private var lastUpdateLabel: UILabel! = nil
  
  //View for buttons
  private var backViewButtons: UIView! = nil
  
  //Buttons
  private var createClientButton: UIButton! = nil
  
  private var isCreatingAForeignClient: Bool = false
  private var positionYForDisplacementView: CGFloat = 0.0
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
    
  }
  
  init(newClientData: Client) {
    
    clientData = newClientData
    
    super.init(nibName: nil, bundle: nil)
    
  }
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
    tapGesture.numberOfTouchesRequired = 1
    self.view.addGestureRecognizer(tapGesture)
    
    self.initNavigationBar()
    self.initMainScrollView()
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.requestToGetAllBanks(actionsToMakeWhenSucceeded: { arrayOfBanks in
      
      self.bankOptions = arrayOfBanks
      
        ServerManager.sharedInstance.getCountries(actionsToMakeWhenSucceeded: { (arrayCountries) in
        
          self.arrayOfCountryOptions = arrayCountries

          self.initClientDataLabel()
          self.initRazonSocialTextField()
          self.initRFCTextField()
          self.initCalleTextField()
          self.initNumeroExteriorTextField()
          self.initNumeroInteriorTextField()
          self.initColoniaTextField()
          self.initCodigoPostalTextField()
          self.initCiudadTextField()
          self.initEstadoTextField()
          self.initLocalidadTextField()
        //      self.initPaisTextField()
          self.initPaisOptionButton()
          self.initRegimenNumberTextField()
          self.initFiscalResidenceTextField()
        
          self.initViewForDisplaceElements()
        
          self.initSecondSeparatorLabel()
        
          self.initClientNameTextField()
          self.initClientLastNameTextField()
          self.initFirstEmailToSendCFDITextField()
          self.initSecondEmailToSendCFDITextField()
          self.initThirdEmailToSendCFDITextField()
          self.initContactPhoneTextField()
        
          self.initBankDataSeparatorLabel()
        
          self.initBankOptionButton()
          self.initAccountNumberTextField()
          self.initBackViewButtons()
        
          UtilityManager.sharedInstance.hideLoader()
        
        
        }, actionsToMakeWhenFailed: {
        
          UtilityManager.sharedInstance.hideLoader()
        
        })
      
      }, actionsToMakeWhenFailed: {
      
        UtilityManager.sharedInstance.hideLoader()
          
      })
    
  }
  
  private func initNavigationBar() {
    
    self.initLeftNavigationView()
    
  }
  
  private func initLeftNavigationView() {
    
    self.title = ViewControllersConstants.NameOfScreen.clientsTableText
    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
  }
  
  private func initMainScrollView() {
    
    mainScrollView = UIScrollView.init(frame: UIScreen.main.bounds)
    
    if UtilityManager.sharedInstance.isIpad() == false {
      
      let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                       height: mainScrollView.frame.size.height + (975.0 * UtilityManager.sharedInstance.conversionHeight))
      mainScrollView.contentSize = newContentSize
      
    } else {
      
      let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                       height: mainScrollView.frame.size.height + (500.0 * UtilityManager.sharedInstance.conversionHeight))
      mainScrollView.contentSize = newContentSize
      
    }
    
    mainScrollView.backgroundColor = UIColor.white
    
    self.view.addSubview(mainScrollView)
    
  }
  
  private func initClientDataLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: mainScrollView.frame.size.width,
                                    height: 56.0 * UtilityManager.sharedInstance.conversionHeight)
    
    clientDataLabel = LabelSeparator.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.CreateClientDetailViewController.clientDataLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    clientDataLabel.attributedText = stringWithFormat
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: clientDataLabel.frame.size.width,
                               height: clientDataLabel.frame.size.height)
    clientDataLabel.frame = newFrame
    clientDataLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(clientDataLabel)
    
  }
  
  private func initRazonSocialTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: clientDataLabel.frame.origin.y + clientDataLabel.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    razonSocialTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                             title: ViewControllersConstants.CreateClientDetailViewController.razonSocialTextFieldText ,
                                                             image: nil,
                                                             colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                             positionInYInsideView: nil,
                                                             positionInXInsideView: nil)
    if clientData.razonSocial != nil {
      
      razonSocialTextField.mainTextField.text = clientData.razonSocial
      
    }
    razonSocialTextField.mainTextField.textColor = UIColor.black
    razonSocialTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.razonSocialTextFieldText
    razonSocialTextField.backgroundColor = UIColor.white
    razonSocialTextField.mainTextField.delegate = self
    razonSocialTextField.mainTextField.tag = 1
    razonSocialTextField.mainTextField.returnKeyType = .next
    
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(razonSocialTextField)
    
  }
  
  private func initRFCTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: razonSocialTextField.frame.origin.y + razonSocialTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    rfcTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                     title: ViewControllersConstants.CreateClientDetailViewController.rfcTextFieldText,
                                                     image: nil,
                                                     colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                     positionInYInsideView: nil,
                                                     positionInXInsideView: nil)
    if clientData.rfc != nil {
      
      rfcTextField.mainTextField.text = clientData.rfc
      
    }
    rfcTextField.mainTextField.textColor = UIColor.black
    rfcTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.rfcTextFieldText
    rfcTextField.backgroundColor = UIColor.white
    rfcTextField.mainTextField.delegate = self
    rfcTextField.mainTextField.tag = 2
    rfcTextField.mainTextField.returnKeyType = .next
    
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(rfcTextField)
    
  }
  
  private func initCalleTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: rfcTextField.frame.origin.y + rfcTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    calleTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                       title: ViewControllersConstants.CreateClientDetailViewController.calleTextFieldText,
                                                       image: nil,
                                                       colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                       positionInYInsideView: nil,
                                                       positionInXInsideView: nil)
    if clientData.calle != nil {
      
      calleTextField.mainTextField.text = clientData.calle
      
    }
    calleTextField.mainTextField.textColor = UIColor.black
    calleTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.calleTextFieldText
    calleTextField.backgroundColor = UIColor.white
    calleTextField.mainTextField.delegate = self
    calleTextField.mainTextField.tag = 3
    calleTextField.mainTextField.returnKeyType = .next
    
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(calleTextField)
    
  }
  
  private func initNumeroExteriorTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: calleTextField.frame.origin.y + calleTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    numeroExteriorTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                title: ViewControllersConstants.CreateClientDetailViewController.numeroExteriorTextFieldText,
                                                                image: nil,
                                                                colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                positionInYInsideView: nil,
                                                                positionInXInsideView: nil)
    if clientData.numero != nil {
      
      numeroExteriorTextField.mainTextField.text = clientData.numero
      
    }
    numeroExteriorTextField.mainTextField.textColor = UIColor.black
    numeroExteriorTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.numeroExteriorTextFieldText
    numeroExteriorTextField.backgroundColor = UIColor.white
    numeroExteriorTextField.mainTextField.delegate = self
    numeroExteriorTextField.mainTextField.tag = 4
    numeroExteriorTextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(numeroExteriorTextField)
    
  }
  
  private func initNumeroInteriorTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: numeroExteriorTextField.frame.origin.y + numeroExteriorTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    numeroInteriorTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                title: ViewControllersConstants.CreateClientDetailViewController.numeroInteriorTextFieldText,
                                                                image: nil,
                                                                colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                positionInYInsideView: nil,
                                                                positionInXInsideView: nil)
    if clientData.interior != nil {
      
      numeroInteriorTextField.mainTextField.text = clientData.interior
      
    }
    numeroInteriorTextField.mainTextField.textColor = UIColor.black
    numeroInteriorTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.numeroInteriorTextFieldText
    numeroInteriorTextField.backgroundColor = UIColor.white
    numeroInteriorTextField.mainTextField.delegate = self
    numeroInteriorTextField.mainTextField.tag = 5
    numeroInteriorTextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(numeroInteriorTextField)
    
  }
  
  private func initColoniaTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: numeroInteriorTextField.frame.origin.y + numeroInteriorTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    coloniaTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                         title: ViewControllersConstants.CreateClientDetailViewController.coloniaTextFieldText,
                                                         image: nil,
                                                         colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                         positionInYInsideView: nil,
                                                         positionInXInsideView: nil)
    if clientData.colonia != nil {
      
      coloniaTextField.mainTextField.text = clientData.colonia
      
    }
    coloniaTextField.mainTextField.textColor = UIColor.black
    coloniaTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.coloniaTextFieldText
    coloniaTextField.backgroundColor = UIColor.white
    coloniaTextField.mainTextField.delegate = self
    coloniaTextField.mainTextField.tag = 6
    coloniaTextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(coloniaTextField)
    
  }
  
  private func initCodigoPostalTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: coloniaTextField.frame.origin.y + coloniaTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    codigoPostalTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                              title: ViewControllersConstants.CreateClientDetailViewController.codigoPostalTextFieldText,
                                                              image: nil,
                                                              colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                              positionInYInsideView: nil,
                                                              positionInXInsideView: nil)
    if clientData.codigoPostal != nil {
      
      codigoPostalTextField.mainTextField.text = clientData.codigoPostal
      
    }
    codigoPostalTextField.mainTextField.textColor = UIColor.black
    codigoPostalTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.codigoPostalTextFieldText
    codigoPostalTextField.backgroundColor = UIColor.white
    codigoPostalTextField.mainTextField.delegate = self
    codigoPostalTextField.mainTextField.tag = 7
    codigoPostalTextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(codigoPostalTextField)
    
  }
  
  private func initCiudadTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: codigoPostalTextField.frame.origin.y + codigoPostalTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    ciudadTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                        title: ViewControllersConstants.CreateClientDetailViewController.ciudadTextFieldText,
                                                        image: nil,
                                                        colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                        positionInYInsideView: nil,
                                                        positionInXInsideView: nil)
    if clientData.ciudad != nil {
      
      ciudadTextField.mainTextField.text = clientData.ciudad
      
    }
    ciudadTextField.mainTextField.textColor = UIColor.black
    ciudadTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.ciudadTextFieldText
    ciudadTextField.backgroundColor = UIColor.white
    ciudadTextField.mainTextField.delegate = self
    ciudadTextField.mainTextField.tag = 8
    ciudadTextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(ciudadTextField)
    
  }
  
  private func initEstadoTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: ciudadTextField.frame.origin.y + ciudadTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    estadoTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                        title: ViewControllersConstants.CreateClientDetailViewController.estadoTextFieldText,
                                                        image: nil,
                                                        colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                        positionInYInsideView: nil,
                                                        positionInXInsideView: nil)
    if clientData.estado != nil {
      
      estadoTextField.mainTextField.text = clientData.estado
      
    }
    estadoTextField.mainTextField.textColor = UIColor.black
    estadoTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.estadoTextFieldText
    estadoTextField.backgroundColor = UIColor.white
    estadoTextField.mainTextField.delegate = self
    estadoTextField.mainTextField.tag = 9
    estadoTextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(estadoTextField)
    
  }
  
  private func initLocalidadTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: estadoTextField.frame.origin.y + estadoTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    localidadTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                           title: ViewControllersConstants.CreateClientDetailViewController.localidadTextFieldText,
                                                           image: nil,
                                                           colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                           positionInYInsideView: nil,
                                                           positionInXInsideView: nil)
    if clientData.localidad != nil {
      
      localidadTextField.mainTextField.text = clientData.localidad
      
    }
    localidadTextField.mainTextField.text = clientData.localidad
    localidadTextField.mainTextField.textColor = UIColor.black
    localidadTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.localidadTextFieldText
    localidadTextField.backgroundColor = UIColor.white
    localidadTextField.mainTextField.delegate = self
    localidadTextField.mainTextField.tag = 10
    localidadTextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(localidadTextField)
    
  }
  
  private func initPaisTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: localidadTextField.frame.origin.y + localidadTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    paisTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                      title: ViewControllersConstants.CreateClientDetailViewController.paisTextFieldText,
                                                      image: nil,
                                                      colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                      positionInYInsideView: nil,
                                                      positionInXInsideView: nil)
    
    paisTextField.mainTextField.textColor = UIColor.black
    paisTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.paisTextFieldText
    paisTextField.backgroundColor = UIColor.white
    paisTextField.mainTextField.delegate = self
    paisTextField.mainTextField.tag = 11
    paisTextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(paisTextField)
    
  }
  
  private func initPaisOptionButton() {
    
    let frameForButton = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: localidadTextField.frame.origin.y + localidadTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var countryOptions = Array<Option>()
    
    for country in arrayOfCountryOptions {
      
      let newCountryOption = Option(id: country.id, name: country.name, type: country.id)
      countryOptions.append(newCountryOption)
      
    }
    
    self.paisOptionButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                     newTitle: ViewControllersConstants.CreateClientDetailViewController.paisTextFieldText,
                                                     newArrayOfOptions: countryOptions)
    paisOptionButton.drawBottomBorder()
    _ = paisOptionButton.selectElementWithName(nameToLookFor: "México")
    //    bankOptionButton.delegate = self
    
    self.mainScrollView.addSubview(paisOptionButton)
    
  }
  
  private func initRegimenNumberTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: paisOptionButton.frame.origin.y + paisOptionButton.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    regimenNumberTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                      title: ViewControllersConstants.CreateClientDetailViewController.regimenNumberTextFieldText,
                                                      image: nil,
                                                      colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                      positionInYInsideView: nil,
                                                      positionInXInsideView: nil)
    
    regimenNumberTextField.mainTextField.textColor = UIColor.black
    regimenNumberTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.regimenNumberTextFieldText
    regimenNumberTextField.backgroundColor = UIColor.white
    regimenNumberTextField.mainTextField.delegate = self
    regimenNumberTextField.mainTextField.tag = 11
    regimenNumberTextField.mainTextField.returnKeyType = .next
    regimenNumberTextField.alpha = 0.0
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(regimenNumberTextField)
    
  }
  
  private func initFiscalResidenceTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: regimenNumberTextField.frame.origin.y + regimenNumberTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    fiscalResidenceTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                               title: ViewControllersConstants.CreateClientDetailViewController.fiscalResidenceTextFieldText,
                                                               image: nil,
                                                               colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                               positionInYInsideView: nil,
                                                               positionInXInsideView: nil)
    
    fiscalResidenceTextField.mainTextField.textColor = UIColor.black
    fiscalResidenceTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.fiscalResidenceTextFieldText
    fiscalResidenceTextField.backgroundColor = UIColor.white
    fiscalResidenceTextField.mainTextField.delegate = self
    fiscalResidenceTextField.mainTextField.tag = 12
    fiscalResidenceTextField.mainTextField.returnKeyType = .next
    fiscalResidenceTextField.alpha = 0.0
    //    razonSocialTextField.delegate = self
    self.mainScrollView.addSubview(fiscalResidenceTextField)
    
  }
  
  private func initViewForDisplaceElements() {
    
    self.positionYForDisplacementView = paisOptionButton.frame.origin.y + paisOptionButton.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight)
  
    let frameForView = CGRect.init(x: 0.0,
                                   y: paisOptionButton.frame.origin.y + paisOptionButton.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                               width: mainScrollView.frame.size.width,
                              height: 900.0 * UtilityManager.sharedInstance.conversionHeight)
    
    self.viewForDisplaceElements = UIView.init(frame: frameForView)
    self.viewForDisplaceElements.backgroundColor = UIColor.white
    self.mainScrollView.addSubview(self.viewForDisplaceElements)
  
  }
  
  private func initSecondSeparatorLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: mainScrollView.frame.size.width,
                                    height: 56.0 * UtilityManager.sharedInstance.conversionHeight)
    
    secondSeparatorLabel = LabelSeparator.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.CreateClientDetailViewController.secondSeparatorLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    secondSeparatorLabel.attributedText = stringWithFormat
    secondSeparatorLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    viewForDisplaceElements.addSubview(secondSeparatorLabel)
    
  }
  
  private func initClientNameTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: secondSeparatorLabel.frame.origin.y + secondSeparatorLabel.frame.size.height + (15.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    clientNameTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                            title: ViewControllersConstants.CreateClientDetailViewController.clientNameTextFieldText,
                                                            image: nil,
                                                            colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                            positionInYInsideView: nil,
                                                            positionInXInsideView: nil)
    if clientData.contacto != nil && clientData.contacto.nombre != nil {
      
      clientNameTextField.mainTextField.text = clientData.contacto.nombre
      
    }
    clientNameTextField.mainTextField.textColor = UIColor.black
    clientNameTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.clientNameTextFieldText
    clientNameTextField.backgroundColor = UIColor.white
    clientNameTextField.mainTextField.delegate = self
    clientNameTextField.mainTextField.tag = 12
    clientNameTextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.viewForDisplaceElements.addSubview(clientNameTextField)
    
  }
  
  private func initClientLastNameTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: clientNameTextField.frame.origin.y + clientNameTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    clientLastNameTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                title: ViewControllersConstants.CreateClientDetailViewController.clientLastNameTextFieldText,
                                                                image: nil,
                                                                colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                positionInYInsideView: nil,
                                                                positionInXInsideView: nil)
    if clientData.contacto != nil && clientData.contacto.apellidos != nil {
      
      clientLastNameTextField.mainTextField.text = clientData.contacto.apellidos
      
    }
    clientLastNameTextField.mainTextField.textColor = UIColor.black
    clientLastNameTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.clientLastNameTextFieldText
    clientLastNameTextField.backgroundColor = UIColor.white
    clientLastNameTextField.mainTextField.delegate = self
    clientLastNameTextField.mainTextField.tag = 13
    clientLastNameTextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.viewForDisplaceElements.addSubview(clientLastNameTextField)
    
  }
  
  private func initFirstEmailToSendCFDITextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: clientLastNameTextField.frame.origin.y + clientLastNameTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    firstEmailToSendCFDITextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                      title: ViewControllersConstants.CreateClientDetailViewController.firstEmailToSendCFDITextFieldText,
                                                                      image: nil,
                                                                      colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                      positionInYInsideView: nil,
                                                                      positionInXInsideView: nil)
    if clientData.contacto != nil && clientData.contacto.email != nil {
      
      firstEmailToSendCFDITextField.mainTextField.text = clientData.contacto.email
      
    }
    firstEmailToSendCFDITextField.mainTextField.textColor = UIColor.black
    firstEmailToSendCFDITextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.firstEmailToSendCFDITextFieldText
    firstEmailToSendCFDITextField.backgroundColor = UIColor.white
    firstEmailToSendCFDITextField.mainTextField.delegate = self
    firstEmailToSendCFDITextField.mainTextField.tag = 14
    firstEmailToSendCFDITextField.mainTextField.autocapitalizationType = .none
    firstEmailToSendCFDITextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.viewForDisplaceElements.addSubview(firstEmailToSendCFDITextField)
    
  }
  
  private func initSecondEmailToSendCFDITextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: firstEmailToSendCFDITextField.frame.origin.y + firstEmailToSendCFDITextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    secondEmailToSendCFDITextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                       title: ViewControllersConstants.CreateClientDetailViewController.secondEmailToSendCFDITextFieldText,
                                                                       image: nil,
                                                                       colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                       positionInYInsideView: nil,
                                                                       positionInXInsideView: nil)
    secondEmailToSendCFDITextField.mainTextField.textColor = UIColor.black
    secondEmailToSendCFDITextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.secondEmailToSendCFDITextFieldText
    secondEmailToSendCFDITextField.backgroundColor = UIColor.white
    secondEmailToSendCFDITextField.mainTextField.delegate = self
    secondEmailToSendCFDITextField.mainTextField.tag = 15
    secondEmailToSendCFDITextField.mainTextField.autocapitalizationType = .none
    secondEmailToSendCFDITextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.viewForDisplaceElements.addSubview(secondEmailToSendCFDITextField)
    
  }
  
  private func initThirdEmailToSendCFDITextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: secondEmailToSendCFDITextField.frame.origin.y + secondEmailToSendCFDITextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    thirdEmailToSendCFDITextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                      title: ViewControllersConstants.CreateClientDetailViewController.thirdEmailToSendCFDITextFieldText,
                                                                      image: nil,
                                                                      colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                      positionInYInsideView: nil,
                                                                      positionInXInsideView: nil)
    thirdEmailToSendCFDITextField.mainTextField.textColor = UIColor.black
    thirdEmailToSendCFDITextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.thirdEmailToSendCFDITextFieldText
    thirdEmailToSendCFDITextField.backgroundColor = UIColor.white
    thirdEmailToSendCFDITextField.mainTextField.delegate = self
    thirdEmailToSendCFDITextField.mainTextField.tag = 16
    thirdEmailToSendCFDITextField.mainTextField.autocapitalizationType = .none
    thirdEmailToSendCFDITextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.viewForDisplaceElements.addSubview(thirdEmailToSendCFDITextField)
    
  }
  
  private func initContactPhoneTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: thirdEmailToSendCFDITextField.frame.origin.y + thirdEmailToSendCFDITextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    contactPhoneTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                              title: ViewControllersConstants.CreateClientDetailViewController.contactPhoneTextFieldText,
                                                              image: nil,
                                                              colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                              positionInYInsideView: nil,
                                                              positionInXInsideView: nil)
    if clientData.contacto != nil && clientData.contacto.telefono != nil {
      
      razonSocialTextField.mainTextField.text = clientData.contacto.telefono
      
    }
    contactPhoneTextField.mainTextField.textColor = UIColor.black
    contactPhoneTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.contactPhoneTextFieldText
    contactPhoneTextField.backgroundColor = UIColor.white
    contactPhoneTextField.mainTextField.delegate = self
    contactPhoneTextField.mainTextField.tag = 17
    contactPhoneTextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.viewForDisplaceElements.addSubview(contactPhoneTextField)
    
  }
  
  private func initBankDataSeparatorLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: contactPhoneTextField.frame.origin.y + contactPhoneTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                    width: mainScrollView.frame.size.width,
                                    height: 56.0 * UtilityManager.sharedInstance.conversionHeight)
    
    bankDataSeparatorLabel = LabelSeparator.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.CreateClientDetailViewController.bankDataSeparatorLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    bankDataSeparatorLabel.attributedText = stringWithFormat
    bankDataSeparatorLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    viewForDisplaceElements.addSubview(bankDataSeparatorLabel)
    
  }
  
  private func initBankOptionButton() {
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: bankDataSeparatorLabel.frame.origin.y + bankDataSeparatorLabel.frame.size.height,
                                     width: UIScreen.main.bounds.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    bankOptionButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                     newTitle: ViewControllersConstants.CreateClientDetailViewController.bankDataSeparatorLabelText,
                                                     newArrayOfOptions: self.bankOptions)
    bankOptionButton.drawBottomBorder()
    //    bankOptionButton.delegate = self
    
    self.viewForDisplaceElements.addSubview(bankOptionButton)
    
  }
  
  private func initAccountNumberTextField() {
    
    let frameForView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                   y: bankOptionButton.frame.origin.y + bankOptionButton.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: mainScrollView.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                   height: 51.0 * UtilityManager.sharedInstance.conversionHeight)
    
    accountNumberTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                               title: ViewControllersConstants.CreateClientDetailViewController.accountNumberTextFieldText,
                                                               image: nil,
                                                               colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                               positionInYInsideView: nil,
                                                               positionInXInsideView: nil)
    accountNumberTextField.mainTextField.textColor = UIColor.black
    accountNumberTextField.mainTextField.placeholder = ViewControllersConstants.CreateClientDetailViewController.accountNumberTextFieldText
    accountNumberTextField.backgroundColor = UIColor.white
    accountNumberTextField.mainTextField.delegate = self
    accountNumberTextField.mainTextField.keyboardType = .numberPad
    accountNumberTextField.mainTextField.delegate = self
    accountNumberTextField.mainTextField.tag = 17
    accountNumberTextField.mainTextField.returnKeyType = .next
    //    razonSocialTextField.delegate = self
    self.viewForDisplaceElements.addSubview(accountNumberTextField)
    
  }
  
  private func initBackViewButtons() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: accountNumberTextField.frame.origin.y + accountNumberTextField.frame.size.height + (15.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: self.view.frame.size.width,
                                   height: 350.0 * UtilityManager.sharedInstance.conversionHeight)
    
    backViewButtons = UIView.init(frame: frameForView)
    backViewButtons.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    self.viewForDisplaceElements.addSubview(backViewButtons)
//    self.initLastUpdateLabel()
    self.initCreateClientButton()
    
  }
  
  private func initLastUpdateLabel() {
    
    let frameForLabel = CGRect.init(x: 16.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 5.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: backViewButtons.frame.size.width - (32.0 * UtilityManager.sharedInstance.conversionWidth),
                                    height: CGFloat.greatestFiniteMagnitude)
    
    lastUpdateLabel = UILabel.init(frame: frameForLabel)
    lastUpdateLabel.numberOfLines = 0
    lastUpdateLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 11.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.CreateClientDetailViewController.lastUpdateLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    lastUpdateLabel.attributedText = stringWithFormat
    lastUpdateLabel.sizeToFit()
    let newFrame = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                               y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                               width: lastUpdateLabel.frame.size.width,
                               height: lastUpdateLabel.frame.size.height)
    lastUpdateLabel.frame = newFrame
    
    self.backViewButtons.addSubview(lastUpdateLabel)
    
  }
  
  private func initCreateClientButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                     y: 21.0 * UtilityManager.sharedInstance.conversionHeight,
                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    createClientButton = UIButton.init(frame: frameForButton)
    createClientButton.addTarget(self,
                                  action: #selector(createClientButtonPressed),
                                  for: .touchUpInside)
    createClientButton.backgroundColor = UtilityManager.sharedInstance.backGroundColorApp
    createClientButton.setBackgroundColor(color: UIColor.init(red: 255.0/255.0, green: 164.0/255.0, blue: 78.0/255.0, alpha: 1.0), forState: .highlighted)
    createClientButton.setAttributedTitle(stringWithFormat, for: .normal)
    createClientButton.contentHorizontalAlignment = .left
    
    createClientButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.CreateClientDetailViewController.registerClientButtonText))
    
    self.backViewButtons.addSubview(createClientButton)
    
  }
  
  private func createGenericLabelForButton(text: String) -> UILabel {
    
    let frameForLabel = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 200.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    let genericLabel = UILabel.init(frame: frameForLabel)
    genericLabel.numberOfLines = 0
    genericLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: text,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    genericLabel.attributedText = stringWithFormat
    genericLabel.sizeToFit()
    let newFrame = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                               y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                               width: genericLabel.frame.size.width,
                               height: genericLabel.frame.size.height)
    genericLabel.frame = newFrame
    
    return genericLabel
    
  }
  
  private func changeAppearanceWithForeignRFC() {
    
    self.appearanceForTextFieldsForForeignRFC()
    self.moveDisplaceElements()
    
  }
  
  private func appearanceForTextFieldsForForeignRFC() {
    
    if isCreatingAForeignClient == true {
      
      UIView.animate(withDuration: 0.25, animations: {
        
        self.regimenNumberTextField.alpha = 1.0
        self.fiscalResidenceTextField.alpha = 1.0
        
      }, completion: { (isFinished) in
        
        if isFinished == true {
          
          
          
        }
        
      })
      
    } else {
      
      UIView.animate(withDuration: 0.25, animations: {
        
        self.regimenNumberTextField.alpha = 0.0
        self.fiscalResidenceTextField.alpha = 0.0
        
      }, completion: { (isFinished) in
        
        if isFinished == true {
          
          
          
        }
        
      })
      
    }
    
  }
  
  private func moveDisplaceElements() {
    
    if isCreatingAForeignClient == true {
      
      UIView.animate(withDuration: 0.25, animations: {
        
        self.viewForDisplaceElements.frame = CGRect.init(x: self.viewForDisplaceElements.frame.origin.x,
                                                         y: self.positionYForDisplacementView + (113.0 * UtilityManager.sharedInstance.conversionHeight),
                                                         width: self.viewForDisplaceElements.frame.size.width,
                                                         height: self.viewForDisplaceElements.frame.size.height)
        
      }, completion: { (isFinished) in
        
        if isFinished == true {
          
          
          
        }
        
      })
      
    } else {
      
      UIView.animate(withDuration: 0.25, animations: {
        
        self.viewForDisplaceElements.frame = CGRect.init(x: self.viewForDisplaceElements.frame.origin.x,
                                                         y: self.positionYForDisplacementView,
                                                         width: self.viewForDisplaceElements.frame.size.width,
                                                         height: self.viewForDisplaceElements.frame.size.height)
        
      }, completion: { (isFinished) in
        
        if isFinished == true {
          
          
          
        }
        
      })
      
    }
    
  }
  
  @objc private func createClientButtonPressed() {
    
    if UtilityManager.sharedInstance.isValidText(testString: razonSocialTextField.mainTextField.text!) == false {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Se requiere la razón social del cliente",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    } else
    if ((rfcTextField.mainTextField.text?.characters.count)! < 12 || (rfcTextField.mainTextField.text?.characters.count)! > 13) && (UtilityManager.sharedInstance.isValidText(testString: rfcTextField.mainTextField.text!) == false) {
        
      let alertController = UIAlertController(title: "ERROR",
                                              message: "El RFC debe tener mínimo 12 caracteres y máximo 13 caracteres",
                                              preferredStyle: UIAlertControllerStyle.alert)
        
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
        
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
        
    } else
    if UtilityManager.sharedInstance.isValidText(testString: calleTextField.mainTextField.text!) == false {
        
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Se requiere una calle",
                                              preferredStyle: UIAlertControllerStyle.alert)
        
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
        
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
        
    } else
    if UtilityManager.sharedInstance.isValidText(testString: numeroExteriorTextField.mainTextField.text!) == false {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Se requiere un número exterior",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    } else
    if UtilityManager.sharedInstance.isValidText(testString: coloniaTextField.mainTextField.text!) == false {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Se requiere el nombre de una colonia",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    } else
    if (UtilityManager.sharedInstance.isValidText(testString: codigoPostalTextField.mainTextField.text!) == false) || (codigoPostalTextField.mainTextField.text?.characters.count != 5) {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Se requiere un código postal de 5 caracteres",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    } else
    if UtilityManager.sharedInstance.isValidText(testString: ciudadTextField.mainTextField.text!) == false {
        
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Se requiere una ciudad o delegación",
                                              preferredStyle: UIAlertControllerStyle.alert)
        
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
        
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
        
    } else
    if UtilityManager.sharedInstance.isValidText(testString: estadoTextField.mainTextField.text!) == false {
          
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Se requiere un estado",
                                                preferredStyle: UIAlertControllerStyle.alert)
          
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
          alertController.addAction(cancelAction)
          
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
          
    } else
    if UtilityManager.sharedInstance.isValidText(testString: paisTextField.mainTextField.text!) == false {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Se requiere un país",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    } else
//    if UtilityManager.sharedInstance.isValidText(testString: clientNameTextField.mainTextField.text!) == false {
//      
//      let alertController = UIAlertController(title: "ERROR",
//                                              message: "Se requiere el nombre del contacto del cliente",
//                                              preferredStyle: UIAlertControllerStyle.alert)
//      
//      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
//      alertController.addAction(cancelAction)
//      
//      let actualController = UtilityManager.sharedInstance.currentViewController()
//      actualController.present(alertController, animated: true, completion: nil)
//      
//    } else
//    if UtilityManager.sharedInstance.isValidText(testString: clientLastNameTextField.mainTextField.text!) == false {
//      
//      let alertController = UIAlertController(title: "ERROR",
//                                              message: "Se requiere los apellidos del contacto del cliente",
//                                              preferredStyle: UIAlertControllerStyle.alert)
//      
//      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
//      alertController.addAction(cancelAction)
//      
//      let actualController = UtilityManager.sharedInstance.currentViewController()
//      actualController.present(alertController, animated: true, completion: nil)
//      
//    } else
    if UtilityManager.sharedInstance.isValidEmail(testStr: firstEmailToSendCFDITextField.mainTextField.text!) == false {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Se requiere un mail válido de contacto del cliente",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    } else {
      
      self.createClient()
      
      
    }
    
  }
  
  private func createClient() {
    
    let finalParams = self.getAllParameters()
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.requestToCreateNewClient(params: finalParams, actionsToMakeWhenSucceeded: { 
    
      UtilityManager.sharedInstance.hideLoader()
      
      let alertController = UIAlertController(title: "Éxito",
                                              message: "Cliente creado satisfactoriamente",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        _ = self.navigationController?.popViewController(animated: true)
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }, actionsToMakeWhenFailed: {
      
      UtilityManager.sharedInstance.hideLoader()
    
    })
    
  }
  
  private func getAllParameters() -> [String: AnyObject] {
    
    var params: [String: AnyObject] = [String: AnyObject]()
    
    let bankId = bankOptionButton.getValueSelected() != nil ? bankOptionButton.getValueSelected()!.id : ""
    
    params["nombre"] = clientNameTextField.mainTextField.text! as AnyObject
    params["apellidos"] = clientLastNameTextField.mainTextField.text! as AnyObject
    params["email"] = firstEmailToSendCFDITextField.mainTextField.text! as AnyObject
    params["telefono"] = contactPhoneTextField.mainTextField.text! as AnyObject
    params["razons"] = razonSocialTextField.mainTextField.text! as AnyObject
    params["rfc"] = rfcTextField.mainTextField.text! as AnyObject
    params["calle"] = calleTextField.mainTextField.text! as AnyObject
    params["numero_exterior"] = numeroExteriorTextField.mainTextField.text! as AnyObject
    params["numero_interior"] = numeroInteriorTextField.mainTextField.text! as AnyObject
    params["codpos"] = codigoPostalTextField.mainTextField.text! as AnyObject
    params["colonia"] = coloniaTextField.mainTextField.text! as AnyObject
    params["estado"] = estadoTextField.mainTextField.text! as AnyObject
    params["ciudad"] = ciudadTextField.mainTextField.text! as AnyObject
    
    if bankId != "" {
      
      params["bank"] = bankId as AnyObject
      params["account"] = accountNumberTextField.mainTextField.text! as AnyObject
      
    }
    
    return params
    
  }
  
  @objc private func hideKeyboard() {
    
    self.view.endEditing(true)
    
  }
  
  //MARK: - UITextFieldDelegate
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    let nextTage = textField.tag + 1
    
    let nextResponder = textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
    
    if (nextResponder != nil){
      
      nextResponder?.becomeFirstResponder()
      
    } else {
      
      textField.resignFirstResponder()
      
    }
    
    return false
    
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    
    if textField == rfcTextField.mainTextField {
      
      if rfcTextField.mainTextField.text! == "XEXX010101000" {
        
        self.isCreatingAForeignClient = true
        self.changeAppearanceWithForeignRFC()
        
      } else {
        
        self.isCreatingAForeignClient = false
        self.changeAppearanceWithForeignRFC()
        
      }
      
    }
    
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    if textField == accountNumberTextField.mainTextField {
    
      guard let text = textField.text else { return true }
      let newLength = text.characters.count + string.characters.count - range.length
      return newLength <= maxLenghtOfAccountNumbers
      
    } else
    
    if textField == codigoPostalTextField.mainTextField {
        
      guard let text = textField.text else { return true }
      let newLength = text.characters.count + string.characters.count - range.length
      return newLength <= maxLenghtOfCodPos
      
    }
    
    return true
    
  }
  
  
}
