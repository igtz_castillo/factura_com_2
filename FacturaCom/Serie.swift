//
//  Serie.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 11/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class Serie {
  
  var id: String! = nil
  var name: String! = nil
  var type: String! = nil
  
  init(newId: String, newName: String, newType: String) {
    
    id = newId
    name = newName
    type = newType
    
  }
  
}
