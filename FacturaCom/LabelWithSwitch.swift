//
//  LabelWithSwitch.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 07/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

protocol LabelWithSwitchUsingPickerDelegate {
  
  func switchChangeToOn(sender: LabelWithSwitch)
  func switchChangeToOff(sender: LabelWithSwitch)
  
}

class LabelWithSwitch: UILabel, UIPickerViewDelegate, UIPickerViewDataSource {
  
  var switchView: UISwitch! = nil
  private var initialValueOfSwitch: Bool = false
  
  private var titleForContainerViewText: String! = nil
  private var titleForContainerView: UILabel! = nil
  private var arrayOfOptions: [Option]! = [Option]()
  private var mainPickerView: UIPickerView! = nil
  private var containerViewForPicker: UIView! = nil
  private var isShowingPickerView: Bool = false
  private var indexSelected: Int = -1
  private var viewWhichShowPickerView: UIView! = nil
  private var constantStringWhenChangeValueOfPicker: String! = nil
  
  var delegateForPicker: LabelWithSwitchUsingPickerDelegate?
  
  private let frameForShowing: CGRect = CGRect.init(x: 0.0,
                                                    y: UIScreen.main.bounds.height - (250.0 * UtilityManager.sharedInstance.conversionHeight),
                                                    width: UIScreen.main.bounds.width,
                                                    height: 250.0 * UtilityManager.sharedInstance.conversionHeight)
  
  private let frameForHiding: CGRect = CGRect.init(x: 0.0,
                                                   y: UIScreen.main.bounds.height,
                                                   width: UIScreen.main.bounds.width,
                                                   height: 250.0 * UtilityManager.sharedInstance.conversionHeight)
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  init(frame: CGRect, newInitialValueOfSwitch: Bool) {
    
    super.init(frame: frame)
    
    self.initInterface()
    
  }
  
  init(frame: CGRect, newInitialValueOfSwitch: Bool, optionsToShow: Array<Option>, titleForPicker: String, viewToAddPickerView: UIView, constantStringWhenChangeValue: String) {
    
    initialValueOfSwitch = newInitialValueOfSwitch
    arrayOfOptions = optionsToShow
    titleForContainerViewText = titleForPicker
    viewWhichShowPickerView = viewToAddPickerView
    constantStringWhenChangeValueOfPicker = constantStringWhenChangeValue
    
    super.init(frame: frame)
    
    self.initInterfaceWithPickerView()
    
  }
  
  private func initInterface() {
    
    self.backgroundColor = UIColor.white
    self.initSwitchView()
    
  }
  
  private func initInterfaceWithPickerView() {
    
    self.initSwitchView()
    
    self.initContainerViewForPicker()
    self.initMainPickerView()
    
  }
  
  private func initSwitchView() {
    
    let frameForSwitch = CGRect.init(x: self.frame.size.width - (56.5 * UtilityManager.sharedInstance.conversionWidth),
                                     y: 9.0 * UtilityManager.sharedInstance.conversionHeight,
                                 width: 40.0 * UtilityManager.sharedInstance.conversionWidth,
                                height: self.frame.size.height - (18.0 * UtilityManager.sharedInstance.conversionWidth))
    
    switchView = UISwitch.init(frame: frameForSwitch)
    switchView.setOn(initialValueOfSwitch, animated: false)
    
    switchView.addTarget(self, action: #selector(switchChangedValue), for: .valueChanged)
    
    self.addSubview(switchView)
    
  }
  
  func getValueOfSwitch() -> Bool {
    
    return switchView.isOn
    
  }
  
  func changeTitleLabel(newTitle: String) {
    
    self.changeTextOfLabel(newText: newTitle)
    
  }
  
  private func changeTextOfLabel(newText: String) {
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = .left

    var finalText = ""
    
    if constantStringWhenChangeValueOfPicker != nil {
      
      finalText = constantStringWhenChangeValueOfPicker + newText
      
    } else {
      
      finalText = newText
      
    }
    
    let stringWithFormat = NSMutableAttributedString(
      string: finalText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    self.attributedText = stringWithFormat
    
  }
  
  override func drawText(in rect: CGRect) {
    
    let insets = UIEdgeInsets.init(top: -20.0 * UtilityManager.sharedInstance.conversionHeight,
                                   left: 0.0 * UtilityManager.sharedInstance.conversionWidth,
                                   bottom: 0.0 * UtilityManager.sharedInstance.conversionHeight,
                                   right: 0.0 * UtilityManager.sharedInstance.conversionWidth)
    
    let newRect = CGRect.init(x: (16.0 * UtilityManager.sharedInstance.conversionWidth),
                              y: (0.0 * UtilityManager.sharedInstance.conversionHeight) ,
                              width: self.frame.size.width,
                              height: 30.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let lastRect = UIEdgeInsetsInsetRect(newRect, insets)
    
    super.drawText(in: lastRect)
    
  }
  
  private func initContainerViewForPicker() {
    
    containerViewForPicker = UIView.init(frame: frameForHiding)
    containerViewForPicker.backgroundColor = UIColor.clear
    
    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
    let blurEffectView = UIVisualEffectView(effect: blurEffect)
    //always fill the view
    blurEffectView.frame = CGRect.init(x: 0.0,
                                       y: 0.0,
                                       width: containerViewForPicker.frame.size.width,
                                       height: containerViewForPicker.frame.size.height)
    containerViewForPicker.addSubview(blurEffectView)
    
    self.initButtonsForContainer()
    
    self.viewWhichShowPickerView.addSubview(containerViewForPicker)
    
    //    let frontViewController = UtilityManager.sharedInstance.currentViewController()
    //    frontViewController.view.addSubview(containerViewForPicker)
    
  }
  
  private func initButtonsForContainer() {
    
    let positionForFirstButton = CGPoint.init(x: 10.0 * UtilityManager.sharedInstance.conversionWidth,
                                              y: 12.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let positionForSecondButton = CGPoint.init(x: containerViewForPicker.frame.size.width - (55.0 * UtilityManager.sharedInstance.conversionWidth),
                                               y: 12.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let cancelButton = createGenericButton(stringToShowInButton: ViewControllersConstants.FacturaCreateViewController.pickerViewCancelButtonText, position: positionForFirstButton)
    let selectButton = createGenericButton(stringToShowInButton: ViewControllersConstants.FacturaCreateViewController.pickerViewSelectButtonText, position: positionForSecondButton)
    
    cancelButton.tag = 1
    selectButton.tag = 2
    
    self.containerViewForPicker.addSubview(cancelButton)
    self.initTitleForContainerView()
    self.containerViewForPicker.addSubview(selectButton)
    
  }
  
  private func initTitleForContainerView() {
    
    let frameForLabel = CGRect.init(x: 90.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 18.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 180.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: 23.0 * UtilityManager.sharedInstance.conversionHeight)
    
    titleForContainerView = UILabel.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: titleForContainerViewText != nil ? titleForContainerViewText : "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    titleForContainerView.attributedText = stringWithFormat
    titleForContainerView.sizeToFit()
    let newFrame = CGRect.init(x: (self.containerViewForPicker.frame.size.width / 2.0) - (titleForContainerView.frame.size.width / 2.0),
                               y: titleForContainerView.frame.origin.y,
                               width: titleForContainerView.frame.size.width,
                               height: titleForContainerView.frame.size.height)
    
    titleForContainerView.frame = newFrame
    
    self.containerViewForPicker.addSubview(titleForContainerView)
    
  }
  
  private func createGenericButton(stringToShowInButton: String, position: CGPoint) -> UIButton {
    
    let genericButton = UIButton.init(frame: CGRect.zero)
    
    let font = UIFont(name: "SFUIDisplay-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.blue
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: stringToShowInButton,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    genericButton.setAttributedTitle(stringWithFormat, for: .normal)
    genericButton.backgroundColor = UIColor.clear
    genericButton.addTarget(self,
                            action: #selector(genericButtonPressed),
                            for: .touchUpInside)
    genericButton.sizeToFit()
    
    let frameForButton = CGRect.init(x: position.x,
                                     y: position.y,
                                     width: genericButton.frame.size.width,
                                     height: genericButton.frame.size.height)
    
    genericButton.frame = frameForButton
    genericButton.layer.cornerRadius = 3.0
    
    return genericButton
    
  }
  
  private func initMainPickerView() {
    
    let frameForPicker = CGRect.init(x: 0.0,
                                     y: (50.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: UIScreen.main.bounds.width,
                                     height: 250.0 * UtilityManager.sharedInstance.conversionHeight)
    
    mainPickerView = UIPickerView.init(frame: frameForPicker)
    mainPickerView.delegate = self
    mainPickerView.dataSource = self
    containerViewForPicker.addSubview(mainPickerView)
    
  }
  
  @objc private func genericButtonPressed(sender: AnyObject) {
    
    if ((sender as? UIButton) != nil) {
      
      if sender.tag == 1 { //Cancel
        
        self.hidePickerView()
        self.switchView.setOn(false, animated: true)
        self.changeTextOfLabel(newText: "")
        self.indexSelected = -1
        self.switchChangedValue(sender: self.switchView)
        
      } else
        
        if sender.tag == 2 {
          
          if arrayOfOptions.count > 0 {
            
            if indexSelected == -1 && arrayOfOptions.count > 0 {
              
              indexSelected = 0
              
            }
            
            self.changeTextOfLabel(newText: arrayOfOptions[mainPickerView.selectedRow(inComponent: 0)].name)
            self.hidePickerView()
            
          }
          
      }
      
    }
    
  }
  
  @objc private func switchChangedValue(sender: UISwitch) {
    
    if self.switchView.isOn == true {
      
      self.showPickerView()
      self.delegateForPicker?.switchChangeToOn(sender: self)
      
    } else {
      
      if constantStringWhenChangeValueOfPicker != nil {
        
        self.changeTextOfLabel(newText: "")
        
      }
      
      self.indexSelected = -1
      
      self.hidePickerView()
      self.delegateForPicker?.switchChangeToOff(sender: self)
      
    }
    
  }
  
  func showPickerView() {
    
    if containerViewForPicker != nil {
      
      UIView.animate(withDuration: 0.3,
                     animations: {
                      
                      self.containerViewForPicker.frame = self.frameForShowing
                      
      }) { (isFinished) in
        
        
        
      }
    
    }
    
  }
  
  func hidePickerView() {
    
    if containerViewForPicker != nil {
      
      UIView.animate(withDuration: 0.3,
                     animations: {
                      
                      self.containerViewForPicker.frame = self.frameForHiding
                      
      }) { (isFinished) in
        
        
        
      }
      
    }
    
  }
  
  func selectElementWithName(nameToLookFor: String) -> Bool {
    
    for i in 0..<arrayOfOptions.count {
      
      if arrayOfOptions[i].name == nameToLookFor {
        
        mainPickerView.selectRow(i, inComponent: 0, animated: true)
        indexSelected = i
        self.changeTextOfLabel(newText: arrayOfOptions[i].name)
        
        return true
        
      }
      
    }
    
    return false
    
  }
  
  func getValueSelected() -> Option? {
    
    if indexSelected == -1 {
      
      return nil
      
    } else {
      
      return arrayOfOptions[mainPickerView.selectedRow(inComponent: 0)]
      
    }
    
  }
  
  //MARK: - PickerViewDelegate - DataSource
  
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    
    return 1
    
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    
    return arrayOfOptions.count
    
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
    return arrayOfOptions[row].name
    
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
    self.changeTextOfLabel(newText: arrayOfOptions[row].name)
    indexSelected = row
    
  }
  
  
}
