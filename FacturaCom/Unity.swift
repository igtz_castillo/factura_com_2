//
//  Unity.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 12/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class Unity {
  
  var name: String! = nil
  var id: String! = nil
  
  init(newId: String, newName: String) {
    
    id = newId
    name = newName
    
  }
  
}
