//
//  ButtonToActiveActionSheetView.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 07/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

protocol ButtonToActiveActionSheetViewDelegate {
  
  func showActionSheet(sender: UIAlertController)
  
}

class ButtonToActiveActionSheetView: UIButton {
  
  private var titleString: String! = ""
  private var messageOfActionSheet: String! = ""
  private var optionsOfActionsToDo: Dictionary<String, () -> Void>! = nil
  private var positionOfButtons: [String: Int] = [String: Int]()
  private var actionSheet: UIAlertController! = nil
  
  var delegate: ButtonToActiveActionSheetViewDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  init(frame: CGRect, title: String, newMessageOfActionSheet: String, nameOfOptionsWithActions: Dictionary<String, () -> Void>) {

    titleString = title
    messageOfActionSheet = newMessageOfActionSheet
    optionsOfActionsToDo = nameOfOptionsWithActions
    
    super.init(frame: frame)
    
    self.initInterface()
    
  }

  private func initInterface() {
    
    self.setBehavior()
    self.initAppearance()
    self.initActionSheet()
    
  }
  
  private func setBehavior() {
    
    self.addTarget(self, action: #selector(showActionSheet), for: .touchUpInside)
    
  }
  
  private func initAppearance() {
  
    self.backgroundColor = UIColor.white
    self.layer.borderColor = UIColor.lightGray.cgColor
    self.layer.borderWidth = 1.0 * UtilityManager.sharedInstance.conversionWidth
    self.titleLabel?.text = titleString
  
  }
  
  private func initActionSheet() {
    
    actionSheet = UIAlertController.init(title: titleString,
                                       message: messageOfActionSheet,
                                preferredStyle: .actionSheet)
    
    let arrayOfTitles = Array(optionsOfActionsToDo.keys)
    
    let cancelActionButton = UIAlertAction.init(title: "Cancelar", style: .cancel) { action -> Void in}
    actionSheet.addAction(cancelActionButton)
    
    for i in 0...arrayOfTitles.count - 1 {
      
      let newActionButton = UIAlertAction.init(title: arrayOfTitles[i], style: .default) { action -> Void in
      
        self.titleLabel?.text = self.titleString
        
        if self.optionsOfActionsToDo[arrayOfTitles[i]] != nil {
          
          self.optionsOfActionsToDo[arrayOfTitles[i]]!()
          
        }
    
      }
        
      actionSheet.addAction(newActionButton)
      
    }
    
  }
  
  @objc private func showActionSheet() {
  
      self.delegate?.showActionSheet(sender: self.actionSheet)
  
  }
  
}
