//
//  CompaniesTableViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 24/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class CompaniesTableViewController: UITableViewController, UISearchResultsUpdating {
  
  var searchController: UISearchController!
  var leftButtonItemView: UIBarButtonItem! = nil
  
  //
  private var arrayOfElements: Array<Company>! = Array<Company>()
  private var filteredElements: Array<Company>! = Array<Company>()
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
    
  }
  
  init(style: UITableViewStyle, newArrayOfCompanies: Array<Company>) {
    
    super.init(style: style)
    
    arrayOfElements = newArrayOfCompanies
    filteredElements = newArrayOfCompanies
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.initNavigationBar()
    self.initSearchController()
    self.initTableView()
    
  }
  
  private func initNavigationBar() {
    
    self.initLeftNavigationView()
    self.initRightNavigationView()
    self.changeNavigationBarTitle()
    
  }

private func changeNavigationBarTitle() {
  
  if #available(iOS 11.0, *) {
    
    self.navigationItem.title = ViewControllersConstants.NameOfScreen.empresasTableText
    self.navigationItem.largeTitleDisplayMode = .never
    
  } else {
    
    let titleLabel = UILabel.init(frame: CGRect.zero)
    
    let font = UIFont(name: "SFUIText-Regular",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.NameOfScreen.empresasTableText,
      attributes:[NSFontAttributeName:font!,
                  NSParagraphStyleAttributeName:style,
                  NSForegroundColorAttributeName:color
      ]
    )
    titleLabel.attributedText = stringWithFormat
    titleLabel.sizeToFit()
    self.navigationItem.titleView = titleLabel
    
  }
  
}
  
  private func initLeftNavigationView() {
    
    self.title = ViewControllersConstants.NameOfScreen.empresasTableText
    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
//    let leftButton = UIBarButtonItem(title: ViewControllersConstants.ClientDetailViewController.backButtonNavigationBarText,
//                                     style: UIBarButtonItemStyle.plain,
//                                     target: self,
//                                     action: #selector(backButtonPressed))
//    
//    let fontForButtonItem =  UIFont(name: "SFUIText-Regular",
//                                    size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
//    
//    let attributesDict: [String:AnyObject] = [NSFontAttributeName: fontForButtonItem!,
//                                              NSForegroundColorAttributeName: UIColor.orange
//    ]
//    
//    leftButton.setTitleTextAttributes(attributesDict, for: .normal)
//    
//    self.navigationItem.leftBarButtonItem = leftButton
    
  }
  
  private func initRightNavigationView() {
    
    let imageBackButton = UIImage.init(named: "plus")?.withRenderingMode(.alwaysOriginal)
    
    let profileButton = UIBarButtonItem.init(image: imageBackButton,
                                             style: .plain,
                                             target: self,
                                             action: #selector(createSomethingButtonPressed))
    
    self.navigationItem.setRightBarButton(profileButton, animated: false)
    
  }
  
  private func initSearchController() {
    
    searchController = UISearchController.init(searchResultsController: nil)
    searchController.searchResultsUpdater = self
    searchController.dimsBackgroundDuringPresentation = false
    self.definesPresentationContext = true
    searchController.searchBar.barTintColor = UtilityManager.sharedInstance.backgroundColorForSearchBar
    self.tableView.tableHeaderView = searchController.searchBar
    
  }
  
  private func initTableView() {
    
    self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      return filteredElements.count
      
    }
    
    return arrayOfElements.count
    
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    return 45.0 * UtilityManager.sharedInstance.conversionHeight
    
  }
  
  private func filterContentForSearchText(searchText: String, scope: String = "All") {
    
    filteredElements = arrayOfElements.filter { element in
      
      return element.name.lowercased().contains(searchText.lowercased()) || element.rfc.lowercased().contains(searchText.lowercased())
      
    }
    
    self.tableView.reloadData()
    
  }
  
  func updateSearchResults(for searchController: UISearchController) {
    
    self.filterContentForSearchText(searchText: searchController.searchBar.text!)
    
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
    
    if cell == nil {
      
      cell = UITableViewCell.init(style: .default, reuseIdentifier: "cell")
      
    }
    
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      cell!.textLabel?.text = filteredElements[indexPath.row].name
      
    } else {
      
      cell!.textLabel?.text = arrayOfElements[indexPath.row].name
      
    }
    
    cell?.textLabel?.textColor = UtilityManager.sharedInstance.labelsAndLinesColor
    cell?.detailTextLabel?.textColor = UtilityManager.sharedInstance.labelsAndLinesColor
    
    return cell!
    
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    var selectedCompany: Company
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      selectedCompany = filteredElements[indexPath.row]
      
    } else {
      
      selectedCompany = arrayOfElements[indexPath.row]
      
    }
    
    let companyScreen = CompanyDetailViewController.init(newCompanyData: selectedCompany)
    self.navigationController?.pushViewController(companyScreen, animated: true)
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
  
    super.viewWillAppear(animated)
    
    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
  }
  
  @objc private func backButtonPressed() {
    
    _ = self.navigationController?.popViewController(animated: true)
    
  }
  
  @objc open func createSomethingButtonPressed() {
    
    let alertController = UIAlertController(title: "Crear empresa",
                                            message: ViewControllersConstants.CompaniesTableViewController.createCompanyMessage,
                                            preferredStyle: UIAlertControllerStyle.alert)
    
    let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
    alertController.addAction(cancelAction)
    
    let actualController = UtilityManager.sharedInstance.currentViewController()
    actualController.present(alertController, animated: true, completion: nil)
    
    
    print("show profile view controller")
    
  }
  
}
