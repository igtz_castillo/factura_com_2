//
//  Option.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 10/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

struct Option {
  
  var id: String! = nil
  var name: String! = nil
  var type: String! = nil
  
}
