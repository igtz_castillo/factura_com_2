//
//  MainTabController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 28/03/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController, UITabBarControllerDelegate {
  
  override func viewDidLoad() {
    
    UINavigationBar.appearance().isTranslucent = true
    UINavigationBar.appearance().tintColor = UtilityManager.sharedInstance.backGroundColorApp
    
    super.viewDidLoad()
    
  }
  
}
