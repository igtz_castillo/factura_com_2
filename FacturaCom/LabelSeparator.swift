//
//  LabelSeparator.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 07/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class LabelSeparator: UILabel {
  
  override func drawText(in rect: CGRect) {
    
    let insets = UIEdgeInsets.init(top: -30 * UtilityManager.sharedInstance.conversionHeight,
                                  left: 0,
                                bottom: -30 * UtilityManager.sharedInstance.conversionHeight,
                                 right: 0)
    
    let newRect = CGRect.init(x: (16.0 * UtilityManager.sharedInstance.conversionWidth),
                              y: (0.0 * UtilityManager.sharedInstance.conversionHeight) ,
                          width: self.frame.size.width,
                         height: 30.0 * UtilityManager.sharedInstance.conversionHeight)
   
    let lastRect = UIEdgeInsetsInsetRect(newRect, insets)
    
    super.drawText(in: lastRect)
    
  }
  
  
  
}
