//
//  DecimalNumber.swift
//  FacturaCom
//
//  Created by Israel on 29/09/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class DecimalNumber {
  
  var id: String! = nil
  var name: String! = nil
  var code: String! = nil
  
  init(newId: String, newName: String, newCode: String) {
    
    id = newId
    name = newName
    code = newCode
    
  }
  
}
