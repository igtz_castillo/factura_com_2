//
//  Country.swift
//  FacturaCom
//
//  Created by Israel on 13/11/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class Country {
  
  var name: String! = nil
  var id: String! = nil
  
  init(newId: String, newName: String) {
    
    id = newId
    name = newName
    
  }
  
}
