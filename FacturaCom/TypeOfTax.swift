//
//  TypeOfTax.swift
//  FacturaCom
//
//  Created by Israel on 07/01/18.
//  Copyright © 2018 Alejandro Aristi C. All rights reserved.
//

import Foundation

class TypeOfTax {
  
  var id: String! = nil
  var name: String! = nil
  var code: String! = nil
  
  init(newId: String, newName: String, newCode: String) {
    
    id = newId
    name = newName
    code = newCode
    
  }
  
}
