//
//  ProductService.swift
//  FacturaCom
//
//  Created by Israel Gutiérrez Castillo on 25/10/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class ProductService {
  
  var code: String! = nil
  var name: String! = nil
  var price: String! = nil
  var sku: String! = nil
  var unidad: String! = nil
  var claveProdServ: String! = nil
  var claveUnidad: String! = nil
  
}
