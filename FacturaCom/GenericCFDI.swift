//
//  GenericCFDI.swift
//  FacturaCom
//
//  Created by Israel on 25/09/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

enum CFDIType {
  
  case FacturaType
  case ReciboType
  case NotaType
  case CartaType
  
}

class GenericCFDI: CFDI {
  
  var uuid: String! = nil
  var typeOfCFDI: TypeOfCFDI! = nil
  var stampingDate: Option! = nil
  var cfdiUseOption: CFDIUseOption! = nil
  var relatedInvoiceOption: RelatedInvoiceOption! = nil
  var typeOfInvoiceRelationship: TypeOfInvoiceRelationship! = nil
  var cfdiRelationship: CFDIRelationship! = nil
  var decimalNumber: Option! = nil
  
  var iva: String! = nil
  var numOrder: String! = nil
  var descuento: String! = nil
  var subtotal: String! = nil
  var referenceClient: String! = nil
  var total: String! = nil
  var uid: String! = nil
  
  //Needed values to create a CFDI
  
  var expeditionPlace: ExpeditionPlace! = nil
  var client: Client! = nil
  var clientOrRFC: Bool! = nil //true is RFC
  var paymentWay: WayToPay! = nil
  var paymentMethod: PaymentMethod! = nil
  var typeOfMoney: Currency! = nil
  var serie: Serie! = nil
  var exchangeRate: String! = nil
  var creditCardNumber: String! = nil
  
  var arrayOfCFDIWithRelationship: Array<CFDIVersionThree> = Array<CFDIVersionThree>()
  
  init(empty: Bool) {
    
    super.init(newFolio: "",
               newStatus: "",
               newFechaTimbrado: "",
               newReceptor: "")
    
  }
  
  init(newUuid: String!, newTypeOfCFDI: TypeOfCFDI, newStatus: String, newFolio: String!, newIva: String!, newNumOrder: String!, newFechaTimbrado: String!, newDescuento: String!,newSubtotal: String!, newReferenceClient: String!, newReceptor: String!, newTotal: String!, newUid: String!) {
    
    super.init(newFolio: newFolio,
               newStatus: newStatus,
               newFechaTimbrado: newFechaTimbrado,
               newReceptor: newReceptor)
    
    uuid = newUuid
    typeOfCFDI = newTypeOfCFDI
    iva = newIva
    numOrder = newNumOrder
    descuento = newDescuento
    subtotal = newSubtotal
    referenceClient = newReferenceClient
    total = newTotal
    uid = newUid
    
  }
  
}
