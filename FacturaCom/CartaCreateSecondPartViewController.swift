//
//  CartaCreateSecondPartViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 13/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class CartaCreateSecondPartViewController: UIViewController, ButtonToActivePickerViewDelegate, CustomTextFieldWithTitleViewDelegate, CreateConceptoForCartaViewControllerDelegate, ConceptoLikeViewCellDelegate {
  
  private var cartaDataToCreate: Carta! = Carta.init(empty: true)
  private var arrayOfConceptos: Array<Concepto>! = Array<Concepto>()
  private var arrayOfConceptoButtons: Array<ConceptoLikeViewCell>! = Array<ConceptoLikeViewCell>()
  private var arrayOfUnits: Array<Unity>! = Array<Unity>()
  
  private var arrayOfViews: Array<UIView>! = Array<UIView>()
  
  private var mainScrollView: UIScrollView! = nil
  
  private var notionsLabel: LabelSeparator! = nil
  private var buttonForCreateConcepto: ConceptoButton! = nil
  //  private var notionCustomTextField: CustomTextFieldWithTitleView! = nil
  //  private var quantityCustomTextField: CustomTextFieldWithTitleView! = nil
  //  private var unityCostCustomTextField: CustomTextFieldWithTitleView! = nil
  //  private var discountCustomTextField: CustomTextFieldWithTitleView! = nil
  
  //  private var taxesLabelSwitch: LabelWithSwitch! = nil
  //  private var customHouseLabelSwitch: LabelWithSwitch! = nil
  
  //labelsForAdditions
  
  private var subTotalAdditionLabel: LabelForAdditions! = nil
  private var subTotalValueAdditionLabel: LabelForAdditions! = nil
  private var discountAdditionLabel: LabelForAdditions! = nil
  private var discountValueAdditionLabel: LabelForAdditions! = nil
  
  //
  
  //  private var ivaLabelSwitch: LabelWithSwitch! = nil
  //  private var ishLabelSwitch: LabelWithSwitch! = nil
  //  private var iepsLabelSwitch: LabelWithSwitch! = nil
  //  private var retIVALabelSwitch: LabelWithSwitch! = nil
  //  private var retISRLabelSwitch: LabelWithSwitch! = nil
  
  //  private var someQuantityTextField: CustomTextFieldWithTitleView! = nil
  private var commentsCustomTextField: CustomTextFieldWithTitleView! = nil
  
  //labelsForAdditions
  
  private var ivaAdditionLabel: LabelForAdditions! = nil
  private var ivaValueAdditionLabel: LabelForAdditions! = nil
  
  private var ishAdditionLabel: LabelForAdditions! = nil
  private var ishValueAdditionLabel: LabelForAdditions! = nil
  
  private var iepsAdditionLabel: LabelForAdditions! = nil
  private var iepsValueAdditionLabel: LabelForAdditions! = nil
  
  private var retIvaAdditionLabel: LabelForAdditions! = nil
  private var retIvaValueAdditionLabel: LabelForAdditions! = nil
  
  private var retIsrAdditionLabel: LabelForAdditions! = nil
  private var retIsrValueAdditionLabel: LabelForAdditions! = nil
  
  private var retIvaPorteAdditionLabel: LabelForAdditions! = nil
  private var retIvaPorteValueAdditionLabel: LabelForAdditions! = nil
  
  private var totalAdditionLabel: LabelForAdditions! = nil
  
  //
  
  private var sendViaMailLabelSwitch: LabelWithSwitch! = nil
//  private var payedLabelSwitch: LabelWithSwitch! = nil
  
  //View for buttons
  private var backViewButtons: UIView! = nil
  
  //Buttons
  private var saveButton: UIButton! = nil
  private var previsualizationButton: UIButton! = nil
  private var createButton: UIButton! = nil
  
  private var isSecondTimeShowing: Bool = false
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
    
  }
  
  init(cartaToCreate: Carta) {
    
    cartaDataToCreate = cartaToCreate
    
    super.init(nibName: nil, bundle: nil)
    
  }
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
    tapGesture.numberOfTouchesRequired = 1
    self.view.addGestureRecognizer(tapGesture)
    
    self.editNavigationController()
    self.initMainScrollView()
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.getUnits(actionsToMakeWhenSucceeded: { (units) in
      
      self.arrayOfUnits = units
      self.initInterface()
      
      UtilityManager.sharedInstance.hideLoader()
      
    }, actionsToMakeWhenFailed: {
      
      UtilityManager.sharedInstance.hideLoader()
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Error de conexión con el servidor, favor de intentar más tarde",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        _ = self.navigationController?.popViewController(animated: true)
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    })
    
  }
  
  private func initInterface() {
    
    self.initNotionsLabel()
    self.initButtonsOfConceptos()
    self.initButtonForCreateConcepto()
    //    self.initNotionCustomTextField()
    //    self.initQuantityCustomTextField()
    //    self.initUnityCostCustomTextField()
    //    self.initDiscountCustomTextField()
    //    self.initTaxesLabelSwitch()
    //    self.initCustomHouseLabelSwitch()
    
    //labelsAddition
    self.initSubTotalAdditionLabel()
    self.initSubTotalValueAdditionLabel()
    self.initDiscountAdditionLabel()
    self.initDiscountValueAdditionLabel()
    //
    
    //    self.initIvaLabelSwitch()
    //    self.initIshLabelSwitch()
    //    self.initIepsLabelSwitch()
    //    self.initRetIVALabelSwitch()
    //    self.initRetISRLabelSwitch()
    //    self.initSomeQuantityTextField()
    
    //labelsAddition
    
    self.initIVAAdditionLabel()
    self.initIvaValueAdditionLabel()
    
    for concepto in arrayOfConceptos {
      
      if concepto.applyIsh == true {
        
        self.initIshAdditionLabel()
        self.initIshValueAdditionLabel()
        
        break
        
      }
      
    }
    
    for concepto in arrayOfConceptos {
      
      if concepto.applyIeps == true {
        
        self.initIepsAdditionLabel()
        self.initIepsValueAdditionLabel()
        
        break
        
      }
      
    }
    
    for concepto in arrayOfConceptos {
      
      if concepto.applyRetIva == true {
        
        self.initRetIvaAdditionLabel()
        self.initRetIvaValueAdditionLabel()
        
        break
        
      }
      
    }
    
    for concepto in arrayOfConceptos {
      
      if concepto.applyRetIsr == true {
        
        self.initRetIsrAdditionLabel()
        self.initRetIsrValueAdditionLabel()
        
        break
        
      }
      
    }
    
    for concepto in arrayOfConceptos {
      
      if concepto.applyRetIvaPorte == true {
        
        self.initRetIvaPorteAdditionLabel()
        self.initRetIvaPorteValueAdditionLabel()
        
        break
        
      }
      
    }
    
    self.initTotalAdditionLabel()
    
    self.initCommentsCustomTextField()
    
    //
    
    self.initsendViaMailLabelSwitch()
//    self.initpayedLabelSwitch()
    self.createBackViewButtons()
    
  }
  
  private func editNavigationController() {
    
    self.changeBackButtonItem()
    self.changeNavigationBarTitle()
    self.changeNavigationRigthButtonItem()
    
  }
  
  private func changeBackButtonItem() {
    
    self.title = ViewControllersConstants.MainTabController.cartasButtonText
    
    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
    //    let leftButton = UIBarButtonItem(title: ViewControllersConstants.FacturaCreateViewController.backButtonNavigationBarText,
    //                                     style: UIBarButtonItemStyle.plain,
    //                                     target: self,
    //                                     action: #selector(backButtonPressed))
    //
    //    let fontForButtonItem =  UIFont(name: "SFUIText-Regular",
    //                                    size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    //
    //    let attributesDict: [String:AnyObject] = [NSFontAttributeName: fontForButtonItem!,
    //                                              NSForegroundColorAttributeName: UIColor.orange
    //    ]
    //
    //    leftButton.setTitleTextAttributes(attributesDict, for: .normal)
    //
    //    self.navigationItem.leftBarButtonItem = leftButton
    
  }
  
  private func changeNavigationBarTitle() {
    
    let titleLabel = UILabel.init(frame: CGRect.zero)
    
    let font = UIFont(name: "SFUIText-Regular",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateViewController.titleNavigationBarTextForPorte,
      attributes:[NSFontAttributeName:font!,
                  NSParagraphStyleAttributeName:style,
                  NSForegroundColorAttributeName:color
      ]
    )
    titleLabel.attributedText = stringWithFormat
    titleLabel.sizeToFit()
    self.navigationItem.titleView = titleLabel
    
  }
  
  private func changeNavigationRigthButtonItem() {
    
    let rightButton = UIBarButtonItem(title: "",
                                      style: UIBarButtonItemStyle.plain,
                                      target: self,
                                      action: nil)
    
    let fontForButtonItem =  UIFont(name: "SFUIText-Regular",
                                    size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    
    let attributesDict: [String:AnyObject] = [NSFontAttributeName: fontForButtonItem!,
                                              NSForegroundColorAttributeName: UIColor.orange
    ]
    
    rightButton.setTitleTextAttributes(attributesDict, for: .normal)
    
    self.navigationItem.rightBarButtonItem = rightButton
    
  }
  
  
  private func initMainScrollView() {
    
    mainScrollView = UIScrollView.init(frame: self.view.frame)
    
    if UtilityManager.sharedInstance.isIpad() == false {
      
      let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                       height: mainScrollView.frame.size.height + (100.0 * UtilityManager.sharedInstance.conversionHeight))
      
      mainScrollView.contentSize = newContentSize
      
    }else {
      
      let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                       height: mainScrollView.frame.size.height + (545.0 * UtilityManager.sharedInstance.conversionHeight))
      
      mainScrollView.contentSize = newContentSize
      
    }
    
    
    
    self.view.addSubview(mainScrollView)
    
  }
  
  private func initNotionsLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: mainScrollView.frame.size.width,
                                    height: 56.0 * UtilityManager.sharedInstance.conversionHeight)
    
    notionsLabel = LabelSeparator.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.notionsLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    notionsLabel.attributedText = stringWithFormat
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: notionsLabel.frame.size.width,
                               height: notionsLabel.frame.size.height)
    notionsLabel.frame = newFrame
    notionsLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(notionsLabel)
    
    self.arrayOfViews.append(notionsLabel)
    
  }
  
  private func initButtonsOfConceptos() {
    
    var frameForButtons = CGRect.init(x: 0.0,
                                      y: notionsLabel.frame.origin.y + notionsLabel.frame.size.height,
                                      width: self.mainScrollView.frame.size.width,
                                      height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    if arrayOfConceptos.count > 0 {
      
      for i in 0...arrayOfConceptos.count - 1 {
        
        let conceptoCell = ConceptoLikeViewCell.init(frame: frameForButtons)
        conceptoCell.setStringLabelOfButton(newName: arrayOfConceptos[i].conceptDescription)
        conceptoCell.tag = i
        conceptoCell.delegate = self
        
        self.mainScrollView.addSubview(conceptoCell)
        
        frameForButtons = CGRect.init(x: 0.0,
                                      y: frameForButtons.origin.y + frameForButtons.size.height,
                                      width: self.mainScrollView.frame.size.width,
                                      height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
        
        arrayOfConceptoButtons.append(conceptoCell)
        
      }
      
    }
    
  }
  
  private func initButtonForCreateConcepto() {
    
    var frameForButton = CGRect.init(x: 0.0,
                                     y: notionsLabel.frame.origin.y + notionsLabel.frame.size.height,
                                     width: UIScreen.main.bounds.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    if arrayOfConceptoButtons.last != nil {
      
      frameForButton = CGRect.init(x: 0.0,
                                   y: (arrayOfConceptoButtons.last?.frame.origin.y)! + (arrayOfConceptoButtons.last?.frame.size.height)! ,
                                   width: UIScreen.main.bounds.width,
                                   height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
      
    }
    
    buttonForCreateConcepto = ConceptoButton.init(frame: frameForButton)
    buttonForCreateConcepto.changeMyTitle("Agregar concepto", color: UIColor.blue)
    buttonForCreateConcepto.drawTopBorder()
    buttonForCreateConcepto.drawBottomBorder()
    buttonForCreateConcepto.addTarget(self, action: #selector(buttonForCreateConceptoPressed), for: UIControlEvents.touchUpInside)
    buttonForCreateConcepto.isUserInteractionEnabled = true
    
    buttonForCreateConcepto.backgroundColor = UIColor.white
    
    
    self.mainScrollView.addSubview(buttonForCreateConcepto)
    
    self.arrayOfViews.append(buttonForCreateConcepto)
    
  }
  
  //  private func initNotionCustomTextField() {
  //
  //    let frameForView = CGRect.init(x: 0.0,
  //                                   y: buttonForCreateConcepto.frame.origin.y + buttonForCreateConcepto.frame.size.height,
  //                                   width: mainScrollView.frame.size.width,
  //                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
  //
  //    notionCustomTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
  //                                                           title: ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText,
  //                                                           image: nil,
  //                                                           colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
  //                                                           positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
  //                                                           positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
  //    notionCustomTextField.mainTextField.textColor = UIColor.black
  //    notionCustomTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText
  //    notionCustomTextField.backgroundColor = UIColor.white
  //    notionCustomTextField.delegate = self
  //    self.mainScrollView.addSubview(notionCustomTextField)
  //
  //    self.arrayOfViews.append(notionCustomTextField)
  //
  //  }
  //
  //  private func initQuantityCustomTextField() {
  //
  //    let frameForView = CGRect.init(x: 0.0,
  //                                   y: notionCustomTextField.frame.origin.y + notionCustomTextField.frame.size.height,
  //                                   width: mainScrollView.frame.size.width,
  //                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
  //
  //    quantityCustomTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
  //                                                              title: ViewControllersConstants.FacturaCreateSecondPartViewController.quantityTextFieldText,
  //                                                              image: nil,
  //                                                              colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
  //                                                              positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
  //                                                              positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
  //    quantityCustomTextField.mainTextField.textColor = UIColor.black
  //    quantityCustomTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.quantityTextFieldText
  //    quantityCustomTextField.backgroundColor = UIColor.white
  //    quantityCustomTextField.delegate = self
  //    self.mainScrollView.addSubview(quantityCustomTextField)
  //
  //    self.arrayOfViews.append(quantityCustomTextField)
  //
  //  }
  //
  //  private func initUnityCostCustomTextField() {
  //
  //    let frameForView = CGRect.init(x: 0.0,
  //                                   y: quantityCustomTextField.frame.origin.y + quantityCustomTextField.frame.size.height,
  //                                   width: mainScrollView.frame.size.width,
  //                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
  //
  //    unityCostCustomTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
  //                                                                title: ViewControllersConstants.FacturaCreateSecondPartViewController.unityCostTextFieldText,
  //                                                                image: nil,
  //                                                                colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
  //                                                                positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
  //                                                                positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
  //    unityCostCustomTextField.mainTextField.textColor = UIColor.black
  //    unityCostCustomTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.unityCostTextFieldText
  //    unityCostCustomTextField.backgroundColor = UIColor.white
  //    unityCostCustomTextField.delegate = self
  //    self.mainScrollView.addSubview(unityCostCustomTextField)
  //
  //    self.arrayOfViews.append(unityCostCustomTextField)
  //
  //  }
  //
  //  private func initDiscountCustomTextField() {
  //
  //    let frameForView = CGRect.init(x: 0.0,
  //                                   y: unityCostCustomTextField.frame.origin.y + unityCostCustomTextField.frame.size.height,
  //                                   width: mainScrollView.frame.size.width,
  //                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
  //
  //    discountCustomTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
  //                                                                 title: ViewControllersConstants.FacturaCreateSecondPartViewController.discountTextFieldText,
  //                                                                 image: nil,
  //                                                                 colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
  //                                                                 positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
  //                                                                 positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
  //    discountCustomTextField.mainTextField.textColor = UIColor.black
  //    discountCustomTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.discountTextFieldText
  //    discountCustomTextField.backgroundColor = UIColor.white
  //    discountCustomTextField.delegate = self
  //    self.mainScrollView.addSubview(discountCustomTextField)
  //
  //    self.arrayOfViews.append(discountCustomTextField)
  //
  //  }
  //
  //  private func initTaxesLabelSwitch() {
  //
  //    let frameForView = CGRect.init(x: 0.0,
  //                                   y: discountCustomTextField.frame.origin.y + discountCustomTextField.frame.size.height,
  //                                   width: mainScrollView.frame.size.width,
  //                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
  //
  //    taxesLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false)
  //    taxesLabelSwitch.isUserInteractionEnabled = true
  //
  //    let font = UIFont(name: "SFUIText-Light",
  //                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
  //    let color = UtilityManager.sharedInstance.labelsAndLinesColor
  //    let style = NSMutableParagraphStyle()
  //    style.alignment = .left
  //
  //    let stringWithFormat = NSMutableAttributedString(
  //      string: ViewControllersConstants.FacturaCreateSecondPartViewController.taxesLabelSwitchText,
  //      attributes:[NSFontAttributeName: font!,
  //                  NSParagraphStyleAttributeName: style,
  //                  NSForegroundColorAttributeName: color
  //      ]
  //    )
  //
  //    taxesLabelSwitch.attributedText = stringWithFormat
  //    self.mainScrollView.addSubview(taxesLabelSwitch)
  //
  //    self.arrayOfViews.append(taxesLabelSwitch)
  //
  //  }
  //
  //  private func initCustomHouseLabelSwitch() {
  //
  //    let frameForView = CGRect.init(x: 0.0,
  //                                   y: taxesLabelSwitch.frame.origin.y + taxesLabelSwitch.frame.size.height,
  //                                   width: mainScrollView.frame.size.width,
  //                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
  //
  //    customHouseLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false)
  //    customHouseLabelSwitch.isUserInteractionEnabled = true
  //
  //    let font = UIFont(name: "SFUIText-Light",
  //                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
  //    let color = UtilityManager.sharedInstance.labelsAndLinesColor
  //    let style = NSMutableParagraphStyle()
  //    style.alignment = .left
  //
  //    let stringWithFormat = NSMutableAttributedString(
  //      string: ViewControllersConstants.FacturaCreateSecondPartViewController.customHouseLabelSwitchText,
  //      attributes:[NSFontAttributeName: font!,
  //                  NSParagraphStyleAttributeName: style,
  //                  NSForegroundColorAttributeName: color
  //      ]
  //    )
  //
  //    customHouseLabelSwitch.attributedText = stringWithFormat
  //    self.mainScrollView.addSubview(customHouseLabelSwitch)
  //
  //    self.arrayOfViews.append(customHouseLabelSwitch)
  //
  //  }
  
  private func initSubTotalAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: buttonForCreateConcepto.frame.origin.y + buttonForCreateConcepto.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    subTotalAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.subTotalAdditionLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    subTotalAdditionLabel.attributedText = stringWithFormat
    subTotalAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(subTotalAdditionLabel)
    
    self.arrayOfViews.append(subTotalAdditionLabel)
    
  }
  
  private func initSubTotalValueAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: subTotalAdditionLabel.frame.origin.y + subTotalAdditionLabel.frame.size.height - (15.0 * UtilityManager.sharedInstance.conversionHeight),
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    subTotalValueAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    var finalSubtotal = 0.0
    
    for concepto in arrayOfConceptos {
      
      finalSubtotal = finalSubtotal + concepto.subtotal
      
    }
    
    let stringWithFormat = NSMutableAttributedString(
      string: "\(ViewControllersConstants.FacturaCreateSecondPartViewController.subtotalValueAdditionLabelText)\(finalSubtotal)",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    subTotalValueAdditionLabel.attributedText = stringWithFormat
    subTotalValueAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(subTotalValueAdditionLabel)
    
    self.arrayOfViews.append(subTotalValueAdditionLabel)
    
  }
  
  private func initDiscountAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: subTotalValueAdditionLabel.frame.origin.y + subTotalValueAdditionLabel.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    discountAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.discountAdditionLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    discountAdditionLabel.attributedText = stringWithFormat
    discountAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(discountAdditionLabel)
    
    self.arrayOfViews.append(discountAdditionLabel)
    
  }
  
  private func initDiscountValueAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: discountAdditionLabel.frame.origin.y + discountAdditionLabel.frame.size.height - (15.0 * UtilityManager.sharedInstance.conversionHeight),
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    discountValueAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    var finalDiscount = 0.0
    
    for concepto in arrayOfConceptos {
      
      finalDiscount = finalDiscount + concepto.finalDiscount
      
    }
    
    
    let stringWithFormat = NSMutableAttributedString(
      string: "\(ViewControllersConstants.FacturaCreateSecondPartViewController.discountAdditionValueLabelText)\(finalDiscount)",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    discountValueAdditionLabel.attributedText = stringWithFormat
    discountValueAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(discountValueAdditionLabel)
    
    self.arrayOfViews.append(discountValueAdditionLabel)
    
  }
  
  
  //  private func initIvaLabelSwitch() {
  //
  //    let frameForView = CGRect.init(x: 0.0,
  //                                   y: subTotalValueAdditionLabel.frame.origin.y + subTotalValueAdditionLabel.frame.size.height + 1.0,
  //                                   width: mainScrollView.frame.size.width,
  //                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
  //
  //    ivaLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false)
  //    ivaLabelSwitch.isUserInteractionEnabled = true
  //
  //    let font = UIFont(name: "SFUIText-Light",
  //                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
  //    let color = UtilityManager.sharedInstance.labelsAndLinesColor
  //    let style = NSMutableParagraphStyle()
  //    style.alignment = .left
  //
  //    let stringWithFormat = NSMutableAttributedString(
  //      string: ViewControllersConstants.FacturaCreateSecondPartViewController.ivaLabelSwitchText,
  //      attributes:[NSFontAttributeName: font!,
  //                  NSParagraphStyleAttributeName: style,
  //                  NSForegroundColorAttributeName: color
  //      ]
  //    )
  //
  //    ivaLabelSwitch.attributedText = stringWithFormat
  //    self.mainScrollView.addSubview(ivaLabelSwitch)
  //
  //    self.arrayOfViews.append(ivaLabelSwitch)
  //
  //  }
  
  //  private func initIshLabelSwitch() {
  //
  //    let frameForView = CGRect.init(x: 0.0,
  //                                   y: subTotalValueAdditionLabel.frame.origin.y + subTotalValueAdditionLabel.frame.size.height,
  //                                   width: mainScrollView.frame.size.width,
  //                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
  //
  //    ishLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false)
  //    ishLabelSwitch.isUserInteractionEnabled = true
  //
  //    let font = UIFont(name: "SFUIText-Light",
  //                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
  //    let color = UtilityManager.sharedInstance.labelsAndLinesColor
  //    let style = NSMutableParagraphStyle()
  //    style.alignment = .left
  //
  //    let stringWithFormat = NSMutableAttributedString(
  //      string: ViewControllersConstants.FacturaCreateSecondPartViewController.ishLabelSwitchText,
  //      attributes:[NSFontAttributeName: font!,
  //                  NSParagraphStyleAttributeName: style,
  //                  NSForegroundColorAttributeName: color
  //      ]
  //    )
  //
  //    ishLabelSwitch.attributedText = stringWithFormat
  //    self.mainScrollView.addSubview(ishLabelSwitch)
  //
  //    self.arrayOfViews.append(ishLabelSwitch)
  //
  //  }
  //
  //  private func initIepsLabelSwitch() {
  //
  //    let frameForView = CGRect.init(x: 0.0,
  //                                   y: ishLabelSwitch.frame.origin.y + ishLabelSwitch.frame.size.height,
  //                                   width: mainScrollView.frame.size.width,
  //                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
  //
  //    iepsLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false)
  //    iepsLabelSwitch.isUserInteractionEnabled = true
  //
  //    let font = UIFont(name: "SFUIText-Light",
  //                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
  //    let color = UtilityManager.sharedInstance.labelsAndLinesColor
  //    let style = NSMutableParagraphStyle()
  //    style.alignment = .left
  //
  //    let stringWithFormat = NSMutableAttributedString(
  //      string: ViewControllersConstants.FacturaCreateSecondPartViewController.iepsLabelSwitchText,
  //      attributes:[NSFontAttributeName: font!,
  //                  NSParagraphStyleAttributeName: style,
  //                  NSForegroundColorAttributeName: color
  //      ]
  //    )
  //
  //    iepsLabelSwitch.attributedText = stringWithFormat
  //    self.mainScrollView.addSubview(iepsLabelSwitch)
  //
  //    self.arrayOfViews.append(iepsLabelSwitch)
  //
  //  }
  //
  //  private func initRetIVALabelSwitch() {
  //
  //    let frameForView = CGRect.init(x: 0.0,
  //                                   y: iepsLabelSwitch.frame.origin.y + iepsLabelSwitch.frame.size.height,
  //                                   width: mainScrollView.frame.size.width,
  //                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
  //
  //    retIVALabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false)
  //    retIVALabelSwitch.isUserInteractionEnabled = true
  //
  //    let font = UIFont(name: "SFUIText-Light",
  //                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
  //    let color = UtilityManager.sharedInstance.labelsAndLinesColor
  //    let style = NSMutableParagraphStyle()
  //    style.alignment = .left
  //
  //    let stringWithFormat = NSMutableAttributedString(
  //      string: ViewControllersConstants.FacturaCreateSecondPartViewController.retIVALabelSwitchText,
  //      attributes:[NSFontAttributeName: font!,
  //                  NSParagraphStyleAttributeName: style,
  //                  NSForegroundColorAttributeName: color
  //      ]
  //    )
  //
  //    retIVALabelSwitch.attributedText = stringWithFormat
  //    self.mainScrollView.addSubview(retIVALabelSwitch)
  //
  //    self.arrayOfViews.append(retIVALabelSwitch)
  //
  //  }
  //
  //  private func initRetISRLabelSwitch() {
  //
  //    let frameForView = CGRect.init(x: 0.0,
  //                                   y: retIVALabelSwitch.frame.origin.y + retIVALabelSwitch.frame.size.height,
  //                                   width: mainScrollView.frame.size.width,
  //                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
  //
  //    retISRLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false)
  //    retISRLabelSwitch.isUserInteractionEnabled = true
  //
  //    let font = UIFont(name: "SFUIText-Light",
  //                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
  //    let color = UtilityManager.sharedInstance.labelsAndLinesColor
  //    let style = NSMutableParagraphStyle()
  //    style.alignment = .left
  //
  //    let stringWithFormat = NSMutableAttributedString(
  //      string: ViewControllersConstants.FacturaCreateSecondPartViewController.retISRLabelSwitchText,
  //      attributes:[NSFontAttributeName: font!,
  //                  NSParagraphStyleAttributeName: style,
  //                  NSForegroundColorAttributeName: color
  //      ]
  //    )
  //
  //    retISRLabelSwitch.attributedText = stringWithFormat
  //    self.mainScrollView.addSubview(retISRLabelSwitch)
  //
  //    self.arrayOfViews.append(retISRLabelSwitch)
  //
  //  }
  
  //  private func initSomeQuantityTextField() {
  //
  //    let frameForView = CGRect.init(x: 0.0,
  //                                   y: retISRLabelSwitch.frame.origin.y + retISRLabelSwitch.frame.size.height,
  //                                   width: mainScrollView.frame.size.width,
  //                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
  //
  //    someQuantityTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
  //                                                              title: "SOME QUANTITY",
  //                                                              image: nil,
  //                                                              colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
  //                                                              positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
  //                                                              positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
  //    someQuantityTextField.mainTextField.textColor = UIColor.black
  //    someQuantityTextField.mainTextField.placeholder = "SOME QUANTITY"
  //    someQuantityTextField.backgroundColor = UIColor.white
  //    someQuantityTextField.delegate = self
  //    self.mainScrollView.addSubview(someQuantityTextField)
  //
  //    self.arrayOfViews.append(someQuantityTextField)
  //
  //  }
  
  
  
  ////////////////////////////
  //LABELS FOR ADDITION
  //
  //
  
  private func initIVAAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: discountValueAdditionLabel.frame.origin.y + discountValueAdditionLabel.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    ivaAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.ivaAdditionLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    ivaAdditionLabel.attributedText = stringWithFormat
    ivaAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(ivaAdditionLabel)
    
    self.arrayOfViews.append(ivaAdditionLabel)
    
  }
  
  private func initIvaValueAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: ivaAdditionLabel.frame.origin.y + ivaAdditionLabel.frame.size.height - (15.0 * UtilityManager.sharedInstance.conversionHeight),
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    ivaValueAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    var finalIva = 0.0
    
    for concepto in arrayOfConceptos {
      
      finalIva = finalIva + concepto.finalIva
      
    }
    
    let stringWithFormat = NSMutableAttributedString(
      string: "\(ViewControllersConstants.FacturaCreateSecondPartViewController.valueAdditionLabelText)\(finalIva)",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    ivaValueAdditionLabel.attributedText = stringWithFormat
    ivaValueAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(ivaValueAdditionLabel)
    
    self.arrayOfViews.append(ivaValueAdditionLabel)
    
  }
  
  private func initIshAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: arrayOfViews.last!.frame.origin.y + arrayOfViews.last!.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    ishAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.ishAdditionLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    ishAdditionLabel.attributedText = stringWithFormat
    ishAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(ishAdditionLabel)
    
    self.arrayOfViews.append(ishAdditionLabel)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.contentSize.height + (40.0 * UtilityManager.sharedInstance.conversionHeight))
    
    mainScrollView.contentSize = newContentSize
    
  }
  
  private func initIshValueAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: arrayOfViews.last!.frame.origin.y + arrayOfViews.last!.frame.size.height - (15.0 * UtilityManager.sharedInstance.conversionHeight),
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    ishValueAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    var finalIsh = 0.0
    
    for concepto in arrayOfConceptos {
      
      finalIsh = finalIsh + concepto.finalIsh
      
    }
    
    let stringWithFormat = NSMutableAttributedString(
      string: "\(ViewControllersConstants.FacturaCreateSecondPartViewController.valueAdditionLabelText)\(finalIsh)",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    ishValueAdditionLabel.attributedText = stringWithFormat
    ishValueAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(ishValueAdditionLabel)
    
    self.arrayOfViews.append(ishValueAdditionLabel)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.contentSize.height + (25.0 * UtilityManager.sharedInstance.conversionHeight))
    
    mainScrollView.contentSize = newContentSize
    
  }
  
  private func initIepsAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: arrayOfViews.last!.frame.origin.y + arrayOfViews.last!.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    iepsAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.iepsAdditionLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    iepsAdditionLabel.attributedText = stringWithFormat
    iepsAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(iepsAdditionLabel)
    
    self.arrayOfViews.append(iepsAdditionLabel)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.contentSize.height + (40.0 * UtilityManager.sharedInstance.conversionHeight))
    
    mainScrollView.contentSize = newContentSize
    
  }
  private func initIepsValueAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: arrayOfViews.last!.frame.origin.y + arrayOfViews.last!.frame.size.height - (15.0 * UtilityManager.sharedInstance.conversionHeight),
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    iepsValueAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    var finalIeps = 0.0
    
    for concepto in arrayOfConceptos {
      
      finalIeps = finalIeps + concepto.finalIeps
      
    }
    
    let stringWithFormat = NSMutableAttributedString(
      string: "\(ViewControllersConstants.FacturaCreateSecondPartViewController.valueAdditionLabelText)\(finalIeps)",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    iepsValueAdditionLabel.attributedText = stringWithFormat
    iepsValueAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(iepsValueAdditionLabel)
    
    self.arrayOfViews.append(iepsValueAdditionLabel)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.contentSize.height + (25.0 * UtilityManager.sharedInstance.conversionHeight))
    
    mainScrollView.contentSize = newContentSize
    
  }
  
  private func initRetIvaAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: arrayOfViews.last!.frame.origin.y + arrayOfViews.last!.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    retIvaAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.retIvaAdditionLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    retIvaAdditionLabel.attributedText = stringWithFormat
    retIvaAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(retIvaAdditionLabel)
    
    self.arrayOfViews.append(retIvaAdditionLabel)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.contentSize.height + (40.0 * UtilityManager.sharedInstance.conversionHeight))
    
    mainScrollView.contentSize = newContentSize
    
  }
  
  private func initRetIvaValueAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: arrayOfViews.last!.frame.origin.y + arrayOfViews.last!.frame.size.height - (15.0 * UtilityManager.sharedInstance.conversionHeight),
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    retIvaValueAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    var finalRetIva = 0.0
    
    for concepto in arrayOfConceptos {
      
      finalRetIva = finalRetIva + concepto.finalRetIva
      
    }
    
    let stringWithFormat = NSMutableAttributedString(
      string: "\(ViewControllersConstants.FacturaCreateSecondPartViewController.discountAdditionValueLabelText)\(finalRetIva)",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    retIvaValueAdditionLabel.attributedText = stringWithFormat
    retIvaValueAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(retIvaValueAdditionLabel)
    
    self.arrayOfViews.append(retIvaValueAdditionLabel)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.contentSize.height + (25.0 * UtilityManager.sharedInstance.conversionHeight))
    
    mainScrollView.contentSize = newContentSize
    
  }
  
  private func initRetIsrAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: arrayOfViews.last!.frame.origin.y + arrayOfViews.last!.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    retIsrAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.retIsrAdditionLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    retIsrAdditionLabel.attributedText = stringWithFormat
    retIsrAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(retIsrAdditionLabel)
    
    self.arrayOfViews.append(retIsrAdditionLabel)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.contentSize.height + (40.0 * UtilityManager.sharedInstance.conversionHeight))
    
    mainScrollView.contentSize = newContentSize
    
  }
  private func initRetIsrValueAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: arrayOfViews.last!.frame.origin.y + arrayOfViews.last!.frame.size.height - (15.0 * UtilityManager.sharedInstance.conversionHeight),
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    retIsrValueAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    var finalRetIsr = 0.0
    
    for concepto in arrayOfConceptos {
      
      finalRetIsr = finalRetIsr + concepto.finalRetIsr
      
    }
    
    let stringWithFormat = NSMutableAttributedString(
      string: "\(ViewControllersConstants.FacturaCreateSecondPartViewController.discountAdditionValueLabelText)\(finalRetIsr)",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    retIsrValueAdditionLabel.attributedText = stringWithFormat
    retIsrValueAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(retIsrValueAdditionLabel)
    
    self.arrayOfViews.append(retIsrValueAdditionLabel)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.contentSize.height + (25.0 * UtilityManager.sharedInstance.conversionHeight))
    
    mainScrollView.contentSize = newContentSize
    
  }
  
  private func initRetIvaPorteAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: arrayOfViews.last!.frame.origin.y + arrayOfViews.last!.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    retIvaPorteAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.retIvaPorteAdditionLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    retIvaPorteAdditionLabel.attributedText = stringWithFormat
    retIvaPorteAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(retIvaPorteAdditionLabel)
    
    self.arrayOfViews.append(retIvaPorteAdditionLabel)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.contentSize.height + (40.0 * UtilityManager.sharedInstance.conversionHeight))
    
    mainScrollView.contentSize = newContentSize
    
  }
  private func initRetIvaPorteValueAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: arrayOfViews.last!.frame.origin.y + arrayOfViews.last!.frame.size.height - (15.0 * UtilityManager.sharedInstance.conversionHeight),
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    retIvaPorteValueAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    var finalRetIvaPorte = 0.0
    
    for concepto in arrayOfConceptos {
      
      finalRetIvaPorte = finalRetIvaPorte + concepto.finalRetIvaPorte
      
    }
    
    let stringWithFormat = NSMutableAttributedString(
      string: "\(ViewControllersConstants.FacturaCreateSecondPartViewController.discountAdditionValueLabelText)\(finalRetIvaPorte)",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    retIvaPorteValueAdditionLabel.attributedText = stringWithFormat
    retIvaPorteValueAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(retIvaPorteValueAdditionLabel)
    
    self.arrayOfViews.append(retIvaPorteValueAdditionLabel)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.contentSize.height + (25.0 * UtilityManager.sharedInstance.conversionHeight))
    
    mainScrollView.contentSize = newContentSize
    
  }
  
  private func initTotalAdditionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: arrayOfViews.last!.frame.origin.y + arrayOfViews.last!.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var finalDiscount = 0.0
    var finalSubtotal = 0.0
    
    var finalIva = 0.0
    var finalIsh = 0.0
    var finalIeps = 0.0
    var finalRetIva = 0.0
    var finalRetIsr = 0.0
    var finalRetIvaPorte = 0.0
    
    for concepto in arrayOfConceptos {
      
      finalIva = finalIva + concepto.finalIva
      finalIsh = finalIsh + concepto.finalIsh
      finalIeps = finalIeps + concepto.finalIeps
      finalRetIva = finalRetIva + concepto.finalRetIva
      finalRetIsr = finalRetIsr + concepto.finalRetIsr
      finalRetIvaPorte = finalRetIvaPorte + concepto.finalRetIvaPorte
      
      finalDiscount = finalDiscount + concepto.finalDiscount
      finalSubtotal = finalSubtotal + concepto.subtotal
      
    }
    
    let finalTotal = finalSubtotal - finalDiscount + finalIva + finalIsh + finalIeps - finalRetIva - finalRetIsr - finalRetIvaPorte
    
    totalAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: "\(ViewControllersConstants.FacturaCreateSecondPartViewController.totalAdditionLabelText)\(finalTotal)",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    totalAdditionLabel.attributedText = stringWithFormat
    totalAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    mainScrollView.addSubview(totalAdditionLabel)
    
    self.arrayOfViews.append(totalAdditionLabel)
    
  }
  
  
  
  //
  //
  //
  ///////////////////////////
  
  private func initCommentsCustomTextField() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: totalAdditionLabel.frame.origin.y + totalAdditionLabel.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    commentsCustomTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                title: ViewControllersConstants.FacturaCreateSecondPartViewController.commentsTextFieldText,
                                                                image: nil,
                                                                colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                                positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    commentsCustomTextField.mainTextField.textColor = UIColor.black
    commentsCustomTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.commentsTextFieldText
    commentsCustomTextField.backgroundColor = UIColor.white
    commentsCustomTextField.delegate = self
    self.mainScrollView.addSubview(commentsCustomTextField)
    
    self.arrayOfViews.append(commentsCustomTextField)
    
  }
  
  private func initsendViaMailLabelSwitch() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: commentsCustomTextField.frame.origin.y + commentsCustomTextField.frame.size.height + 1.0,
                                   width: mainScrollView.frame.size.width,
                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
    
    sendViaMailLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false)
    sendViaMailLabelSwitch.isUserInteractionEnabled = true
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = .left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.sendViaMailLabelSwitchText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    sendViaMailLabelSwitch.attributedText = stringWithFormat
    self.mainScrollView.addSubview(sendViaMailLabelSwitch)
    
    self.arrayOfViews.append(sendViaMailLabelSwitch)
    
  }
  
//  private func initpayedLabelSwitch() {
//    
//    let frameForView = CGRect.init(x: 0.0,
//                                   y: sendViaMailLabelSwitch.frame.origin.y + sendViaMailLabelSwitch.frame.size.height,
//                                   width: mainScrollView.frame.size.width,
//                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
//
//    payedLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false)
//    payedLabelSwitch.isUserInteractionEnabled = true
//    
//    let font = UIFont(name: "SFUIText-Light",
//                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
//    let color = UtilityManager.sharedInstance.labelsAndLinesColor
//    let style = NSMutableParagraphStyle()
//    style.alignment = .left
//    
//    let stringWithFormat = NSMutableAttributedString(
//      string: ViewControllersConstants.FacturaCreateSecondPartViewController.payedLabelSwitchText,
//      attributes:[NSFontAttributeName: font!,
//                  NSParagraphStyleAttributeName: style,
//                  NSForegroundColorAttributeName: color
//      ]
//    )
//    
//    payedLabelSwitch.attributedText = stringWithFormat
//    self.mainScrollView.addSubview(payedLabelSwitch)
//    
//    self.arrayOfViews.append(payedLabelSwitch)
//    
//  }
  
  private func createBackViewButtons() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: sendViaMailLabelSwitch.frame.origin.y + sendViaMailLabelSwitch.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 200.0 * UtilityManager.sharedInstance.conversionHeight)

    
    backViewButtons = UIView.init(frame: frameForView)
    backViewButtons.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    self.mainScrollView.addSubview(backViewButtons)
    
    self.arrayOfViews.append(backViewButtons)
    
//    self.initPrevisualizationButton()
    self.initSaveButton()
    self.initCreateButton()
    
  }
  
  //Buttons
//  private func initPrevisualizationButton() {
//    
//    let font = UIFont.init(name: "SFUIText-Light",
//                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
//    let color = UIColor.white
//    let style = NSMutableParagraphStyle()
//    style.alignment = NSTextAlignment.left
//    
//    let stringWithFormat = NSMutableAttributedString(
//      string: "",
//      attributes:[NSFontAttributeName: font!,
//                  NSParagraphStyleAttributeName: style,
//                  NSForegroundColorAttributeName: color
//      ]
//    )
//    
//    let frameForButton = CGRect.init(x: (19.0 * UtilityManager.sharedInstance.conversionWidth),
//                                     y: (11.0 * UtilityManager.sharedInstance.conversionHeight),
//                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
//                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
//    previsualizationButton = UIButton.init(frame: frameForButton)
//    //    downloadButton.addTarget(self,
//    //                             action: nil,
//    //                             for: .touchUpInside)
//    previsualizationButton.backgroundColor = UtilityManager.sharedInstance.labelsAndLinesColor
//    previsualizationButton.setAttributedTitle(stringWithFormat, for: .normal)
//    
//    previsualizationButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.GenericCFDIDetail.downloadPDFButtonText))
//    
//    
//    self.backViewButtons.addSubview(previsualizationButton)
//    
//  }
  
  private func initSaveButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: (19.0 * UtilityManager.sharedInstance.conversionWidth),
                                     y: (11.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    saveButton = UIButton.init(frame: frameForButton)
    saveButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
    //    cancelButton.addTarget(self,
    //                           action: nil,
    //                           for: .touchUpInside)
    saveButton.backgroundColor = UIColor.gray
    //    saveButton.setBackgroundColor(color: UIColor.lightGray, forState: .highlighted)
    saveButton.setAttributedTitle(stringWithFormat, for: .normal)
    saveButton.contentHorizontalAlignment = .left
    
    saveButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.GenericCFDIDetail.cancelButtonText))
    
    self.backViewButtons.addSubview(saveButton)
    
  }
  
  private func initCreateButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: (19.0 * UtilityManager.sharedInstance.conversionWidth),
                                     y: saveButton.frame.origin.y + saveButton.frame.size.height + (11.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    createButton = UIButton.init(frame: frameForButton)
    createButton.addTarget(self,
                           action: #selector(createButtonPressed),
                           for: .touchUpInside)
    createButton.backgroundColor = UIColor.orange
    createButton.setBackgroundColor(color: UIColor.init(red: 255.0/255.0, green: 164.0/255.0, blue: 78.0/255.0, alpha: 1.0), forState: .highlighted)
    createButton.setAttributedTitle(stringWithFormat, for: .normal)
    createButton.contentHorizontalAlignment = .left
    
    createButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.GenericCFDIDetail.generateCFDI))
    
    self.backViewButtons.addSubview(createButton)
    
  }
  
  private func createGenericLabelForButton(text: String) -> UILabel {
    
    let frameForLabel = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 200.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    let genericLabel = UILabel.init(frame: frameForLabel)
    genericLabel.numberOfLines = 0
    genericLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: text,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    genericLabel.attributedText = stringWithFormat
    genericLabel.sizeToFit()
    let newFrame = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                               y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                               width: genericLabel.frame.size.width,
                               height: genericLabel.frame.size.height)
    genericLabel.frame = newFrame
    
    return genericLabel
    
  }
  
  @objc private func cancelButtonPressed() {
    
    let alertController = UIAlertController(title: "AVISO",
                                            message: "Al cancelar toda la información creada se perderá, ¿deseas continuar?",
                                            preferredStyle: UIAlertControllerStyle.alert)
    
    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
      
      _ = self.navigationController?.popToRootViewController(animated: true)
      
    }
    
    let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) { (result: UIAlertAction) in }
    
    alertController.addAction(okAction)
    alertController.addAction(cancelAction)
    
    let actualController = UtilityManager.sharedInstance.currentViewController()
    actualController.present(alertController, animated: true, completion: nil)
    
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    
    super.viewWillDisappear(animated)
    
    self.isSecondTimeShowing = true
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    
    if isSecondTimeShowing == true {
      
      for view in arrayOfConceptoButtons {
        
        view.removeFromSuperview()
        
      }
      
      for view in arrayOfViews {
        
        view.removeFromSuperview()
        
      }
      
      arrayOfConceptoButtons.removeAll()
      arrayOfViews.removeAll()
      
      self.initInterface()
      
    }
    
  }
  
  @objc private func backButtonPressed() {
    
    _ = self.navigationController?.popViewController(animated: true)
    
  }
  
  @objc private func buttonForCreateConceptoPressed() {
    
    let createConceptoScreen = CreateConceptoForCartaViewController.init(newArrayOfUnity: self.arrayOfUnits)
    createConceptoScreen.delegate = self
    self.navigationController?.pushViewController(createConceptoScreen, animated: true)
    
  }
  
  @objc private func createButtonPressed() {
    
    let params = self.createParams()
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.requestToCreateNewPorte(params: params, actionsToMakeWhenSucceeded: {
      
      print()
      
      UtilityManager.sharedInstance.hideLoader()
      
      
      let alertController = UIAlertController(title: "¡Éxito!",
                                              message: "La carta porte fue creada satisfactoriamente.",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        _ = self.navigationController?.popToRootViewController(animated: true)
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }, actionsToMakeWhenFailed: {
      
      UtilityManager.sharedInstance.hideLoader()
      
    })
    
  }
  
  private func createParams() -> [String: AnyObject] {
    
    var items: Array<[String: AnyObject]> = Array<[String: AnyObject]>()
    
    for concepto in arrayOfConceptos {
      
      var conceptParams: [String: AnyObject] = [
        
        "cantidad": concepto.quantity as AnyObject,
        "unidad": concepto.unit.name as AnyObject,
        "concepto": concepto.conceptDescription as AnyObject,
        "descuento": concepto.discountPercentage as AnyObject,
        "precio": concepto.price as AnyObject,
        "subtotal": concepto.subtotal as AnyObject,
        
        ]
      
      if concepto.applyIva == true {
        
        conceptParams["iva"] = concepto.ivaValueToSendToServer as AnyObject
        
      }
      
      if concepto.applyRetIva == true {
        
        conceptParams["iva_ret"] = concepto.ivaValueToSendToServer as AnyObject
        
      }
      
      if concepto.applyRetIsr == true {
        
        conceptParams["isr"] = concepto.retIsrValueToSendToServer as AnyObject
        
      }
      
      if concepto.applyIsh == true {
        
        conceptParams["ish"] = concepto.ishValueToSendToServer as AnyObject
        
      }
      
      if concepto.applyIeps == true {
        
        conceptParams["ieps"] = concepto.iepsValueToSendToServer as AnyObject
        
      }
      
      if concepto.applyRetIvaPorte == true {
        
        conceptParams["porte"] = concepto.iepsValueToSendToServer as AnyObject
        
      }
      
      items.append(conceptParams)
      
    }
    
    var params: [String: AnyObject] = [
      
      "rfc": cartaDataToCreate.client.rfc as AnyObject,
      "formapago": cartaDataToCreate.paymentWay.name as AnyObject,
      "metodopago": cartaDataToCreate.paymentMethod.id as AnyObject,
      "currencie": cartaDataToCreate.typeOfMoney.name as AnyObject,
      "seriefactura": cartaDataToCreate.serie.name as AnyObject,
      "send_email": sendViaMailLabelSwitch.getValueOfSwitch() as AnyObject,
      "invoice_comments": self.commentsCustomTextField.mainTextField.text! as AnyObject,
      "items": items as AnyObject
      
    ]
    
    if cartaDataToCreate.creditCardNumber != nil {
      
      params["numerocuenta"] = cartaDataToCreate.creditCardNumber as AnyObject
      
    }
    
    if cartaDataToCreate.numOrder != nil {
      
      params["num_order"] = cartaDataToCreate.numOrder as AnyObject
      
    }
    
    if cartaDataToCreate.exchangeRate != nil {
      
      params["exchange_rate"] = cartaDataToCreate.exchangeRate as AnyObject
      
    }
    return params
    
  }
  
  //MARK: - ButtonToActivePickerViewDelegate
  func lastButtonToShowPickerView(sender: ButtonToActivePickerView) {}
  
  func selectActionFinalized(sender: ButtonToActivePickerView) {}
  
  //MARK: - CustomTextFieldWithTitleViewDelegate
  
  func customTextFieldSelected(sender: CustomTextFieldWithTitleView) {}
  
  //MARK: - CreateConceptoViewControllerDelegate
  
  func conceptoDidCreate(newConcepto: Concepto) {
    
    if isSecondTimeShowing == true {
      
      for view in arrayOfConceptoButtons {
        
        view.removeFromSuperview()
        
      }
      
      for view in arrayOfViews {
        
        view.removeFromSuperview()
        
      }
      
      arrayOfConceptoButtons.removeAll()
      arrayOfViews.removeAll()
      
      arrayOfConceptos.append(newConcepto)
      
      let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                       height: self.view.frame.size.height + (100.0 * UtilityManager.sharedInstance.conversionHeight) + (CGFloat(arrayOfConceptos.count + 1) * 44.0 * UtilityManager.sharedInstance.conversionHeight))
      
      mainScrollView.contentSize = newContentSize
      
      self.initInterface()
      
    }
    
  }
  
  //MARK: - ConceptoLikeViewCell
  
  func conceptoCellPressed(cell: ConceptoLikeViewCell) {
    
    
    
  }
  
  func deleteConcepto(cell: ConceptoLikeViewCell) {
    
    let alertController = UIAlertController(title: "Aviso",
                                            message: "¿Estás seguro de eliminar este concepto?",
                                            preferredStyle: UIAlertControllerStyle.alert)
    
    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (result: UIAlertAction) -> Void in }
    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result: UIAlertAction) -> Void in
      
      UtilityManager.sharedInstance.showLoader()
      
      self.arrayOfConceptos.remove(at: cell.tag)
      
      for view in self.arrayOfConceptoButtons {
        
        view.removeFromSuperview()
        
      }
      
      for view in self.arrayOfViews {
        
        view.removeFromSuperview()
        
      }
      
      self.arrayOfConceptoButtons.removeAll()
      self.arrayOfViews.removeAll()
      
      self.initInterface()
      
      UtilityManager.sharedInstance.hideLoader()
      
    }
    
    alertController.addAction(cancelAction)
    alertController.addAction(okAction)
    
    let actualController = UtilityManager.sharedInstance.currentViewController()
    actualController.present(alertController, animated: true, completion: nil)
    
  }
  
  @objc private func hideKeyboard() {
    
    self.view.endEditing(true)
    
  }
  
}
