//
//  Carta.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 13/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class Carta: CFDI {
  
  var uuid: String! = nil
  var iva: String! = nil
  var numOrder: String! = nil
  var descuento: String! = nil
  var subtotal: String! = nil
  var referenceClient: String! = nil
  var total: String! = nil
  var uid: String! = nil
  
  //Needed values to create a Factura
  
  var expeditionPlace: ExpeditionPlace! = nil
  var client: Client! = nil
  var clientOrRFC: Bool! = nil //true is RFC
  var paymentWay: WayToPay! = nil
  var paymentMethod: PaymentMethod! = nil
  var typeOfMoney: Currency! = nil
  var serie: Serie! = nil
  var exchangeRate: String! = nil
  var creditCardNumber: String! = nil
  
  init(empty: Bool) {
    
    super.init(newFolio: "",
               newStatus: "",
               newFechaTimbrado: "",
               newReceptor: "")
    
  }
  
  init(newUuid: String!, newStatus: String, newFolio: String!, newIva: String!, newNumOrder: String!, newFechaTimbrado: String!, newDescuento: String!,newSubtotal: String!, newReferenceClient: String!, newReceptor: String!, newTotal: String!, newUid: String!) {
    
    super.init(newFolio: newFolio,
               newStatus: newStatus,
               newFechaTimbrado: newFechaTimbrado,
               newReceptor: newReceptor)
    
    uuid = newUuid
    iva = newIva
    numOrder = newNumOrder
    descuento = newDescuento
    subtotal = newSubtotal
    referenceClient = newReferenceClient
    total = newTotal
    uid = newUid
    
  }
  
}
