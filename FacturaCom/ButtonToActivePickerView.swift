//
//  ButtonToActivePickerView.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 10/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

protocol ButtonToActivePickerViewDelegate {
  
  func lastButtonToShowPickerView(sender: ButtonToActivePickerView)
  func selectActionFinalized(sender: ButtonToActivePickerView)
  
}

protocol ButtonToActivePickerViewCreateNewExpeditionPlaceDelegate {
  
  func createNewExpeditionPlace(sender: ButtonToActivePickerView)
  
}

class ButtonToActivePickerView: UIView, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
  
  enum SpecialOption {
    static let createNewExpeditionPlace = "-666"
  }
  
  private var titleText: String! = ""
  private var titleLabel: UILabel! = nil
  private var titleForContainerView: UILabel! = nil
  private var arrayOfOptions: [Option]! = [Option]()
  private var mainPickerView: UIPickerView! = nil
  private var containerViewForPicker: UIView! = nil
  private var isShowingPickerView: Bool = false
  private var indexSelected: Int = -1
  
  private var mainTextFieldForSearchClients: UITextField! = nil
  private var showTextFieldForSearchClients: Bool = false
  
  var delegate: ButtonToActivePickerViewDelegate?
  var delegateForCreateNewExpeditionPlace: ButtonToActivePickerViewCreateNewExpeditionPlaceDelegate?
  
  private let frameForShowingWhenIsFilteringThePickerView: CGRect =  CGRect.init(x: 0.0,
                                                                                 y: UIScreen.main.bounds.height - (530.0 * UtilityManager.sharedInstance.conversionHeight),
                                                                             width: UIScreen.main.bounds.width,
                                                                            height: 350.0 * UtilityManager.sharedInstance.conversionHeight)
  
  private let frameForShowing: CGRect = CGRect.init(x: 0.0,
                                                    y: UIScreen.main.bounds.height - (250.0 * UtilityManager.sharedInstance.conversionHeight),
                                                width: UIScreen.main.bounds.width,
                                               height: 350.0 * UtilityManager.sharedInstance.conversionHeight)
  
  private let frameForHiding: CGRect = CGRect.init(x: 0.0,
                                                   y: UIScreen.main.bounds.height,
                                               width: UIScreen.main.bounds.width,
                                              height: 350.0 * UtilityManager.sharedInstance.conversionHeight)
  
  
  required init?(coder aDecoder: NSCoder) {
   
    fatalError("init(coder:) has not been implemented")
    
  }
  
  init(frame: CGRect, newTitle: String, newArrayOfOptions: [Option]) {
    
    titleText = newTitle
    arrayOfOptions = newArrayOfOptions
    super.init(frame: frame)
    
    self.initActions()
    self.initInterface()
    
  }
  
  init(frame: CGRect, newTitle: String, newArrayOfOptions: [Option], withSearchTextField: Bool) {
    
    showTextFieldForSearchClients = withSearchTextField
    titleText = newTitle
    arrayOfOptions = newArrayOfOptions
    super.init(frame: frame)
    
    self.initActions()
    self.initInterface()
    
  }
  
  private func initActions() {
  
    self.isUserInteractionEnabled = true
    let action = UITapGestureRecognizer.init(target: self, action: #selector(myselfPressed))
    action.numberOfTapsRequired = 1
    
    self.addGestureRecognizer(action)
    
//    self.addTarget(self, action: #selector(myselfPressed), for: .touchUpInside)
    
  }
  
  private func initInterface() {
    
    self.changeMyTitle(titleText)
    self.initContainerViewForPicker()
    self.initMainPickerView()
    
  }
  
  func changeMyTitle(_ to: String) {
    
    self.backgroundColor = UIColor.white
    titleText = to
    
    if titleLabel == nil {
      
      let frameForLabel = CGRect.init(x: 16.0 * UtilityManager.sharedInstance.conversionWidth,
                                      y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                      width: self.frame.size.width,
                                      height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
      
      titleLabel = UILabel.init(frame: frameForLabel)
      
    }
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: titleText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    titleLabel.attributedText = stringWithFormat
    titleLabel.sizeToFit()
    let newFrame = CGRect.init(x: titleLabel.frame.origin.x,
                               y: titleLabel.frame.origin.y,
                               width: titleLabel.frame.size.width,
                               height: titleLabel.frame.size.height)
    
    titleLabel.frame = newFrame
    
    self.addSubview(titleLabel)
    
  }
  
  private func initContainerViewForPicker() {
  
    containerViewForPicker = UIView.init(frame: frameForHiding)
    containerViewForPicker.backgroundColor = UIColor.clear
    
    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
    let blurEffectView = UIVisualEffectView(effect: blurEffect)
    //always fill the view
    blurEffectView.frame = CGRect.init(x: 0.0,
                                       y: 0.0,
                                   width: containerViewForPicker.frame.size.width,
                                  height: containerViewForPicker.frame.size.height)
    containerViewForPicker.addSubview(blurEffectView)
    
    
    self.initButtonsForContainer()

    let frontViewController = UtilityManager.sharedInstance.currentViewController()
    frontViewController.view.addSubview(containerViewForPicker)
    
  }
  
  private func initButtonsForContainer() {
    
    let positionForFirstButton = CGPoint.init(x: 10.0 * UtilityManager.sharedInstance.conversionWidth,
                                              y: 12.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let positionForSecondButton = CGPoint.init(x: containerViewForPicker.frame.size.width - ( 55.0 * UtilityManager.sharedInstance.conversionWidth),
                                               y: 12.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let cancelButton = createGenericButton(stringToShowInButton: ViewControllersConstants.FacturaCreateViewController.pickerViewCancelButtonText, position: positionForFirstButton)
    let selectButton = createGenericButton(stringToShowInButton: ViewControllersConstants.FacturaCreateViewController.pickerViewSelectButtonText, position: positionForSecondButton)
    
    cancelButton.tag = 1
    selectButton.tag = 2
    
    self.containerViewForPicker.addSubview(cancelButton)
    self.initTitleForContainerView()
    if showTextFieldForSearchClients == true {
      
      self.initMainTextFieldForSearchClients()
      
    }
    self.containerViewForPicker.addSubview(selectButton)
    
  }
  
  private func initTitleForContainerView() {
  
    let frameForLabel = CGRect.init(x: 90.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 18.0 * UtilityManager.sharedInstance.conversionHeight,
                                width: 180.0 * UtilityManager.sharedInstance.conversionWidth,
                               height: 23.0 * UtilityManager.sharedInstance.conversionHeight)
    
    titleForContainerView = UILabel.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: titleText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: UtilityManager.sharedInstance.labelsAndLinesColor
      ]
    )
    
    titleForContainerView.attributedText = stringWithFormat
    titleForContainerView.sizeToFit()
    let newFrame = CGRect.init(x: (self.containerViewForPicker.frame.size.width / 2.0) - (titleForContainerView.frame.size.width / 2.0),
                               y: titleForContainerView.frame.origin.y,
                               width: titleForContainerView.frame.size.width,
                               height: titleForContainerView.frame.size.height)
    
    titleForContainerView.frame = newFrame
    
    self.containerViewForPicker.addSubview(titleForContainerView)
  
  }
  
  private func createGenericButton(stringToShowInButton: String, position: CGPoint) -> UIButton {
    
    let genericButton = UIButton.init(frame: CGRect.zero)
    
    let font = UIFont(name: "SFUIDisplay-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.blue
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: stringToShowInButton,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    genericButton.setAttributedTitle(stringWithFormat, for: .normal)
    genericButton.backgroundColor = UIColor.clear
    genericButton.addTarget(self,
                            action: #selector(genericButtonPressed),
                            for: .touchUpInside)
    genericButton.sizeToFit()
    
    let frameForButton = CGRect.init(x: position.x,
                                     y: position.y,
                                     width: genericButton.frame.size.width,
                                     height: genericButton.frame.size.height)
    
    genericButton.frame = frameForButton
    genericButton.layer.cornerRadius = 3.0
    
    return genericButton
    
  }
  
  private func initMainPickerView() {
    
    var yPosition: CGFloat = 50.0
    
    if showTextFieldForSearchClients == true {
      
      yPosition = 60.0
      
    }
    
    let frameForPicker = CGRect.init(x: 0.0,
                                     y: (yPosition * UtilityManager.sharedInstance.conversionHeight),
                                     width: UIScreen.main.bounds.width,
                                     height: 250.0 * UtilityManager.sharedInstance.conversionHeight)
    
    mainPickerView = UIPickerView.init(frame: frameForPicker)
    mainPickerView.delegate = self
    mainPickerView.dataSource = self
    containerViewForPicker.addSubview(mainPickerView)
    
  }
  
  private func initMainTextFieldForSearchClients() {
    
    let frameForTextField = CGRect.init(x: (self.containerViewForPicker.frame.size.width / 2.0) - (150.0 * UtilityManager.sharedInstance.conversionWidth),
                                        y: self.titleForContainerView.frame.origin.y + self.titleForContainerView.frame.size.height + (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                    width: 300.0 * UtilityManager.sharedInstance.conversionWidth,
                                   height: 30.0 * UtilityManager.sharedInstance.conversionHeight)
    
    mainTextFieldForSearchClients = UITextField.init(frame: frameForTextField)
    mainTextFieldForSearchClients.layer.borderWidth = 0.5 * UtilityManager.sharedInstance.conversionWidth
    mainTextFieldForSearchClients.layer.cornerRadius = 5.0 * UtilityManager.sharedInstance.conversionWidth
    mainTextFieldForSearchClients.layer.borderColor = UIColor.lightGray.cgColor
    mainTextFieldForSearchClients.backgroundColor = UIColor.white
    mainTextFieldForSearchClients.addTarget(self, action: #selector(textSearchingForChanged), for: .allEditingEvents)
    self.containerViewForPicker.addSubview(mainTextFieldForSearchClients)
    
  }
  
  @objc private func genericButtonPressed(sender: AnyObject) {
    
    if ((sender as? UIButton) != nil) {
      
      if sender.tag == 1 { //Cancel
        
        UtilityManager.sharedInstance.currentViewController().view.endEditing(true)
        self.hidePickerView()
        
      } else
      
        if sender.tag == 2 {
          
          UtilityManager.sharedInstance.currentViewController().view.endEditing(true)
          
          if arrayOfOptions.count > 0 {
            
            if indexSelected == -1 && arrayOfOptions.count > 0 {
              
              indexSelected = 0
              
            }
            
            self.changeMyTitle(arrayOfOptions[mainPickerView.selectedRow(inComponent: 0)].name)
            self.hidePickerView()
            self.delegate?.selectActionFinalized(sender: self)
            
            if self.arrayOfOptions[mainPickerView.selectedRow(inComponent: 0)].id == SpecialOption.createNewExpeditionPlace {
              
              self.delegateForCreateNewExpeditionPlace?.createNewExpeditionPlace(sender: self)
            
            }
            
          }
          
        }
      
    }
    
  }
  
  @objc private func myselfPressed() {
    
    if isShowingPickerView == false {
      
      self.delegate?.lastButtonToShowPickerView(sender: self)
      self.showPickerView()
      isShowingPickerView = true
      
    } else {
      
      self.hidePickerView()
      isShowingPickerView = false
      
    }
    
  }
  
  private func showPickerView() {
    
    UIView.animate(withDuration: 0.35, animations: { 
      
      self.containerViewForPicker.frame = self.frameForShowing
      
    }) { (isFinished) in
      
      if isFinished == true {
        
        
        
      }
      
    }
    
  }
  
  func hidePickerView() {
    
    if isShowingPickerView == true {
      
      UIView.animate(withDuration: 0.35, animations: {
        
        self.containerViewForPicker.frame = self.frameForHiding
        
      }) { (isFinished) in
        
        if isFinished == true {
          
          
          
        }
        
      }
      
    }
    
  }
  
  func getValueSelected() -> Option? {
    
    if indexSelected == -1 {
      
      return nil
      
    } else {
      
      return arrayOfOptions[mainPickerView.selectedRow(inComponent: 0)]
      
    }    
    
  }
  
  func lookForElementName(nameToLookFor: String) -> String? {
    
    for option in arrayOfOptions {
      
      if option.name == nameToLookFor {
        
        return option.id
        
      }
      
    }
    
    return nil
    
  }
  
  func selectFirstOption() {
    
    if arrayOfOptions.count > 0 {
      
      mainPickerView.selectRow(0, inComponent: 0, animated: true)
      indexSelected = 0
      self.changeMyTitle(arrayOfOptions[0].name)
      
    }
    
  }
  
  func selectElementWithName(nameToLookFor: String) -> Bool {
    
    for i in 0..<arrayOfOptions.count {
      
      if arrayOfOptions[i].name == nameToLookFor {
        
        mainPickerView.selectRow(i, inComponent: 0, animated: true)
        indexSelected = i
        self.changeMyTitle(arrayOfOptions[i].name)
        
        return true
        
      }
      
    }
    
    return false
    
  }
  
  func getTextButton() -> String {
    
    return titleText
    
  }
  
  func drawTopBorder() {
    
    let topBorder = CALayer()
    topBorder.backgroundColor = UIColor.lightGray.cgColor
    topBorder.frame = CGRect(x:0,y: 0, width:self.frame.size.width, height:0.5 * UtilityManager.sharedInstance.conversionWidth)
    self.layer.addSublayer(topBorder)
    
  }
  
  func drawBottomBorder() {
    
    let bottomBorder = CALayer()
    bottomBorder.backgroundColor = UIColor.lightGray.cgColor
    bottomBorder.frame = CGRect(x:0, y:self.frame.size.height - (0.5 * UtilityManager.sharedInstance.conversionWidth), width:self.frame.size.width, height:0.5 * UtilityManager.sharedInstance.conversionWidth)
    self.layer.addSublayer(bottomBorder)
    
  }
  
  //MARK: - UITextFieldDelegate 
  
  @objc private func textSearchingForChanged(textField: UITextField) {
    
    if self.containerViewForPicker.frame != frameForShowingWhenIsFilteringThePickerView {
      
      self.bringSubview(toFront: self.containerViewForPicker)
      
      UIView.animate(withDuration: 0.18, animations: {
        
        self.containerViewForPicker.frame = self.frameForShowingWhenIsFilteringThePickerView
        
      })
      
    }
    
    let arrayOfFilteredOptions = arrayOfOptions.filter { element in
      
      return element.name.lowercased().contains(textField.text!.lowercased()) || element.type.lowercased().contains(textField.text!.lowercased())
      
    }
    
    if arrayOfFilteredOptions.first != nil {
      
      for i in 0..<arrayOfOptions.count {
        
        if arrayOfFilteredOptions.first!.id == arrayOfOptions[i].id {
          
          mainPickerView.selectRow(i, inComponent: 0, animated: true)
          return
          
        }
        
      }
      
    }
    
  }
  
  //MARK: - PickerViewDelegate - DataSource
  
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    
    return 1
    
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    
    return arrayOfOptions.count
    
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
    return arrayOfOptions[row].name
    
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
    indexSelected = row
    
    if indexSelected < arrayOfOptions.count {
    
      self.changeMyTitle(arrayOfOptions[row].name)
      
    }
    
  }
  
}
