//
//  MissedPasswordViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 29/03/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class MissedPasswordViewController: UIViewController {
  
  private var logoImageView: UIImageView! = nil
  private var descriptionLabel: UILabel! = nil
  private var emailTextField: CustomTextFieldWithTitleView! = nil
  private var sendMailButton: UIButton! = nil
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.white
    
    var backgroundImageName = "fondo_nuevo"
    
    if UtilityManager.sharedInstance.isIpad() == true {
      
      backgroundImageName = "iPad_Login_img"
      
    }
    
    let imageViewBackground = UIImageView.init(image: UIImage.init(named: backgroundImageName)!)
    imageViewBackground.contentMode = .scaleToFill
    imageViewBackground.frame = self.view.frame
    self.view.addSubview(imageViewBackground)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.editNavigationBar()
    
    self.createLogo()
    self.createDescriptionLabel()
    self.createEmailTextField()
    self.createSendMailButton()
    
  }
  
  private func editNavigationBar() {
    
    self.navigationController?.isNavigationBarHidden = false
    
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    self.navigationController?.navigationBar.shadowImage = UIImage()
    self.navigationController?.navigationBar.isTranslucent = true
    self.navigationController?.navigationBar.barTintColor = UIColor.clear
    
    self.changeLeftButtonNavigationBar()
    
  }
  
  private func changeLeftButtonNavigationBar() {

    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
//    let leftButton = UIBarButtonItem(title: ViewControllersConstants.MissedPasswordViewController.leftButtonText,
//                                     style: .plain,
//                                     target: self,
//                                     action: #selector(leftNavigationButtonPressed))
//    
//    let fontForButtonItem =  UIFont(name: "SFUIText-Regular",
//                                    size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
//    
//    let attributesDict: [String: AnyObject] = [NSFontAttributeName: fontForButtonItem!,
//                                               NSForegroundColorAttributeName: UIColor.init(red: 254.0/255.0, green: 136.0/255.0, blue: 30.0/255.0, alpha: 1.0)
//    ]
//    
//    leftButton.setTitleTextAttributes(attributesDict, for: .normal)
//    
//    self.navigationItem.leftBarButtonItem = leftButton
    
  }
  
  private func createLogo() {
    
    logoImageView = UIImageView.init(image: UIImage.init(named: "logo"))
    let goldenPitchStarFrame = CGRect.init(x: (self.view.frame.size.width / 2.0) - (80.0 * UtilityManager.sharedInstance.conversionWidth),
                                           y: (20.0 * UtilityManager.sharedInstance.conversionHeight),
                                           width: 160.0 * UtilityManager.sharedInstance.conversionWidth,
                                           height: 104.35 * UtilityManager.sharedInstance.conversionHeight)
    logoImageView.frame = goldenPitchStarFrame
    
    self.view.addSubview(logoImageView)
    
  }
  
  private func createDescriptionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: UIScreen.main.bounds.size.width - (40.0 * UtilityManager.sharedInstance.conversionWidth),
                                    height: CGFloat.greatestFiniteMagnitude)
    
    descriptionLabel = UILabel.init(frame: frameForLabel)
    descriptionLabel.numberOfLines = 0
    descriptionLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.MissedPasswordViewController.descriptionLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    descriptionLabel.attributedText = stringWithFormat
    descriptionLabel.sizeToFit()
    let newFrame = CGRect.init(x: (self.view.frame.size.width / 2.0) - (descriptionLabel.frame.size.width / 2.0),
                               y: logoImageView.frame.origin.y + logoImageView.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                           width: descriptionLabel.frame.size.width,
                          height: descriptionLabel.frame.size.height)
    
    descriptionLabel.frame = newFrame
    
    self.view.addSubview(descriptionLabel)
    
  }
  
  private func createEmailTextField() {
    
    var positionInY = descriptionLabel.frame.origin.y + descriptionLabel.frame.size.height + (15.0 * UtilityManager.sharedInstance.conversionHeight)
    
    if UtilityManager.sharedInstance.isIpad() {
      
      positionInY = descriptionLabel.frame.origin.y + descriptionLabel.frame.size.height + (100.00 - UIApplication.shared.statusBarFrame.size.height) * UtilityManager.sharedInstance.conversionHeight
      
    }
    
    let frameOfView = CGRect.init(x: 20.0 * UtilityManager.sharedInstance.conversionWidth,
                                  y: positionInY,
                                  width: UIScreen.main.bounds.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                  height: 47.0 * UtilityManager.sharedInstance.conversionHeight)
    
    emailTextField = CustomTextFieldWithTitleView.init(frame: frameOfView,
                                                       title: ViewControllersConstants.LoginViewController.emailTextFieldDescriptionText,
                                                       image: nil,
                                                       colorOfLabelAndLine: UIColor.white)
    emailTextField.mainTextField.textColor = UIColor.white
    
    self.view.addSubview(emailTextField)
    
  }
  
  private func createSendMailButton() {
    
    sendMailButton = UIButton.init(frame: CGRect.zero)
    
    let font = UIFont.systemFont(ofSize: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.MissedPasswordViewController.sendMailButtonText,
      attributes:[NSFontAttributeName: font,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    sendMailButton.backgroundColor = UIColor.orange
    
    sendMailButton.setBackgroundColor(color: UIColor.init(red: 255.0/255.0, green: 164.0/255.0, blue: 78.0/255.0, alpha: 1.0), forState: .highlighted)
    sendMailButton.layer.cornerRadius = 8.0 * UtilityManager.sharedInstance.conversionWidth
    sendMailButton.setAttributedTitle(stringWithFormat, for: .normal)
    sendMailButton.addTarget(self,
                          action: #selector(sendMailButtonPressed),
                          for: .touchUpInside)
    sendMailButton.sizeToFit()
    
    let frameForButton = CGRect.init(x: self.view.center.x - (50.0 * UtilityManager.sharedInstance
      .conversionWidth),
                                     y: emailTextField.frame.origin.y + emailTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: (100.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 35.0 * UtilityManager.sharedInstance.conversionHeight)
    
    sendMailButton.frame = frameForButton
    
    self.view.addSubview(sendMailButton)
    
  }
  
  @objc private func sendMailButtonPressed() {
    
    let params: [String: AnyObject] = [
                                       "email": emailTextField.mainTextField.text! as AnyObject,
                                       "api_key" : "XjWJpwEPy2ks23Kh" as AnyObject
                                      ]
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.requestForForgottenPassword(params: params, actionsToMakeWhenSucceeded: { 
      
      UtilityManager.sharedInstance.hideLoader()
      
      let alertController = UIAlertController(title: "¡Éxito!",
                                              message: "Se mando un correo a tu email para poder seguir con la petición de reinicio de password",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        _ = self.navigationController?.popViewController(animated: true) //FUCKIN SWIFT!!!
        
      }
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    }, actionsToMakeWhenFailed: {
      
      UtilityManager.sharedInstance.hideLoader()
    
      _ = self.navigationController?.popViewController(animated: true)
    
    })
    
  }
  
  
  override func viewDidLoad() {
    
    
    
  }
  
  @objc private func leftNavigationButtonPressed() {
    
    _ = self.navigationController?.popViewController(animated: true)
    
  }

}
