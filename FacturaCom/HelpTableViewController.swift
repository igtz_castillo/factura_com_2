//
//  HelpTableViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 19/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class HelpTableViewController: UITableViewController {
  
  private var leftButtonItemView: UIBarButtonItem! = nil
  
  private let kNumberOfOptions = 10
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override init(style: UITableViewStyle) {
    
    super.init(style: style)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.initNavigationBar()
    
  }
  
  private func initNavigationBar() {
    
    
    
    let leftButton = UIBarButtonItem(title: "",
                                     style: UIBarButtonItemStyle.plain,
                                     target: self,
                                     action: nil)
    
    self.navigationItem.rightBarButtonItem = leftButton
    
    self.initLeftNavigationView()
    self.initRightNavigationView()
    self.initTableView()
    
  }
  
  private func initLeftNavigationView() {
    
    self.title = ViewControllersConstants.NameOfScreen.helpTableText
    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
  }
  
  private func initRightNavigationView() {
    
    let imageBackButton = UIImage.init(named: "profile")?.withRenderingMode(.alwaysOriginal)
    
    let profileButton = UIBarButtonItem.init(image: imageBackButton,
                                             style: .plain,
                                             target: self,
                                             action: #selector(profileButtonPressed))
    
    self.navigationItem.setRightBarButton(profileButton, animated: false)
    
  }
  
  private func initTableView() {
    
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    
  }
  
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return kNumberOfOptions
    
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    return 44.0 * UtilityManager.sharedInstance.conversionHeight
    
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
    
    cell?.selectionStyle = UITableViewCellSelectionStyle.none
    
    switch indexPath.row {
    case 0:
      cell?.textLabel?.text = ViewControllersConstants.HelpTableViewController.howToCreateClientsText
      cell?.tag = 0
      break
      
    case 1:
      cell?.textLabel?.text = ViewControllersConstants.HelpTableViewController.howToCreateCompaniesText
      cell?.tag = 1
      break
      
    case 2:
      cell?.textLabel?.text = ViewControllersConstants.HelpTableViewController.howToCreateCFDIsText
      cell?.tag = 2
      break
      
    case 3:
      cell?.textLabel?.text = ViewControllersConstants.HelpTableViewController.howToCreateProductsText
      cell?.tag = 3
      break
      
    case 4:
      cell?.textLabel?.text = ViewControllersConstants.HelpTableViewController.howToCreateFasterCFDIsText
      cell?.tag = 4
      break
      
    case 5:
      cell?.textLabel?.text = ViewControllersConstants.HelpTableViewController.howToCreateSeriesText
      cell?.tag = 5
      break
      
    case 6:
      cell?.textLabel?.text = ViewControllersConstants.HelpTableViewController.howToEditClientsText
      cell?.tag = 6
      break
      
    case 7:
      cell?.textLabel?.text = ViewControllersConstants.HelpTableViewController.howToEditCompaniesText
      cell?.tag = 7
      break
      
    case 8:
      cell?.textLabel?.text = ViewControllersConstants.HelpTableViewController.howToEditProductsText
      cell?.tag = 8
      break
      
    case 9:
      cell?.textLabel?.text = ViewControllersConstants.HelpTableViewController.howToEditProfileText
      cell?.tag = 9
      break
      
    default:
      cell?.textLabel?.text = ViewControllersConstants.HelpTableViewController.howToCreateClientsText
      cell?.tag = 0
    }
    
    cell?.accessoryType = .disclosureIndicator
    
    return cell != nil ? cell! : UITableViewCell.init()
    
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    switch indexPath.row {
    case 0:

      let strings: Array<String> = ["Para crear un nuevo cliente en una cuenta existente en factura.com, es necesario seguir los siguientes pasos:",
                     "1. Hacer clic sobre el submenú de clientes en la barra lateral.",
                     "2. Clic en el botón Nuevo Cliente ubicado en la parte superir derecha de la pantalla.",
                     "3. Completar el formulario con la información del cliente.",
                     "4. Clic en el botón Registrar ubicado al final del formulario.",
                     "Al finalizar este proceso el nuevo cliente habrá sido creado y podrás visualizarlo en la lista de clientes."]
      
      let howToCreateClientsScreen = GenericHowToViewController.init(newArrayOfStrings: strings)
      self.navigationController?.pushViewController(howToCreateClientsScreen, animated: true)
      
      break
      
    case 1:

      let strings: Array<String> = ["Bla bla bla bla bla:",
                                    "1. Bla 1.",
                                    "2. Clic en el botón Nuevo Cliente ubicado en la parte superir derecha de la pantalla.",
                                    "3. Completar el formulario con la información del cliente.",
                                    "Al finalizar blabla bla."]
      
      let howToCreateClientsScreen = GenericHowToViewController.init(newArrayOfStrings: strings)
      self.navigationController?.pushViewController(howToCreateClientsScreen, animated: true)
    
      break
      
    case 2:

      let strings: Array<String> = ["Bla bla bla bla bla:",
                                    "Al finalizar blabla bla."]
      
      let howToCreateClientsScreen = GenericHowToViewController.init(newArrayOfStrings: strings)
      self.navigationController?.pushViewController(howToCreateClientsScreen, animated: true)
      
      break
      
    case 3:

      let strings: Array<String> = ["Bla bla bla bla bla"]
      
      let howToCreateClientsScreen = GenericHowToViewController.init(newArrayOfStrings: strings)
      self.navigationController?.pushViewController(howToCreateClientsScreen, animated: true)
      
      break
      
    case 4:

      let strings: Array<String> = ["Bla bla bla bla bla:",
                                    "1. Bla 1.",
                                    "2. Clic en el botón Nuevo Cliente ubicado en la parte superir derecha de la pantalla.",
                                    "Al finalizar blabla bla."]
      
      let howToCreateClientsScreen = GenericHowToViewController.init(newArrayOfStrings: strings)
      self.navigationController?.pushViewController(howToCreateClientsScreen, animated: true)
      
      break
      
    case 5:

      
      
      break
      
    case 6:
 
      
      
      break
      
    case 7:

      
      
      break
      
    case 8:

      
      
      break
      
    case 9:

      
      
      break
      
    default:

      break
      
      
    }
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(true)
    
    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
  }
  
  @objc private func leftButtonItemPressed() {
    
    print("show action sheet")
    
  }
  
  @objc private func profileButtonPressed() {
    
    let profileScreen = ProfileViewController()
    self.navigationController?.pushViewController(profileScreen, animated: true)
    
    print("show profile view controller")
    
  }
  
  
}
