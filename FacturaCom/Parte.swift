//
//  Parte.swift
//  FacturaCom
//
//  Created by Israel on 19/10/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class Parte {
  
  //var conceptoDataFromServer: ConceptoFromServer! = nil
  var productService: ProductService! = nil
  var claveSAT: String! = nil
  var parteSKU: String! = nil
  var unity: Unity! = nil
  var unityCode: String! = nil
  var quantity: Double = 0.0
  var partePrice: Double = 0.0
  var finalImport: Double = 0.0
  
  init(){}
  
  init(newClaveSAT: String, newParteSKU: String, newUnity: Unity, newQuantity: Double, newPartePrice: Double, newProductService: ProductService) {
    
    claveSAT = newClaveSAT
    parteSKU = newParteSKU
    unity = newUnity
    quantity = newQuantity
    partePrice = newPartePrice
    productService = newProductService
    
  }
  
}
