//
//  CreateConceptoForGenericCFDIViewController.swift
//  FacturaCom
//
//  Created by Israel on 09/10/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

protocol CreateConceptoForGenericCFDIViewControllerDelegate {
  
  func conceptoForGenericCFDIDidCreate(newConcepto: Concepto)
  
}

class CreateConceptoForGenericCFDIViewController: UIViewController, ButtonToActivePickerViewDelegate, CustomTextFieldWithTitleViewDelegate, LabelWithSwitchUsingPickerDelegate, ParteLikeViewCellDelegate, CreatePartForGenericCFDIViewControllerDelegate, UITextFieldDelegate, ProductsTableViewControllerDelegate {
  
  private var conceptoToCreate: Concepto! = Concepto.init()
  private var arrayOfUnits: Array<Unity>! = Array<Unity>()
  private var isShowingDiscountCustomTextField: Bool = false
  private var arrayOfParteLikeViewCell: Array<ParteLikeCellView> = Array<ParteLikeCellView>()
  private var arrayOfProductService: Array<ProductService> = Array<ProductService>()
  private var productServiceSelected: ProductService! = nil
  
  private var mainScrollView: UIScrollView! = nil
  
  private var unityButton: ButtonToActivePickerView! = nil
  private var notionCustomTextField: CustomTextFieldWithTitleView! = nil
  private var unityCostCustomTextField: CustomTextFieldWithTitleView! = nil
  private var quantityCustomTextField: CustomTextFieldWithTitleView! = nil
  
  private var percentageSwitch: LabelWithSwitch! = nil
  private var discountCustomTextField: CustomTextFieldWithTitleView! = nil
  
  private var viewForTaxSwitches: UIView! = nil
  private var separatorForTaxes: LabelSeparator! = nil
  private var typeOfApplyingTaxes: LabelWithSwitch! = nil
  private var ivaLabelSwitch: LabelWithSwitch! = nil
  private var ishLabelSwitch: LabelWithSwitch! = nil
  private var iepsLabelSwitch: LabelWithSwitch! = nil
  private var retIVALabelSwitch: LabelWithSwitch! = nil
  private var retISRLabelSwitch: LabelWithSwitch! = nil

  //The next two are going to be inside viewForTaxSwitches
  private var separatorForAduana: LabelSeparator! = nil
  private var aduanaSwitch: LabelWithSwitch! = nil
  
  private var buttonForCreatePart: ParteButton! = nil
  
  private var arrayOfLabelWithSwitch: Array<LabelWithSwitch>! = Array<LabelWithSwitch>()
  
  private var optionsIva: Array<Option>! = nil
  private var optionsIsh: Array<Option>! = nil
  private var optionsIeps: Array<Option>! = nil
  private var optionsRetIva: Array<Option>! = nil
  private var optionsRetIsr: Array<Option>! = nil
  private var optionsRetIvaPorte: Array<Option>! = nil
  
  private var subTotalAdditionLabel: LabelForAdditions! = nil
  private var subTotalDouble: Double = -1.0

  private var createButton: UIButton! = nil
  private var pedimentoTextField: UITextField = UITextField()
  
  private var taxIncludedInPrice: Bool = true
  
  var delegate: CreateConceptoViewControllerDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  init(newArrayOfUnity: Array<Unity>) {
    
    self.arrayOfUnits = newArrayOfUnity
    
    super.init(nibName: nil, bundle: nil)
    
  }
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.white
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
    tapGesture.numberOfTouchesRequired = 1
    
    self.view.addGestureRecognizer(tapGesture)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.initMainScrollView()
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.getAllUserProducts({ (arrayProdServ) in
      
      self.arrayOfProductService = arrayProdServ
      
      ServerManager.sharedInstance.requestToGetAllKindOfTaxes(actionsToMakeWhenSucceeded: { (iepsOptions, ishOptions, ivaOptions, retIsrOptions, retIvaOptions, retIvaPorteOptions) in
        
        print(iepsOptions, ishOptions, ivaOptions, retIsrOptions, retIvaOptions, retIvaPorteOptions)
        
        self.optionsIeps = iepsOptions
        self.optionsIsh = ishOptions
        self.optionsIva = ivaOptions
        self.optionsRetIsr = retIsrOptions
        self.optionsRetIva = retIvaOptions
        self.optionsRetIvaPorte = retIvaPorteOptions
        
        self.createAllElements()
        
        UtilityManager.sharedInstance.hideLoader()
        
      }, actionsToMakeWhenFailed: {
        
        _ = self.navigationController?.popViewController(animated: true)
        
        UtilityManager.sharedInstance.hideLoader()
        
      })
      
    }, actionsToMakeWhenFailed: {
      
      _ = self.navigationController?.popViewController(animated: true)
      
      UtilityManager.sharedInstance.hideLoader()
      
    })
    
  }
  
  private func createAllElements() {
    
    self.initNotionCustomTextField()
    self.initUnityButton()
    self.initUnityCostCustomTextField()
    self.initQuantityCustomTextField()
    self.initPercentageSwitch()
    self.initDiscountCustomTextField()
    
    self.initViewForTaxSwitches()
    self.initSeparatorForTaxes()
    self.initTypeOfApplyingTaxes()
    self.initIvaLabelSwitch()
    self.initIshLabelSwitch()
    self.initIepsLabelSwitch()
    self.initRetIVALabelSwitch()
    self.initRetISRLabelSwitch()
    self.initSeparatorForAduana()
    self.initAduanaSwitch()
    
    self.createElementsForParts()
    
  }
  
  private func createElementsForParts() {
  
    self.initParteButtons()
    self.initButtonForCreatePart()
    
    self.initSecondPartSubTotalAdditionLabel()
    
    self.initCreateButton()
  
  }
  
  private func initMainScrollView() {
    
    mainScrollView = UIScrollView.init(frame: self.view.frame)
    
    let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                     height: mainScrollView.frame.size.height + (400.0 * UtilityManager.sharedInstance.conversionHeight))
    
    mainScrollView.contentSize = newContentSize
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
    tapGesture.numberOfTouchesRequired = 1
    
    self.mainScrollView.addGestureRecognizer(tapGesture)

    self.view.addSubview(mainScrollView)
    
  }
  
  private func initNotionCustomTextField() {
    
    if notionCustomTextField != nil {
      
      self.notionCustomTextField.removeFromSuperview()
      self.notionCustomTextField = nil
      
    }
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: 0.0,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    notionCustomTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                              title: ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText,
                                                              image: nil,
                                                              colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                              positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                              positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    notionCustomTextField.mainTextField.textColor = UIColor.black
    notionCustomTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.notionTextFieldText
    notionCustomTextField.backgroundColor = UIColor.white
    notionCustomTextField.delegate = self
    notionCustomTextField.mainTextField.delegate = self
    notionCustomTextField.mainTextField.tag = 1
    notionCustomTextField.mainTextField.returnKeyType = .next
    self.mainScrollView.addSubview(notionCustomTextField)
    
  }
  
  private func initUnityButton() {
    
    if unityButton != nil {
      
      self.unityButton.removeFromSuperview()
      self.unityButton = nil
      
    }
    
    let frameForButton = CGRect.init(x: 0.0,
                                     y: notionCustomTextField.frame.origin.y + notionCustomTextField.frame.size.height,
                                     width: UIScreen.main.bounds.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    var arrayOfOptions: [Option] = [Option]()
    
    for unit in arrayOfUnits {
      
      let newOption = Option(id: unit.id, name: unit.name, type: "")
      arrayOfOptions.append(newOption)
      
    }
    
    unityButton = ButtonToActivePickerView.init(frame: frameForButton,
                                                newTitle: ViewControllersConstants.FacturaCreateSecondPartViewController.unityButtonText,
                                                newArrayOfOptions: arrayOfOptions)
    unityButton.drawBottomBorder()
    unityButton.delegate = self
    
    self.mainScrollView.addSubview(unityButton)
    
  }
  
  private func initUnityCostCustomTextField() {
    
    if unityCostCustomTextField != nil {
      
      self.unityCostCustomTextField.removeFromSuperview()
      self.unityCostCustomTextField = nil
      
    }
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: unityButton.frame.origin.y + unityButton.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    unityCostCustomTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                 title: ViewControllersConstants.FacturaCreateSecondPartViewController.unityCostTextFieldText,
                                                                 image: nil,
                                                                 colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                 positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                                 positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    unityCostCustomTextField.mainTextField.textColor = UIColor.black
    unityCostCustomTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.unityCostTextFieldText
    unityCostCustomTextField.mainTextField.keyboardType = .decimalPad
    unityCostCustomTextField.backgroundColor = UIColor.white
    unityCostCustomTextField.delegate = self
    unityCostCustomTextField.mainTextField.delegate = self
    unityCostCustomTextField.mainTextField.tag = 2
    self.mainScrollView.addSubview(unityCostCustomTextField)
    
  }
  
  private func initQuantityCustomTextField() {
    
    if quantityCustomTextField != nil {
      
      self.quantityCustomTextField.removeFromSuperview()
      self.quantityCustomTextField = nil
      
    }
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: unityCostCustomTextField.frame.origin.y + unityCostCustomTextField.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    quantityCustomTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                title: ViewControllersConstants.FacturaCreateSecondPartViewController.quantityTextFieldText,
                                                                image: nil,
                                                                colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                                positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    quantityCustomTextField.mainTextField.textColor = UIColor.black
    quantityCustomTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.quantityTextFieldText
    quantityCustomTextField.backgroundColor = UIColor.white
    quantityCustomTextField.mainTextField.keyboardType = .decimalPad
    quantityCustomTextField.delegate = self
    quantityCustomTextField.mainTextField.delegate = self
    quantityCustomTextField.mainTextField.tag = 3
    self.mainScrollView.addSubview(quantityCustomTextField)
    
  }
  
  private func initPercentageSwitch() {
    
    if percentageSwitch != nil {
      
      self.percentageSwitch.removeFromSuperview()
      self.percentageSwitch = nil
      
    }
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: quantityCustomTextField.frame.origin.y + quantityCustomTextField.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let optionOne = Option(id: "1", name: "\(ViewControllersConstants.FacturaCreateConceptoForGenericCFDI.typeOfDisccountSeparatorText): Porcentaje", type: "1")
    let optionTwo = Option(id: "2", name: "\(ViewControllersConstants.FacturaCreateConceptoForGenericCFDI.typeOfDisccountSeparatorText): Cantidad", type: "2")
    
    
    percentageSwitch = LabelWithSwitch.init(frame: frameForView,
                          newInitialValueOfSwitch: false,
                                    optionsToShow: [optionOne, optionTwo],
                                   titleForPicker: ViewControllersConstants.FacturaCreateConceptoForGenericCFDI.typeOfDisccountSeparatorText,
                              viewToAddPickerView: UtilityManager.sharedInstance.currentViewController().view,
                              constantStringWhenChangeValue: "")
    
    percentageSwitch.isUserInteractionEnabled = true
    percentageSwitch.delegateForPicker = self
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = .left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateConceptoForGenericCFDI.typeOfDisccountSeparatorText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    percentageSwitch.attributedText = stringWithFormat
    arrayOfLabelWithSwitch.append(percentageSwitch)
    self.mainScrollView.addSubview(percentageSwitch)
    
  }
  
  private func initDiscountCustomTextField() {
    
    if discountCustomTextField != nil {
      
      self.discountCustomTextField.removeFromSuperview()
      self.discountCustomTextField = nil
      
    }
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: percentageSwitch.frame.origin.y + percentageSwitch.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 89.0 * UtilityManager.sharedInstance.conversionHeight)
    
    discountCustomTextField = CustomTextFieldWithTitleView.init(frame: frameForView,
                                                                title: ViewControllersConstants.FacturaCreateSecondPartViewController.discountTextFieldText,
                                                                image: nil,
                                                                colorOfLabelAndLine: UtilityManager.sharedInstance.labelsAndLinesColor,
                                                                positionInYInsideView: 15.0 * UtilityManager.sharedInstance.conversionHeight,
                                                                positionInXInsideView: 19.0 * UtilityManager.sharedInstance.conversionWidth)
    discountCustomTextField.mainTextField.textColor = UIColor.black
    discountCustomTextField.mainTextField.placeholder = ViewControllersConstants.FacturaCreateSecondPartViewController.discountTextFieldText
    discountCustomTextField.mainTextField.keyboardType = .decimalPad
    discountCustomTextField.backgroundColor = UIColor.white
    discountCustomTextField.delegate = self
    discountCustomTextField.mainTextField.delegate = self
    discountCustomTextField.mainTextField.tag = 4
    discountCustomTextField.alpha = 0.0
    self.mainScrollView.addSubview(discountCustomTextField)
    
  }
  
  private func initViewForTaxSwitches() {
    
    if viewForTaxSwitches != nil {
      
      self.viewForTaxSwitches.removeFromSuperview()
      self.viewForTaxSwitches = nil
      
    }
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: percentageSwitch.frame.origin.y + percentageSwitch.frame.size.height,
                               width: self.mainScrollView.frame.size.width,
                              height: 658.0 * UtilityManager.sharedInstance.conversionHeight)
    
    viewForTaxSwitches = UIView.init(frame: frameForView)
    viewForTaxSwitches.backgroundColor = UIColor.white
    self.mainScrollView.addSubview(viewForTaxSwitches)
    
  }
  
  private func initSeparatorForTaxes() {
    
    if separatorForTaxes != nil {
      
      self.separatorForTaxes.removeFromSuperview()
      self.separatorForTaxes = nil
      
    }
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: mainScrollView.frame.size.width,
                                    height: 56.0 * UtilityManager.sharedInstance.conversionHeight)
    
    separatorForTaxes = LabelSeparator.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.titleForTaxesArea,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    separatorForTaxes.attributedText = stringWithFormat
    separatorForTaxes.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    viewForTaxSwitches.addSubview(separatorForTaxes)
    
  }
  
  private func initTypeOfApplyingTaxes() {
    
    if typeOfApplyingTaxes != nil {
      
      self.typeOfApplyingTaxes.removeFromSuperview()
      self.typeOfApplyingTaxes = nil
      
    }
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: separatorForTaxes.frame.origin.y + separatorForTaxes.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
    
    let optionOne = Option(id: "1", name: "Impuestos incluidos en el precio", type: "1")
    let optionTwo = Option(id: "2", name: "Impuestos no incluidos en el precio", type: "2")
    
    typeOfApplyingTaxes = LabelWithSwitch.init(frame: frameForView,
                             newInitialValueOfSwitch: false,
                                       optionsToShow: [optionOne, optionTwo],
                                      titleForPicker: "¿Impuestos incluidos?",
                                 viewToAddPickerView: UtilityManager.sharedInstance.currentViewController().view,
                       constantStringWhenChangeValue: "")
    
    typeOfApplyingTaxes.isUserInteractionEnabled = true
    typeOfApplyingTaxes.delegateForPicker = self
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = .left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.typeOfApplyingTaxes,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    typeOfApplyingTaxes.attributedText = stringWithFormat
    arrayOfLabelWithSwitch.append(typeOfApplyingTaxes)
    _ = typeOfApplyingTaxes.selectElementWithName(nameToLookFor: "Impuestos incluidos en el precio")
    self.viewForTaxSwitches.addSubview(typeOfApplyingTaxes)
    
  }
  
  private func initIvaLabelSwitch() {
    
    if ivaLabelSwitch != nil {
      
      self.ivaLabelSwitch.removeFromSuperview()
      self.ivaLabelSwitch = nil
      
    }
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: typeOfApplyingTaxes.frame.origin.y + typeOfApplyingTaxes.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
    
    ivaLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: true, optionsToShow: self.optionsIva, titleForPicker: "Opciones de IVA", viewToAddPickerView: UtilityManager.sharedInstance.currentViewController().view, constantStringWhenChangeValue: "IVA ")
    
    ivaLabelSwitch.isUserInteractionEnabled = true
    ivaLabelSwitch.delegateForPicker = self
    _ = ivaLabelSwitch.selectElementWithName(nameToLookFor: "16%")
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = .left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.ivaLabelSwitchText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    ivaLabelSwitch.attributedText = stringWithFormat
    arrayOfLabelWithSwitch.append(ivaLabelSwitch)
    self.viewForTaxSwitches.addSubview(ivaLabelSwitch)
    
  }
  
  private func initIshLabelSwitch() {
    
    if ishLabelSwitch != nil {
      
      self.ishLabelSwitch.removeFromSuperview()
      self.ishLabelSwitch = nil
      
    }
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: ivaLabelSwitch.frame.origin.y + ivaLabelSwitch.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
    
    ishLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false, optionsToShow: self.optionsIsh, titleForPicker: "Opciones de ISH", viewToAddPickerView: UtilityManager.sharedInstance.currentViewController().view, constantStringWhenChangeValue: "ISH ")
    
    ishLabelSwitch.isUserInteractionEnabled = true
    ishLabelSwitch.delegateForPicker = self
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = .left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.ishLabelSwitchText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    ishLabelSwitch.attributedText = stringWithFormat
    arrayOfLabelWithSwitch.append(ishLabelSwitch)
    self.viewForTaxSwitches.addSubview(ishLabelSwitch)
    
  }
  
  private func initIepsLabelSwitch() {
    
    if iepsLabelSwitch != nil {
      
      self.iepsLabelSwitch.removeFromSuperview()
      self.iepsLabelSwitch = nil
      
    }
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: ishLabelSwitch.frame.origin.y + ishLabelSwitch.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
    
    iepsLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false, optionsToShow: self.optionsIeps, titleForPicker: "Opciones de IEPS", viewToAddPickerView: UtilityManager.sharedInstance.currentViewController().view, constantStringWhenChangeValue: "IEPS ")
    
    iepsLabelSwitch.isUserInteractionEnabled = true
    iepsLabelSwitch.delegateForPicker = self
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = .left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.iepsLabelSwitchText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    iepsLabelSwitch.attributedText = stringWithFormat
    arrayOfLabelWithSwitch.append(iepsLabelSwitch)
    self.viewForTaxSwitches.addSubview(iepsLabelSwitch)
    
  }
  
  private func initRetIVALabelSwitch() {
    
    if retIVALabelSwitch != nil {
      
      self.retIVALabelSwitch.removeFromSuperview()
      self.retIVALabelSwitch = nil
      
    }
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: iepsLabelSwitch.frame.origin.y + iepsLabelSwitch.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
    
    retIVALabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false, optionsToShow: self.optionsRetIva, titleForPicker: "Opciones de Ret IVA", viewToAddPickerView: UtilityManager.sharedInstance.currentViewController().view, constantStringWhenChangeValue: "RET. IVA ")
    
    retIVALabelSwitch.isUserInteractionEnabled = true
    retIVALabelSwitch.delegateForPicker = self
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = .left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.retIVALabelSwitchText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    retIVALabelSwitch.attributedText = stringWithFormat
    arrayOfLabelWithSwitch.append(retIVALabelSwitch)
    self.viewForTaxSwitches.addSubview(retIVALabelSwitch)
    
  }
  
  private func initRetISRLabelSwitch() {
    
    if retISRLabelSwitch != nil {
      
      self.retISRLabelSwitch.removeFromSuperview()
      self.retISRLabelSwitch = nil
      
    }
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: retIVALabelSwitch.frame.origin.y + retIVALabelSwitch.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
    
    retISRLabelSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false, optionsToShow: self.optionsRetIsr, titleForPicker: "Opciones de Ret ISR", viewToAddPickerView: UtilityManager.sharedInstance.currentViewController().view, constantStringWhenChangeValue: "RET. ISR ")
    
    retISRLabelSwitch.isUserInteractionEnabled = true
    retISRLabelSwitch.delegateForPicker = self
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = .left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.retISRLabelSwitchText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    retISRLabelSwitch.attributedText = stringWithFormat
    arrayOfLabelWithSwitch.append(retISRLabelSwitch)
    self.viewForTaxSwitches.addSubview(retISRLabelSwitch)
    
  }
  
  private func initSeparatorForAduana() {
    
    if separatorForAduana != nil {
      
      self.separatorForAduana.removeFromSuperview()
      self.separatorForAduana = nil
      
    }
  
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: retISRLabelSwitch.frame.origin.y + retISRLabelSwitch.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 56.0 * UtilityManager.sharedInstance.conversionHeight)
    
    separatorForAduana = LabelSeparator.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 13.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateConceptoForGenericCFDI.separatorForAduanaText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    separatorForAduana.attributedText = stringWithFormat
    separatorForAduana.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    viewForTaxSwitches.addSubview(separatorForAduana)
  
  }
  
  private func initAduanaSwitch() {
    
    if aduanaSwitch != nil {
      
      self.aduanaSwitch.removeFromSuperview()
      self.aduanaSwitch = nil
      
    }
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: separatorForAduana.frame.origin.y + separatorForAduana.frame.size.height,
                                   width: mainScrollView.frame.size.width,
                                   height: 54.0 * UtilityManager.sharedInstance.conversionHeight)
    
    aduanaSwitch = LabelWithSwitch.init(frame: frameForView, newInitialValueOfSwitch: false)
    
    aduanaSwitch.isUserInteractionEnabled = true
    aduanaSwitch.delegateForPicker = self
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = .left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateConceptoForGenericCFDI.aduanaSwitchText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    aduanaSwitch.attributedText = stringWithFormat
    arrayOfLabelWithSwitch.append(aduanaSwitch)
    self.viewForTaxSwitches.addSubview(aduanaSwitch)
    
  }
  
  private func initParteButtons() {
    
    if arrayOfParteLikeViewCell.count > 0 {
      
      for parteCell in arrayOfParteLikeViewCell {
        
        parteCell.removeFromSuperview()
        
      }
      
      self.arrayOfParteLikeViewCell.removeAll()
      
    }
    
    var frameForButtons = CGRect.init(x: 0.0,
                                      y: aduanaSwitch.frame.origin.y + aduanaSwitch.frame.size.height,
                                      width: self.mainScrollView.frame.size.width,
                                      height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    
    ////DELETE THIS IN FUTURE

//    let newParte = Parte.init(newClaveSAT: "666", newParteSKU: "666", newUnity: Unity.init(newName: "Unidad"), newQuantity: 0, newPartePrice: 0, newProductService: ProductService.init())
//    conceptoToCreate.arrayOfParts.append(newParte)

    /////
    
    if conceptoToCreate.arrayOfParts.count > 0 {
      
      for i in 0...conceptoToCreate.arrayOfParts.count - 1 {
        
        let parteCell = ParteLikeCellView.init(frame: frameForButtons)
        parteCell.setStringLabelOfButton(newName: "\(conceptoToCreate.arrayOfParts[i].parteSKU!): $\(conceptoToCreate.arrayOfParts[i].finalImport)")
        parteCell.tag = i
        parteCell.delegate = self
        
        self.viewForTaxSwitches.addSubview(parteCell)
        self.viewForTaxSwitches.frame = CGRect.init(x: self.viewForTaxSwitches.frame.origin.x,
                                                    y: self.viewForTaxSwitches.frame.origin.y,
                                                width: self.viewForTaxSwitches.frame.size.width,
                                               height: self.viewForTaxSwitches.frame.size.height + ( 44.0 * UtilityManager.sharedInstance.conversionHeight ))
        
        self.mainScrollView.contentSize = CGSize.init(width: self.mainScrollView.contentSize.width,
                                                     height: self.mainScrollView.contentSize.height + ( 44.0 * UtilityManager.sharedInstance.conversionHeight ))
        
        frameForButtons = CGRect.init(x: 0.0,
                                      y: frameForButtons.origin.y + frameForButtons.size.height,
                                      width: self.mainScrollView.frame.size.width,
                                      height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
        
        arrayOfParteLikeViewCell.append(parteCell)
        
      }
      
    }
    
  }
  
  private func initButtonForCreatePart() {
    
    if buttonForCreatePart != nil {
      
      self.buttonForCreatePart.removeFromSuperview()
      self.buttonForCreatePart = nil
      
    }
  
    var frameForButton = CGRect.init(x: 0.0,
                                     y: aduanaSwitch.frame.origin.y + aduanaSwitch.frame.size.height,
                                     width: UIScreen.main.bounds.width,
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    
    if arrayOfParteLikeViewCell.last != nil {
      
      frameForButton = CGRect.init(x: 0.0,
                                   y: (arrayOfParteLikeViewCell.last?.frame.origin.y)! + (arrayOfParteLikeViewCell.last?.frame.size.height)! ,
                                   width: UIScreen.main.bounds.width,
                                   height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
      
      self.viewForTaxSwitches.frame = CGRect.init(x: 0.0,
                                                  y: self.viewForTaxSwitches.frame.origin.y,
                                              width: self.viewForTaxSwitches.frame.size.width,
                                             height: self.viewForTaxSwitches.frame.size.height + (44.0 * UtilityManager.sharedInstance.conversionHeight))
      
    }
    
    buttonForCreatePart = ParteButton.init(frame: frameForButton)
    buttonForCreatePart.changeMyTitle("Agregar parte", color: UIColor.blue)
    buttonForCreatePart.drawTopBorder()
    buttonForCreatePart.drawBottomBorder()
    buttonForCreatePart.addTarget(self, action: #selector(buttonForCreatePartePressed), for: UIControlEvents.touchUpInside)
    buttonForCreatePart.isUserInteractionEnabled = true
    
    buttonForCreatePart.backgroundColor = UIColor.white
    
    
    self.viewForTaxSwitches.addSubview(buttonForCreatePart)
    
//    self.arrayOfViews.append(buttonForCreatePart)
  
  }
  
  private func initSecondPartSubTotalAdditionLabel() {
    
    if subTotalAdditionLabel != nil {
      
      self.subTotalAdditionLabel.removeFromSuperview()
      self.subTotalAdditionLabel = nil
      
    }
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: buttonForCreatePart.frame.origin.y + buttonForCreatePart.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    subTotalAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    subTotalAdditionLabel.attributedText = stringWithFormat
    subTotalAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    self.viewForTaxSwitches.addSubview(subTotalAdditionLabel)
    
  }
  
  private func initCreateButton() {
    
    if createButton != nil {
      
      self.createButton.removeFromSuperview()
      self.createButton = nil
      
    }
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: (19.0 * UtilityManager.sharedInstance.conversionWidth),
                                     y: subTotalAdditionLabel.frame.origin.y + subTotalAdditionLabel.frame.size.height + (11.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    createButton = UIButton.init(frame: frameForButton)
    createButton.addTarget(self,
                           action: #selector(createButtonPressed),
                           for: .touchUpInside)
    createButton.backgroundColor = UIColor.orange
    createButton.setBackgroundColor(color: UIColor.init(red: 255.0/255.0, green: 164.0/255.0, blue: 78.0/255.0, alpha: 1.0), forState: .highlighted)
    createButton.setAttributedTitle(stringWithFormat, for: .normal)
    createButton.contentHorizontalAlignment = .left
    
    createButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.CreateConceptoViewController.createConceptoButtonText))
    
    self.viewForTaxSwitches.addSubview(createButton)
    
  }
  
  private func createGenericLabelForButton(text: String) -> UILabel {
    
    let frameForLabel = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 200.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    let genericLabel = UILabel.init(frame: frameForLabel)
    genericLabel.numberOfLines = 0
    genericLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: text,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    genericLabel.attributedText = stringWithFormat
    genericLabel.sizeToFit()
    let newFrame = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                               y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                               width: genericLabel.frame.size.width,
                               height: genericLabel.frame.size.height)
    genericLabel.frame = newFrame
    
    return genericLabel
    
  }
  
  //MARK: - ParteLikeCellViewDelegate
  
  func parteCellPressed(cell: ParteLikeCellView) {
    
    
    
  }
  
  func deleteParte(cell: ParteLikeCellView) {
    
    let alertController = UIAlertController(title: "Aviso",
                                            message: "¿Estás seguro de eliminar esta parte?",
                                            preferredStyle: UIAlertControllerStyle.alert)
    
    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (result: UIAlertAction) -> Void in }
    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result: UIAlertAction) -> Void in
      
      UtilityManager.sharedInstance.showLoader()
      
      self.conceptoToCreate.arrayOfParts.remove(at: cell.tag)

      for view in self.arrayOfParteLikeViewCell {
        
        view.removeFromSuperview()
        
      }
      
      self.createElementsForParts()
      
      UtilityManager.sharedInstance.hideLoader()
      
    }
    
    alertController.addAction(okAction)
    alertController.addAction(cancelAction)
    
    self.present(alertController, animated: true, completion: nil)
    
  }
  
  //MARK: - ButtonToActivePickerViewDelegate
  
  func lastButtonToShowPickerView(sender: ButtonToActivePickerView) {
    
    for labelWithSwitch in arrayOfLabelWithSwitch {
      
      if sender != labelWithSwitch {
        
        labelWithSwitch.hidePickerView()
        
      }
      
    }
    
  }
  
  @objc private func buttonForCreatePartePressed() {
    
    if self.productServiceSelected == nil {
      
      let alert = UIAlertController.init(title: "Error", message: "Elige un concepto primero", preferredStyle: .alert)
      let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
      
      alert.addAction(okAction)
      self.present(alert, animated: true, completion: nil)
      
    } else {
      
      let createPartScreen = CreatePartForGenericCFDIViewController.init(newConceptoToAddParts: self.conceptoToCreate, newProductServiceSelected:  self.productServiceSelected, newArrayOfUnits: self.arrayOfUnits)
      createPartScreen.delegate = self
      self.navigationController?.pushViewController(createPartScreen, animated: true)
      
    }
    
  }
  
  func selectActionFinalized(sender: ButtonToActivePickerView) {
    
    //    if sender == typeOfMoneyButton {
    //
    //      let currencyName = self.getNameOfCurrencySelected()
    //
    //      if currencyName != "MXN" {
    //
    //        self.showExchangeRateTextField()
    //
    //      }
    //
    //    }
    
  }
  
  @objc private func createButtonPressed() {
  
    let unityValueSelected = unityButton.getValueSelected()
    if self.productServiceSelected == nil {
      
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor selecciona un concepto",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    } else
      
    if unityValueSelected == nil {
      
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor selecciona un tipo de unidad",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    } else
      
    if notionCustomTextField.mainTextField.text?.isEmpty == true {
        
        let alertController = UIAlertController(title: "Error",
                                                message: "Por favor escribe el nombre del concepto",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
    } else
        
    if unityCostCustomTextField.mainTextField.text?.isEmpty == true && unityCostCustomTextField.mainTextField.text!.rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted) != nil {
          
          let alertController = UIAlertController(title: "Error",
                                                  message: "Por favor escribe un costo por unidad válido del concepto",
                                                  preferredStyle: UIAlertControllerStyle.alert)
          
          let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
          alertController.addAction(cancelAction)
          
          let actualController = UtilityManager.sharedInstance.currentViewController()
          actualController.present(alertController, animated: true, completion: nil)
          
    } else
          
    if quantityCustomTextField.mainTextField.text?.isEmpty == true && quantityCustomTextField.mainTextField.text!.rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted) != nil {
            
            let alertController = UIAlertController(title: "Error",
                                                    message: "Por favor escribe la cantidad de elementos para el concepto",
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
            alertController.addAction(cancelAction)
            
            let actualController = UtilityManager.sharedInstance.currentViewController()
            actualController.present(alertController, animated: true, completion: nil)
            
    } else
          
    if self.subTotalDouble == -1.0 {
          
              let alertController = UIAlertController(title: "Error",
                                                      message: "Por favor establece un importe del concepto",
                                                      preferredStyle: UIAlertControllerStyle.alert)
              
              let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
              alertController.addAction(cancelAction)
              
              let actualController = UtilityManager.sharedInstance.currentViewController()
              actualController.present(alertController, animated: true, completion: nil)
              
    } else
      
    if self.conceptImportIsEqualToPartImport() == false {
      
      let alertController = UIAlertController(title: "Error",
                                              message: "La suma del importe de todas las partes no coincide con el importe total del concepto. Favor de revisar los datos.",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
    
    } else
    
    if self.optionSelectedForTypeOfAplyingTaxes() == false {
      
      let alertController = UIAlertController(title: "Error",
                                              message: "Por favor selecciona si los impuestos ya están incluidos en el precio o no.",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
      alertController.addAction(cancelAction)
      
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
      
    } else {
      
      self.conceptoToCreate.claveProdServ = self.productServiceSelected.claveProdServ
      self.conceptoToCreate.numberIdentification = self.productServiceSelected.sku
      self.conceptoToCreate.conceptDescription = notionCustomTextField.mainTextField.text!
      self.conceptoToCreate.unit = Unity(newId: unityValueSelected!.id, newName: unityValueSelected!.name)
      self.conceptoToCreate.price = Double(unityCostCustomTextField.mainTextField.text!)
      self.conceptoToCreate.quantity = Double(quantityCustomTextField.mainTextField.text!)
      
      if percentageSwitch.getValueOfSwitch() {
        if let valueSelected = percentageSwitch.getValueSelected() {
          if discountCustomTextField.mainTextField.text?.isEmpty != true && discountCustomTextField.mainTextField.text!.rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted) == nil {
            if Double(discountCustomTextField.mainTextField.text!)! > 0.0 {
              if valueSelected.id == "1" { //porcentaje
                self.conceptoToCreate.discountPercentage = Double(discountCustomTextField.mainTextField.text!)
                self.conceptoToCreate.finalDiscount = (self.conceptoToCreate.price * self.conceptoToCreate.quantity) * (self.conceptoToCreate.discountPercentage / 100.0)
              }else{ //cantidad
                self.conceptoToCreate.finalDiscount = Double(discountCustomTextField.mainTextField.text!)
              }
            } else {
              let alertController = UIAlertController(title: "Error",
                                                      message: "Si hay un descuento favor de escribir un valor válido.",
                                                      preferredStyle: UIAlertControllerStyle.alert)
              
              let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in return}
              alertController.addAction(cancelAction)
              
              let actualController = UtilityManager.sharedInstance.currentViewController()
              actualController.present(alertController, animated: true, completion: nil)
            }
          } else {
            let alertController = UIAlertController(title: "Error",
                                                    message: "Si hay un descuento favor de escribir un valor válido.",
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in return}
            alertController.addAction(cancelAction)
            
            let actualController = UtilityManager.sharedInstance.currentViewController()
            actualController.present(alertController, animated: true, completion: nil)
          }
        }
      }
      
      self.conceptoToCreate.subtotal = (self.conceptoToCreate.price * self.conceptoToCreate.quantity) - self.conceptoToCreate.finalDiscount
      self.conceptoToCreate.finalBase = self.conceptoToCreate.subtotal
      let taxIncludedInPriceOption = self.typeOfApplyingTaxes.getValueSelected()
      var finalTaxToApplyWhenIncludedInPrice: Double = 0.0
      
      if ivaLabelSwitch.getValueOfSwitch() == true {
        let ivaOptionSelected = ivaLabelSwitch.getValueSelected()
        if ivaOptionSelected != nil {
          self.conceptoToCreate.applyIva = true
          self.conceptoToCreate.ivaValueToSendToServer = Double(ivaOptionSelected!.type)!
          if taxIncludedInPriceOption != nil {
            switch taxIncludedInPriceOption!.id {
            case "1":
              finalTaxToApplyWhenIncludedInPrice = finalTaxToApplyWhenIncludedInPrice + Double(ivaOptionSelected!.type)! / 100.0
            case "2":
              self.conceptoToCreate.finalIva = self.conceptoToCreate.subtotal * ( Double(ivaOptionSelected!.type)! / 100.0)
            default:
              self.conceptoToCreate.finalIva = self.conceptoToCreate.subtotal * ( Double(ivaOptionSelected!.type)! / 100.0)
            }
          }
        }
      }
      if ishLabelSwitch.getValueOfSwitch() == true {
        let ishOptionSelected = ishLabelSwitch.getValueSelected()
        if ishOptionSelected != nil {
          self.conceptoToCreate.applyIsh = true
          self.conceptoToCreate.ishValueToSendToServer = Double(ishOptionSelected!.type)!
            if taxIncludedInPriceOption != nil {
              switch taxIncludedInPriceOption!.id {
              case "1":
                finalTaxToApplyWhenIncludedInPrice = finalTaxToApplyWhenIncludedInPrice + Double(ishOptionSelected!.type)! / 100.0
              case "2":
                self.conceptoToCreate.finalIsh = self.conceptoToCreate.subtotal * ( Double(ishOptionSelected!.type)! / 100.0)
              default:
                self.conceptoToCreate.finalIsh = self.conceptoToCreate.subtotal * ( Double(ishOptionSelected!.type)! / 100.0)
              }
            }
          }
        }
        if iepsLabelSwitch.getValueOfSwitch() == true {
          let iepsOptionSelected = iepsLabelSwitch.getValueSelected()
          if iepsOptionSelected != nil {
            self.conceptoToCreate.applyIeps = true
            self.conceptoToCreate.iepsValueToSendToServer = Double(iepsOptionSelected!.type)!
            if taxIncludedInPriceOption != nil {
              switch taxIncludedInPriceOption!.id {
                case "1":
                  finalTaxToApplyWhenIncludedInPrice = finalTaxToApplyWhenIncludedInPrice + Double(iepsOptionSelected!.type)! / 100.0
                case "2":
                  self.conceptoToCreate.finalIeps = self.conceptoToCreate.subtotal * ( Double(iepsOptionSelected!.type)! / 100.0)
                default:
                  self.conceptoToCreate.finalIeps = self.conceptoToCreate.subtotal * ( Double(iepsOptionSelected!.type)! / 100.0)
              }
            }
          }
        }
        if retIVALabelSwitch.getValueOfSwitch() == true {
          let retIVAOptionSelected = retIVALabelSwitch.getValueSelected()
          if retIVAOptionSelected != nil {
            self.conceptoToCreate.applyRetIva = true
            self.conceptoToCreate.retIvaValueToSendToServer = Double(retIVAOptionSelected!.type)!
            if taxIncludedInPriceOption != nil {
              switch taxIncludedInPriceOption!.id {
                case "1":
                  finalTaxToApplyWhenIncludedInPrice = finalTaxToApplyWhenIncludedInPrice + Double(retIVAOptionSelected!.type)! / 100.0
                case "2":
                  self.conceptoToCreate.finalRetIva = self.conceptoToCreate.subtotal * ( Double(retIVAOptionSelected!.type)! / 100.0)
                default:
                  self.conceptoToCreate.finalRetIva = self.conceptoToCreate.subtotal * ( Double(retIVAOptionSelected!.type)! / 100.0)
              }
            }
          }
        }
      if retISRLabelSwitch.getValueOfSwitch() == true {
        let retISROptionSelected = retISRLabelSwitch.getValueSelected()
        if retISROptionSelected != nil {
          self.conceptoToCreate.applyRetIsr = true
          self.conceptoToCreate.retIsrValueToSendToServer = Double(retISROptionSelected!.type)!
          if taxIncludedInPriceOption != nil {
            switch taxIncludedInPriceOption!.id {
            case "1":
              finalTaxToApplyWhenIncludedInPrice = finalTaxToApplyWhenIncludedInPrice + Double(retISROptionSelected!.type)! / 100.0
            case "2":
              self.conceptoToCreate.finalRetIsr = self.conceptoToCreate.subtotal * ( Double(retISROptionSelected!.type)! / 100.0)
            default:
              self.conceptoToCreate.finalRetIsr = self.conceptoToCreate.subtotal * ( Double(retISROptionSelected!.type)! / 100.0)
            }
          }
        }
      }
      if taxIncludedInPriceOption != nil {
        
        switch taxIncludedInPriceOption!.id {
        case "1":
          if finalTaxToApplyWhenIncludedInPrice != 0.0 {
            self.conceptoToCreate.finalBase = self.conceptoToCreate.subtotal / (1.0 + finalTaxToApplyWhenIncludedInPrice)
            
            if self.conceptoToCreate.applyIva {
              self.conceptoToCreate.finalIva = self.conceptoToCreate.finalBase * (self.conceptoToCreate.ivaValueToSendToServer/100.0)
            }
            if self.conceptoToCreate.applyIsh {
              self.conceptoToCreate.finalIsh = self.conceptoToCreate.finalBase * (self.conceptoToCreate.ishValueToSendToServer/100.0)
            }
            if self.conceptoToCreate.applyIeps {
              self.conceptoToCreate.finalIeps = self.conceptoToCreate.finalBase * (self.conceptoToCreate.iepsValueToSendToServer/100.0)
            }
            if self.conceptoToCreate.applyRetIva {
              self.conceptoToCreate.finalRetIva = self.conceptoToCreate.finalBase * (self.conceptoToCreate.retIvaValueToSendToServer/100.0)
            }
            if self.conceptoToCreate.applyRetIsr {
              self.conceptoToCreate.finalRetIsr = self.conceptoToCreate.finalBase * (self.conceptoToCreate.retIsrValueToSendToServer/100.0)
            }
            
            self.conceptoToCreate.finalBase = self.conceptoToCreate.finalBase + self.conceptoToCreate.finalDiscount
            self.conceptoToCreate.price = (self.conceptoToCreate.finalBase/self.conceptoToCreate.quantity)
          } else {
            
            //SHOW ALERT FOR SELECT A TAX
            
          }
        case "2":
          self.conceptoToCreate.finalBase = self.conceptoToCreate.price * self.conceptoToCreate.quantity
        default:
          self.conceptoToCreate.finalBase = self.conceptoToCreate.price * self.conceptoToCreate.quantity
        }
      }
      if self.pedimentoTextField.text! != "" {
        self.conceptoToCreate.pedimentoAduana = pedimentoTextField.text!
      }
      let alertController = UIAlertController(title: "Éxito",
                                            message: "El concepto ha sido creado correctamente",
                                     preferredStyle: UIAlertControllerStyle.alert)
            
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
      self.delegate?.conceptoDidCreate(newConcepto: self.conceptoToCreate)
      _ = self.navigationController?.popViewController(animated: true)
      }
      alertController.addAction(cancelAction)
            
      let actualController = UtilityManager.sharedInstance.currentViewController()
      actualController.present(alertController, animated: true, completion: nil)
   
    }   
  }
  
  private func optionSelectedForTypeOfAplyingTaxes() -> Bool {
    
    if let optionSelected = typeOfApplyingTaxes.getValueSelected() {
      
      switch optionSelected.id {
        
      case "1":
        self.taxIncludedInPrice = true
        self.conceptoToCreate.isTaxIncludedInPrice = self.taxIncludedInPrice
        
      case "2":
        self.taxIncludedInPrice = false
        self.conceptoToCreate.isTaxIncludedInPrice = self.taxIncludedInPrice
        
      default:
        self.taxIncludedInPrice = true
        self.conceptoToCreate.isTaxIncludedInPrice = self.taxIncludedInPrice
        
      }
      
      return true
      
    }
    
    return false //supposedly never happen
    
  }
  
  private func conceptImportIsEqualToPartImport() -> Bool{
    
    if conceptoToCreate.arrayOfParts.count > 0 {
    
      var totalPartsImport: Double = 0.0
      
      for part in conceptoToCreate.arrayOfParts {
        
        totalPartsImport = totalPartsImport + part.finalImport
        
      }
      
      return totalPartsImport == subTotalDouble
      
    }
    
    return true
    
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    
    super.viewWillDisappear(animated)
    
    self.hideKeyboard()
    
    unityButton?.hidePickerView()
    
    for labelSwitch in arrayOfLabelWithSwitch {
      
      labelSwitch.hidePickerView()
      
    }
    
  }
  
  //MARK: - LabelWithSwitchPickerDelegate
  
  func switchChangeToOn(sender: LabelWithSwitch) {
    
    if sender == aduanaSwitch {
      
      let alertController = UIAlertController.init(title: "Información aduanal", message: "Por favor agrega el número de pedimento", preferredStyle: .alert)
      
      alertController.addTextField(configurationHandler: { (textField) in
   
        self.pedimentoTextField = textField
        textField.delegate = self
        textField.placeholder = "15 caracteres"
        
      })
      
      let addAction = UIAlertAction(title: "Agregar", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        
        let newTitle = "\(ViewControllersConstants.FacturaCreateConceptoForGenericCFDI.aduanaSwitchText): \(self.pedimentoTextField.text!)"
        
        self.aduanaSwitch.changeTitleLabel(newTitle: newTitle)
        
      }
      
      let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
        
        self.aduanaSwitch.changeTitleLabel(newTitle: ViewControllersConstants.FacturaCreateConceptoForGenericCFDI.aduanaSwitchText)
        
      }
      alertController.addAction(addAction)
      alertController.addAction(cancelAction)
      
      self.present(alertController, animated: true, completion: {})
      
    } else
    
    if sender == percentageSwitch {
      
      self.discountCustomTextFieldAppears()
      
    } else
    
      if sender == ishLabelSwitch {
      
        retIVALabelSwitch.switchView.setOn(false, animated: true)
        retIVALabelSwitch.switchView.isEnabled = false
        
        retISRLabelSwitch.switchView.setOn(false, animated: true)
        retISRLabelSwitch.switchView.isEnabled = false
      
        iepsLabelSwitch.switchView.setOn(false, animated: true)
        iepsLabelSwitch.switchView.isEnabled = false
      
    } else
      
      if sender == iepsLabelSwitch {
        
        retIVALabelSwitch.switchView.setOn(false, animated: true)
        retIVALabelSwitch.switchView.isEnabled = false
        
        retISRLabelSwitch.switchView.setOn(false, animated: true)
        retISRLabelSwitch.switchView.isEnabled = false
        
        ishLabelSwitch.switchView.setOn(false, animated: true)
        ishLabelSwitch.switchView.isEnabled = false
        
      } else
        
        if sender == retISRLabelSwitch || sender == retIVALabelSwitch {
          
          iepsLabelSwitch.switchView.setOn(false, animated: true)
          iepsLabelSwitch.switchView.isEnabled = false
          
          ishLabelSwitch.switchView.setOn(false, animated: true)
          ishLabelSwitch.switchView.isEnabled = false
          
    }
    
    for labelWithSwitch in arrayOfLabelWithSwitch {
      
      if sender != labelWithSwitch {
        
        labelWithSwitch.hidePickerView()
        
      }
      
    }
    
  }
  
  func switchChangeToOff(sender: LabelWithSwitch) {
    
    if sender == aduanaSwitch {
      
      aduanaSwitch.changeTitleLabel(newTitle: ViewControllersConstants.FacturaCreateConceptoForGenericCFDI.aduanaSwitchText)
      
    } else
    
    if sender == percentageSwitch {
      
      self.percentageSwitch.changeTitleLabel(newTitle: ViewControllersConstants.FacturaCreateConceptoForGenericCFDI.typeOfDisccountSeparatorText)
      self.discountCustomTextFieldDisappears()
      
    } else
      
    if sender == ishLabelSwitch {
      
      retIVALabelSwitch.switchView.isEnabled = true
      retISRLabelSwitch.switchView.isEnabled = true
      iepsLabelSwitch.switchView.isEnabled = true
      
    } else
      
      if sender == iepsLabelSwitch {
        
        retIVALabelSwitch.switchView.isEnabled = true
        retISRLabelSwitch.switchView.isEnabled = true
        ishLabelSwitch.switchView.isEnabled = true
        
      } else
        
        if sender == retISRLabelSwitch || sender == retIVALabelSwitch && (retIVALabelSwitch.switchView.isOn == false && retISRLabelSwitch.switchView.isOn == false ) {
          
          iepsLabelSwitch.switchView.isEnabled = true
          ishLabelSwitch.switchView.isEnabled = true
          
    }
    
  }
  
  private func discountCustomTextFieldAppears() {
    
    self.mainScrollView.contentSize = CGSize.init(width: mainScrollView.contentSize.width,
                                                 height: mainScrollView.contentSize.height + (89.0 * UtilityManager.sharedInstance.conversionHeight))
    
    self.showDiscountCustomTextField()
    self.moveDownViewOfTaxSwitches()
    
  }
  
  private func discountCustomTextFieldDisappears() {
    
    self.mainScrollView.contentSize = CGSize.init(width: mainScrollView.contentSize.width,
                                                  height: mainScrollView.contentSize.height - (89.0 * UtilityManager.sharedInstance.conversionHeight))
    
    self.hideDiscountCustomTextField()
    self.moveUpViewOfTaxSwitches()
    
  }
  
  private func showDiscountCustomTextField() {
    
    UIView.animate(withDuration: 0.35, animations: {
      
      self.discountCustomTextField.alpha = 1.0
      
    }, completion: { isFnished in
      
      
      
    })
    
  }
  
  private func hideDiscountCustomTextField() {
    
    UIView.animate(withDuration: 0.35, animations: {
      
      self.discountCustomTextField.alpha = 0.0
      
    }, completion: { isFnished in
      
      
      
    })
    
  }
  
  private func moveDownViewOfTaxSwitches() {
    
    let frameToMove = CGRect.init(x: 0.0,
                                  y: percentageSwitch.frame.origin.y + percentageSwitch.frame.size.height + (89.0 * UtilityManager.sharedInstance.conversionWidth),
                              width: self.viewForTaxSwitches.frame.size.width,
                             height: self.viewForTaxSwitches.frame.size.height)
    
    UIView.animate(withDuration: 0.35, animations: {
      
      self.viewForTaxSwitches.frame = frameToMove
      
    }, completion: { finished in
      
      
      
    })
    
  }
  
  private func moveUpViewOfTaxSwitches() {
    
    let frameToMove = CGRect.init(x: 0.0,
                                  y: percentageSwitch.frame.origin.y + percentageSwitch.frame.size.height,
                                  width: self.viewForTaxSwitches.frame.size.width,
                                  height: self.viewForTaxSwitches.frame.size.height)
    
    UIView.animate(withDuration: 0.35, animations: {
      
      self.viewForTaxSwitches.frame = frameToMove
      
    }, completion: { finished in
      
      
      
    })
    
  }
  
  //MARK: - CustomTextFieldWithTitleViewDelegate
  
  func customTextFieldSelected(sender: CustomTextFieldWithTitleView) {
    
    
    
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    
    for labelWithSwitch in arrayOfLabelWithSwitch {
      
      labelWithSwitch.hidePickerView()
      
    }
    
    unityButton.hidePickerView()
    
    if textField == self.notionCustomTextField.mainTextField {
      
      let selectProductViewController = ProductsTableViewController.init(style: .plain, newArrayOfElements: self.arrayOfProductService)
      selectProductViewController.delegate = self
      self.navigationController?.pushViewController(selectProductViewController, animated: true)
      
    } else
  
    if textField == self.quantityCustomTextField.mainTextField || textField == self.unityCostCustomTextField.mainTextField {
      let quantityDoubleValue = Double(self.quantityCustomTextField.mainTextField.text!)
      let unitCostDoubleValue = Double(self.unityCostCustomTextField.mainTextField.text!)
        
      if quantityDoubleValue != nil && unitCostDoubleValue != nil {
        self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String(quantityDoubleValue! * unitCostDoubleValue!))
        self.subTotalDouble = quantityDoubleValue! * unitCostDoubleValue!
      }
    } else
        
    if textField == self.discountCustomTextField.mainTextField {
      let quantityDoubleValue = Double(self.quantityCustomTextField.mainTextField.text!)
      let unitCostDoubleValue = Double(self.unityCostCustomTextField.mainTextField.text!)
      let discountDoubleValue = Double(self.discountCustomTextField.mainTextField.text!)
          
      if quantityDoubleValue != nil && unitCostDoubleValue != nil && discountDoubleValue != nil {
        if let optionSelected = percentageSwitch.getValueSelected() {
          if optionSelected.id == "1" {
            self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String((quantityDoubleValue! * unitCostDoubleValue!) - (quantityDoubleValue! * unitCostDoubleValue! * (discountDoubleValue! / 100.0))))
            self.subTotalDouble = (quantityDoubleValue! * unitCostDoubleValue!) - (quantityDoubleValue! * unitCostDoubleValue! * (discountDoubleValue! / 100.0))
          } else {
            self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String((quantityDoubleValue! * unitCostDoubleValue!) - discountDoubleValue!))
            self.subTotalDouble = (quantityDoubleValue! * unitCostDoubleValue!) - discountDoubleValue!
          }
        }
      }
    }
  }
  
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    
    if textField == self.quantityCustomTextField.mainTextField || textField == self.unityCostCustomTextField.mainTextField {
      let quantityDoubleValue = Double(self.quantityCustomTextField.mainTextField.text!)
      let unitCostDoubleValue = Double(self.unityCostCustomTextField.mainTextField.text!)

      if quantityDoubleValue != nil && unitCostDoubleValue != nil {
        self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String(quantityDoubleValue! * unitCostDoubleValue!))
        self.subTotalDouble = quantityDoubleValue! * unitCostDoubleValue!
      }
    } else
    if textField == self.discountCustomTextField.mainTextField {
      let quantityDoubleValue = Double(self.quantityCustomTextField.mainTextField.text!)
      let unitCostDoubleValue = Double(self.unityCostCustomTextField.mainTextField.text!)
      let discountDoubleValue = Double(self.discountCustomTextField.mainTextField.text!)
      
      if quantityDoubleValue != nil && unitCostDoubleValue != nil && discountDoubleValue != nil {
        if let optionSelected = percentageSwitch.getValueSelected() {
          if optionSelected.id == "1" {
            self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String((quantityDoubleValue! * unitCostDoubleValue!) - (quantityDoubleValue! * unitCostDoubleValue! * (discountDoubleValue! / 100.0))))
            self.subTotalDouble = (quantityDoubleValue! * unitCostDoubleValue!) - (quantityDoubleValue! * unitCostDoubleValue! * (discountDoubleValue! / 100.0))
          } else {
            self.changeTextOfSubtotalLabel(newString: ViewControllersConstants.FacturaCreateSecondPartViewController.secondPartSubTotalAdditionLabelText + String((quantityDoubleValue! * unitCostDoubleValue!) - discountDoubleValue!))
            self.subTotalDouble = (quantityDoubleValue! * unitCostDoubleValue!) - discountDoubleValue!
          }
        }
      }
    }
    
    return true
    
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    let nextTage = textField.tag + 1
    
    let nextResponder = textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
    
    if (nextResponder != nil){
      
      nextResponder?.becomeFirstResponder()
      
    } else {
      
      textField.resignFirstResponder()
      
    }
    
    return false
    
  }
  
  private func changeTextOfSubtotalLabel(newString: String) {
    
    if subTotalAdditionLabel != nil {
      
      self.subTotalAdditionLabel.removeFromSuperview()
      self.subTotalAdditionLabel = nil
      
    }
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: buttonForCreatePart.frame.origin.y + buttonForCreatePart.frame.size.height,
                                    width: mainScrollView.frame.size.width,
                                    height: 40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    subTotalAdditionLabel = LabelForAdditions.init(frame: frameForLabel)
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    
    let stringWithFormat = NSMutableAttributedString(
      string: newString,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    subTotalAdditionLabel.attributedText = stringWithFormat
    subTotalAdditionLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    self.viewForTaxSwitches.addSubview(subTotalAdditionLabel)
    
  }
  
  //MARK: - CreatePartForGenericCFDIViewControllerDelegate
  
  func parteJustCreated() {
    
    self.createElementsForParts()
    
  }
  
  @objc private func hideKeyboard() {
    
    self.view.endEditing(true)
    
  }
  
  //MARK: - ProoductTableViewController
  
  func saveProductServiceSelected(_ productServiceSelected: ProductService) {
    
    self.productServiceSelected = productServiceSelected
    self.notionCustomTextField.mainTextField.text = self.productServiceSelected.name
    _ = self.unityButton.selectElementWithName(nameToLookFor: self.productServiceSelected.unidad)
    
  }
  
}


