//
//  ProductsTableViewController.swift
//  FacturaCom
//
//  Created by Israel on 03/01/18.
//  Copyright © 2018 Alejandro Aristi C. All rights reserved.
//

import UIKit

protocol ProductsTableViewControllerDelegate {
  
  func saveProductServiceSelected(_ productServiceSelected: ProductService)
  
}

class ProductsTableViewController: UITableViewController {
  
  var searchController: UISearchController! = nil
  var leftButtonItemView: UIBarButtonItem! = nil
  let kCellID = "cell"
  
  
  //
  private var arrayOfElements: Array<ProductService>! = Array<ProductService>()
  private var filteredElements: Array<ProductService>! = Array<ProductService>()
  
  var delegate: ProductsTableViewControllerDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
    
  }
  
  init(style: UITableViewStyle, newArrayOfElements: Array<ProductService>) {
    
    super.init(style: style)
    
    arrayOfElements = newArrayOfElements
    filteredElements = newArrayOfElements
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
//    self.initNavigationBar()
    self.initSearchController()
    self.initTableView()
    
  }
  
  private func initSearchController() {
    
    searchController = UISearchController.init(searchResultsController: nil)
    searchController.searchResultsUpdater = self
    searchController.dimsBackgroundDuringPresentation = false
    searchController.searchBar.placeholder = "Buscar"
    self.definesPresentationContext = true
    searchController.searchBar.barTintColor = UtilityManager.sharedInstance.backgroundColorForSearchBar
    
  }
  
  private func initTableView() {
    
    self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: kCellID)
    self.tableView.tableHeaderView = searchController.searchBar
    
  }
  
  func filterContentForSearchText(searchText: String, scope: String = "All") {
    
    filteredElements = arrayOfElements.filter { element in
      
      return element.name.lowercased().contains(searchText.lowercased()) //|| element. .lowercased().contains(searchText.lowercased())
      
    }
    
    self.tableView.reloadData()
    
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      return filteredElements.count
      
    }
    
    return arrayOfElements.count
    
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: kCellID, for: indexPath)
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      cell.textLabel?.text = filteredElements[indexPath.row].name
      cell.detailTextLabel?.text = filteredElements[indexPath.row].sku
      
    } else {
      
      cell.textLabel?.text = arrayOfElements[indexPath.row].name
      cell.detailTextLabel?.text = arrayOfElements[indexPath.row].sku
      
    }
    
    return cell
    
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    var productServiceSelected: ProductService
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      productServiceSelected = filteredElements[indexPath.row]
      
    } else {
      
      productServiceSelected = arrayOfElements[indexPath.row]
      
    }
    
    self.delegate?.saveProductServiceSelected(productServiceSelected)
    self.navigationController?.popViewController(animated: true)
    
  }

}

extension ProductsTableViewController: UISearchResultsUpdating {
  
  func updateSearchResults(for searchController: UISearchController) {
    
    self.filterContentForSearchText(searchText: searchController.searchBar.text!)
    
  }
  
}

