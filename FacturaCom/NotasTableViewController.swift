//
//  NotasTableViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 13/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class NotasTableViewController: TableViewWithSearchBarViewController {
  
  private var notasArray: Array<Nota>! = Array<Nota>()
  private var filteredNotasArray: Array<Nota>! = Array<Nota>()
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
    
  }
  
  init(style: UITableViewStyle, newArrayOfNotasArray: Array<Nota>) {
    
    notasArray = newArrayOfNotasArray
    
    super.init(style: style, newArrayOfElements: notasArray)
    
    self.editNavigationBar()
    self.initValues()
    
  }
  
  private func editNavigationBar() {
    
    self.title = ViewControllersConstants.MainTabController.notasButtonText
    self.changeNavigationBarTitle()
    
  }
  
  private func changeNavigationBarTitle() {
    
    let titleLabel = UILabel.init(frame: CGRect.zero)
    
    let font = UIFont(name: "SFUIText-Regular",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.NameOfCFDIScreen.notasTableTitleText,
      attributes:[NSFontAttributeName:font!,
                  NSParagraphStyleAttributeName:style,
                  NSForegroundColorAttributeName:color
      ]
    )
    titleLabel.attributedText = stringWithFormat
    titleLabel.sizeToFit()
    self.navigationItem.titleView = titleLabel
    
  }
  
  private func initValues() {
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.requestToGetAllNotas(params: nil, actionsToMakeWhenSucceeded: { arrayOfNotas in
      
      print(arrayOfNotas)
      self.notasArray = arrayOfNotas
      self.tableView.reloadData()
      
      UtilityManager.sharedInstance.hideLoader()
      
    }, actionsToMakeWhenFailed: {
      
      UtilityManager.sharedInstance.hideLoader()
      
    })
    
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      return filteredNotasArray.count
      
    }
    
    return notasArray.count
    
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CustomCellTableViewCell
    
    cell.selectionStyle = UITableViewCellSelectionStyle.none
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      cell.setData(newData: filteredNotasArray[indexPath.row])
      
    } else {
      
      cell.setData(newData: notasArray[indexPath.row])
      
    }
    
    return cell
    
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    var notaData: Nota
    
    if searchController.isActive && searchController.searchBar.text != "" {
      
      notaData = filteredNotasArray[indexPath.row]
      
    } else {
      
      notaData = notasArray[indexPath.row]
      
    }
    
    let facturaDetailScreen = NotaDetailViewController.init(newNotaData: notaData)
    self.navigationController?.pushViewController(facturaDetailScreen, animated: true)
    
  }
  
  func filterContentForSearchText(searchText: String, scope: String = "All") {
    
    filteredNotasArray = notasArray.filter { element in
      
      return element.folio.lowercased().contains(searchText.lowercased()) || element.fechaTimbrado.lowercased().contains(searchText.lowercased()) || element.razonSocialReceptor.lowercased().contains(searchText.lowercased())
      
    }
    
    self.tableView.reloadData()
    
  }
  
  override func actionToDoWhenChangeCompany() {
    
    self.initValues()
    
  }
  
  override func updateSearchResults(for searchController: UISearchController) {
    
    self.filterContentForSearchText(searchText: searchController.searchBar.text!)
    
  }
  
  override func createSomethingButtonPressed() {
    
    let createNotaScreen = NotaCreateViewController()
    self.navigationController?.pushViewController(createNotaScreen, animated: true)
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    
    self.initValues()
    
    if User.session.actualUsingCompany.urlLogo != nil {
      
      self.companyView.imageFromUrl(urlString: User.session.actualUsingCompany.urlLogo!)
      
    }
    
  }
  
}
