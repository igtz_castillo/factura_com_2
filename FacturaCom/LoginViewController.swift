//
//  LoginViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 24/03/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
  
  private var mainTabBarController: MainTabBarController! = nil
  //
  private var logoImageView: UIImageView! = nil
  private var emailTextField: CustomTextFieldForLoginView! = nil
  private var passwordTextField: CustomTextFieldForLoginView! = nil
  private var missedPasswordButton: UIButton! = nil
  private var loginButton: UIButton! = nil
  private var createAccountButton: UIButton! = nil
  private var dontHaveAccountLabel: UILabel! = nil
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.white //UIColor.init(patternImage: UIImage.init(named: "fondo_login")!)
    
    var backgroundImageName = "fondo_nuevo"
    
    if UtilityManager.sharedInstance.isIpad() == true {
      
      backgroundImageName = "iPad_Login_img"
      
    }
    
    let imageViewBackground = UIImageView.init(image: UIImage.init(named: backgroundImageName)!)
    imageViewBackground.contentMode = .scaleToFill
    imageViewBackground.frame = self.view.frame
    self.view.addSubview(imageViewBackground)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.editNavigationBar()
    
    self.createLogo()
    self.createEmailTextField()
    self.createPasswordTextField()
    self.createMissedPasswordButton()
    self.createLoginButton()
    self.createCreateAccountButton()
//    self.createDontHaveAccountLabel()
    
  }
  
  private func editNavigationBar() {
    
    self.title = "Atrás"
    self.navigationController?.isNavigationBarHidden = true
    
  }
  
  private func createLogo() {
    
    logoImageView = UIImageView.init(image: UIImage.init(named: "logo"))
    let goldenPitchStarFrame = CGRect.init(x: (self.view.frame.size.width / 2.0) - (80.0 * UtilityManager.sharedInstance.conversionWidth),
                                           y: (40.0 * UtilityManager.sharedInstance.conversionHeight),
                                       width: 160.0 * UtilityManager.sharedInstance.conversionWidth,
                                      height: 104.35 * UtilityManager.sharedInstance.conversionHeight)
    logoImageView.frame = goldenPitchStarFrame
    
    self.view.addSubview(logoImageView)
    
  }
  
  private func createEmailTextField() {
    
    var positionInY = logoImageView.frame.origin.y + logoImageView.frame.size.height + (40.0 * UtilityManager.sharedInstance.conversionHeight)
    
    if UtilityManager.sharedInstance.isIpad() {
      
      positionInY = logoImageView.frame.origin.y + logoImageView.frame.size.height + (350.00 - UIApplication.shared.statusBarFrame.size.height) * UtilityManager.sharedInstance.conversionHeight
      
    }
    
    
    let frameOfView = CGRect.init(x: 20.0 * UtilityManager.sharedInstance.conversionWidth,
                                  y: positionInY,
                              width: UIScreen.main.bounds.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                             height: 47.0 * UtilityManager.sharedInstance.conversionHeight)
    
    emailTextField = CustomTextFieldForLoginView.init(frame: frameOfView,
                                                      imageName: "mail_Icon",
                                                      newPlaceholderString: "Email")
    emailTextField.mainTextField.autocapitalizationType = .none
    
    self.view.addSubview(emailTextField)
    
  }
  
  private func createPasswordTextField() {
    
    let frameOfView = CGRect.init(x: 20.0 * UtilityManager.sharedInstance.conversionWidth,
                                  y: emailTextField.frame.origin.y + emailTextField.frame.size.height + (25.0 * UtilityManager.sharedInstance.conversionWidth),
                              width: UIScreen.main.bounds.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                             height: 47.0 * UtilityManager.sharedInstance.conversionHeight)
    
    passwordTextField = CustomTextFieldForLoginView.init(frame: frameOfView,
                                                         imageName: "pass_Icon",
                                                         newPlaceholderString: "Contraseña")
    passwordTextField.mainTextField.isSecureTextEntry = true
    
    self.view.addSubview(passwordTextField)
    
  }
  
  private func createMissedPasswordButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 12.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.LoginViewController.missedPasswordText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: (self.view.frame.size.width / 2.0) - (96.5 * UtilityManager.sharedInstance.conversionWidth),
                                     y: passwordTextField.frame.origin.y + passwordTextField.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                 width: 193.0 * UtilityManager.sharedInstance.conversionWidth,
                                height: 14.0 * UtilityManager.sharedInstance.conversionHeight)
    missedPasswordButton = UIButton.init(frame: frameForButton)
    missedPasswordButton.addTarget(self,
                                   action: #selector(missedButtonPressed),
                                   for: .touchUpInside)
    missedPasswordButton.backgroundColor = UIColor.clear
    missedPasswordButton.setAttributedTitle(stringWithFormat, for: .normal)
    
    self.view.addSubview(missedPasswordButton)
    
  }
  
  private func createLoginButton() {
    
    loginButton = UIButton.init(frame: CGRect.zero)
    
    let font = UIFont.systemFont(ofSize: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.LoginViewController.loginButtonText,
      attributes:[NSFontAttributeName: font,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    loginButton.backgroundColor = UIColor.orange
    loginButton.setBackgroundColor(color: UIColor.init(red: 255.0/255.0, green: 164.0/255.0, blue: 78.0/255.0, alpha: 1.0), forState: .highlighted)
    loginButton.layer.cornerRadius = 8.0 * UtilityManager.sharedInstance.conversionWidth
    
    loginButton.setAttributedTitle(stringWithFormat, for: .normal)
    loginButton.addTarget(self,
                          action: #selector(loginButtonPressed),
                          for: .touchUpInside)
    loginButton.sizeToFit()
    
    let frameForButton = CGRect.init(x: self.view.center.x - (50.0 * UtilityManager.sharedInstance
      .conversionWidth),
                                     y: missedPasswordButton.frame.origin.y + missedPasswordButton.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                 width: (100.0 * UtilityManager.sharedInstance.conversionWidth),
                                height: 35.0 * UtilityManager.sharedInstance.conversionHeight)
    
    loginButton.frame = frameForButton
    
    self.view.addSubview(loginButton)
    
  }
  
  private func createCreateAccountButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 12.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.LoginViewController.createAccountButtonText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: (self.view.frame.size.width / 2.0) - (96.5 * UtilityManager.sharedInstance.conversionWidth),
                                     y: loginButton.frame.origin.y + loginButton.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: 193.0 * UtilityManager.sharedInstance.conversionWidth,
                                     height: 14.0 * UtilityManager.sharedInstance.conversionHeight)
    createAccountButton = UIButton.init(frame: frameForButton)
    createAccountButton.addTarget(self,
                                   action: #selector(createAccountButtonPressed),
                                   for: .touchUpInside)
    createAccountButton.backgroundColor = UIColor.clear
    createAccountButton.setAttributedTitle(stringWithFormat, for: .normal)
    
    self.view.addSubview(createAccountButton)
    
  }
  
  private func createDontHaveAccountLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: UIScreen.main.bounds.size.width - (40.0 * UtilityManager.sharedInstance.conversionWidth),
                                    height: CGFloat.greatestFiniteMagnitude)
    
    dontHaveAccountLabel = UILabel.init(frame: frameForLabel)
    dontHaveAccountLabel.numberOfLines = 0
    dontHaveAccountLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 12.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    let finalString = ViewControllersConstants.LoginViewController.dontHaveAccountLabelText as NSString
    
    let stringWithFormat = NSMutableAttributedString(
      string: finalString as String,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let boldFontAttribute = [NSFontAttributeName: UIFont(name: "SFUIText-Semibold",
                                                         size: 12.0 * UtilityManager.sharedInstance.conversionWidth)!]
    stringWithFormat.addAttributes(boldFontAttribute, range: finalString.range(of: "¿No tienes una cuenta?"))
    
    dontHaveAccountLabel.attributedText = stringWithFormat
    dontHaveAccountLabel.sizeToFit()
    let newFrame = CGRect.init(x: (self.view.frame.size.width / 2.0) - (dontHaveAccountLabel.frame.size.width / 2.0),
                               y: loginButton.frame.origin.y + loginButton.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                               width: dontHaveAccountLabel.frame.size.width,
                               height: dontHaveAccountLabel.frame.size.height)
    
    dontHaveAccountLabel.frame = newFrame
    
    self.view.addSubview(dontHaveAccountLabel)
    
  }
  
  @objc private func missedButtonPressed() {
    
    let missedScreen = MissedPasswordViewController()
    
    self.navigationController?.pushViewController(missedScreen, animated: true)
    
  }
  
  @objc private func loginButtonPressed() {
    
    if UtilityManager.sharedInstance.isValidEmail(testStr: emailTextField.mainTextField.text!) == false {
      
      let alertController = UIAlertController(title: "ERROR",
                                              message: "Debes de escribir un mail válido",
                                              preferredStyle: UIAlertControllerStyle.alert)
      
      let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in }
      alertController.addAction(cancelAction)
      
      self.present(alertController, animated: true, completion: nil)
      
    } else
      if passwordTextField.mainTextField.text?.isEmpty == true || UtilityManager.sharedInstance.isValidText(testString: passwordTextField.mainTextField.text!) == false {
        
        let alertController = UIAlertController(title: "ERROR",
                                                message: "Debes de escribir un password",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in }
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
      } else {
        
        UtilityManager.sharedInstance.showLoader()
        
        ServerManager.sharedInstance.requestToLogin(mail: emailTextField.mainTextField.text!,
                                                password: passwordTextField.mainTextField.text!,
                                                actionsToMakeWhenSucceeded: { (json) in
                                                
                                                  self.changeValuesOfCompany(json: json)
      
                                                  UtilityManager.sharedInstance.hideLoader()
      
                                                  self.initAndChangeRootToMainTabBarController()
                                                  
                                                },
                                                actionsToMakeWhenFailed: {
        
                                                  UtilityManager.sharedInstance.hideLoader()
                                                  
                                                })
        
      }
    
  }
  
  @objc private func createAccountButtonPressed() {
    
    let createAccountViewController = CreateAccountViewController()
    self.navigationController?.pushViewController(createAccountViewController, animated: true)
    
  }
  
  private func changeValuesOfCompany(json: [String: AnyObject]) {
    
    let arrayOfCompanies = (json["data"] as? Array<[String: AnyObject]> != nil ? json["data"] as! Array<[String: AnyObject]> : Array<[String: AnyObject]>())
    
    if arrayOfCompanies.count > 0 {
      
      var companies = Array<Company>()
      
      for company in arrayOfCompanies {
        
        let uidAccount: String = (company["cuenta_uid"] as? String != nil ? company["cuenta_uid"] as! String : "")
        let urlLogo: String? = (company["logo_full"] as? String != nil ? company["logo_full"] as? String : nil)
        let name: String = (company["name"] as? String != nil ? company["name"] as! String : "")
        let rfc: String = (company["rfc"] as? String != nil ? company["rfc"] as! String : "")
        let apiKey: String = (company["apikey"] as? String != nil ? company["apikey"] as! String : "")
        let secretKey: String = (company["secretkey"] as? String != nil ? company["secretkey"] as! String : "")
        
        let newCompany = Company.init()
        
        newCompany.uidAccount = uidAccount
        newCompany.urlLogo = urlLogo
        newCompany.name = name
        newCompany.rfc = rfc
        newCompany.apiKey = apiKey
        newCompany.secretKey = secretKey
        
        companies.append(newCompany)
        
      }
      
      User.session.arrayOfCompanies = companies
      
      let companySelectedRawData = arrayOfCompanies[0]
      let uidAccount: String = (companySelectedRawData["cuenta_uid"] as? String != nil ? companySelectedRawData["cuenta_uid"] as! String : "")
      let urlLogo: String? = (companySelectedRawData["logo_full"] as? String != nil ? companySelectedRawData["logo_full"] as? String : nil)
      let name: String = (companySelectedRawData["name"] as? String != nil ? companySelectedRawData["name"] as! String : "")
      let rfc: String = (companySelectedRawData["rfc"] as? String != nil ? companySelectedRawData["rfc"] as! String : "")
      let apiKey: String = (companySelectedRawData["apikey"] as? String != nil ? companySelectedRawData["apikey"] as! String : "")
      let secretKey: String = (companySelectedRawData["secretkey"] as? String != nil ? companySelectedRawData["secretkey"] as! String : "")
    
      User.session.uidAccount = uidAccount
      User.session.urlLogo = urlLogo
      User.session.name = name
      User.session.rfc = rfc
      User.session.apiKey = apiKey
      User.session.secretKey = secretKey
      
      User.session.actualUsingCompany.uidAccount = uidAccount
      User.session.actualUsingCompany.urlLogo = urlLogo
      User.session.actualUsingCompany.name = name
      User.session.actualUsingCompany.rfc = rfc
      User.session.actualUsingCompany.apiKey = apiKey
      User.session.actualUsingCompany.secretKey = secretKey
      
      let userInfo = (json["profile"] as? [String: AnyObject] != nil ? json["profile"] as! [String: AnyObject] : [String: AnyObject]())
      if userInfo.count > 0 {
        
        let newEmail: String = (userInfo["email"] as? String != nil ? userInfo["email"] as! String : "")
        let newImageURL: String = (userInfo["image"] as? String != nil ? userInfo["image"] as! String : "")
        let newName: String = (userInfo["name"] as? String != nil ? userInfo["name"] as! String : "")
        
        if newEmail != "" {
          
          User.session.email = newEmail
          
        }
        if newImageURL != "" {
          
          User.session.urlLogo = newImageURL
          
        }
        if newName != "" {
          
          User.session.name = newName
          
        }
        
      }
      
    }
    
//    let data = (json["data"] as? Array<AnyObject>)?[0] as? [String: AnyObject] != nil ? (json["data"] as? Array<AnyObject>)?[0] as! [String: AnyObject] : [String: AnyObject]()
//    
//    let uidAccount: String = (data["cuenta_uid"] as? String != nil ? data["cuenta_uid"] as! String : "")
//    let urlLogo: String? = (data["logo"] as? String != nil ? data["logo"] as? String : nil)
//    let name: String = (data["name"] as? String != nil ? data["name"] as! String : "")
//    let rfc: String = (data["rfc"] as? String != nil ? data["rfc"] as! String : "")
//    let apiKey: String = (data["apikey"] as? String != nil ? data["apikey"] as! String : "")
//    let secretKey: String = (data["secretkey"] as? String != nil ? data["secretkey"] as! String : "")
//    
//    Company.session.name = name
//    Company.session.uidAccount = uidAccount
//    Company.session.urlLogo = urlLogo
//    Company.session.rfc = rfc
//    Company.session.apiKey = apiKey
//    Company.session.secretKey = secretKey
    
  }
  
  private func initAndChangeRootToMainTabBarController() {
    
    mainTabBarController = MainTabBarController()
    mainTabBarController.tabBar.barTintColor = UtilityManager.sharedInstance.backgroundColorForTabBar
    mainTabBarController.tabBar.isTranslucent = false
    mainTabBarController.tabBar.tintColor = UIColor.white
    
    var arrayOfViewControllers = [UINavigationController]()
    arrayOfViewControllers.append(self.createFirstBarItem())
    arrayOfViewControllers.append(self.createSecondBarItem())
//    arrayOfViewControllers.append(self.createThirdBarItem())
    arrayOfViewControllers.append(self.createFourthBarItem())
    arrayOfViewControllers.append(self.createFifthBarItem())
        
    mainTabBarController.viewControllers = arrayOfViewControllers
    mainTabBarController.selectedIndex = 0
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        UIView.transition(with: appDelegate.window!,
                      duration: 0.25,
                       options: UIViewAnimationOptions.transitionCrossDissolve,
                    animations: {
                        self.view.alpha = 0.0
                        appDelegate.window?.rootViewController = self.mainTabBarController
                        appDelegate.window?.makeKeyAndVisible()
                    }, completion: nil)
    
  }

  
  override func viewDidLoad() {
    
    
    
  }
  
  //MARK: - TabController
  
  //MARK: - CFDI's ViewController
  private func createFirstBarItem() -> UINavigationController {
    
//    let imageFacturasNonSelected = UIImage.init(named: "Tab_1")?.withRenderingMode(.alwaysOriginal)
//    let imageFacturasSelected = UIImage.init(named: "Tab_1_on_Dark_ON")?.withRenderingMode(.alwaysOriginal)
    
    let cfdisTableViewController =  AllCFDIsTableViewController.init(style: .plain, newArrayOfCFDIVersionThree: Array<CFDIVersionThree>())
    
    let tabOneBarItem = UITabBarItem.init(title: ViewControllersConstants.MainTabController.cfdisButtonBarItemText,
                                          image: nil,
                                          selectedImage: nil)
    
    let newNavController = UINavigationController.init(rootViewController: cfdisTableViewController)
    newNavController.navigationBar.barTintColor = UtilityManager.sharedInstance.backGroundColorApp
    
    tabOneBarItem.tag = 1
    tabOneBarItem.imageInsets = UIEdgeInsets.init(top: tabOneBarItem.imageInsets.top + (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                 left: tabOneBarItem.imageInsets.left,
                                               bottom: tabOneBarItem.imageInsets.bottom - (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                right: tabOneBarItem.imageInsets.right)
    newNavController.tabBarItem = tabOneBarItem
    newNavController.navigationBar.barStyle = .black
    
    return newNavController
    
  }
  
  //MARK: - Clientes ViewController
  private func createSecondBarItem() -> UINavigationController {
    
//    let imageRecibosNonSelected = UIImage.init(named: "Tab_2")?.withRenderingMode(.alwaysOriginal)
//    let imageRecibosSelected = UIImage.init(named: "Tab_2_on_Dark_ON")?.withRenderingMode(.alwaysOriginal)
    
    let listOfClientsScreen = ClientsTableViewController.init(style: .plain, newArrayOfElements: Array<Client>())
    
    //This will be changed in future
    
    let tabTwoBarItem = UITabBarItem.init(title: ViewControllersConstants.MainTabController.clientsButtonBarItemText,
                                          image: nil,
                                          selectedImage: nil)
    
    let newNavController = UINavigationController.init(rootViewController: listOfClientsScreen)
    newNavController.navigationBar.barTintColor = UtilityManager.sharedInstance.backGroundColorApp
    
    tabTwoBarItem.tag = 2
    tabTwoBarItem.imageInsets = UIEdgeInsets.init(top: tabTwoBarItem.imageInsets.top + (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                  left: tabTwoBarItem.imageInsets.left,
                                                  bottom: tabTwoBarItem.imageInsets.bottom - (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                  right: tabTwoBarItem.imageInsets.right)
    newNavController.tabBarItem = tabTwoBarItem
    newNavController.navigationBar.barStyle = .black
    
    return newNavController
    
  }
  
  //MARK: -  Productos ViewController
  private func createThirdBarItem() -> UINavigationController  {
    
//    let imageNotasNonSelected = UIImage.init(named: "Tab_3")?.withRenderingMode(.alwaysOriginal)
//    let imageNotasSelected = UIImage.init(named: "Tab_3_on_Dark_ON")?.withRenderingMode(.alwaysOriginal)
    
    let productsViewController = UIViewController()
    
    let tabThirdBarItem = UITabBarItem.init(title: ViewControllersConstants.MainTabController.productsButtonBarItemText,
                                          image: nil,
                                          selectedImage: nil)
    
    let newNavController = UINavigationController.init(rootViewController: productsViewController)
    newNavController.navigationBar.barTintColor = UtilityManager.sharedInstance.backGroundColorApp
    
    tabThirdBarItem.tag = 3
    tabThirdBarItem.imageInsets = UIEdgeInsets.init(top: tabThirdBarItem.imageInsets.top + (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                  left: tabThirdBarItem.imageInsets.left,
                                                  bottom: tabThirdBarItem.imageInsets.bottom - (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                  right: tabThirdBarItem.imageInsets.right)
    newNavController.tabBarItem = tabThirdBarItem
    newNavController.navigationBar.barStyle = .black
    
    return newNavController
    
  }
  
  //MARK: - Empresas ViewController
  private func createFourthBarItem() -> UINavigationController  {
    
//    let imageCartasNonSelected = UIImage.init(named: "Tab_4")?.withRenderingMode(.alwaysOriginal)
//    let imageCartasSelected = UIImage.init(named: "Tab_4_on_Dark_ON")?.withRenderingMode(.alwaysOriginal)
    
    let listOfCompaniesScreen = CompaniesTableViewController.init(style: .plain, newArrayOfCompanies: User.session.arrayOfCompanies)
    
    let tabFourthBarItem = UITabBarItem.init(title: ViewControllersConstants.MainTabController.companiesButtonBarItemText,
                                            image: nil,
                                            selectedImage: nil)
    
    let newNavController = UINavigationController.init(rootViewController: listOfCompaniesScreen)
    newNavController.navigationBar.barTintColor = UtilityManager.sharedInstance.backGroundColorApp
    
    tabFourthBarItem.tag = 4
    tabFourthBarItem.imageInsets = UIEdgeInsets.init(top: tabFourthBarItem.imageInsets.top + (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                    left: tabFourthBarItem.imageInsets.left,
                                                    bottom: tabFourthBarItem.imageInsets.bottom - (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                    right: tabFourthBarItem.imageInsets.right)
    newNavController.tabBarItem = tabFourthBarItem
    newNavController.navigationBar.barStyle = .black
    
    return newNavController
    
  }
  
  //MARK: - MiCuenta ViewController
  private func createFifthBarItem() -> UINavigationController  {
    
//    let imageMenuNonSelected = UIImage.init(named: "Tab_5")?.withRenderingMode(.alwaysOriginal)
//    let imageMenuSelected = UIImage.init(named: "Tab_5_on_Dark_ON")?.withRenderingMode(.alwaysOriginal)
    
    let profileScreen = ProfileViewController()
    
    let tabFifthBarItem = UITabBarItem.init(title: ViewControllersConstants.MainTabController.myAccountButtonBarItemText,
                                             image: nil,
                                             selectedImage: nil)
    
    let newNavController = UINavigationController.init(rootViewController: profileScreen)
    newNavController.navigationBar.barTintColor = UtilityManager.sharedInstance.backGroundColorApp
    
    tabFifthBarItem.tag = 5
    tabFifthBarItem.imageInsets = UIEdgeInsets.init(top: tabFifthBarItem.imageInsets.top + (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                     left: tabFifthBarItem.imageInsets.left,
                                                     bottom: tabFifthBarItem.imageInsets.bottom - (5.0 * UtilityManager.sharedInstance.conversionHeight),
                                                     right: tabFifthBarItem.imageInsets.right)
    newNavController.tabBarItem = tabFifthBarItem
    newNavController.navigationBar.barStyle = .black
    
    return newNavController
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    self.editNavigationBar()
    
  }

  
}
