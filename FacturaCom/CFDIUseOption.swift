//
//  CFDIUseOption.swift
//  FacturaCom
//
//  Created by Israel on 25/09/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class CFDIUseOption {
  
  var id: String! = nil
  var name: String! = nil
  var use: String! = nil
  
  init(newId: String, newName: String, newUse: String) {
    
    id = newId
    name = newName
    use = newUse
    
  }
  
}
