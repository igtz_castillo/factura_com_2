//
//  CFDI.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 04/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import Foundation

class CFDI {
  
  var folio: String! = nil
  var status: String! = nil
  var fechaTimbrado: String! = nil
  var receptor: String! = nil
  var razonSocialReceptor: String! = nil
  
  init(newFolio: String!, newStatus: String!, newFechaTimbrado: String!, newReceptor: String!) {
  
    folio = newFolio
    status = newStatus
    fechaTimbrado = newFechaTimbrado
    receptor = newReceptor
    
  }
  
}
