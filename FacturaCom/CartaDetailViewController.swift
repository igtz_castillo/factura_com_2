//
//  CartaDetailViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 13/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit

class CartaDetailViewController: UIViewController {
  
  //Data
  private var cartaData: Carta! = nil
  
  //Interface
  private var mainScrollView: UIScrollView! = nil
  
  //RFC
  private var rfcLabel: UILabel! = nil
  private var rfcDataLabel: UILabel! = nil
  
  //Folio
  private var folioLabel: UILabel! = nil
  private var folioDataLabel: UILabel! = nil
  
  //OrdenPedido
  private var ordenPedidoLabel: UILabel! = nil
  private var ordenDataLabel: UILabel! = nil
  
  //IVA
  private var ivaLabel: UILabel! = nil
  private var ivaDataLabel: UILabel! = nil
  
  //Descuento
  private var descuentoLabel: UILabel! = nil
  private var descuentoDataLabel: UILabel! = nil
  
  //Subtotal
  private var subtotalLabel: UILabel! = nil
  private var subtotalDataLabel: UILabel! = nil
  
  //Total
  private var totalLabel: UILabel! = nil
  private var totalDataLabel: UILabel! = nil
  
//  //Pagado
  private var pagadoLabel: UILabel! = nil
  private var pagadoDataLabel: UILabel! = nil
  
  //Status
  private var statusLabel: UILabel! = nil
  private var statusDataLabel: UILabel! = nil
  
  //FechaCreacion
  private var fechaCreacionLabel: UILabel! = nil
  private var fechaCreacionDataLabel: UILabel! = nil
  
  //View for buttons
  private var backViewButtons: UIView! = nil
  
  //Buttons
  private var cancelButton: UIButton! = nil
  private var downloadPDFButton: UIButton! = nil
  private var downloadXMLButton: UIButton! = nil
  private var sendToClientButton: UIButton! = nil
  
  private var arrayOfLabels: Array<UILabel>! = Array<UILabel>()
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
    
  }
  
  init(newCartaData: Carta) {
    
    cartaData = newCartaData
    
    super.init(nibName: nil, bundle: nil)
    
  }
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.white
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.initNavigationBar()
    
    //Interface
    self.initMainScrollView()
    
    //RFC
    self.initRfcLabel()
    self.initRfcDataLabel()
    
    //Folio
    self.initFolioLabel()
    self.initFolioDataLabel()
    
    //OrdenPedido
    self.initOrdenPedidoLabel()
    self.initOrdenDataLabel()
    
    //IVA
    self.initIvaLabel()
    self.initIvaDataLabel()
    
    //Descuento
    self.initDescuentoLabel()
    self.initDescuentoDataLabel()
    
    //Subtotal
    self.initSubtotalLabel()
    self.initSubtotalDataLabel()
    
    //Total
    self.initTotalLabel()
    self.initTotalDataLabel()
    
    //Pagado
//    self.initPagadoLabel()
//    self.initPagadoDataLabel()
    
    //Status
    self.initStatusLabel()
    self.initStatusDataLabel()
    
    //FechaCreacion
    self.initFechaCreacionLabel()
    self.initFechaCreacionDataLabel()
    
    self.addAllTheLabels()
    
    //Buttons
    self.createBackViewButtons()
    
  }
  
  private func initNavigationBar() {
    
    let rightButton = UIBarButtonItem(title: "",
                                     style: UIBarButtonItemStyle.plain,
                                     target: self,
                                     action: nil)
    
    self.navigationItem.rightBarButtonItem = rightButton
    
    self.initLeftNavigationView()
//    self.initRightNavigationView()
    
  }
  
  private func initLeftNavigationView() {
    
    self.title = ViewControllersConstants.MainTabController.cartasButtonText
    self.navigationController?.navigationBar.tintColor = UIColor.orange
    
  }
  
  private func initRightNavigationView() {
    
    let imageBackButton = UIImage.init(named: "profile")?.withRenderingMode(.alwaysOriginal)
    
    let profileButton = UIBarButtonItem.init(image: imageBackButton,
                                             style: .plain,
                                             target: self,
                                             action: #selector(profileButtonPressed))
    
    self.navigationItem.setRightBarButton(profileButton, animated: false)
    
  }
  
  //Interface
  private func initMainScrollView() {
    
    mainScrollView = UIScrollView.init(frame: self.view.bounds)
    
    if UtilityManager.sharedInstance.isIpad() == false {
      
      let newContentSize = CGSize.init(width: mainScrollView.frame.size.width,
                                       height: mainScrollView.frame.size.height + (180.0 * UtilityManager.sharedInstance.conversionHeight))
      mainScrollView.contentSize = newContentSize
      
    }
    
    mainScrollView.backgroundColor = UIColor.clear
    self.view.addSubview(mainScrollView)
    
  }
  
  //RFC
  private func initRfcLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    rfcLabel = UILabel.init(frame: frameForLabel)
    rfcLabel.numberOfLines = 0
    rfcLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.GenericCFDIDetail.rfcLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    rfcLabel.attributedText = stringWithFormat
    rfcLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: rfcLabel.frame.size.width,
                               height: rfcLabel.frame.size.height)
    rfcLabel.frame = newFrame
    
    arrayOfLabels.append(rfcLabel)
    
  }
  
  private func initRfcDataLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    rfcDataLabel = UILabel.init(frame: frameForLabel)
    rfcDataLabel.numberOfLines = 0
    rfcDataLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.black
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: cartaData.receptor,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    rfcDataLabel.attributedText = stringWithFormat
    rfcDataLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: rfcDataLabel.frame.size.width,
                               height: rfcDataLabel.frame.size.height)
    rfcDataLabel.frame = newFrame
    
    arrayOfLabels.append(rfcDataLabel)
    
  }
  
  //Folio
  private func initFolioLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    folioLabel = UILabel.init(frame: frameForLabel)
    folioLabel.numberOfLines = 0
    folioLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.GenericCFDIDetail.folioLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    folioLabel.attributedText = stringWithFormat
    folioLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: folioLabel.frame.size.width,
                               height: folioLabel.frame.size.height)
    folioLabel.frame = newFrame
    
    arrayOfLabels.append(folioLabel)
    
  }
  
  private func initFolioDataLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    folioDataLabel = UILabel.init(frame: frameForLabel)
    folioDataLabel.numberOfLines = 0
    folioDataLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.black
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: cartaData.folio,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    folioDataLabel.attributedText = stringWithFormat
    folioDataLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: folioDataLabel.frame.size.width,
                               height: folioDataLabel.frame.size.height)
    folioDataLabel.frame = newFrame
    
    arrayOfLabels.append(folioDataLabel)
    
  }
  
  //OrdenPedido
  private func initOrdenPedidoLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    ordenPedidoLabel = UILabel.init(frame: frameForLabel)
    ordenPedidoLabel.numberOfLines = 0
    ordenPedidoLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.GenericCFDIDetail.ordenPedidoLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    ordenPedidoLabel.attributedText = stringWithFormat
    ordenPedidoLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: ordenPedidoLabel.frame.size.width,
                               height: ordenPedidoLabel.frame.size.height)
    ordenPedidoLabel.frame = newFrame
    
    arrayOfLabels.append(ordenPedidoLabel)
    
  }
  
  private func initOrdenDataLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    ordenDataLabel = UILabel.init(frame: frameForLabel)
    ordenDataLabel.numberOfLines = 0
    ordenDataLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.black
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: cartaData.numOrder,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    ordenDataLabel.attributedText = stringWithFormat
    ordenDataLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: ordenDataLabel.frame.size.width,
                               height: ordenDataLabel.frame.size.height)
    ordenDataLabel.frame = newFrame
    
    arrayOfLabels.append(ordenDataLabel)
    
  }
  
  //IVA
  private func initIvaLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    ivaLabel = UILabel.init(frame: frameForLabel)
    ivaLabel.numberOfLines = 0
    ivaLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.GenericCFDIDetail.ivaLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    ivaLabel.attributedText = stringWithFormat
    ivaLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: ivaLabel.frame.size.width,
                               height: ivaLabel.frame.size.height)
    ivaLabel.frame = newFrame
    
    arrayOfLabels.append(ivaLabel)
    
  }
  
  private func initIvaDataLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    ivaDataLabel = UILabel.init(frame: frameForLabel)
    ivaDataLabel.numberOfLines = 0
    ivaDataLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.black
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: cartaData.iva,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    ivaDataLabel.attributedText = stringWithFormat
    ivaDataLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: ivaDataLabel.frame.size.width,
                               height: ivaDataLabel.frame.size.height)
    ivaDataLabel.frame = newFrame
    
    arrayOfLabels.append(ivaDataLabel)
    
  }
  
  //Descuento
  private func initDescuentoLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    descuentoLabel = UILabel.init(frame: frameForLabel)
    descuentoLabel.numberOfLines = 0
    descuentoLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.GenericCFDIDetail.descuentoLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    descuentoLabel.attributedText = stringWithFormat
    descuentoLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: descuentoLabel.frame.size.width,
                               height: descuentoLabel.frame.size.height)
    descuentoLabel.frame = newFrame
    
    arrayOfLabels.append(descuentoLabel)
    
  }
  
  private func initDescuentoDataLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    descuentoDataLabel = UILabel.init(frame: frameForLabel)
    descuentoDataLabel.numberOfLines = 0
    descuentoDataLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.black
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: cartaData.descuento,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    descuentoDataLabel.attributedText = stringWithFormat
    descuentoDataLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: descuentoDataLabel.frame.size.width,
                               height: descuentoDataLabel.frame.size.height)
    descuentoDataLabel.frame = newFrame
    
    arrayOfLabels.append(descuentoDataLabel)
    
  }
  
  //Subtotal
  private func initSubtotalLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    subtotalLabel = UILabel.init(frame: frameForLabel)
    subtotalLabel.numberOfLines = 0
    subtotalLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.GenericCFDIDetail.subtotalLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    subtotalLabel.attributedText = stringWithFormat
    subtotalLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: subtotalLabel.frame.size.width,
                               height: subtotalLabel.frame.size.height)
    subtotalLabel.frame = newFrame
    
    arrayOfLabels.append(subtotalLabel)
    
  }
  
  private func initSubtotalDataLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    subtotalDataLabel = UILabel.init(frame: frameForLabel)
    subtotalDataLabel.numberOfLines = 0
    subtotalDataLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.black
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: cartaData.subtotal,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    subtotalDataLabel.attributedText = stringWithFormat
    subtotalDataLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: subtotalDataLabel.frame.size.width,
                               height: subtotalDataLabel.frame.size.height)
    subtotalDataLabel.frame = newFrame
    
    arrayOfLabels.append(subtotalDataLabel)
    
  }
  
  //Total
  private func initTotalLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    totalLabel = UILabel.init(frame: frameForLabel)
    totalLabel.numberOfLines = 0
    totalLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.GenericCFDIDetail.totalLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    totalLabel.attributedText = stringWithFormat
    totalLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: totalLabel.frame.size.width,
                               height: totalLabel.frame.size.height)
    totalLabel.frame = newFrame
    
    arrayOfLabels.append(totalLabel)
    
  }
  
  private func initTotalDataLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    totalDataLabel = UILabel.init(frame: frameForLabel)
    totalDataLabel.numberOfLines = 0
    totalDataLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.black
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: cartaData.total,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    totalDataLabel.attributedText = stringWithFormat
    totalDataLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: totalDataLabel.frame.size.width,
                               height: totalDataLabel.frame.size.height)
    totalDataLabel.frame = newFrame
    
    arrayOfLabels.append(totalDataLabel)
    
  }
  
  //Pagado
  private func initPagadoLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    pagadoLabel = UILabel.init(frame: frameForLabel)
    pagadoLabel.numberOfLines = 0
    pagadoLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.GenericCFDIDetail.pagadoLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    pagadoLabel.attributedText = stringWithFormat
    pagadoLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: pagadoLabel.frame.size.width,
                               height: pagadoLabel.frame.size.height)
    pagadoLabel.frame = newFrame
    
    arrayOfLabels.append(pagadoLabel)
    
  }
  
  private func initPagadoDataLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    pagadoDataLabel = UILabel.init(frame: frameForLabel)
    pagadoDataLabel.numberOfLines = 0
    pagadoDataLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.black
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "CHECAR ESTE VALOR CON PACO",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    pagadoDataLabel.attributedText = stringWithFormat
    pagadoDataLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: pagadoDataLabel.frame.size.width,
                               height: pagadoDataLabel.frame.size.height)
    pagadoDataLabel.frame = newFrame
    
    arrayOfLabels.append(pagadoDataLabel)
    
  }
  
  //Status
  private func initStatusLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    statusLabel = UILabel.init(frame: frameForLabel)
    statusLabel.numberOfLines = 0
    statusLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.GenericCFDIDetail.statusLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    statusLabel.attributedText = stringWithFormat
    statusLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: statusLabel.frame.size.width,
                               height: statusLabel.frame.size.height)
    statusLabel.frame = newFrame
    
    arrayOfLabels.append(statusLabel)
    
  }
  
  private func initStatusDataLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    statusDataLabel = UILabel.init(frame: frameForLabel)
    statusDataLabel.numberOfLines = 0
    statusDataLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.black
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: cartaData.status,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    statusDataLabel.attributedText = stringWithFormat
    statusDataLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: statusDataLabel.frame.size.width,
                               height: statusDataLabel.frame.size.height)
    statusDataLabel.frame = newFrame
    
    arrayOfLabels.append(statusDataLabel)
    
  }
  
  //FechaCreacion
  private func initFechaCreacionLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    fechaCreacionLabel = UILabel.init(frame: frameForLabel)
    fechaCreacionLabel.numberOfLines = 0
    fechaCreacionLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.GenericCFDIDetail.fechaCreacionLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    fechaCreacionLabel.attributedText = stringWithFormat
    fechaCreacionLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: fechaCreacionLabel.frame.size.width,
                               height: fechaCreacionLabel.frame.size.height)
    fechaCreacionLabel.frame = newFrame
    
    arrayOfLabels.append(fechaCreacionLabel)
    
  }
  
  private func initFechaCreacionDataLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    fechaCreacionDataLabel = UILabel.init(frame: frameForLabel)
    fechaCreacionDataLabel.numberOfLines = 0
    fechaCreacionDataLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 16.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.black
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: cartaData.fechaTimbrado,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    fechaCreacionDataLabel.attributedText = stringWithFormat
    fechaCreacionDataLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0,
                               y: 0.0,
                               width: fechaCreacionDataLabel.frame.size.width,
                               height: fechaCreacionDataLabel.frame.size.height)
    fechaCreacionDataLabel.frame = newFrame
    
    arrayOfLabels.append(fechaCreacionDataLabel)
    
  }
  
  private func createBackViewButtons() {
    
    let frameForView = CGRect.init(x: 0.0,
                                   y: (arrayOfLabels.last?.frame.origin.y)! + (arrayOfLabels.last?.frame.size.height)! + (15.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: self.view.frame.size.width,
                                   height: 400.0 * UtilityManager.sharedInstance.conversionHeight)
    
    backViewButtons = UIView.init(frame: frameForView)
    backViewButtons.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    self.mainScrollView.addSubview(backViewButtons)
    self.initDownloadPDFButton()
    self.initDownloadXMLButton()
    self.initCancelButton()
    self.initSendToClientButton()
    
  }
  
  private func addAllTheLabels() {
    
    if arrayOfLabels.count == 1 {
      
      let labelFromArray = arrayOfLabels.first!
      
      let frameForLabel = CGRect.init(x: 19.0 * UtilityManager.sharedInstance.conversionWidth,
                                      y: 11.5 * UtilityManager.sharedInstance.conversionHeight,
                                      width: labelFromArray.frame.size.width,
                                      height: labelFromArray.frame.size.height)
      
      labelFromArray.frame = frameForLabel
      
    } else
      if arrayOfLabels.count > 1 {
        
        var labelFromArray: UILabel
        
        var frameForLabel = CGRect.init(x: 19.0 * UtilityManager.sharedInstance.conversionWidth,
                                        y: 0.0,
                                        width: 0.0,
                                        height: 0.0)
        
        var heightVariation: CGFloat = 0.0
        
        for i in 0...arrayOfLabels.count - 1 {
          
          heightVariation = i % 2 == 1 ? 0.0 : 10.0
          
          labelFromArray = arrayOfLabels[i]
          
          
          
          frameForLabel = CGRect.init(x: 19.0 * UtilityManager.sharedInstance.conversionWidth,
                                      y: frameForLabel.origin.y + frameForLabel.size.height + ( (7.0 + heightVariation) * UtilityManager.sharedInstance.conversionHeight),
                                      width: labelFromArray.frame.size.width,
                                      height: labelFromArray.frame.size.height)
          
          labelFromArray.frame = frameForLabel
          
          self.mainScrollView.addSubview(labelFromArray)
          
        }
        
    }
    
  }
  
  //Buttons
  private func initDownloadPDFButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: (19.0 * UtilityManager.sharedInstance.conversionWidth),
                                     y: (11.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    downloadPDFButton = UIButton.init(frame: frameForButton)
    downloadPDFButton.addTarget(self,
                                action: #selector(downloadPDFButtonPressed),
                                for: .touchUpInside)
    downloadPDFButton.backgroundColor = UtilityManager.sharedInstance.labelsAndLinesColor
    downloadPDFButton.setAttributedTitle(stringWithFormat, for: .normal)
    
    downloadPDFButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.GenericCFDIDetail.downloadPDFButtonText))
    
    
    self.backViewButtons.addSubview(downloadPDFButton)
    
  }
  
  private func initDownloadXMLButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: (19.0 * UtilityManager.sharedInstance.conversionWidth),
                                     y: downloadPDFButton.frame.origin.y + downloadPDFButton.frame.size.height + (11.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    downloadXMLButton = UIButton.init(frame: frameForButton)
    downloadXMLButton.addTarget(self,
                                action: #selector(downloadXMLButtonPressed),
                                for: .touchUpInside)
    downloadXMLButton.backgroundColor = UtilityManager.sharedInstance.labelsAndLinesColor
    downloadXMLButton.setAttributedTitle(stringWithFormat, for: .normal)
    
    downloadXMLButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.GenericCFDIDetail.downloadXMLButtonText))
    
    
    self.backViewButtons.addSubview(downloadXMLButton)
    
  }
  
  private func initCancelButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: (19.0 * UtilityManager.sharedInstance.conversionWidth),
                                     y: downloadXMLButton.frame.origin.y + downloadXMLButton.frame.size.height + (11.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    cancelButton = UIButton.init(frame: frameForButton)
    cancelButton.addTarget(self,
                           action: #selector(cancelButtonPressed),
                           for: .touchUpInside)
    cancelButton.backgroundColor = UIColor.orange
    cancelButton.setBackgroundColor(color: UIColor.init(red: 255.0/255.0, green: 164.0/255.0, blue: 78.0/255.0, alpha: 1.0), forState: .highlighted)
    cancelButton.setAttributedTitle(stringWithFormat, for: .normal)
    cancelButton.contentHorizontalAlignment = .left
    
    cancelButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.GenericCFDIDetail.cancelButtonText))
    
    self.backViewButtons.addSubview(cancelButton)
    
  }
  
  private func initSendToClientButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: (19.0 * UtilityManager.sharedInstance.conversionWidth),
                                     y: cancelButton.frame.origin.y + cancelButton.frame.size.height + (11.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    sendToClientButton = UIButton.init(frame: frameForButton)
    sendToClientButton.addTarget(self,
                                 action: #selector(sendToClientButtonPressed),
                                 for: .touchUpInside)
    sendToClientButton.backgroundColor = UtilityManager.sharedInstance.labelsAndLinesColor
    sendToClientButton.setAttributedTitle(stringWithFormat, for: .normal)
    sendToClientButton.contentHorizontalAlignment = .left
    
    sendToClientButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.GenericCFDIDetail.sendToClientButtonText))
    
    self.backViewButtons.addSubview(sendToClientButton)
    
  }
  
  private func createGenericLabelForButton(text: String) -> UILabel {
    
    let frameForLabel = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 200.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    let genericLabel = UILabel.init(frame: frameForLabel)
    genericLabel.numberOfLines = 0
    genericLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: text,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    genericLabel.attributedText = stringWithFormat
    genericLabel.sizeToFit()
    let newFrame = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                               y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                               width: genericLabel.frame.size.width,
                               height: genericLabel.frame.size.height)
    genericLabel.frame = newFrame
    
    return genericLabel
    
  }
  
  @objc private func leftNavigationButtonPressed() {
    
    _ = self.navigationController?.popViewController(animated: true)
    
  }
  
  @objc private func profileButtonPressed() {
    
    
    
  }
  
  @objc private func sendToClientButtonPressed() {
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.sendCartaPorteViaEMail(uidValue: cartaData.uid, actionsToMakeWhenSucceeded: {
      
      UtilityManager.sharedInstance.hideLoader()
      
    }, actionsToMakeWhenFailed: {
      
      UtilityManager.sharedInstance.hideLoader()
      
    })
    
  }
  
  @objc private func downloadPDFButtonPressed() {
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.downloadCartaPortePDF(uidValue: cartaData.uid, actionsToMakeWhenSucceeded: { (pdfDestinationURL) in
      
      UtilityManager.sharedInstance.hideLoader()
      
      if pdfDestinationURL != nil {
        
        let pdfDestinationURLWithoutWarning: URL = pdfDestinationURL!
        
        if UtilityManager.sharedInstance.isIpad() == false {
          
          let objectsToShare: [URL] = [pdfDestinationURLWithoutWarning]
          let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
          
          self.present(activityVC, animated: true, completion: nil)
          
        } else {
          
          let objectsToShare: [URL] = [pdfDestinationURLWithoutWarning]
          let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
          activityVC.modalPresentationStyle = UIModalPresentationStyle.popover
          
          let popOverVC = UIPopoverController.init(contentViewController: activityVC)
          popOverVC.present(from: CGRect.init(x: (self.downloadPDFButton.bounds.origin.x + self.downloadPDFButton.frame.size.width / 2.0) - (200.0 * UtilityManager.sharedInstance.conversionWidth),
                                              y: (650.0 * UtilityManager.sharedInstance.conversionHeight),
                                              width: 400.0,
                                              height: 400.0),
                            in: self.view,
                            permittedArrowDirections: .any,
                            animated: true)
          
        }
        
      }
      
    }, actionsToMakeWhenFailed: {
      
      UtilityManager.sharedInstance.hideLoader()
      
    })
    
  }
  
  @objc private func downloadXMLButtonPressed() {
    
    UtilityManager.sharedInstance.showLoader()
    
    ServerManager.sharedInstance.downloadCartaPorteXML(uidValue: cartaData.uid, actionsToMakeWhenSucceeded: { (pdfDestinationURL) in
      
      UtilityManager.sharedInstance.hideLoader()
      
      if pdfDestinationURL != nil {
        
        let pdfDestinationURLWithoutWarning: URL = pdfDestinationURL!
        
        if UtilityManager.sharedInstance.isIpad() == false {
          
          let objectsToShare: [URL] = [pdfDestinationURLWithoutWarning]
          let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
          
          self.present(activityVC, animated: true, completion: nil)
          
        } else {
          
          let objectsToShare: [URL] = [pdfDestinationURLWithoutWarning]
          let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
          activityVC.modalPresentationStyle = UIModalPresentationStyle.popover
          
          let popOverVC = UIPopoverController.init(contentViewController: activityVC)
          popOverVC.present(from: CGRect.init(x: (self.downloadXMLButton.bounds.origin.x + self.downloadXMLButton.frame.size.width / 2.0) - (200.0 * UtilityManager.sharedInstance.conversionWidth),
                                              y: (703.0 * UtilityManager.sharedInstance.conversionHeight),
                                              width: 400.0,
                                              height: 400.0),
                            in: self.view,
                            permittedArrowDirections: .any,
                            animated: true)
          
        }
        
      }
      
    }, actionsToMakeWhenFailed: {
      
      UtilityManager.sharedInstance.hideLoader()
      
    })
    
  }
  
  @objc private func cancelButtonPressed() {
    
    let alertController = UIAlertController(title: "Aviso",
                                            message: "¿Estás seguro que deseas cancelar?",
                                            preferredStyle: UIAlertControllerStyle.alert)
    
    let acceptAction = UIAlertAction.init(title: "Acepto", style: .default) { result in
      
      UtilityManager.sharedInstance.showLoader()
      
      ServerManager.sharedInstance.requestToCancelFactura(facturaUid: self.cartaData.uid, actionsToMakeWhenSucceeded: {
        
        let alertController = UIAlertController(title: "Éxito",
                                                message: "La factura ha sido cancelada",
                                                preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
          
          UtilityManager.sharedInstance.hideLoader()
          
          _ = self.navigationController?.popViewController(animated: true)
          
        }
        alertController.addAction(cancelAction)
        
        let actualController = UtilityManager.sharedInstance.currentViewController()
        actualController.present(alertController, animated: true, completion: nil)
        
      }, actionsToMakeWhenFailed: {
        
        UtilityManager.sharedInstance.hideLoader()
        
      })
      
    }
    
    let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) { (result: UIAlertAction) -> Void in }
    alertController.addAction(acceptAction)
    alertController.addAction(cancelAction)
    
    let actualController = UtilityManager.sharedInstance.currentViewController()
    actualController.present(alertController, animated: true, completion: nil)
    
  }
  
//  func drawPDFfromURL(url: URL) -> Array<UIImage>? {
//    
//    let cfurl: CFURL = url as CFURL
//    
//    guard let document = CGPDFDocument(cfurl) else { return nil }
//    
//    var arrayOfImages = Array<UIImage>()
//    
//    for i in 1...document.numberOfPages {
//      
//      guard let page = document.page(at: i) else { return nil }
//      
//      var img = UIImage.init()
//      
//      let pageRect = page.getBoxRect(.mediaBox)
//      
//      if #available(iOS 10.0, *) {
//        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
//        
//        img = renderer.image { ctx in
//          UIColor.white.set()
//          ctx.fill(pageRect)
//          
//          ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height);
//          ctx.cgContext.scaleBy(x: 1.0, y: -1.0);
//          
//          ctx.cgContext.drawPDFPage(page);
//        }
//        
//        arrayOfImages.append(img)
//        
//      } else {
//        // Fallback on earlier versions
//      }
//      
//    }
//    
//    return arrayOfImages
//    
//  }
  
  
  
}

