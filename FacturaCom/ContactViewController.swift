//
//  ContactViewController.swift
//  FacturaCom
//
//  Created by Alejandro Aristi C on 21/04/17.
//  Copyright © 2017 Alejandro Aristi C. All rights reserved.
//

import UIKit
import MessageUI

class ContactViewController: UIViewController, UITextViewDelegate, MFMailComposeViewControllerDelegate {
  
  private var firstLabel: UILabel! = nil
  private var secondLabel: UILabel! = nil
  
  private var containerView: UIView! = nil
  private var textEditor: UITextView! = nil
  private var sendButton: UIButton! = nil
  
  private var thirdLabel: UILabel! = nil
  private var fourthLabel: UILabel! = nil
  
  override func loadView() {
    
    self.view = UIView.init(frame: UIScreen.main.bounds)
    self.view.backgroundColor = UIColor.white // UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
    tapGesture.numberOfTapsRequired = 1
    
    self.view.addGestureRecognizer(tapGesture)
    
    self.initInterface()
    
  }
  
  private func initInterface() {
    
    self.initFirstLabel()
    self.initSecondLabel()
    
    self.initTextEditor()
    self.initSendButton()
    
    self.initFourthLabel()
    self.initThirdLabel()

    
  }
  
  private func initFirstLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    firstLabel = UILabel.init(frame: frameForLabel)
    firstLabel.numberOfLines = 0
    firstLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.ContactViewController.firstLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    firstLabel.attributedText = stringWithFormat
    firstLabel.sizeToFit()
    let newFrame = CGRect.init(x: (self.view.frame.size.width / 2.0) - (firstLabel.frame.size.width / 2.0) ,
                               y: ((self.navigationController?.navigationBar.frame.height)! + (self.navigationController?.navigationBar.frame.origin.y)!) + 20.0 * UtilityManager.sharedInstance.conversionHeight,
                           width: firstLabel.frame.size.width,
                          height: firstLabel.frame.size.height)
    firstLabel.frame = newFrame
    
    self.view.addSubview(firstLabel)
    
  }
  
  private func initSecondLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    secondLabel = UILabel.init(frame: frameForLabel)
    secondLabel.numberOfLines = 0
    secondLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.ContactViewController.secondLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    secondLabel.attributedText = stringWithFormat
    secondLabel.sizeToFit()
    let newFrame = CGRect.init(x: (self.view.frame.size.width / 2.0) - (secondLabel.frame.size.width / 2.0) ,
                               y: firstLabel.frame.origin.y + firstLabel.frame.size.height + 70.0 * UtilityManager.sharedInstance.conversionHeight,
                               width: secondLabel.frame.size.width,
                               height: secondLabel.frame.size.height)
    secondLabel.frame = newFrame
    
    self.view.addSubview(secondLabel)
    
  }
  
  private func initTextEditor() {
    
    let frameForTextView = CGRect.init(x: 18.0 * UtilityManager.sharedInstance.conversionWidth,
                                       y: secondLabel.frame.origin.y + secondLabel.frame.size.height + (63.0 * UtilityManager.sharedInstance.conversionHeight),
                                   width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                  height: 80.0 * UtilityManager.sharedInstance.conversionHeight)
    
    textEditor = UITextView.init(frame: frameForTextView)
    textEditor.tag = 1
    textEditor.backgroundColor = UIColor.white  //init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    textEditor.font = UIFont(name:"SFUIText-Regular",
                             size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    textEditor.text = ViewControllersConstants.ContactViewController.textEditorText
    textEditor.textColor = UtilityManager.sharedInstance.labelsAndLinesColor
    textEditor.delegate = self
    
    let border = CALayer()
    border.borderColor = UIColor.darkGray.cgColor
    border.borderWidth = 1.0 * UtilityManager.sharedInstance.conversionHeight
    border.frame = CGRect.init(x: 0.0,
                               y: textEditor.frame.size.height - (1.0 * UtilityManager.sharedInstance.conversionHeight),
                               width: textEditor.frame.size.width,
                               height: 1.0 * UtilityManager.sharedInstance.conversionHeight)
    textEditor.layer.addSublayer(border)
    textEditor.layer.masksToBounds = true
    
    self.view.addSubview(textEditor)
    
  }
  
  private func initSendButton() {
    
    let font = UIFont.init(name: "SFUIText-Light",
                           size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: "",
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    let frameForButton = CGRect.init(x: (18.0 * UtilityManager.sharedInstance.conversionWidth),
                                     y: textEditor.frame.origin.y + textEditor.frame.size.height + (10.0 * UtilityManager.sharedInstance.conversionHeight),
                                     width: self.view.frame.size.width - (36.0 * UtilityManager.sharedInstance.conversionWidth),
                                     height: 44.0 * UtilityManager.sharedInstance.conversionHeight)
    sendButton = UIButton.init(frame: frameForButton)
    sendButton.addTarget(self,
                           action: #selector(sendButtonPressed),
                           for: .touchUpInside)
    sendButton.backgroundColor = UIColor.orange
    sendButton.setBackgroundColor(color: UIColor.init(red: 255.0/255.0, green: 164.0/255.0, blue: 78.0/255.0, alpha: 1.0), forState: .highlighted)
    sendButton.setAttributedTitle(stringWithFormat, for: .normal)
    sendButton.contentHorizontalAlignment = .left
    
    sendButton.addSubview(self.createGenericLabelForButton(text: ViewControllersConstants.ContactViewController.sendButtonText))
    
    self.view.addSubview(sendButton)
    
  }
  
  private func createGenericLabelForButton(text: String) -> UILabel {
    
    let frameForLabel = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                                    y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                                    width: 200.0 * UtilityManager.sharedInstance.conversionWidth,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    let genericLabel = UILabel.init(frame: frameForLabel)
    genericLabel.numberOfLines = 0
    genericLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 14.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UIColor.white
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.left
    
    let stringWithFormat = NSMutableAttributedString(
      string: text,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    genericLabel.attributedText = stringWithFormat
    genericLabel.sizeToFit()
    let newFrame = CGRect.init(x: 28.0 * UtilityManager.sharedInstance.conversionWidth,
                               y: 11.0 * UtilityManager.sharedInstance.conversionHeight,
                               width: genericLabel.frame.size.width,
                               height: genericLabel.frame.size.height)
    genericLabel.frame = newFrame
    
    return genericLabel
    
  }
  
  private func initFourthLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    fourthLabel = UILabel.init(frame: frameForLabel)
    fourthLabel.numberOfLines = 0
    fourthLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 17.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.ContactViewController.fourthLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    fourthLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    fourthLabel.attributedText = stringWithFormat
    fourthLabel.sizeToFit()
    
    let newFrame = CGRect.init(x: 0.0 ,
                               y: self.view.frame.size.height - ((60.0 * UtilityManager.sharedInstance.conversionHeight) + (self.navigationController?.tabBarController?.tabBar.frame.size.height ?? 0) ),
                               width: self.view.frame.size.width,
                               height: 60.0 * UtilityManager.sharedInstance.conversionHeight)
    fourthLabel.frame = newFrame
    
    self.view.addSubview(fourthLabel)
    
  }
  
  private func initThirdLabel() {
    
    let frameForLabel = CGRect.init(x: 0.0,
                                    y: 0.0,
                                    width: self.view.frame.size.width,
                                    height: CGFloat.greatestFiniteMagnitude)
    
    thirdLabel = UILabel.init(frame: frameForLabel)
    thirdLabel.numberOfLines = 0
    thirdLabel.lineBreakMode = .byWordWrapping
    
    let font = UIFont(name: "SFUIText-Light",
                      size: 15.0 * UtilityManager.sharedInstance.conversionWidth)
    let color = UtilityManager.sharedInstance.labelsAndLinesColor
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let stringWithFormat = NSMutableAttributedString(
      string: ViewControllersConstants.ContactViewController.thirdLabelText,
      attributes:[NSFontAttributeName: font!,
                  NSParagraphStyleAttributeName: style,
                  NSForegroundColorAttributeName: color
      ]
    )
    
    thirdLabel.backgroundColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    thirdLabel.attributedText = stringWithFormat
    thirdLabel.sizeToFit()
    let newFrame = CGRect.init(x: 0.0 ,
                               y: self.view.frame.size.height - (thirdLabel.frame.size.height + (80.0 * UtilityManager.sharedInstance.conversionHeight) + (self.navigationController?.tabBarController?.tabBar.frame.size.height ?? 0)),
                               width: self.view.frame.size.width,
                               height: 80.0 * UtilityManager.sharedInstance.conversionHeight)
    thirdLabel.frame = newFrame
    
    self.view.addSubview(thirdLabel)
    
  }
  
  
  func textViewDidBeginEditing(_ textView: UITextView) {
    
    if textView.tag == 1 {
      
      self.removePlaceHolderOfTextView(textView: textView)
      
    }
    
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    
    if textView.tag == 1 {
      
      self.setPlaceHolderOfTextView(textView: textView)
      
    }
    
  }
  
  private func removePlaceHolderOfTextView(textView: UITextView) {
    
    if textView.text == ViewControllersConstants.ContactViewController.textEditorText {
      
      textView.text = ""
      textView.textColor = UIColor.black
      
    }
  }
  
  private func setPlaceHolderOfTextView(textView: UITextView) {
    
    if textView.text == "" {
      
      textView.text = ViewControllersConstants.ContactViewController.textEditorText
      textView.textColor = UtilityManager.sharedInstance.labelsAndLinesColor
      
    }
    
  }
  
  @objc private func sendButtonPressed() {
    
    let mailComposeViewController = configuredMailComposeViewController()
    
    if MFMailComposeViewController.canSendMail() {
      
      self.present(mailComposeViewController, animated: true, completion: nil)
      
    } else {
      
      self.showSendMailErrorAlert()
      
    }
    
  }
  
  func configuredMailComposeViewController() -> MFMailComposeViewController {
    
    let mailComposerVC = MFMailComposeViewController()
    mailComposerVC.mailComposeDelegate = self
    
    mailComposerVC.setToRecipients([ViewControllersConstants.ContactViewController.directionToSendEmail])
    mailComposerVC.setSubject("Contacto desde la aplicación de iOS")
    mailComposerVC.setMessageBody(self.textEditor.text, isHTML: false)
    
    return mailComposerVC
    
  }
  
  func showSendMailErrorAlert() {
    
    let alertController = UIAlertController(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", preferredStyle: UIAlertControllerStyle.alert)
    
    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
      
    }
    
    alertController.addAction(okAction)
    self.present(alertController, animated: true, completion: nil)
    
  }
  
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    
    controller.dismiss(animated: true, completion: nil)
    
  }
  
  @objc private func hideKeyboard() {
    
    self.view.endEditing(true)
    
  }
  
}
